-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for trapedia_fix
CREATE DATABASE IF NOT EXISTS `trapedia_fix` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `trapedia_fix`;

-- Dumping structure for table trapedia_fix.hibernate_sequence
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.hibernate_sequence: 1 rows
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` (`next_val`) VALUES
	(6);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_admin
CREATE TABLE IF NOT EXISTS `trp_admin` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_admin: ~2 rows (approximately)
/*!40000 ALTER TABLE `trp_admin` DISABLE KEYS */;
INSERT INTO `trp_admin` (`username`, `password`, `enabled`) VALUES
	('admin', '$2a$10$O7Bfq7vaaZa6FH0lLJmHIeh01liYnNF4frzvmKC9kixV2uCryTHnq', 1),
	('alex', '$2a$10$16yMtsQHaWgJRI2wRpiQmewdJ7ggidO8KLTtyLqwIDi/0AHhCO6Ji', 1);
/*!40000 ALTER TABLE `trp_admin` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_admin_roles
CREATE TABLE IF NOT EXISTS `trp_admin_roles` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `uni_username_role` (`role`,`username`),
  KEY `fk_username_idx` (`username`),
  CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `trp_admin` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_admin_roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `trp_admin_roles` DISABLE KEYS */;
INSERT INTO `trp_admin_roles` (`user_role_id`, `username`, `role`, `id`) VALUES
	(1, 'admin', 'ROLE_USER', 0),
	(2, 'admin', 'ROLE_ADMIN', 0),
	(3, 'alex', 'ROLE_USER', 0);
/*!40000 ALTER TABLE `trp_admin_roles` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_m_category
CREATE TABLE IF NOT EXISTS `trp_m_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_m_category: 10 rows
/*!40000 ALTER TABLE `trp_m_category` DISABLE KEYS */;
INSERT INTO `trp_m_category` (`id`, `deleted`, `description`, `images`, `title`) VALUES
	(1, '0', 'Menyediakan berbagai hotel', '/img/icon/03_hotel_64.png', 'Hotels'),
	(2, '0', 'Penginapan Vila', '/img/icon/06_shop_64.png', 'Vila'),
	(3, '0', 'Penginapan Apartement', '/img/icon/06_shop_64.png', 'Apartement'),
	(4, '0', NULL, '/img/icon/28_calendar_64.png', 'Event'),
	(5, '0', NULL, '/img/icon/19_suitcase_64.png', 'Paket Wisata'),
	(6, '0', NULL, '/img/icon/05_sight_64.png', 'Homestay'),
	(7, '0', NULL, '/img/icon/35_coconut_64.png', 'Tempat Kuliner'),
	(8, '0', NULL, '/img/icon/39_fruit_64.png', 'Oleh-oleh'),
	(9, '0', NULL, '/img/icon/31_map_64.png', 'Wisata'),
	(10, '0', NULL, '/img/icon/41_airplane_64.png', 'Travel');
/*!40000 ALTER TABLE `trp_m_category` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_m_provinces
CREATE TABLE IF NOT EXISTS `trp_m_provinces` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_m_provinces: 34 rows
/*!40000 ALTER TABLE `trp_m_provinces` DISABLE KEYS */;
INSERT INTO `trp_m_provinces` (`id`, `deleted`, `name`) VALUES
	(11, '0', 'ACEH'),
	(12, '0', 'SUMATERA UTARA'),
	(13, '0', 'SUMATERA BARAT'),
	(14, '0', 'RIAU'),
	(15, '0', 'JAMBI'),
	(16, '0', 'SUMATERA SELATAN'),
	(17, '0', 'BENGKULU'),
	(18, '0', 'LAMPUNG'),
	(19, '0', 'KEPULAUAN BANGKA BELITUNG'),
	(21, '0', 'KEPULAUAN RIAU'),
	(31, '0', 'DKI JAKARTA'),
	(32, '0', 'JAWA BARAT'),
	(33, '0', 'JAWA TENGAH'),
	(34, '0', 'DI YOGYAKARTA'),
	(35, '0', 'JAWA TIMUR'),
	(36, '0', 'BANTEN'),
	(51, '0', 'BALI'),
	(52, '0', 'NUSA TENGGARA BARAT'),
	(53, '0', 'NUSA TENGGARA TIMUR'),
	(61, '0', 'KALIMANTAN BARAT'),
	(62, '0', 'KALIMANTAN TENGAH'),
	(63, '0', 'KALIMANTAN SELATAN'),
	(64, '0', 'KALIMANTAN TIMUR'),
	(65, '0', 'KALIMANTAN UTARA'),
	(71, '0', 'SULAWESI UTARA'),
	(72, '0', 'SULAWESI TENGAH'),
	(73, '0', 'SULAWESI SELATAN'),
	(74, '0', 'SULAWESI TENGGARA'),
	(75, '0', 'GORONTALO'),
	(76, '0', 'SULAWESI BARAT'),
	(81, '0', 'MALUKU'),
	(82, '0', 'MALUKU UTARA'),
	(91, '0', 'PAPUA BARAT'),
	(94, '0', 'PAPUA');
/*!40000 ALTER TABLE `trp_m_provinces` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_m_regencies
CREATE TABLE IF NOT EXISTS `trp_m_regencies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` varchar(255) DEFAULT '0',
  `images` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `popular` varchar(255) DEFAULT NULL,
  `province_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkkxrcjjgivirhf6es6d7abwjk` (`province_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9472 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_m_regencies: 97 rows
/*!40000 ALTER TABLE `trp_m_regencies` DISABLE KEYS */;
INSERT INTO `trp_m_regencies` (`id`, `deleted`, `images`, `name`, `popular`, `province_id`) VALUES
	(1171, '0', '/img/sea_tour_img_3.jpg', 'BANDA ACEH', '1', 11),
	(1172, '0', NULL, 'SABANG', NULL, 11),
	(1173, '0', NULL, 'LANGSA', NULL, 11),
	(1174, '0', NULL, 'LHOKSEUMAWE', NULL, 11),
	(1175, '0', NULL, 'SUBULUSSALAM', NULL, 11),
	(1271, '0', NULL, 'SIBOLGA', NULL, 12),
	(1272, '0', NULL, 'TANJUNGBALAI', NULL, 12),
	(1273, '0', NULL, 'PEMATANGSIANTAR', NULL, 12),
	(1274, '0', NULL, 'TEBINGTINGGI', NULL, 12),
	(1275, '0', NULL, 'MEDAN', NULL, 12),
	(1276, '0', NULL, 'BINJAI', NULL, 12),
	(1277, '0', NULL, 'PADANGSIDIMPUAN', NULL, 12),
	(1278, '0', NULL, 'GUNUNGSITOLI', NULL, 12),
	(1371, '0', NULL, 'PADANG', NULL, 13),
	(1372, '0', NULL, 'SOLOK', NULL, 13),
	(1373, '0', NULL, 'SAWAHLUNTO', NULL, 13),
	(1374, '0', NULL, 'PADANGPANJANG', NULL, 13),
	(1375, '0', NULL, 'BUKITTINGGI', NULL, 13),
	(1376, '0', NULL, 'PAYAKUMBUH', NULL, 13),
	(1377, '0', NULL, 'PARIAMAN', NULL, 13),
	(1471, '0', NULL, 'PEKANBARU', NULL, 14),
	(1473, '0', NULL, 'DUMAI', NULL, 14),
	(1571, '0', NULL, 'JAMBI', NULL, 15),
	(1572, '0', NULL, 'SUNGAIPENUH', NULL, 15),
	(1671, '0', NULL, 'PALEMBANG', NULL, 16),
	(1672, '0', NULL, 'PRABUMULIH', NULL, 16),
	(1673, '0', NULL, 'PAGARALAM', NULL, 16),
	(1674, '0', NULL, 'LUBUKLINGGAU', NULL, 16),
	(1771, '0', NULL, 'BENGKULU', NULL, 17),
	(1871, '0', NULL, 'BANDARLAMPUNG', NULL, 18),
	(1872, '0', NULL, 'METRO', NULL, 18),
	(1971, '0', NULL, 'PANGKALPINANG', NULL, 19),
	(2171, '0', '/img/sea_tour_img_5.jpg', 'BATAM', '1', 21),
	(2172, '0', NULL, 'TANJUNGPINANG', NULL, 21),
	(3171, '0', NULL, 'JAKARTA SELATAN', NULL, 31),
	(3172, '0', NULL, 'JAKARTA TIMUR', NULL, 31),
	(3173, '0', NULL, 'JAKARTA PUSAT', NULL, 31),
	(3174, '0', NULL, 'JAKARTA BARAT', NULL, 31),
	(3175, '0', NULL, 'JAKARTA UTARA', NULL, 31),
	(3271, '0', NULL, 'BOGOR', NULL, 32),
	(3273, '0', '/img/sea_tour_img_1.jpg', 'BANDUNG', '1', 32),
	(3274, '0', NULL, 'CIREBON', NULL, 32),
	(3275, '0', '/img/sea_tour_img_1.jpg', 'BEKASI', '1', 32),
	(3276, '0', NULL, 'DEPOK', NULL, 32),
	(3277, '0', '/img/sea_tour_img_2.jpg', 'CIMAHI', '1', 32),
	(3278, '0', NULL, 'TASIKMALAYA', NULL, 32),
	(3279, '0', NULL, 'BANJAR', NULL, 32),
	(3371, '0', NULL, 'MAGELANG', NULL, 33),
	(3372, '0', NULL, 'SURAKARTA', NULL, 33),
	(3373, '0', NULL, 'SALATIGA', NULL, 33),
	(3374, '0', NULL, 'SEMARANG', NULL, 33),
	(3375, '0', NULL, 'PEKALONGAN', NULL, 33),
	(3376, '0', NULL, 'TEGAL', NULL, 33),
	(3471, '0', '/img/sea_tour_img_4.jpg', 'YOGYAKARTA', '1', 34),
	(3571, '0', NULL, 'KEDIRI', NULL, 35),
	(3572, '0', NULL, 'BLITAR', NULL, 35),
	(3573, '0', '/img/sea_tour_img_4.jpg', 'MALANG', '1', 35),
	(3574, '0', NULL, 'PROBOLINGGO', NULL, 35),
	(3575, '0', NULL, 'PASURUAN', NULL, 35),
	(3576, '0', NULL, 'MOJOKERTO', NULL, 35),
	(3577, '0', NULL, 'MADIUN', NULL, 35),
	(3578, '0', NULL, 'SURABAYA', NULL, 35),
	(3579, '0', NULL, 'BATU', NULL, 35),
	(3671, '0', NULL, 'TANGERANG', NULL, 36),
	(3672, '0', NULL, 'CILEGON', NULL, 36),
	(3673, '0', NULL, 'SERANG', NULL, 36),
	(3674, '0', NULL, 'TANGERANGSELATAN', NULL, 36),
	(5171, '0', NULL, 'DENPASAR', NULL, 51),
	(5271, '0', NULL, 'MATARAM', NULL, 52),
	(5272, '0', NULL, 'BIMA', NULL, 52),
	(5371, '0', NULL, 'KUPANG', NULL, 53),
	(6171, '0', 'img/sea_tour_img_6.jpg', 'PONTIANAK', '1', 61),
	(6172, '0', NULL, 'SINGKAWANG', NULL, 61),
	(6271, '0', NULL, 'PALANGKARAYA', NULL, 62),
	(6371, '0', NULL, 'BANJARMASIN', NULL, 63),
	(6372, '0', NULL, 'BANJARBARU', NULL, 63),
	(6471, '0', NULL, 'BALIKPAPAN', NULL, 64),
	(6472, '0', NULL, 'SAMARINDA', NULL, 64),
	(6474, '0', NULL, 'BONTANG', NULL, 64),
	(6571, '0', NULL, 'TARAKAN', NULL, 65),
	(7171, '0', NULL, 'MANADO', NULL, 71),
	(7172, '0', NULL, 'BITUNG', NULL, 71),
	(7173, '0', NULL, 'TOMOHON', NULL, 71),
	(7174, '0', NULL, 'MOBAGU', NULL, 71),
	(7271, '0', NULL, 'PALU', NULL, 72),
	(7371, '0', NULL, 'MAKASSAR', NULL, 73),
	(7372, '0', NULL, 'PAREPARE', NULL, 73),
	(7373, '0', NULL, 'PALOPO', NULL, 73),
	(7471, '0', NULL, 'KENDARI', NULL, 74),
	(7472, '0', NULL, 'BAUBAU', NULL, 74),
	(7571, '0', NULL, 'GORONTALO', NULL, 75),
	(8171, '0', NULL, 'AMBON', NULL, 81),
	(8172, '0', NULL, 'TUAL', NULL, 81),
	(8271, '0', NULL, 'TERNATE', NULL, 82),
	(8272, '0', NULL, 'TIDOREKEPULAUAN', NULL, 82),
	(9171, '0', NULL, 'SORONG', NULL, 91),
	(9471, '0', NULL, 'JAYAPURA', NULL, 94);
/*!40000 ALTER TABLE `trp_m_regencies` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_pages
CREATE TABLE IF NOT EXISTS `trp_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `deleted` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `publish` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_pages: 0 rows
/*!40000 ALTER TABLE `trp_pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `trp_pages` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_review
CREATE TABLE IF NOT EXISTS `trp_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `service_detail_id` int(11) NOT NULL DEFAULT '0',
  `transaction_detail_id` int(11) NOT NULL DEFAULT '0',
  `rating_review` int(11) NOT NULL DEFAULT '0',
  `review` varchar(250) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_review: ~0 rows (approximately)
/*!40000 ALTER TABLE `trp_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `trp_review` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_service
CREATE TABLE IF NOT EXISTS `trp_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cancellation_policy` varchar(255) DEFAULT '0',
  `count_rating` bigint(20) DEFAULT '0',
  `count_review` bigint(20) DEFAULT NULL,
  `deleted` varchar(255) DEFAULT NULL,
  `popular` varchar(1) DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `images_1` varchar(255) DEFAULT NULL,
  `images_2` varchar(255) DEFAULT NULL,
  `images_3` varchar(255) DEFAULT NULL,
  `images_4` varchar(255) DEFAULT NULL,
  `images_5` varchar(255) DEFAULT NULL,
  `publish` varchar(255) DEFAULT NULL,
  `redemption_instructions` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `terms_conditions` varchar(255) DEFAULT NULL,
  `category_id` bigint(20) NOT NULL,
  `regencies_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKnqt2lcfd6wnw7vuv95dtt2msx` (`category_id`),
  KEY `FK65ku0jb54mlh5xaii4weikndd` (`regencies_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_service: 14 rows
/*!40000 ALTER TABLE `trp_service` DISABLE KEYS */;
INSERT INTO `trp_service` (`id`, `cancellation_policy`, `count_rating`, `count_review`, `deleted`, `popular`, `description`, `images_1`, `images_2`, `images_3`, `images_4`, `images_5`, `publish`, `redemption_instructions`, `service_name`, `terms_conditions`, `category_id`, `regencies_id`) VALUES
	(1, 'Lorem Ipsum Can', 4, 0, '1', '0', 'Lorem Ipsum Des', NULL, NULL, NULL, NULL, NULL, '1', 'Lorem Ipsum', 'Waterboom Bali #2', 'Lorem Ipsum', 2, 1171),
	(2, 'Lorem Ipsum Can', 0, 0, '1', '0', 'Lorem Ipsum Des', NULL, NULL, NULL, NULL, NULL, '1', 'Lorem Ipsum', 'xxxx', 'Lorem Ipsum', 2, 1172),
	(3, '...', 0, 0, '0', '0', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Jakarta Old Batavia Walking Tour ', '...', 1, 1275),
	(4, '...', 0, 0, '0', '1', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Jakarta Landmarks Tour di Indonesia', '...', 2, 1372),
	(5, '...', 2, 0, '0', '0', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Chinatown Walking Day Tour di Jakarta', '...', 3, 1371),
	(6, '...', 0, 0, '0', '0', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Wisata Kuliner Cikini', '...', 3, 1377),
	(7, '...', 0, 0, '0', '0', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Wisata Sehari Pribadi Pulau Gili Lombok', '...', 3, 1372),
	(8, '...', 0, 0, '0', '0', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Wisata Gilis Tenggara dan Pantai Pink', '...', 4, 1373),
	(9, '...', 0, 0, '0', '0', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Wisata Pantai Semeti dan Nambung', '...', 8, 3273),
	(10, '...', 5, 0, '0', '1', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Borobudur Sunrise Tour', '...', 5, 3273),
	(11, '...', 0, 0, '0', '1', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Borobudur Sunrise & Prambanan Temple Trip', '...', 3, 1171),
	(12, '...', 5, 0, '0', '1', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Rental Mobil Pribadi di Bali', '...', 8, 1171),
	(13, '...', 0, 0, '0', '0', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'Hokkaido Jozankei Resort Spa Mori No Uta Onsen Day Tour', '...', 6, 1171),
	(14, '...', 3, 0, '0', '0', '...', NULL, NULL, NULL, NULL, NULL, '1', '...', 'xxxx', '...', 2, 1171);
/*!40000 ALTER TABLE `trp_service` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_service_detail
CREATE TABLE IF NOT EXISTS `trp_service_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `publish` varchar(255) DEFAULT NULL,
  `service_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8hdynh6ipmy5vjt32k2vsvfjw` (`service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_service_detail: 15 rows
/*!40000 ALTER TABLE `trp_service_detail` DISABLE KEYS */;
INSERT INTO `trp_service_detail` (`id`, `deleted`, `description`, `name`, `price`, `publish`, `service_id`) VALUES
	(1, '1', 'Lorem Ipsum', 'SINGLE DAY PASS #1', 20000, '1', 1),
	(2, '1', 'Lorem Ipsum', 'SINGLE DAY PASS #1', 20000, '1', 2),
	(3, '0', '...', 'Tiket Administrasi', 12000, '1', 3),
	(4, '0', '....', 'Tiket Administrasi', 13000, '1', 4),
	(5, '0', '...', 'Tiket Administrasi', 5000, '1', 5),
	(6, '0', '...', 'Tiket Administrasi', 6000, '1', 6),
	(7, '0', '...', 'Tiket Administrasi', 5000, '1', 7),
	(8, '0', '...', 'Tiket Administrasi', 2000, '1', 8),
	(9, '0', '...', 'Tiket Administrasi', 6000, '1', 9),
	(10, '0', '...', 'Tiket Administrasi', 12000, '1', 10),
	(11, '0', '...', 'Tiket Administrasi', 90000, '1', 11),
	(12, '0', '....', 'Rental Harian', 12000, '1', 12),
	(13, '0', '....', 'Rental Mingguan', 13000, '1', 12),
	(14, '0', '...', 'Tiket Administrasi', 12000, '1', 13),
	(15, '0', '...', 'Tiket Administrasi', 12000, '1', 14);
/*!40000 ALTER TABLE `trp_service_detail` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_transaction
CREATE TABLE IF NOT EXISTS `trp_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` varchar(1) DEFAULT '0',
  `discount_voucher_code` bigint(20) DEFAULT NULL,
  `fee_admin` bigint(20) DEFAULT NULL,
  `summary_service` bigint(20) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `traveling_email` varchar(255) DEFAULT NULL,
  `traveling_name` varchar(255) DEFAULT NULL,
  `traveling_phone` varchar(255) DEFAULT NULL,
  `voucher_code` varchar(255) DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_channel` varchar(255) DEFAULT NULL,
  `payment_code` varchar(255) DEFAULT NULL,
  `payment_amount` bigint(20) DEFAULT NULL,
  `status_payment` varchar(1) DEFAULT '0',
  `user_id` bigint(20) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK172wodslrddnopgcdewx5ujb8` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_transaction: 5 rows
/*!40000 ALTER TABLE `trp_transaction` DISABLE KEYS */;
INSERT INTO `trp_transaction` (`id`, `deleted`, `discount_voucher_code`, `fee_admin`, `summary_service`, `total`, `traveling_email`, `traveling_name`, `traveling_phone`, `voucher_code`, `invoice_number`, `payment_method`, `payment_channel`, `payment_code`, `payment_amount`, `status_payment`, `user_id`, `date`, `createdate`) VALUES
	(9, NULL, NULL, 7000, 12000, 19000, 'xxx@xxx.com', 'xxx', '123123', NULL, NULL, 'Bank Mandiri', '001', NULL, 12123, '0', 5, '2019-09-25 23:54:03', NULL),
	(8, NULL, NULL, 7000, 12000, 19000, 'xxx@xxx.xon', 'xxx', '123', NULL, NULL, 'Bank Mandiri', '001', NULL, 12123, '0', 5, '2019-09-25 23:54:03', NULL),
	(10, NULL, NULL, 7000, 39000, 46000, 'nur@gmail.com', 'Nur', '123123', NULL, NULL, 'Bank Mandiri', '001', NULL, 46123, '0', 5, '2019-09-25 23:54:03', NULL),
	(11, NULL, NULL, 7000, 12000, 19000, 'xx@xx.com', 'xxx', '123123', NULL, NULL, 'Bank Mandiri', '001', NULL, 19123, '0', 5, '2019-09-25 23:54:03', NULL),
	(12, NULL, NULL, 7000, 24000, 31000, 'nur@nur.com', 'Nur', '123123', NULL, NULL, 'Bank Mandiri', '001', NULL, 31123, '0', 5, '2019-09-26 00:58:40', NULL);
/*!40000 ALTER TABLE `trp_transaction` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_transaction_detail
CREATE TABLE IF NOT EXISTS `trp_transaction_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_visit` date DEFAULT NULL,
  `deleted` varchar(1) DEFAULT '0',
  `price` bigint(20) DEFAULT NULL,
  `qty` bigint(20) DEFAULT NULL,
  `service_detail_name` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `service_detail_id` bigint(20) NOT NULL,
  `service_id` bigint(20) NOT NULL,
  `transaction_id` bigint(20) NOT NULL,
  `has_review` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK4w6odo7wq3lsn35mx5fbodp1p` (`service_detail_id`),
  KEY `FKjtptwnehxypjb1a4ps2s14a6q` (`service_id`),
  KEY `FK7c1q69vdo158jwsr4kbonll0w` (`transaction_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_transaction_detail: 6 rows
/*!40000 ALTER TABLE `trp_transaction_detail` DISABLE KEYS */;
INSERT INTO `trp_transaction_detail` (`id`, `date_visit`, `deleted`, `price`, `qty`, `service_detail_name`, `service_name`, `total`, `service_detail_id`, `service_id`, `transaction_id`, `has_review`) VALUES
	(10, NULL, NULL, 12000, 1, 'Tiket Administrasi', 'Jakarta Old Batavia Walking Tour ', 12000, 3, 3, 7, '0'),
	(11, NULL, NULL, 12000, 1, 'Tiket Administrasi', 'Jakarta Old Batavia Walking Tour ', 12000, 3, 3, 8, '0'),
	(12, NULL, NULL, 12000, 1, 'Tiket Administrasi', 'Jakarta Old Batavia Walking Tour ', 12000, 3, 3, 9, '0'),
	(13, NULL, NULL, 13000, 3, 'Rental Mingguan', 'Rental Mobil Pribadi di Bali', 39000, 13, 12, 10, '0'),
	(14, NULL, NULL, 12000, 1, 'Tiket Administrasi', 'Jakarta Old Batavia Walking Tour ', 12000, 3, 3, 11, '0'),
	(15, NULL, NULL, 12000, 2, 'Tiket Administrasi', 'Borobudur Sunrise Tour', 24000, 10, 10, 12, '0');
/*!40000 ALTER TABLE `trp_transaction_detail` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_user
CREATE TABLE IF NOT EXISTS `trp_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activation` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `bank_account_name` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_number` varchar(255) DEFAULT NULL,
  `deleted` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2bu374unf71oiq3gy7u700usj` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_user: 1 rows
/*!40000 ALTER TABLE `trp_user` DISABLE KEYS */;
INSERT INTO `trp_user` (`id`, `activation`, `address`, `bank_account_name`, `bank_name`, `bank_number`, `deleted`, `dob`, `email`, `gender`, `name`, `password`, `phone`) VALUES
	(5, '1', 'Jl. Petojo Enclek III No. 11, Gambir, Jakarta Pusat', 'anto', 'Bank BCA', '123', '0', '1991-01-29', 'nuryanto.aza@gmail.com', 'L', 'Herdi azacdddd', '$2a$10$16yMtsQHaWgJRI2wRpiQmewdJ7ggidO8KLTtyLqwIDi/0AHhCO6Ji', '085720123201');
/*!40000 ALTER TABLE `trp_user` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_verification_token
CREATE TABLE IF NOT EXISTS `trp_verification_token` (
  `id` bigint(20) NOT NULL,
  `expire_date` datetime DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qxiu2rpl9okl2r51s8vsnl8cl` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_verification_token: 1 rows
/*!40000 ALTER TABLE `trp_verification_token` DISABLE KEYS */;
INSERT INTO `trp_verification_token` (`id`, `expire_date`, `token`, `user_id`) VALUES
	(5, '2019-09-08 15:21:29', 'ffab9143-b5d3-4d0e-a2a9-d99d95ec99ac', 5);
/*!40000 ALTER TABLE `trp_verification_token` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_wishlist
CREATE TABLE IF NOT EXISTS `trp_wishlist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_detail_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpsa789k8utfva21hc5n1mkk1e` (`service_detail_id`),
  KEY `FKjfm9sbkffeojes40et3jdaaja` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_wishlist: 1 rows
/*!40000 ALTER TABLE `trp_wishlist` DISABLE KEYS */;
INSERT INTO `trp_wishlist` (`id`, `service_detail_id`, `user_id`) VALUES
	(15, 8, 5);
/*!40000 ALTER TABLE `trp_wishlist` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
