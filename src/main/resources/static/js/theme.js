$(document).ready(function() {
	var table = $('#data-table-default, #data-table-default2').dataTable({
		"destroy": true,
        "processing": true,
        "serverSide": false,
		"bFilter": true,
    });
	
	setInterval(checkLogin, 60000 * 11);

    user_form_data_table();
	set_validation();
    download_report();
	re_prosses_notif();
	combo_version_param();
	info_user();
	combo_version_agregator();
	toRp2();
	graph();
	hak_dan_kewajiban_table();
	popup_biller();
	editor_textarea();
	editor_textarea_sms();
	date_config();
	popup_cashback_volume_info();

});

function date_config(){
	$(".datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii'
	});
	
	$(".datetime_start").datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
	}).on('changeDate', function(ev){
        $('.datetime_end').val("");
        $('.datetime_end').datetimepicker('setStartDate', $(this).val())
    });

	
	$(".datetime_end").datetimepicker({
		format: 'yyyy-mm-dd hh:ii'
	});
	
	$(".datetime_min_today").datepicker({
		format: 'yyyy-mm-dd',
		startDate: new Date().toISOString().substring(0, 10)
	});
}

function checkLogin () {
           $.ajax({
               url: "/admin/home",
               type: "GET",
               datatype: 'JSON',
               success: function(data) {
				   if(data == null || data == 'anonymousUser'){
					   window.location.href = "/admin/auth/login";
				   }
               },
               error: function() {
                   window.location.href = "/admin/auth/login";
               }
           });	
}


function editor_textarea() {
	//$('#template').wysihtml5();	
	
	var myCustomTemplates = {
		addCustomerName: function(context) {
			return  "<li>" +
					"<a class='btn btn-sm btn-default' data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='${customerName}'>+ Customer Name</a>" +
					"</li>";
		},
		addVaid: function(context) {
			return  "<li>" +
					"<a class='btn btn-sm btn-default' data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='${vaid}'>+ Vaid</a>" +
					"</li>";
		},
		addAmount: function(context) {
			return  "<li>" +
					"<a class='btn btn-sm btn-default' data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='${amount}'>+ Amount</a>" +
					"</li>";
		},
	};
	
	var editor = $("#template").wysihtml5({
		toolbar: {
			"font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
			"emphasis": true, //Italics, bold, etc. Default true
			"lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
			"html": true, //Button which allows you to edit the generated HTML. Default false
			"link": true, //Button to insert a link. Default true
			"image": true, //Button to insert an image. Default true,
			"color": false, //Button to change color of font  
			"blockquote": false, //Blockquote
			"addCustomerName" : true,
			"addVaid" : true,
			"addAmount" : true,
		},
		customTemplates: myCustomTemplates	
	});
}


function editor_textarea_sms() {
	//$('#template').wysihtml5();	
	
	var myCustomTemplates = {
		addCustomerName: function(context) {
			return  "<li>" +
					"<a class='btn btn-sm btn-default' data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='${customerName}'>+ Customer Name</a>" +
					"</li>";
		},
		addVaid: function(context) {
			return  "<li>" +
					"<a class='btn btn-sm btn-default' data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='${vaid}'>+ Vaid</a>" +
					"</li>";
		},
		addAmount: function(context) {
			return  "<li>" +
					"<a class='btn btn-sm btn-default' data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='${amount}'>+ Amount</a>" +
					"</li>";
		},
	};
	
	var editor = $("#template_sms").wysihtml5({
		toolbar: {
			"font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
			"emphasis": false, //Italics, bold, etc. Default true
			"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
			"html": false, //Button which allows you to edit the generated HTML. Default false
			"link": false, //Button to insert a link. Default true
			"image": false, //Button to insert an image. Default true,
			"color": false, //Button to change color of font  
			"blockquote": false, //Blockquote
			"addCustomerName" : true,
			"addVaid" : true,
			"addAmount" : true,
		},
		customTemplates: myCustomTemplates	
	});
}



function popup_biller() {
	var table = $('#data-table-default, #data-table-default2').dataTable({
		"destroy": true,
        "processing": true,
        "serverSide": false,
		"bFilter": true,
    });
	
	$( "#data-table tbody, #data-table-default tbody" ).on( "click", ".add_biller_promo", function() {
		var id_promo = $(this).attr("data-id-promo");
		var type_promo = $(this).attr("data-id-type");
		var url_promo = "/promo/get_data_biller_promo_fix.htm?promoId="+id_promo;
		
		var url_promo_c = "/promo/get_data_biller_promo.htm";
		 if(type_promo == "promo_volume"){
			 url_promo_c = "/promo_volume/get_data_biller_promo_volume.htm";
		 }
		 
		 $("#promoId").val(id_promo);
		 
		 $('#billerModal').modal('show');
		 
			if($('.promoBiller').length <= 0) {
						  var d_biller = "";
						  $.ajax({
							 url: url_promo_c,
							 type: "GET",
							 data: {promoId: id_promo},
							 success: function(data) {
								 var data = jQuery.parseJSON(data);
								 $.each( data, function( key, value ) {
								   console.log(key + ": " + value.idBiller);
								   d_biller += value.idBiller + ",";
								   $("input[name=idBiller_c]", table.fnGetNodes()).each(function(){
									   if($(this).attr("id") == value.idBiller){
										   console.log("=============");
										   console.log($(this).attr("id"));
										   $(this).prop('checked', true);
									   }
								   });
								 });
								 $("#idBiller").val(d_biller);
							 },
							 error: function() {}
						 });
			}
		
			var tablesPromo = $('.promoBiller').dataTable({
					"destroy": true,
					"processing": true,
					"autoWidth": false,
					"ajax": {
						"url": url_promo,
						"type": "GET",
					},
					"columns": [
						{ "data": "checkbox" },
						{ "data": "name" },
						{ "data": "companyName" },
						{ "data": "prefix" },
						{ "data": "email" }
					],
					"initComplete": function(settings, json) {
						
						  var d_biller = "";
						  $.ajax({
							 url: url_promo_c,
							 type: "GET",
							 data: {promoId: id_promo},
							 success: function(data) {
								 var data = jQuery.parseJSON(data);
								 $.each( data, function( key, value ) {
								   console.log(key + ": " + value.idBiller);
								   d_biller += value.idBiller + ",";
								   $("input[name=idBiller_c]", table.fnGetNodes()).each(function(){
									   if($(this).attr("id") == value.idBiller){
										   console.log("=============");
										   console.log($(this).attr("id"));
										   $(this).prop('checked', true);
									   }
								   });
								 });
								 $("#idBiller").val(d_biller);
							 },
							 error: function() {}
						 });
					}
			});	
			

		 

		

		return false;
	});	
	
	$( "#data-table tbody, #data-table-default tbody" ).on( "click", ".idBiller_c", function() {
		var d_biller = "";
		$("input[name=idBiller_c]:checked", table.fnGetNodes()).each(function(){
			d_biller += $(this).val() + ",";
		});
		$("#idBiller").val(d_biller);
	});	
	
	
	 $('#cek_all_biller_promo').on('click', function(){
		var cek = false;
		if($(this).prop('checked') === true){
           cek = true;
        }
		$("input[name=idBiller_c]", table.fnGetNodes()).each(function(){
			$(this).prop('checked', cek);
		});		 
		
		var d_biller = "";
		$("input[name=idBiller_c]:checked", table.fnGetNodes()).each(function(){
			d_biller += $(this).val() + ",";
		});
		$("#idBiller").val(d_biller);
	 });
	 
	 $('#add_range_form').on('click', function(){
		 var last_val = $(".range_volume:last").attr("data-val");
		 last_val = (Number(last_val)+Number(1));
		 
		var form_range ='<tr>'+
							'<td><input type="text" class="form-control validate[required, min[1], funcCall[range_validation], custom[integer],custom[onlyLetterNumber], maxSize[10]] range_volume" name="startRange" id="range_volume_'+last_val+'" data-val="'+last_val+'" placeholder="Start Tiering"></td>'+
							'<td><input type="text" class="form-control validate[required, min[1], funcCall[range_validation], custom[integer],custom[onlyLetterNumber], maxSize[10]] range_volume" name="endRange" id="range_volume_'+(Number(last_val) + Number(1))+'"  data-val="'+(Number(last_val) + Number(1))+'" placeholder="End Tiering"></td>'+
							'<td><input type="text" class="form-control validate[required, min[1], custom[integer],custom[onlyLetterNumber], maxSize[10]]" name="cashback" id="" placeholder="Cashback"></td>'+
							'<td><a href="#" class="delete_range" title="Delete Promo Range" onclick="return confirm(\'Deleted this data ?\')"><i class="fa fa-trash-o"></i></a></td>'+
						'</tr>';
		$('#range_table_form > tbody:last-child').append(form_range);
		return false;
	 });
	 
	 $( "#range_table_form" ).on( "click", ".delete_range", function() {
		$(this).parents('tr').remove();
	 });
	 
	 $( "#data-table tbody, #data-table-default tbody" ).on( "click", ".bersama_info", function() {
		var id = $(this).attr("data-biller-id");
		var bersamaid = $(this).attr("data-bersamaid");
		$("#idbillerPopup").val(id);
		console.log(bersamaid);
		if(bersamaid == '""'){
			$("#display-json").hide();
		}else{
			bersamaid = jQuery.parseJSON(bersamaid);
			console.log(bersamaid);
			$("#data-json-bersamaid").html(bersamaid);
			var options = {
			  collapsed: true,
			  withQuotes: false
			};
			
			try {
			  var input = eval('(' + bersamaid + ')');
			}
			catch (error) {
			  return alert("Cannot eval JSON: " + error);
			}
			$('#data-json-bersamaid').jsonViewer(input, options);
			
			$("#display-json").show();
		}
		
		$('#myModal').modal('show');
		
		return false;
	});	
}

var retVal = null;

function user_form_data_table() {
    if ($('#user_form_data_table').length > 0) {

        var url = $("#user_form_data_table").attr('action');
		var val_form = $("#user_form_data_table").serializeFormJSON();
        loadDataTable(url, val_form, arrColumns, defColumns);
		console.log(val_form);
		//OTHER INFO COUNT
		if ($('#count_info_dt').length > 0) {
			info_count_dataDB();
		}

        $('#user_form_data_table').submit(function () {
           var val_form = $(this).serializeFormJSON();
		   var url = $("#user_form_data_table").attr('action');
		   loadDataTable(url, val_form, arrColumns, defColumns);
		   
		   //OTHER INFO COUNT
			if ($('#count_info_dt').length > 0) {
				info_count_dataDB();
			}
			
           return false;
        })


    }
}

function set_validation() {
    if ($('.validation_default').length > 0) {
		$(".validation_default").validationEngine();
    }
}

function custum_email_validation(field, rules, i, options){
	var name = field[0].name;
	var id = $("input[name="+name+"]").val();
	if(!id.match(/^[a-zA-Z0-9_.@]*$/gm)){
		return "Invalid email address";
	}
}

function space_validation(field, rules, i, options){
	var id = $("#price").val();
	if (id.split(/\s+/).length > 1) {
		return "Space format not allowed"
	}
}

function alfabet_space_validation(field, rules, i, options){
	var name = field[0].name;
	var id = $("input[name="+name+"]").val();
	if(!id.match(/^[a-zA-Z/\s]*$/gm)){
		return "Format alphabet only";
	}
}

function role_validation(field, rules, i, options){
	var id = $("#id").val();
	if(!id.match(/^[a-zA-Z0-9_]*$/gm)){
		return "Format not allowed";
	}
}

function fix_percent_validation(field, rules, i, options){
	var feeType = $("#feeType").val();
	var id = $("#charge").val();
	if(feeType == '1'){
		if(!id.match(/^[0-9]*$/gm)){
			return "Format integer only"
		}
	}else{
		if(feeType == '2' && $("#charge").val() > 100){
			return "*Maximal 100% percent";
		}
		if(!id.match(/^[0-9].*$/gm)){
			return "*Format decimal only"
		}
	}
}


function fix_percent_validation_two(field, rules, i, options){
	console.log("OK");
	var feeType = $("#feeType2").val();
	var id = $("#charge2").val();

	console.log(feeType+ "  " + id);

	if(feeType == '1'){
		if(!id.match(/^[0-9]*$/gm)){
			return "Format integer only"
		}
	}else{
		if(feeType == '2' && $("#charge2").val() > 100){
			return "*Maximal 100% percent";
		}
		if(!id.match(/^[0-9].*$/gm)){
			return "*Format decimal only"
		}
	}
}

function numeric_only(field, rules, i, options){
	var typePrice = $("#typePrice").val();
	var id = $(".price").val();
	if(typePrice == "FIX"){
		if(!id.match(/^[0-9]*$/gm)){
			return "Format integer only"
		}
	}else{
		if($(".price").val()>100){
			return "*Maximal 100%"
		}
		if(!id.match(/^[0-9].*$/gm)){
			return "*Format decimal only"
		}
	}
}

function range_validation(field, rules, i, options){
	var position = field.attr("data-val");
	var last_position = position - 1;
	console.log($("#range_volume_"+position).val());
	console.log($("#range_volume_"+last_position).val());
	
	
	
	if(position > 1){
		if(parseInt($("#range_volume_"+position).val()) <= parseInt($("#range_volume_"+last_position).val())){
			return "*Range value not valid";
		}
		
		var plus = parseInt($("#range_volume_"+last_position).val()) + 1;
		if(position % 2 ==1 && parseInt($("#range_volume_"+position).val()) != plus){
			return "*Range value not valid";
		}
	}
}

function numeric_only_aggregator(field, rules, i, options){
	var typePrice = $("#typePrice").val();
	var id = $(".price_agregator").val();
	if(typePrice == "FIX"){
		if(!id.match(/^[0-9]*$/gm)){
			return "Format integer only"
		}
	}else{
		if($(".price_agregator").val()>100){
			return "*Maximal 100%"
		}
		if(!id.match(/^[0-9].*$/gm)){
			return "*Format decimal only"
		}
	}
}


function prefix_validation(field, rules, i, options){
	var type_transaction = $("#trxType").val();
	var prefix = parseInt($("#prefix").val());
	
	if(type_transaction == "OTP"){
		if(prefix > 499){
			return "Format number OTP not valid"
		}
	}else if(type_transaction == "OPEN"){
		if(prefix < 500){
			return "Format number OPEN not valid"
		}
	}
//
//	var id_biller = $("#idbiller").val();
//	var prefix = $("#prefix").val();
//
//	if(id_biller == ""){
//		id_biller = 0;
//	}
//	$.ajax({
//		url: "/biller/ajax_cek_prefix.htm",
//		type: "GET",
//		data: {id_biller: 0, prefix: prefix},
//		datatype: 'JSON',
//		success: function(data) {
//			var data = jQuery.parseJSON(data);
//			ajaxCallBack(data);
//		},
//		error: function() {
//
//		}
//	});
//
//	if(retVal.status != 'TRUE'){
//		return "* " + retVal.message;
//	}
}


//
//function mailCheckValidation(field, rules, i, options){
//	var id_biller = $("#idbiller").val();
//	var email = $("#email").val();
//
//	if(id_biller == ""){
//		id_biller = 0;
//	}
//	 $.ajax({
//		url: "/biller/ajax_cek_mail.htm",
//		type: "GET",
//		data: {id_biller: 0, mail: email},
//		datatype: 'JSON',
//		success: function(data) {
//			var data = jQuery.parseJSON(data);
//			ajaxCallBack(data);
//		},
//		error: function() {
//
//		}
//	});
//
//	if(retVal.status != 'TRUE'){
//		return "* " + retVal.message;
//	}
// }

function ajaxCallBack(data){
    retVal = data;
}
 
 


function loadDataTable(ajaxUrl, param, arrColumns, defColumns) {
	var arrColumns = [];
	
    var table = $('#data-table').dataTable({
		"destroy": true,
        "processing": true,
        "serverSide": true,
		"bFilter": false,
		"autoWidth": false,
		// dom: 'Bfrtip',
		// buttons: [
			// 'pageLength',
            // 'copyHtml5',
			// {
                // extend: 'excelHtml5',
                // title: 'Data export Excel'
            // },{
                // extend: 'pdfHtml5',
                // title: 'Data export PDF'
            // },
        // ],
        "ajax": {
            "url": ajaxUrl,
            "type": "GET",
			"data": function (d) {
				if(param != ""){
					d.searchValue = JSON.stringify(param);
				}else{
					d.searchValue = "";
				}
				
            }
        },
        "columns": arrColumns,
        "aoColumnDefs": defColumns
    });
}

function graph_demo() {
    if ($('#graph').length > 0) {
		$('#graph').highcharts({
				chart: {
					type: 'column'
				},
				title: {
					text: 'Monthly Transaction VAID'
				},
				subtitle: {
					text: ''
				},
				xAxis: {
					categories: [
						'Jan',
						'Feb',
						'Mar',
						'Apr',
						'May',
						'Jun',
						'Jul',
						'Aug',
						'Sep',
						'Oct',
						'Nov',
						'Dec'
					],
					crosshair: true
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Count'
					}
				},
				tooltip: {
					headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
						'<td style="padding:0"><b>{point.y} </b></td></tr>',
					footerFormat: '</table>',
					shared: true,
					useHTML: true
				},
				plotOptions: {
					column: {
						pointPadding: 0.2,
						borderWidth: 0
					}
				},
				series: [
					{
						name: 'Transaction PAID',
						data: [49, 71, 106, 129, 144, 176, 135, 148, 216, 19, null, null]
					},{
						name: 'Transaction OPEN',
						data: [89, 85, 10, 12, 300, 179, 180, 150, 220, 14, null, null]
					},
				]
		});

    }
}

function download_report(){

    $(".downloadReport").click(function(){

           var generateReportUrl = $(this).attr('data-generate-url');
           var downloadReportUrl = $(this).attr('data-download-url');
           var dataPost = $('#user_form_data_table').serialize();
           $.ajax({
               url: generateReportUrl,
               type: "GET",
               data: dataPost,
               datatype: 'JSON',
               success: function(data) {
                   window.location.replace(downloadReportUrl);
               },
               error: function() {
                   alert('File not available!');
               }
           });
    });

}

function info_count_dataDB(){
    var url = $('#count_info_dt').attr("action");
    var dataPost = $('#user_form_data_table').serialize();
    //var val_form = $("#user_form_data_table").serializeFormJSON();
		
    $.ajax({
        url: url,
        type: "GET",
        data: dataPost,
        datatype: 'JSON',
        success: function(data) {
			var obj = jQuery.parseJSON(data);	
			if ($('#var_a').length > 0) $('#var_a').html(obj[0][0])
			if ($('#var_b').length > 0) $('#var_b').html(toRp(obj[0][1]))
			if ($('#var_c').length > 0) $('#var_c').html(obj[1][0])
			if ($('#var_d').length > 0) $('#var_d').html(toRp(obj[1][1]))
        },
        error: function() {
            alert('File not available!');
        }
    });		   	
}

function re_prosses_notif(){
	$( "#data-table tbody" ).on( "click", ".re-process-notif", function() {
		var id_invoice = $(this).attr("data-id");
		
		$.ajax({
			url: "/transaction/re_prosses_notif.htm?idInvoice="+id_invoice,
			type: "GET",
			data: "",
			success: function(data) {
				$("#"+id_invoice).html("IN_PROCESS");
				$("#"+id_invoice).removeClass("text-warning");
				$("#"+id_invoice).addClass("text-muted");
			},
			error: function() {
			}
		});	
	
		return false;
	});	
}

function combo_version_param(){
	
	if ($('#idVersion').length > 0) {
	
		$("#idVersion").change(function(){
			var userdataId = $("#userdataId").val();
			var id_biller = $("#idbiller").val();
			var idDetailUser = $("#idDetailUser").val();
			var id_version = $(this).val();
			var text_field = "";
			$("#dinamic_version_text").html(text_field);
			$.ajax({
				url: "/biller/comboParameterTransactionData.htm?id_version="+id_version+"&id_detail_user="+idDetailUser,
				type: "GET",
				data: "",
				success: function(data) {
					var data = jQuery.parseJSON(data);
					$.each(data, function( k, v ) {
						text_field += '<div class="form-group"> '+
											'<label class="col-sm-3 control-label">'+v.description+'</label>'+
											'<div class="col-sm-8">'+
												'<input type="text" class="form-control" name="'+v.id+'" value="'+v.value+'">'+
											'</div>'+
										'</div>';	
						$("#dinamic_version_text").html(text_field);
					});
				},
				error: function() {
				}
			});	
		});
		
		var idDetailUser = $("#idDetailUser").val();
		
		if(idDetailUser != ""){
			var id_version = $("#idVersion").val();
			var text_field = "";
			$("#dinamic_version_text").html(text_field);
			$.ajax({
				url: "/biller/comboParameterTransactionData.htm?id_version="+id_version+"&id_detail_user="+idDetailUser,
				type: "GET",
				data: "",
				success: function(data) {
					var data = jQuery.parseJSON(data);
					$.each(data, function( k, v ) {
						text_field += '<div class="form-group"> '+
											'<label class="col-sm-3 control-label">'+v.description+'</label>'+
											'<div class="col-sm-8">'+
												'<input type="text" class="form-control" name="'+v.id+'" value="'+v.value+'">'+
											'</div>'+
										'</div>';	
						$("#dinamic_version_text").html(text_field);
					});
				},
				error: function() {
				}
			});				
		}
	
	}
	
}

function combo_version_agregator(){
	$("#trxTypeMerchant").change(function(){
		var trx_type = $(this).val();
		version_agregator(trx_type);
	});
	
	
	if ($('#trxTypeMerchant').length > 0) {
		var trx_type = $("#trxTypeMerchant").val();
		version_agregator(trx_type);
	}
	
	function version_agregator(trx_type){
		var id_biller = $("input[name=idbiller]").val();
		var id_version = $("#version").attr("data-val");
		var text_field = "<option value=''>Choice :</option>";
		$("#version").html(text_field);
		$.ajax({
			url: "/biller_agregator/comboParameterVersionTransactionAgregatorData.htm?idBiller="+id_biller+"&trxType="+trx_type,
			type: "GET",
			data: "",
			success: function(data) {
				var data = jQuery.parseJSON(data);
				$.each(data, function( k, v ) {
					var selected = "";
					if(v.id == id_version){
						selected = "selected";
					}
					text_field += "<option value='"+v.id+"' "+selected+">"+v.description+"</option>";
				});	
				$("#version").html(text_field);
				//console.log(data);
			},
			error: function() {
			
			}
		});			
		
	}
}

function info_user(){
    $.ajax({
        url: "/base/access.htm",
        type: "GET",
        data: "",
        datatype: 'JSON',
        success: function(data) {
			var data = jQuery.parseJSON(data);
			if(data === null){
				window.location =  '/home/index.htm';
			}else{
				$(".username").html(data.name);
				var menu = get_style_menu(data.menu);
				$("ul.menu_content").html(menu);
			}
        },
        error: function() {
            
        }
    });

}

function toRp3(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += ',';
        }
    }
    return  rev2.split('').reverse().join('');
}

function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += ',';
        }
    }
    return 'Rp.' + rev2.split('').reverse().join('');
}

function toRp2(){
	if ($('.angka').length > 0) {
		var angka = $(".angka").html();
		var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
		var rev2    = '';
		for(var i = 0; i < rev.length; i++){
			rev2  += rev[i];
			if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
				rev2 += ',';
			}
		}
		$(".angka").html('Rp. ' + rev2.split('').reverse().join(''));
	}
}


var menu_fix = "";
// function get_style_menu(data_menu, tmp_parent = 0){
//
// 	for(var index in data_menu) {
// 		var menu_fix_child = "";
// 		if(data_menu[index].rMenu.parent == null){
// 			menu_fix += ' <li class=""><a href="'+data_menu[index].rMenu.url+'"> '+data_menu[index].rMenu.icon+' <span class="title">'+data_menu[index].rMenu.menuName+'</span>';
// 			for(var index2 in data_menu){
// 				if(data_menu[index2].rMenu.parent != null && data_menu[index2].rMenu.parent.id == data_menu[index].rMenu.id){
// 					menu_fix_child += '<li> <a href="'+data_menu[index2].rMenu.url+'">'+data_menu[index2].rMenu.menuName+'</a> </li>';
// 				}
// 			}
// 			if(menu_fix_child != ""){
// 				menu_fix += '<span class="arrow "></span> </a>';
// 			}else{
// 				menu_fix += '</a>';
// 			}
// 			if(menu_fix_child != ""){
// 				menu_fix += '<ul class="sub-menu">';
// 				menu_fix += menu_fix_child;
// 				menu_fix += '</ul>';
// 			}
// 			menu_fix += "</li>";
// 		}
// 	}
// 	return menu_fix;
//
//
// }

 function passwordValidation(field, rules, i, options){
    if (! /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8,})$/.test(field.val())) {
         // this allows the use of i18 for the error msgs
         return "*This page must include both numeric and alphabetic characters upper and lower case";
    }
 }

 function noSpecialChars(field, rules, i, options){
    if (! /^[0-9a-zA-Z\ \'\-\,]+$/.test(field.val())) {
         // this allows the use of i18 for the error msgs
         return "*Text contains invalid characters";
    }
 }
 
  function noSpecialChars2(field, rules, i, options){
    // if (! /^[a-zA-Z0-9-_]+$/.test(field.val())) {
         // this allows the use of i18 for the error msgs
         // return "*Text contains invalid characters";
    //}
	
	var regexp = /^[a-zA-Z0-9-_]+$/;
	var check = field.val();
	if (check.search(regexp) == -1){
		return "*Text contains invalid characters";
	}

 }
 
 function html_entities(text){
	return text.replace('<','&#x3C;');
 }

function graph() {
    if ($('#graph_dashboard').length > 0) {
		
		$.ajax({
			url: "/home/graph_transaction.htm",
			type: "GET",
			data: "",
			datatype: 'JSON',
			success: function(data) {
				var data = jQuery.parseJSON(data);
				show_graph(data);
				
			},
			error: function() {
				
			}
		});		
		
		function show_graph(data){
			$('#graph_dashboard').highcharts({
					chart: {
						type: 'column'
					},
					title: {
						text: ''
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						categories: [
							'Jan',
							'Feb',
							'Mar',
							'Apr',
							'May',
							'Jun',
							'Jul',
							'Aug',
							'Sep',
							'Oct',
							'Nov',
							'Dec'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Count'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y} </b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: data
			});			
			
			
		}
		

    }
	
	if ($('#change_graph_transaction').length > 0) {
		
			var year = new Date().getFullYear();
			$.ajax({
				url: "/graph/graph_transaction.htm?year="+year,
				type: "GET",
				data: "",
				datatype: 'JSON',
				success: function(data) {
					var data = jQuery.parseJSON(data);
					show_graph(data);
				},
				error: function() {
					
				}
			});			
		
		
		$("#change_graph_transaction").change(function(){
			var year = $(this).val();
			
			$.ajax({
				url: "/graph/graph_transaction.htm?year="+year,
				type: "GET",
				data: "",
				datatype: 'JSON',
				success: function(data) {
					var data = jQuery.parseJSON(data);
					show_graph(data);
				},
				error: function() {
					
				}
			});	
			
		});		
	}
}

function hak_dan_kewajiban_table(){
	if ($('.btn-hak-kewajiban').length > 0) {
		$(".btn-hak-kewajiban").click(function(){
			var dataPost = $('#user_form_data_table').serialize();
			$.ajax({
				url: "/reconcile/count_hak_kewajiban.htm",
				type: "GET",
				data: dataPost,
				success: function(data) {
					var data = JSON.parse(data);
					console.log(data);
					$(".ammountFD").html(toRp(data.ammountFD));
					$(".ppnFD").html(toRp(data.ppnFD));
					$(".totalAmmountFD").html(toRp(data.totalAmmountFD));
					
					$(".ammountTFP").html(toRp(data.ammountTFP));
					$(".ppnTFP").html(toRp(data.ppnTFP));
					$(".totalAmmountTFP").html(toRp(data.totalAmmountTFP));
					
					$(".totalAmmount").html(toRp(data.totalAmmount));
					$(".totalPPN").html(toRp(data.totalPPN));
					$(".totalTotalAmmount").html(toRp(data.totalTotalAmmount));
					
					$(".totalAmmountTFP2").html(toRp(data.totalAmmountTFP));
					$(".totalAmmountFD2").html(toRp(data.totalAmmountFD));
					$(".kewajibanTfpMerchant").html(toRp(data.kewajibanTfpMerchant));
					
					if(data.transactionType == "isopp"){
						$("#info-isopp-data").show();
						$("#info-atmb-data").hide();
					}else{
						$("#info-isopp-data").hide();
						$("#info-atmb-data").show();
					}
					
					
				},
				error: function() {
					
				}
			});	
			
			if($("#dateTransaction").val().substring(8, 10) == "01" && $("#transactionType").val() == "atmb"){
				$("#cashback-promo tbody").html("");
				$.ajax({
					url: "/reconcile/cashback_promo_volume.htm",
					type: "GET",
					data: dataPost,
					success: function(data) {
						var data = JSON.parse(data);
						var html = "";
						$.each(data, function(key, value ) {
							console.log(value.promoName);
							html += "<tr>" +
										"<td>"+value.billerName+"</td>"+
										"<td class='text-right'>"+toRp3(value.countTransaction)+"</td>"+
										"<td class='text-right'>"+toRp(value.totalCashback)+"</td>"+
										"<td>"+value.promoName+"</td>"+
									"</tr>";	
						});				
						$("#cashback-promo tbody").html(html);
						$("#info-promo-volume").show();
					},
					error: function() {
						$("#cashback-promo tbody").html("");
					}
				});	
				
			}else{
				$("#info-promo-volume").hide();
			}
						
		});		
	}
}

function popup_cashback_volume_info() {
	$("#data-table-default tbody" ).on( "click", ".detailVolumePromo", function() {
		$('#cashBackDetailModal').modal('show');
		
		var idPromo = $(this).attr("data-id-promo");
		var idBiller = $(this).attr("data-id-biller");
		
        $.ajax({
               url: "/promo_volume/ajax_cashback_detail.htm?idPromo="+idPromo+"&idBiller="+idBiller,
               type: "GET",
               datatype: 'JSON',
               success: function(data) {
					var data = jQuery.parseJSON(data);
					var html = "";
					var count_t = 0;
					var count_c = 0;
					$.each(data, function(key, value ) {
						html += "<tr>" +
									"<td>"+value.startTiering+"</td>"+
									"<td>"+value.endTiering+"</td>"+
									"<td>"+toRp(value.cashback)+"</td>"+
									"<td>"+toRp3(value.countTransaction)+"</td>"+
									"<td>"+toRp(value.totalCashback)+"</td>"+
								"</tr>";	
						count_t += parseInt(value.countTransaction);
						count_c += parseInt(value.totalCashback);
					});				
					html += "<tr>" +
									"<td colspan='3' class='text-center text-bold'>ALL COUNT</td>"+
									"<td class='text-bold'>"+toRp3(count_t)+"</td>"+
									"<td class='text-bold'>"+toRp(count_c)+"</td>"+
							"</tr>";
					$("#detail_cash_back tbody").html(html);
			  }
        });	
		   
		return false;
	});
}

$.fn.serializeFormJSON = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
};