$(document).ready(function() {
    search_form_data_table();
    editor_textarea();
    graph();

});



function graph() {
    if ($('#graph_dashboard').length > 0) {

		$.ajax({
			url: "/api/public/graph/transaction",
			type: "GET",
			data: "",
			datatype: 'JSON',
			success: function(data) {
				var data = jQuery.parseJSON(data);
				show_graph(data.data);

			},
			error: function() {

			}
		});

		function show_graph(data){
			$('#graph_dashboard').highcharts({
					chart: {
						type: 'column'
					},
					title: {
						text: ''
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						categories: [
							'Jan',
							'Feb',
							'Mar',
							'Apr',
							'May',
							'Jun',
							'Jul',
							'Aug',
							'Sep',
							'Oct',
							'Nov',
							'Dec'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Count'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y} </b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: data
			});


		}


    }

	if ($('#change_graph_transaction').length > 0) {

			var year = new Date().getFullYear();
			$.ajax({
				url: "/api/graph/graph_transaction.htm?year="+year,
				type: "GET",
				data: "",
				datatype: 'JSON',
				success: function(data) {
					var data = jQuery.parseJSON(data);
					show_graph(data);
				},
				error: function() {

				}
			});


		$("#change_graph_transaction").change(function(){
			var year = $(this).val();

			$.ajax({
				url: "/api/graph/graph_transaction.htm?year="+year,
				type: "GET",
				data: "",
				datatype: 'JSON',
				success: function(data) {
					var data = jQuery.parseJSON(data);
					show_graph(data);
				},
				error: function() {

				}
			});

		});
	}
}



function html_entities(text){
	return text.replace('<','&#x3C;');
 }

function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += ',';
        }
    }
    return 'Rp.' + rev2.split('').reverse().join('');
}


function editor_textarea() {
	$('#template').wysihtml5();

//	var myCustomTemplates = {
//		addCustomerName: function(context) {
//			return  "<li>" +
//					"<a class='btn btn-sm btn-default' data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='${customerName}'>+ Customer Name</a>" +
//					"</li>";
//		},
//		addVaid: function(context) {
//			return  "<li>" +
//					"<a class='btn btn-sm btn-default' data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='${vaid}'>+ Vaid</a>" +
//					"</li>";
//		},
//		addAmount: function(context) {
//			return  "<li>" +
//					"<a class='btn btn-sm btn-default' data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='${amount}'>+ Amount</a>" +
//					"</li>";
//		},
//	};
//
//	var editor = $(".editor-wys").wysihtml5({
//		toolbar: {
//			"font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
//			"emphasis": true, //Italics, bold, etc. Default true
//			"lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
//			"html": true, //Button which allows you to edit the generated HTML. Default false
//			"link": true, //Button to insert a link. Default true
//			"image": true, //Button to insert an image. Default true,
//			"color": false, //Button to change color of font
//			"blockquote": false, //Blockquote
//			"addCustomerName" : true,
//			"addVaid" : true,
//			"addAmount" : true,
//		},
//		customTemplates: myCustomTemplates
//	});
}


function search_form_data_table() {
    if ($('#form_search_data_table').length > 0) {

        var url = $("#form_search_data_table").attr('action');
		var val_form = $("#form_search_data_table").serializeFormJSON();
        loadDataTable(url, val_form, arrColumns, defColumns);

        $('#form_search_data_table').submit(function () {
           var val_form = $(this).serializeFormJSON();
		   var url = $("#form_search_data_table").attr('action');
		   loadDataTable(url, val_form, arrColumns, defColumns);

           return false;
        })


    }
}


function loadDataTable(ajaxUrl, param, arrColumns, defColumns) {
    var _defColumns = defColumns == undefined || !defColumns ? [] : defColumns;


    var table = $('#data-table').dataTable({
		"destroy": true,
        "processing": true,
        "serverSide": true,
		"bFilter": false,
        "ajax": {
            "url": ajaxUrl,
            "type": "GET",
			"data": function (d) {
				if(param != ""){
					d.searchValue = JSON.stringify(param);
				}else{
					d.searchValue = "";
				}

            }
        },
        "columns": arrColumns,
        "aoColumnDefs": _defColumns
    });
}



$.fn.serializeFormJSON = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
};