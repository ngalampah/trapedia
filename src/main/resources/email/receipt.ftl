<!doctype html>
<html>
<head>
    <meta charset='utf-8'>
    <title>Receipt</title>

    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #0a3954;
            color: #fff;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            text-align: center;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr td.total {
            border-bottom: 2px solid #eee;
            font-weight: bold;
        }

        .invoice-box table tr.footer td {
            background: #0a3954;
            color: #fff;
            font-weight: bold;
            font-size: 0.7em;
            text-align: center;
        }

        .center {
            text-align: center;
        }

        .right {
            text-align: right;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }

        .email-footer {
            width: 570px;
            margin: 0 auto;
            padding: 0;
            -premailer-width: 570px;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            text-align: center;
            background-color: #0a3954;
        }

        .email-footer p {
            color: #FFF;
        }

        .social_icon {
            height: 20px;
            margin: 0 8px 10px 8px;
            padding: 0;
        }
    </style>
</head>

<body>
<div class='invoice-box'>
    <table cellpadding='0' cellspacing='0'>
        <tr class='top'>
            <td colspan='5'>
                <table>
                    <tr>
                        <td class='title'>
                            <img src='cid:logo' style='width:100%; max-width:300px;'>
                        </td>

                        <td>
                            <h3>RECEIPT</h3>
                            Number: ${invoiceId}<br>
                            Date: ${date}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class='information'>
            <td colspan='5'>
                <table>
                    <tr>
                        <td>
                            <h3>CUSTOMER DETAILS</h3>
                            Name : ${name}<br>
                            Email : ${email}<br>
                            Contact Number : ${phone}
                        </td>

                        <td>
                            <h3>PAYMENT DETAILS</h3>
                            Methode : ${paymentMethode}<br>
                            Status : ${paymentStatus}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class='heading'>
            <td>
                SERVICE
            </td>
            <td>
                DATE VISIT
            </td>
            <td>
                QTY
            </td>
            <td>
                PRICE
            </td>
            <td>
                TOTAL
            </td>
        </tr>

        <#list services as service>
        <tr class='item'>
            <td>
                ${service.serviceName}
            </td>
            <td>
                ${service.dateVisit}
            </td>
            <td class='center'>
                ${service.qty}
            </td>
            <td class='right'>
                Rp. ${service.price}
            </td>
            <td class='right'>
                Rp. ${service.total}
            </td>
        </tr>
        </#list>

        <tr>
            <td colspan="3"></td>
            <td class='total right'>TOTAL</td>
            <td class='total right'>
                Rp. ${total}
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class='total right'>ADMIN FEE</td>
            <td class='total right'>
                RP. ${adminFee}
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class='total right'>DISCOUNT</td>
            <td class='total right'>
                Rp. ${discount}
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class='total right'>UNIQUE CODE</td>
            <td class='total right'>
                Rp. ${uniqueCode}
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class='total right'>PAYMENT AMOUNT</td>
            <td class='total right'>
                Rp. ${paymentAmount}
            </td>
        </tr>
        <tr class='information'>
            <td colspan='5'>
                <table>
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan='5'>
                <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0"
                       role="presentation">
                    <tr>
                        <td class="content-cell" align="center">
                            <p class="f-fallback sub">
                                <b>Connect with us</b><br>Social media channels<br><br>
                                <a href="https://www.instagram.com/trapediacom/"><img src="cid:instagram"
                                                                                      class="social_icon"></a>
                                <a href="#"><img src="cid:linkedin" class="social_icon"></a>
                                <a href="https://www.facebook.com/trapedia"><img src="cid:facebook"
                                                                                 class="social_icon"></a>
                            </p>
                            <p class="f-fallback sub">
                                &copy; 2019 Trapedia. All rights reserved.
                            </p>
                            <p class="f-fallback sub">
                                PT. Galaksi Trapedia Muda
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>