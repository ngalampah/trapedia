<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="x-apple-disable-message-reformatting" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    <style type="text/css" rel="stylesheet" media="all">
        /* Base ------------------------------ */

        @import url("https://fonts.googleapis.com/css?family=Nunito+Sans:400,700&display=swap");
        body {
            width: 100% !important;
            height: 100%;
            margin: 0;
            -webkit-text-size-adjust: none;
        }

        a {
            color: #3869D4;
        }

        a img {
            border: none;
        }

        td {
            word-break: break-word;
        }

        .preheader {
            display: none !important;
            visibility: hidden;
            mso-hide: all;
            font-size: 1px;
            line-height: 1px;
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
        }
        /* Type ------------------------------ */

        body,
        td,
        th {
            font-family: "Nunito Sans", Helvetica, Arial, sans-serif;
        }

        h1 {
            margin-top: 0;
            color: #333333;
            font-size: 22px;
            font-weight: bold;
            text-align: left;
        }

        h2 {
            margin-top: 0;
            color: #333333;
            font-size: 16px;
            font-weight: bold;
            text-align: left;
        }

        h3 {
            margin-top: 0;
            color: #333333;
            font-size: 14px;
            font-weight: bold;
            text-align: left;
        }

        td,
        th {
            font-size: 16px;
        }

        p,
        ul,
        ol,
        blockquote {
            margin: .4em 0 1.1875em;
            font-size: 16px;
            line-height: 1.625;
        }

        p.sub {
            font-size: 13px;
        }
        /* Utilities ------------------------------ */

        .align-right {
            text-align: right;
        }

        .align-left {
            text-align: left;
        }

        .align-center {
            text-align: center;
        }
        /* Buttons ------------------------------ */

        .button {
            background-color: #3869D4;
            border-top: 10px solid #3869D4;
            border-right: 18px solid #3869D4;
            border-bottom: 10px solid #3869D4;
            border-left: 18px solid #3869D4;
            display: inline-block;
            color: #FFF;
            text-decoration: none;
            border-radius: 3px;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
            -webkit-text-size-adjust: none;
            box-sizing: border-box;
        }

        .button--green {
            background-color: #22BC66;
            border-top: 10px solid #22BC66;
            border-right: 18px solid #22BC66;
            border-bottom: 10px solid #22BC66;
            border-left: 18px solid #22BC66;
        }

        .button--red {
            background-color: #FF6136;
            border-top: 10px solid #FF6136;
            border-right: 18px solid #FF6136;
            border-bottom: 10px solid #FF6136;
            border-left: 18px solid #FF6136;
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
                text-align: center !important;
            }
        }
        /* Attribute list ------------------------------ */

        .attributes {
            margin: 0 0 21px;
        }

        .attributes_content {
            background-color: #F4F4F7;
            padding: 16px;
        }

        .attributes_item {
            padding: 0;
        }
        /* Related Items ------------------------------ */

        .related {
            width: 100%;
            margin: 0;
            padding: 25px 0 0 0;
            -premailer-width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
        }

        .related_item {
            padding: 10px 0;
            color: #CBCCCF;
            font-size: 15px;
            line-height: 18px;
        }

        .related_item-title {
            display: block;
            margin: .5em 0 0;
        }

        .related_item-thumb {
            display: block;
            padding-bottom: 10px;
        }

        .related_heading {
            border-top: 1px solid #CBCCCF;
            text-align: center;
            padding: 25px 0 10px;
        }
        /* Discount Code ------------------------------ */

        .discount {
            width: 100%;
            margin: 0;
            padding: 24px;
            -premailer-width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            background-color: #F4F4F7;
            border: 2px dashed #CBCCCF;
        }

        .discount_heading {
            text-align: center;
        }

        .discount_body {
            text-align: center;
            font-size: 15px;
        }
        /* Social Icons ------------------------------ */

        .social {
            width: auto;
        }

        .social td {
            padding: 0;
            width: auto;
        }

        .social_icon {
            height: 20px;
            margin: 0 8px 10px 8px;
            padding: 0;
        }
        /* Data table ------------------------------ */

        .purchase {
            width: 100%;
            margin: 0;
            padding: 35px 0;
            -premailer-width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
        }

        .purchase_content {
            width: 100%;
            margin: 0;
            padding: 25px 0 0 0;
            -premailer-width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
        }

        .purchase_item {
            padding: 10px 0;
            color: #51545E;
            font-size: 15px;
            line-height: 18px;
            border-bottom: 1px solid #EAEAEC;
        }

        .purchase_heading {
            padding-bottom: 8px;
            border-bottom: 1px solid #EAEAEC;
        }

        .purchase_heading p {
            margin: 0;
            color: #85878E;
            font-size: 12px;
        }

        .purchase_footer {
            padding-top: 15px;
        }

        .purchase_total {
            margin: 0;
            text-align: right;
            font-weight: bold;
            color: #333333;
        }

        .purchase_total--label {
            padding: 0 15px 0 0;
        }

        body {
            background-color: #0a3954;
            color: #51545E;
        }

        p {
            color: #51545E;
        }

        p.sub {
            color: #6B6E76;
        }

        .email-wrapper {
            width: 100%;
            margin: 0;
            padding: 0;
            -premailer-width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            background-color: #0a3954;
        }

        .email-content {
            width: 100%;
            margin: 0;
            padding: 0;
            -premailer-width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
        }
        /* Masthead ----------------------- */

        .email-masthead {
            padding: 25px 0;
            text-align: center;
        }

        .email-masthead_logo {
            width: 94px;
        }

        .email-masthead_name {
            font-size: 16px;
            font-weight: bold;
            color: #FFF;
            text-decoration: none;
            text-shadow: 0 1px 0 white;
        }
        /* Body ------------------------------ */

        .email-body {
            width: 100%;
            margin: 0;
            padding: 0;
            -premailer-width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            background-color: #FFFFFF;
        }

        .email-body_inner {
            width: 570px;
            margin: 0 auto;
            padding: 0;
            -premailer-width: 570px;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            background-color: #FFFFFF;
        }

        .email-footer {
            width: 570px;
            margin: 0 auto;
            padding: 0;
            -premailer-width: 570px;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            text-align: center;
        }

        .email-footer p {
            color: #FFF;
        }

        .body-action {
            width: 100%;
            margin: 30px auto;
            padding: 0;
            -premailer-width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            text-align: center;
        }

        .body-sub {
            margin-top: 25px;
            padding-top: 25px;
            border-top: 1px solid #EAEAEC;
        }

        .content-cell {
            padding: 35px;
        }
        /*Media Queries ------------------------------ */

        @media only screen and (max-width: 600px) {
            .email-body_inner,
            .email-footer {
                width: 100% !important;
            }
        }

        @media (prefers-color-scheme: dark) {
            body,
            .email-body,
            .email-body_inner,
            .email-content,
            .email-wrapper,
            .email-masthead,
            .email-footer {
                background-color: #0a3954 !important;
                color: #FFF !important;
            }
            p,
            ul,
            ol,
            blockquote,
            h1,
            h2,
            h3 {
                color: #FFF !important;
            }
            .attributes_content,
            .discount {
                background-color: #222 !important;
            }
            .email-masthead_name {
                text-shadow: none !important;
            }
        }
    </style>
    <!--[if mso]>
    <style type="text/css">
        .f-fallback {
            font-family: Arial, sans-serif;
        }
    </style>
    <![endif]-->
</head>

<body>
<table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
    <tr>
        <td align="center">
            <table class="email-content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                <tr>
                    <td class="email-masthead">
                        <a href="https://trapedia.com" class="f-fallback email-masthead_name">
                            <img src="cid:logo" class="social" height="50px" width="auto">
                        </a>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell">
                                    <div class="f-fallback">
                                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px" bgcolor="transparent">
                                            <tbody>
                                            <tr style="margin:0;padding:0">
                                                <td style="margin:0;padding:0">
                                                    <h5 style="line-height:32px;color:#666;font-weight:700;font-size:24px;margin:0 0 20px;padding:0">Please complete <br/>your payment immediately<br/><br/>
                                                        <p style="font-weight:normal;color:#999;font-size:16px;line-height:1.6;margin:0 0 20px;padding:0">
                                                            Checkout successful on ${date}
                                                        </p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table style="width:100%;margin-bottom:24px;padding:0 20px">
                                            <thead>
                                            <tr>
                                                <td style="width:50%">
                                                    <span style="font-size:12px;color:#666;font-weight:600">Total Payment</span>
                                                </td>
                                                <td style="width:50%">
                                                    <span style="font-size:12px;color:#666;font-weight:600">Due Payment</span>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td style="vertical-align:top">

                                                    <p style="font-size:16px;color:#999;margin:0 0 5px 0">Rp ${paymentAmount}</p>

                                                </td>
                                                <td style="vertical-align:top">
                                                    <p style="font-size:16px;color:#999;margin:0 0 5px 0">${dueDate}</p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table style="width:100%;margin-bottom:24px;padding:0 20px">
                                            <thead>
                                            <tr>
                                                <td style="width:50%">
                                                    <span style="font-size:12px;color:#666;font-weight:600">Payment Methode</span>
                                                </td>
                                                <td style="width:50%">
                                                    <span style="font-size:12px;color:#666;font-weight:600">Payment Reference</span>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td style="vertical-align:top">

                                                    <img src="cid:bank" style="margin-top:5px" class="CToWUd" width="100">
                                                    <br>
                                                    <p style="font-size:16px;color:#999;margin:5px 0">${paymentMethode}</p>

                                                    <p style="font-size:12px;color:#666;font-weight:600;margin-top:35px">Payment Code</p>
                                                    <p style="font-size:16px;color:#999">${paymentCode}</p>

                                                </td>
                                                <td style="vertical-align:top">
                                                    <p style="font-size:16px;color:#999">${paymentReference}</p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table style="width:100%;margin-bottom:24px;padding:0 20px">
                                            <thead>
                                            <tr>
                                                <td style="width:50%">
                                                    <span style="font-size:12px;color:#666;font-weight:600">Payment Status</span>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td style="vertical-align:top">
                                                    <span style="font-size:16px;color:#999">${paymentStatus}</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <h2 style="font-size:14px;font-weight:600;color:#666;margin:64px 0 16px 0;padding:0 20px">Detail Pemesanan</h2>
                                        <span style="width:30px;border:2px solid #ff5722;display:inline-block;margin-left:20px"></span>
                                        <table style="width:80%;padding-bottom:18px;margin-top:16px;margin-bottom:18px;border-bottom:1px solid #e0e0e0;margin-left:20px">
                                            <tbody>

                                            <tr>
                                                <td style="padding-top:16px">
                                                    <a style="color:#42b549;text-decoration:none"></a>
                                                </td>
                                            </tr>
                                            <#list services as service>
                                                <tr>
                                                    <td style="vertical-align:top">
                                                        <p style="font-size:14px;color:#999;margin-bottom:5px">${service.serviceName}</p>
                                                        <p style="font-size:14px;color:#999;margin-bottom:8px">${service.qty} x Rp ${service.price}</p>
                                                    </td>
                                                    <td style="vertical-align:top;text-align:right;width:120px">
                                                        <span style="float:left;font-size:14px;color:#999;">Rp</span>
                                                        <p style="float:right;font-size:14px;color:#999;">${service.total}</p>
                                                    </td>
                                                </tr>
                                            </#list>
                                            <tr>
                                                <td style="vertical-align:top">
                                                    <p style="font-size:14px;color:#999;margin-bottom:5px">Admin Fee</p>
                                                </td>
                                                <td style="vertical-align:top;text-align:right;width:120px">
                                                    <span style="float:left;font-size:14px;color:#999;">Rp</span>
                                                    <p style="float:right;font-size:14px;color:#999;">${adminFee}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:top">
                                                    <p style="font-size:14px;color:#999;margin-bottom:5px">Discount</p>
                                                </td>
                                                <td style="vertical-align:top;text-align:right;width:120px">
                                                    <span style="float:left;font-size:14px;color:#999;">Rp</span>
                                                    <p style="float:right;font-size:14px;color:#999;">${discount}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:top">
                                                    <p style="font-size:14px;color:#999;margin-bottom:5px">Unique Code</p>
                                                </td>
                                                <td style="vertical-align:top;text-align:right;width:120px">
                                                    <span style="float:left;font-size:14px;color:#999;">Rp</span>
                                                    <p style="float:right;font-size:14px;color:#999;">${uniqueCode}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top:16px">
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                        <table style="width:80%;margin-top:18px;margin-bottom:40px;padding-bottom:18px;margin-left:20px">
                                            <tbody>
                                            <tr style="margin-bottom:16px">
                                                <td style="vertical-align:top">
                                                    <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;font-weight:bold">Total Payment</p>
                                                </td>
                                                <td style="vertical-align:top;text-align:right;width:120px">
                                                    <span style="float:left;font-size:14px;color:#999;margin-top:5px;font-weight:bold">Rp</span>
                                                    <p style="float:right;font-size:14px;color:#999;margin-top:5px;font-weight:bold">${paymentAmount}</p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style="font-weight:normal;color:#999;font-size:16px;line-height:1.6;margin:0 0 20px;padding:0">If you have any questions about this invoice, simply reply to this email or reach out to our <a href="mailto: contact@trapedia.com">support team</a> for help.</p>
                                        <p style="font-weight:normal;color:#999;font-size:16px;line-height:1.6;margin:0 0 20px;padding:0">Cheers,
                                            <br>The Trapedia Team</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                            <tr>
                                <td class="content-cell" align="center">
                                    <p class="f-fallback sub">
                                        <b>Connect with us</b>
                                        <br>Social media channels
                                        <br>
                                        <br>
                                        <a href="https://www.instagram.com/trapediacom/"><img src="cid:instagram" class="social_icon"></a>
                                        <a href="#"><img src="cid:linkedin" class="social_icon"></a>
                                        <a href="https://www.facebook.com/trapedia"><img src="cid:facebook" class="social_icon"></a>
                                    </p>
                                    <p class="f-fallback sub">
                                        &copy; 2019 Trapedia. All rights reserved.
                                    </p>
                                    <p class="f-fallback sub">
                                        PT. Galaksi Trapedia Muda
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>