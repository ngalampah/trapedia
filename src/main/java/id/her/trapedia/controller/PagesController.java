package id.her.trapedia.controller;

import id.her.trapedia.dto.PagesDto;
import id.her.trapedia.service.PagesService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class PagesController {

    @Autowired
    private PagesService pagesService;

    @RequestMapping(value = "/list")
    public String list() {
        Map<String, Object> response = pagesService.findAll();
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public String insert(@RequestBody PagesDto dto) {
        Map<String, Object> response = pagesService.insert(dto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@RequestBody PagesDto dto) {
        Map<String, Object> response = pagesService.update(dto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/page/page-by-type", method = RequestMethod.POST)
    public String getPageByType(@RequestBody PagesDto dto) {
        Map<String, Object> response = pagesService.getServiceByType(dto.getType());
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/page/page-by-id", method = RequestMethod.POST)
    public String getPageById(@RequestBody PagesDto dto) {
        Map<String, Object> response = pagesService.getServiceById(dto.getId());
        return JSONProcessor.convertToJSON(response).toString();
    }
}
