package id.her.trapedia.controller;

import id.her.trapedia.dto.PagingDto;
import id.her.trapedia.dto.ReqServiceDto;
import id.her.trapedia.dto.ReqTransactionDto;
import id.her.trapedia.service.ServiceService;
import id.her.trapedia.service.TransactionService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(value = "/transaction/save", method = RequestMethod.POST)
    public String list(@RequestBody ReqTransactionDto reqTransactionDto) {
        Map<String, Object> response = transactionService.insert(reqTransactionDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/transaction/cancel", method = RequestMethod.POST)
    public String cancel(HttpServletRequest request, @RequestBody ReqTransactionDto reqTransactionDto) {
        Map<String, Object> response = transactionService.cancel(reqTransactionDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/transaction/get")
    public String getTransactionById(HttpServletRequest request, @RequestBody ReqTransactionDto reqTransactionDto) {
        Map<String, Object> response = transactionService.getTranById(reqTransactionDto.getId(), request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/transaction/list", method = RequestMethod.POST)
    public String list(HttpServletRequest request) {
        Map<String, Object> response = transactionService.findByUser(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/sales/list", method = RequestMethod.POST)
    public String salesList(HttpServletRequest request) {
        Map<String, Object> response = transactionService.salesByUser(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }





}
