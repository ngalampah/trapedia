package id.her.trapedia.controller;

import id.her.trapedia.dto.ReqReviewDto;
import id.her.trapedia.dto.ReqServiceDto;
import id.her.trapedia.dto.ReqWishlistDto;
import id.her.trapedia.service.ReviewService;
import id.her.trapedia.service.WishlistService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @RequestMapping(value = "/review/insert")
    public String insert(HttpServletRequest request, @RequestBody ReqReviewDto reqReviewDto) {
        Map<String, Object> response = reviewService.insert(reqReviewDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/review/list")
    public String list(HttpServletRequest request) {
        Map<String, Object> response = reviewService.findAllByUser(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }



    @RequestMapping(value = "/review/detail-transaction")
    public String list(HttpServletRequest request, @RequestBody ReqReviewDto reqReviewDto) {
        Map<String, Object> response = reviewService.detailTransactionService(reqReviewDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/review/list/by-service", method = RequestMethod.POST)
    public String listByService(@RequestBody ReqReviewDto reqReviewDto) {
        Map<String, Object> response = reviewService.findAllByService(reqReviewDto.getServiceId());
        return JSONProcessor.convertToJSON(response).toString();
    }


}
