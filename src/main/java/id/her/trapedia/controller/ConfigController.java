package id.her.trapedia.controller;

import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.ConfigDto;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.service.ConfigService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class ConfigController {

    @Autowired
    private ConfigService configService;



    @RequestMapping(value = "/public/config/get", method = RequestMethod.POST)
    public String get(@RequestBody ConfigDto configDto) {
        Map<String, Object> response = configService.getConfigById(configDto.getConfigName());
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/config/all", method = RequestMethod.POST)
    public String all() {
        Map<String, Object> response = configService.getConfigAll();
        return JSONProcessor.convertToJSON(response).toString();
    }
}
