package id.her.trapedia.controller;

import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.service.HomeService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class GraphController {

    @Autowired
    private HomeService homeService;

    @RequestMapping(value = "/public/graph/transaction")
    public String getGraph() {
        Map<String, Object> response = homeService.graphMonthYear();
        return JSONProcessor.convertToJSON(response).toString();
    }
}
