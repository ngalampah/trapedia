package id.her.trapedia.controller;

import id.her.trapedia.dto.CommentServiceDto;
import id.her.trapedia.dto.NotifDto;
import id.her.trapedia.service.CommentServiceService;
import id.her.trapedia.service.NotifService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class NotifController {

    @Autowired
    private NotifService notifService;


    @RequestMapping(value = "/notif/by-user", method = RequestMethod.POST)
    public String getCommentByService(HttpServletRequest request, @RequestBody NotifDto notifDto) {
        Map<String, Object> response = notifService.getListNotifByUser(notifDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/notif/by-user-count", method = RequestMethod.POST)
    public String getCountByStatus(HttpServletRequest request, @RequestBody NotifDto notifDto) {
        Map<String, Object> response = notifService.getCountStatus(notifDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/notif/detail", method = RequestMethod.POST)
    public String getDetail(HttpServletRequest request, @RequestBody NotifDto notifDto) {
        Map<String, Object> response = notifService.getDetail(notifDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/notif/set-status", method = RequestMethod.POST)
    public String setStatus(HttpServletRequest request, @RequestBody NotifDto notifDto) {
        Map<String, Object> response = notifService.setStatus(notifDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }


}
