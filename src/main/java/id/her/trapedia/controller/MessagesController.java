package id.her.trapedia.controller;

import id.her.trapedia.dto.MessagesDto;
import id.her.trapedia.dto.SubscribeDto;
import id.her.trapedia.service.MessagesService;
import id.her.trapedia.service.SubscribeService;
import id.her.trapedia.service.VerificationTokenService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class MessagesController {

    @Autowired
    private MessagesService messagesService;

    @RequestMapping(value = "/messages/add", method = RequestMethod.POST)
    public String add(HttpServletRequest request, @RequestBody MessagesDto messagesDto) {
        Map<String, Object> response = messagesService.add(messagesDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/messages/by-user", method = RequestMethod.POST)
    public String getMessageByUser(HttpServletRequest request) {
        Map<String, Object> response = messagesService.getListMessageByUser(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/messages/detail-by-user", method = RequestMethod.POST)
    public String getMessageDetailByUser(HttpServletRequest request, @RequestBody MessagesDto dto) {
        Map<String, Object> response = messagesService.getMessageDetailByUser(request.getHeader("Authorization"), dto.getId());
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/messages/add-detail", method = RequestMethod.POST)
    public String addDetail(HttpServletRequest request, @RequestBody MessagesDto messagesDto) {
        Map<String, Object> response = messagesService.addDetail(messagesDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/messages/detail-list-by-user", method = RequestMethod.POST)
    public String getMessageDetailList(HttpServletRequest request, @RequestBody MessagesDto dto) {
        Map<String, Object> response = messagesService.getMessageDetailListByUser(request.getHeader("Authorization"), dto.getId());
        return JSONProcessor.convertToJSON(response).toString();
    }
}
