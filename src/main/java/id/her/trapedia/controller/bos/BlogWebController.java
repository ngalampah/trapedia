package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.BlogDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseBlogDto;
import id.her.trapedia.service.BlogService;
import id.her.trapedia.util.ImageValidator;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/blog")
public class BlogWebController extends BaseController {

    private Logger logger = LogManager.getLogger(BlogWebController.class);

    private static String title = "Article";
    private static String activeMenu = "article";

    @Autowired
    private BlogService blogService;
    @Autowired
    private ImageValidator imageValidator;

    @RequestMapping("/index")
    public ModelAndView home (HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login") ;
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/blog/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseBlogDto responseBlogDto = new ResponseBlogDto();
        BlogDto blogDto = new BlogDto();
        if(!dto.getSearchValue().equals("")) {
            blogDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), BlogDto.class);
        }
        responseBlogDto = blogService.getListDT(blogDto, dto);
        responseBlogDto.setDraw(dto.getDraw());
        responseBlogDto.setRecordsFiltered(String.valueOf(responseBlogDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseBlogDto).toString();
        return jsonResponse;
    }

    @RequestMapping("/form")
    public ModelAndView add (BlogDto blogForm, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login") ;
        }

        BlogDto blogDto;
        if (blogForm.getId() != null && !blogForm.getId().equals("")) {
            blogDto = blogService.getBlogById(blogForm.getId());
        } else {
            blogDto = new BlogDto();
        }

        mav = initPage(mav, title, activeMenu);
        mav.addObject("blogForm", blogDto);
        mav.setViewName("admin/blog/form");
        return mav;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save (@ModelAttribute("blogForm") @Valid BlogDto blogForm, BindingResult bindingResult) {
//        if (!isLoggedIn(request)) {
//            return new ModelAndView("redirect:/admin/auth/login") ;
//        }

//        imageValidator.validate(blogForm.getFile(), bindingResult);

        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/blog/form");
        } else {
            boolean status = blogService.saveDataForm(blogForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/blog/index");
            } else {
                mav = new ModelAndView("admin/blog/form");
            }
        }
        return mav;
    }

    @RequestMapping("/delete")
    public ModelAndView delete (HttpServletRequest request, BlogDto blog) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login") ;
        }

        blog = blogService.delete(blog);

        mav = new ModelAndView("redirect:/admin/blog/index");
        return mav;
    }

}
