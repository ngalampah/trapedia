package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.AdminDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseAdminDto;
import id.her.trapedia.service.AdminService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/user-admin")
public class AdminWebController extends BaseController {
    private Logger logger = LogManager.getLogger(CategoryWebController.class);

    private static String title = "User Admin";
    private static String activeMenu = "user";

    @Autowired
    private AdminService adminService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/user-admin/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseAdminDto responseAdminDto = new ResponseAdminDto();
        AdminDto adminDto = new AdminDto();
        if(!dto.getSearchValue().equals("")) {
            adminDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), AdminDto.class);
        }
        responseAdminDto = adminService.getListDT(adminDto, dto);
        responseAdminDto.setDraw(dto.getDraw());
        responseAdminDto.setRecordsFiltered(String.valueOf(responseAdminDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseAdminDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public ModelAndView form(AdminDto adminForm) {
        AdminDto adminDto;
        if (adminForm.getId() != null && !adminForm.getId().equals("")) {
            adminDto = adminService.getAdminByIdAdmin(adminForm.getId());
        } else {
            adminDto = new AdminDto();
        }
        ModelAndView mav = new ModelAndView("admin/user-admin/form");
        mav = initPage(mav, title, activeMenu);
        mav.addObject("adminForm", adminDto);
        return mav;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("adminForm") @Valid AdminDto adminForm, BindingResult bindingResult) {
        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/user-admin/form");
        } else {
            boolean status = adminService.saveDataForm(adminForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/user-admin/index");
            } else {
                mav = new ModelAndView("admin/user-admin/form");
            }
        }
        return mav;
    }


}
