package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.*;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.service.SubscribeService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/subscribe")
public class SubscribeWebController extends BaseController {
    private Logger logger = LogManager.getLogger(SubscribeWebController.class);

    private static String title = "Email Subscribe";
    private static String activeMenu = "inbox";

    @Autowired
    private SubscribeService subscribeService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/subscribe/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseSubscribeDto responseSubscribeDto = new ResponseSubscribeDto();
        SubscribeDto subscribeDto = new SubscribeDto();
        if(!dto.getSearchValue().equals("")) {
            subscribeDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), SubscribeDto.class);
        }
        responseSubscribeDto = subscribeService.getListDT(subscribeDto, dto);
        responseSubscribeDto.setDraw(dto.getDraw());
        responseSubscribeDto.setRecordsFiltered(String.valueOf(responseSubscribeDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseSubscribeDto).toString();
        return jsonResponse;
    }




}
