package id.her.trapedia.controller.bos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.javalite.activeweb.AppController;
import org.javalite.activeweb.Captcha;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

public class BaseController extends AppController {

    private Logger logger = LogManager.getLogger(BaseController.class);

    public Map<String, Object> initCaptcha(HttpServletRequest request){
        Map<String, Object> mapCaptcha = new HashMap<>();
        String captchaText = Captcha.generateText();
        captchaText = captchaText.substring(0,4);
        request.getSession().setAttribute("captchaText", captchaText);
        byte[] captchaImage = Captcha.generateImage(captchaText);
        String captcha = Base64.encodeBase64String(captchaImage);
        mapCaptcha.put("captchaText", captchaText);
        mapCaptcha.put("captcha", captcha);

        HttpSession session = request.getSession();
        session.setAttribute("captchaText", captchaText);

        return mapCaptcha;
    }

    public boolean isLoggedIn(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("isLoggedIn") == null) {
            return false;
        } else {
            return true;
        }
    }

    public ModelAndView initPage(ModelAndView resp, String title, String activeMenu) {
        resp.addObject("title", title);
        resp.addObject("activeMenu", activeMenu);

        return resp;
    }

}
