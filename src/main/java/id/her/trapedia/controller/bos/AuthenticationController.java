package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.AdminDto;
import id.her.trapedia.dto.LoginDto;
import id.her.trapedia.service.AdminService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class AuthenticationController extends BaseController {

    private Logger logger = LogManager.getLogger(AuthenticationController.class);

    @Autowired
    private AdminService adminService;

    @RequestMapping("")
    public String index() {
        return "redirect:admin/auth/login";
    }

    @RequestMapping(value = "/auth/login", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView auth(LoginDto login, HttpServletRequest request, HttpSession session) {
        ModelAndView mav = new ModelAndView();

        AdminDto auth = adminService.authenticate(login.getUsername(), login.getPassword());

        if (auth != null) {
            session = request.getSession();
            session.setMaxInactiveInterval(10 * 60);
            session.setAttribute("isLoggedIn", true);
            session.setAttribute("username", auth.getUsername());
            session.setAttribute("name", auth.getName());
            mav.setViewName("redirect:/admin/home");
        } else {
            mav.setViewName("login");
        }

        return mav;
    }

    @RequestMapping(value = "/auth/logout", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView logout(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("redirect:/admin/auth/login");
        HttpSession session = request.getSession(false);
        session.removeAttribute("isLoggedIn");
        session.removeAttribute("username");
        session.getMaxInactiveInterval();
        return mav;
    }

}
