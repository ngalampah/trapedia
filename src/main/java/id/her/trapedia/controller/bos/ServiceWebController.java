package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.*;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.service.ServiceService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin/service")
public class ServiceWebController extends BaseController {
    private Logger logger = LogManager.getLogger(ServiceWebController.class);

    @Value("${trapedia.url.frontend}")
    private String urlFrontend;

    private static String title = "Service";
    private static String activeMenu = "service";

    @Autowired
    private ServiceService serviceService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/service/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseServiceDto responseCategoryDto = new ResponseServiceDto();
        ServiceDto serviceDto = new ServiceDto();
        if(!dto.getSearchValue().equals("")) {
            serviceDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), ServiceDto.class);
        }
        responseCategoryDto = serviceService.getListDT(serviceDto, dto);
        responseCategoryDto.setDraw(dto.getDraw());
        responseCategoryDto.setRecordsFiltered(String.valueOf(responseCategoryDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseCategoryDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public ModelAndView form(Long id) {
        ServiceDto serviceDto = serviceService.getServiceByIdAdmin(id);
        List<ServiceDetailDto> serviceDetailDtoList = serviceService.getServiceDetailByIdAdmin(id);
        ModelAndView mav = new ModelAndView("admin/service/detail");
        mav.addObject("data", serviceDto);
        mav.addObject("urlFrontend", urlFrontend);
        mav.addObject("serviceDetail", serviceDetailDtoList);
        return mav;
    }

    @RequestMapping(value = "/approve", method = RequestMethod.GET)
    public ModelAndView approve(@ModelAttribute("serviceDto") ServiceDto serviceDto) {
        ModelAndView mav;

        boolean status = serviceService.setApproveService(serviceDto);
        if (status) {
            mav = new ModelAndView("redirect:/admin/service/index");
        } else {
            mav = new ModelAndView("redirect:/admin/service/detail?id=" + serviceDto.getId());
        }
        return mav;
    }



}
