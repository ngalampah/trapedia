package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.*;
import id.her.trapedia.service.RevenuesService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/revenues")
public class RevenuesWebController extends BaseController {
    private Logger logger = LogManager.getLogger(RevenuesWebController.class);

    private static String title = "Revenues";
    private static String activeMenu = "revenues";

    @Autowired
    private RevenuesService revenuesService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/revenues/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseRevenuesDto responseRevenuesDto = new ResponseRevenuesDto();
        RevenueDto revenueDto = new RevenueDto();
        if(!dto.getSearchValue().equals("")) {
            revenueDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), RevenueDto.class);
        }
        responseRevenuesDto = revenuesService.getListDT(revenueDto, dto);
        responseRevenuesDto.setDraw(dto.getDraw());
        responseRevenuesDto.setRecordsFiltered(String.valueOf(responseRevenuesDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseRevenuesDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public ModelAndView form(Long id) {
        RevenueDto revenueDto = revenuesService.getRevenuesByIdAdmin(id);
        ModelAndView mav = new ModelAndView("admin/revenues/detail");
        mav.addObject("data", revenueDto);
        return mav;
    }


}
