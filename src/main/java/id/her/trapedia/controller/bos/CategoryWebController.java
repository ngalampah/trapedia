package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseCategoryDto;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/category")
public class CategoryWebController extends BaseController {
    private Logger logger = LogManager.getLogger(CategoryWebController.class);

    private static String title = "Service";
    private static String activeMenu = "service";

    @Autowired
    private CategoryService categoryService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/category/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseCategoryDto responseCategoryDto = new ResponseCategoryDto();
        CategoryDto categoryDto = new CategoryDto();
        if(!dto.getSearchValue().equals("")) {
            categoryDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), CategoryDto.class);
        }
        responseCategoryDto = categoryService.getListDT(categoryDto, dto);
        responseCategoryDto.setDraw(dto.getDraw());
        responseCategoryDto.setRecordsFiltered(String.valueOf(responseCategoryDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseCategoryDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public ModelAndView form(@ModelAttribute("categoryForm") CategoryDto categoryForm) {
        CategoryDto categoryDto;
        if (categoryForm.getId() != null && !categoryForm.getId().equals("")) {
            categoryDto = categoryService.getCategoriByIdAdmin(categoryForm.getId());
        } else {
            categoryDto = new CategoryDto();
        }
        ModelAndView mav = new ModelAndView("admin/category/form");
        mav.addObject("categoryForm", categoryDto);
        return mav;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("categoryForm") @Valid CategoryDto categoryForm, BindingResult bindingResult) {
//        categoryFormValidator.validate(dto, bindingResult);
        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/category/form");
        } else {
            boolean status = categoryService.saveDataForm(categoryForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/category/index");
            } else {
                mav = new ModelAndView("admin/category/form");
            }
        }
        return mav;
    }


}
