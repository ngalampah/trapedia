package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.ProvincesDto;
import id.her.trapedia.dto.RegenciesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseRegenciesDto;
import id.her.trapedia.service.ProvincesService;
import id.her.trapedia.service.RegenciesService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin/regencies")
public class RegenciesWebController extends BaseController {
    private Logger logger = LogManager.getLogger(RegenciesWebController.class);

    private static String title = "Regencies";
    private static String activeMenu = "location";

    @Autowired
    private RegenciesService regenciesService;

    @Autowired
    private ProvincesService provincesService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/regencies/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseRegenciesDto responseRegenciesDto = new ResponseRegenciesDto();
        RegenciesDto regenciesDto = new RegenciesDto();
        if(!dto.getSearchValue().equals("")) {
            regenciesDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), RegenciesDto.class);
        }
        responseRegenciesDto = regenciesService.getListDT(regenciesDto, dto);
        responseRegenciesDto.setDraw(dto.getDraw());
        responseRegenciesDto.setRecordsFiltered(String.valueOf(responseRegenciesDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseRegenciesDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public ModelAndView form(RegenciesDto regenciesForm) {
        List<ProvincesDto> provinces = provincesService.getAllProvinces();
        RegenciesDto regenciesDto;
        if (regenciesForm.getId() != null && !regenciesForm.getId().equals("")) {
            regenciesDto = regenciesService.getRegenciesByIdAdmin(regenciesForm.getId());
        } else {
            regenciesDto = new RegenciesDto();
        }
        ModelAndView mav = new ModelAndView("admin/regencies/form");
        mav.addObject("regenciesForm", regenciesDto);
        mav.addObject("provinces", provinces);
        return mav;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("regenciesForm") @Valid RegenciesDto regenciesForm, BindingResult bindingResult) {
        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/regencies/form");
            List<ProvincesDto> provinces = provincesService.getAllProvinces();
            mav.addObject("provinces", provinces);
        } else {
            boolean status = regenciesService.saveDataForm(regenciesForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/regencies/index");
            } else {
                mav = new ModelAndView("admin/regencies/form");
            }
        }
        return mav;
    }


}
