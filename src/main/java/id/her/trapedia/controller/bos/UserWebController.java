package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseUserDto;
import id.her.trapedia.dto.UserDto;
import id.her.trapedia.service.UserService;
import id.her.trapedia.service.VerificationTokenService;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/user")
public class UserWebController extends BaseController {
    private Logger logger = LogManager.getLogger(UserWebController.class);

    @Autowired
    private VerificationTokenService verificationTokenService;

    @Value("${trapedia.url.backend}")
    private String urlBackend;

    @Value("${trapedia.url.frontend}")
    private String urlFrontend;

    private static String title = "User Member";
    private static String activeMenu = "user";

    @Autowired
    private UserService userService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/user/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto rpdt) {
        ResponseUserDto responseCategoryDto = new ResponseUserDto();
        UserDto userDto = new UserDto();
        if(!rpdt.getSearchValue().equals("")) {
            userDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(rpdt.getSearchValue()), UserDto.class);
        }
        responseCategoryDto = userService.getListDT(userDto, rpdt);
        responseCategoryDto.setDraw(rpdt.getDraw());
        responseCategoryDto.setRecordsFiltered(String.valueOf(responseCategoryDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseCategoryDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/form-profile", method = RequestMethod.GET)
    public ModelAndView form_profile(UserDto userForm) {
        UserDto userDto;
        if (userForm.getId() != null && !userForm.getId().equals("")) {
            userDto = userService.getUserByIdAdmin(userForm.getId());
        } else {
            userDto = new UserDto();
        }
        ModelAndView mav = new ModelAndView("admin/user/form-profile");
        mav.addObject("userForm", userDto);
        return mav;
    }

    @RequestMapping(value = "/save-profile", method = RequestMethod.POST)
    public ModelAndView save_profile(@ModelAttribute("userForm") @Valid UserDto userForm, BindingResult bindingResult) {
        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/user/form-profile");
        } else {
            boolean status = userService.saveDataProfileForm(userForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/user/index");
            } else {
                mav = new ModelAndView("admin/user/form-profile");
            }
        }
        return mav;
    }

    @RequestMapping(value = "/activation", method = RequestMethod.GET)
    public RedirectView activation(@RequestParam("token") String token) {
        String response = verificationTokenService.verificationToken(token);
        if (Constant.STATUS.SUCCESS.equalsIgnoreCase(response)) {
            return new RedirectView(urlFrontend+"/login?activation=success");
        } else {
            return new RedirectView(urlFrontend+"/login?activation=failed");
        }
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
    public RedirectView resetPassword(@RequestParam("username") String username) {
        String response = userService.resetPassword(username);
        if (Constant.STATUS.SUCCESS.equalsIgnoreCase(response)) {
            return new RedirectView(urlFrontend + "/login?resetpassword=success");
        } else {
            return new RedirectView(urlFrontend + "/login?resetpassword=failed");
        }
    }
}
