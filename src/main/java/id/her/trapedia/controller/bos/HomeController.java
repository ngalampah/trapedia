package id.her.trapedia.controller.bos;

import id.her.trapedia.service.HomeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin")
public class HomeController extends BaseController {

    private Logger logger = LogManager.getLogger(HomeController.class);

    private static String title = "Home";
    private static String activeMenu = "home";

    @Autowired
    HomeService homeService;

    @RequestMapping("/home")
    public ModelAndView home(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);

        mav.setViewName("admin/home");
        return mav;
    }


}
