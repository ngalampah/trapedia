package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.*;
import id.her.trapedia.service.CategoryDetailService;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin/category-detail")
public class CategoryDetailWebController extends BaseController {
    private Logger logger = LogManager.getLogger(CategoryDetailWebController.class);

    private static String title = "Category Detail";
    private static String activeMenu = "service";

    @Autowired
    private CategoryDetailService categoryDetailService;

    @Autowired
    private CategoryService categoryService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/category-detail/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseCategoryDetailDto responseCategoryDetailDto = new ResponseCategoryDetailDto();
        CategoryDetailDto categoryDetailDto = new CategoryDetailDto();
        if(!dto.getSearchValue().equals("")) {
            categoryDetailDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), CategoryDetailDto.class);
        }
        responseCategoryDetailDto = categoryDetailService.getListDT(categoryDetailDto, dto);
        responseCategoryDetailDto.setDraw(dto.getDraw());
        responseCategoryDetailDto.setRecordsFiltered(String.valueOf(responseCategoryDetailDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseCategoryDetailDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public ModelAndView form(@ModelAttribute("categoryDetailForm") CategoryDetailDto categoryDetailForm) {
        List<CategoryDto> category = categoryService.getAllCategory();
        CategoryDetailDto categoryDetailDto;
        if (categoryDetailForm.getId() != null && !categoryDetailForm.getId().equals("")) {
            categoryDetailDto = categoryDetailService.getCategoriDetailByIdAdmin(categoryDetailForm.getId());
        } else {
            categoryDetailDto = new CategoryDetailDto();
        }
        ModelAndView mav = new ModelAndView("admin/category-detail/form");
        mav.addObject("category", category);
        mav.addObject("categoryDetailForm", categoryDetailDto);
        return mav;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("categoryDetailForm") @Valid CategoryDetailDto categoryDetailForm, BindingResult bindingResult) {
//        categoryFormValidator.validate(dto, bindingResult);
        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/category-detail/form");
        } else {
            boolean status = categoryDetailService.saveDataForm(categoryDetailForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/category-detail/index");
            } else {
                mav = new ModelAndView("admin/category-detail/form");
            }
        }
        return mav;
    }


}
