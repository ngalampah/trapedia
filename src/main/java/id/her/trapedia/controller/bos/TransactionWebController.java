package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.*;
import id.her.trapedia.service.RevenuesService;
import id.her.trapedia.service.TransactionService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/transaction")
public class TransactionWebController extends BaseController {
    private Logger logger = LogManager.getLogger(TransactionWebController.class);

    private static String title = "Transaction";
    private static String activeMenu = "transaction";

    @Autowired
    private TransactionService transactionService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/transaction/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseTransactionDto responseTransactionDto = new ResponseTransactionDto();
        TransactionDto transactionDto = new TransactionDto();
        if(!dto.getSearchValue().equals("")) {
            transactionDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), TransactionDto.class);
        }
        responseTransactionDto = transactionService.getListDT(transactionDto, dto);
        responseTransactionDto.setDraw(dto.getDraw());
        responseTransactionDto.setRecordsFiltered(String.valueOf(responseTransactionDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseTransactionDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public ModelAndView form(Long id) {
        TransactionDto transactionDto = transactionService.getTransactionByIdAdmin(id);
        ModelAndView mav = new ModelAndView("admin/transaction/detail");
        mav.addObject("data", transactionDto);
        return mav;
    }

    @RequestMapping(value = "/approve", method = RequestMethod.GET)
    public ModelAndView approve(@ModelAttribute("transactionDto") TransactionDto transactionDto) {
        ModelAndView mav;

        boolean status = transactionService.setApproveTransaction(transactionDto);
        if (status) {
//            mav = new ModelAndView("redirect:/admin/transaction/index");
            mav = new ModelAndView("redirect:/admin/transaction/detail?id=" + transactionDto.getId());
        } else {
            mav = new ModelAndView("redirect:/admin/transaction/detail?id=" + transactionDto.getId());
        }
        return mav;
    }


}
