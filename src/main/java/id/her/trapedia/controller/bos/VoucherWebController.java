package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.VoucherDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseVoucherDto;
import id.her.trapedia.service.VoucherService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/voucher")
public class VoucherWebController extends BaseController {

    private Logger logger = LogManager.getLogger(VoucherWebController.class);

    private static String title = "Voucher";
    private static String activeMenu = "voucher";

    @Autowired
    private VoucherService voucherService;

    @RequestMapping("/index")
    public ModelAndView home (HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login") ;
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/voucher/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseVoucherDto responseVoucherDto = new ResponseVoucherDto();
        VoucherDto voucherDto = new VoucherDto();
        if(!dto.getSearchValue().equals("")) {
            voucherDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), VoucherDto.class);
        }
        responseVoucherDto = voucherService.getListDT(voucherDto, dto);
        responseVoucherDto.setDraw(dto.getDraw());
        responseVoucherDto.setRecordsFiltered(String.valueOf(responseVoucherDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseVoucherDto).toString();
        return jsonResponse;
    }

    @RequestMapping("/form")
    public ModelAndView add (VoucherDto voucherForm, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login") ;
        }

        VoucherDto voucherDto;
        if (voucherForm.getId() != null && !voucherForm.getId().equals("")) {
            voucherDto = voucherService.getVoucherById(voucherForm.getId());
        } else {
            voucherDto = new VoucherDto();
        }

        mav = initPage(mav, title, activeMenu);
        mav.addObject("voucherForm", voucherDto);
        mav.setViewName("admin/voucher/form");
        return mav;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save (@ModelAttribute("voucherForm") @Valid VoucherDto voucherForm, BindingResult bindingResult) {
//        if (!isLoggedIn(request)) {
//            return new ModelAndView("redirect:/admin/auth/login") ;
//        }

        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/voucher/form");
        } else {
            boolean status = voucherService.saveDataForm(voucherForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/voucher/index");
            } else {
                mav = new ModelAndView("admin/voucher/form");
            }
        }
        return mav;
    }

    @RequestMapping("/delete")
    public ModelAndView delete (HttpServletRequest request, VoucherDto voucher) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login") ;
        }

        mav = initPage(mav, title, activeMenu);

        voucher = voucherService.delete(voucher);

        mav.addObject("voucher", voucher);
        mav.setViewName("list");
        return mav;
    }

}
