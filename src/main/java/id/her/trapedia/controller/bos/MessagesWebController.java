package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.*;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.service.CommentServiceService;
import id.her.trapedia.service.MessagesService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/messages")
public class MessagesWebController extends BaseController {
    private Logger logger = LogManager.getLogger(MessagesWebController.class);

    private static String title = "Messages";
    private static String activeMenu = "inbox";

    @Autowired
    private MessagesService messagesService;


    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/messages/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseMessagesDto responseMessagesDto = new ResponseMessagesDto();
        MessagesDto messagesDto = new MessagesDto();
        if(!dto.getSearchValue().equals("")) {
            messagesDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), MessagesDto.class);
        }
        responseMessagesDto = messagesService.getListDT(messagesDto, dto);
        responseMessagesDto.setDraw(dto.getDraw());
        responseMessagesDto.setRecordsFiltered(String.valueOf(responseMessagesDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseMessagesDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public ModelAndView form(@ModelAttribute("messagesForm") MessagesDto messagesDto) {
        MessagesDto cs;
        if (messagesDto.getId() != null && !messagesDto.getId().equals("")) {
            cs = messagesService.getMessagesByIdAdmin(messagesDto.getId());
        } else {
            cs = new MessagesDto();
        }
        ModelAndView mav = new ModelAndView("admin/messages/form");
        mav.addObject("messagesForm", cs);
        return mav;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("categoryForm") @Valid MessagesDto messagesDto, BindingResult bindingResult) {
//        categoryFormValidator.validate(dto, bindingResult);
        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/messages/form");
        } else {
            boolean status = messagesService.saveDataForm(messagesDto);
            if (status) {
                mav = new ModelAndView("redirect:/admin/messages/index");
            } else {
                mav = new ModelAndView("admin/messages/form");
            }
        }
        return mav;
    }


}
