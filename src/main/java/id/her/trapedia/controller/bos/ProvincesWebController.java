package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.ProvincesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseProvincesDto;
import id.her.trapedia.service.ProvincesService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/provinces")
public class ProvincesWebController extends BaseController {
    private Logger logger = LogManager.getLogger(ProvincesWebController.class);

    private static String title = "Provinces";
    private static String activeMenu = "location";

    @Autowired
    private ProvincesService provincesService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/provinces/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseProvincesDto responseProvincesDto = new ResponseProvincesDto();
        ProvincesDto provincesDto = new ProvincesDto();
        if(!dto.getSearchValue().equals("")) {
            provincesDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), ProvincesDto.class);
        }
        responseProvincesDto = provincesService.getListDT(provincesDto, dto);
        responseProvincesDto.setDraw(dto.getDraw());
        responseProvincesDto.setRecordsFiltered(String.valueOf(responseProvincesDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseProvincesDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public ModelAndView form(ProvincesDto provincesForm) {
        ProvincesDto provincesDto;
        if (provincesForm.getId() != null && !provincesForm.getId().equals("")) {
            provincesDto = provincesService.getProvincesByIdAdmin(provincesForm.getId());
        } else {
            provincesDto = new ProvincesDto();
        }
        ModelAndView mav = new ModelAndView("admin/provinces/form");
        mav.addObject("provincesForm", provincesDto);
        return mav;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("provincesForm") @Valid ProvincesDto provincesForm, BindingResult bindingResult) {
        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/provinces/form");
        } else {
            boolean status = provincesService.saveDataForm(provincesForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/provinces/index");
            } else {
                mav = new ModelAndView("admin/provinces/form");
            }
        }
        return mav;
    }


}
