package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.*;
import id.her.trapedia.service.PagesService;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ImageValidator;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/pages")
public class PagesWebController extends BaseController {

    private Logger logger = LogManager.getLogger(PagesWebController.class);

    private static String title = "Article";
    private static String activeMenu = "article";

    @Autowired
    private PagesService pagesService;
    @Autowired
    private ImageValidator imageValidator;

    @RequestMapping("/index")
    public ModelAndView home (HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login") ;
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/pages/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponsePagesDto responsePagesDto = new ResponsePagesDto();
        PagesDto pagesDto = new PagesDto();
        if(!dto.getSearchValue().equals("")) {
            pagesDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), PagesDto.class);
        }
        responsePagesDto = pagesService.getListDT(pagesDto, dto);
        responsePagesDto.setDraw(dto.getDraw());
        responsePagesDto.setRecordsFiltered(String.valueOf(responsePagesDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responsePagesDto).toString();
        return jsonResponse;
    }

    @RequestMapping("/form")
    public ModelAndView add (PagesDto pagesForm, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login") ;
        }

        PagesDto pagesDto;
        if (pagesForm.getId() != null && !pagesForm.getId().equals("")) {
            pagesDto = pagesService.getPagesById(pagesForm.getId());
        } else {
            pagesDto = new PagesDto();
        }

        mav = initPage(mav, title, activeMenu);
        mav.addObject("pagesForm", pagesDto);
        mav.setViewName("admin/pages/form");
        return mav;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save (@ModelAttribute("pagesForm") @Valid PagesDto pagesForm, BindingResult bindingResult) {
//        if (!isLoggedIn(request)) {
//            return new ModelAndView("redirect:/admin/auth/login") ;
//        }

//        imageValidator.validate(pagesForm.getFile(), bindingResult);

        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/pages/form");
        } else {
            boolean status = pagesService.saveDataForm(pagesForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/pages/index");
            } else {
                mav = new ModelAndView("admin/pages/form");
            }
        }
        return mav;
    }

    @RequestMapping("/delete")
    public ModelAndView delete (HttpServletRequest request, PagesDto pages) {
        ModelAndView mav = new ModelAndView();

        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login") ;
        }

        pages = pagesService.delete(pages);

        mav = new ModelAndView("redirect:/admin/pages/index");
        return mav;
    }

}
