package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.*;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.service.CommentServiceService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin/comment")
public class CommentWebController extends BaseController {
    private Logger logger = LogManager.getLogger(CommentWebController.class);

    private static String title = "Comment Service";
    private static String activeMenu = "inbox";

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CommentServiceService commentServiceService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/comment/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponseCommentServiceDto responseCommentServiceDto = new ResponseCommentServiceDto();
        CommentServiceDto commentServiceDto = new CommentServiceDto();
        if(!dto.getSearchValue().equals("")) {
            commentServiceDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), CommentServiceDto.class);
        }
        responseCommentServiceDto = commentServiceService.getListDT(commentServiceDto, dto);
        responseCommentServiceDto.setDraw(dto.getDraw());
        responseCommentServiceDto.setRecordsFiltered(String.valueOf(responseCommentServiceDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responseCommentServiceDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public ModelAndView form(@ModelAttribute("commentForm") CommentServiceDto commentServiceDto) {
        CommentServiceDto cs;
        if (commentServiceDto.getId() != null && !commentServiceDto.getId().equals("")) {
            cs = commentServiceService.getCommentByIdAdmin(commentServiceDto.getId());
        } else {
            cs = new CommentServiceDto();
        }
        ModelAndView mav = new ModelAndView("admin/comment/form");
        mav.addObject("commentForm", cs);
        return mav;
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("categoryForm") @Valid CommentServiceDto commentServiceForm, BindingResult bindingResult) {
//        categoryFormValidator.validate(dto, bindingResult);
        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            mav = new ModelAndView("admin/comment/form");
        } else {
            boolean status = commentServiceService.saveDataForm(commentServiceForm);
            if (status) {
                mav = new ModelAndView("redirect:/admin/comment/index");
            } else {
                mav = new ModelAndView("admin/comment/form");
            }
        }
        return mav;
    }


}
