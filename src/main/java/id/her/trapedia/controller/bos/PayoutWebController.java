package id.her.trapedia.controller.bos;

import id.her.trapedia.dto.*;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.service.PayoutService;
import id.her.trapedia.util.JSONProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin/payout")
public class PayoutWebController extends BaseController {
    private Logger logger = LogManager.getLogger(PayoutWebController.class);

    private static String title = "Payout";
    private static String activeMenu = "payout";

    @Autowired
    private PayoutService payoutService;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        if (!isLoggedIn(request)) {
            return new ModelAndView("redirect:/admin/auth/login");
        }

        mav = initPage(mav, title, activeMenu);
        mav.setViewName("admin/payout/list");
        return mav;
    }

    @RequestMapping(value = "/datatable", method = RequestMethod.GET)
    @ResponseBody
    public String datatable(@ModelAttribute RequestPagingDataTablesBaseDto dto) {
        ResponsePayoutDto responsePayoutDto = new ResponsePayoutDto();
        PayoutDto payoutDto = new PayoutDto();
        if(!dto.getSearchValue().equals("")) {
            payoutDto = JSONProcessor.getObjectFromJSON(JSONProcessor.convertToJSON(dto.getSearchValue()), PayoutDto.class);
        }
        responsePayoutDto = payoutService.getListDT(payoutDto, dto);
        responsePayoutDto.setDraw(dto.getDraw());
        responsePayoutDto.setRecordsFiltered(String.valueOf(responsePayoutDto.getRecordsTotal()));
        String jsonResponse = JSONProcessor.convertToJSON(responsePayoutDto).toString();
        return jsonResponse;
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public ModelAndView form(Long id) {
        PayoutDto payoutDto = payoutService.getPayoutByIdAdmin(id);
        ModelAndView mav = new ModelAndView("admin/payout/detail");
        mav.addObject("data", payoutDto);
        return mav;
    }

    @RequestMapping(value = "/approve", method = RequestMethod.GET)
    public ModelAndView approve(@ModelAttribute("payoutDto") PayoutDto payoutDto) {
        ModelAndView mav;

        boolean status = payoutService.setApproveService(payoutDto);
        if (status) {
            mav = new ModelAndView("redirect:/admin/payout/index");
        } else {
            mav = new ModelAndView("redirect:/admin/payout/detail?id=" + payoutDto.getId());
        }
        return mav;
    }


}
