package id.her.trapedia.controller;

import freemarker.template.TemplateException;
import id.her.trapedia.dto.EmailDto;
import id.her.trapedia.dto.ReqTransactionDetailDto;
import id.her.trapedia.dto.ReqTransactionDto;
import id.her.trapedia.service.SendingEmailService;
import id.her.trapedia.service.TransactionService;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.TrapediaHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class EmailController {
    private Logger logger = LogManager.getLogger(EmailController.class);

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private SendingEmailService emailService;




    @Value("${trapedia.url.backend}")
    private String urlBackend;

    @Value("${trapedia.url.frontend}")
    private String urlFrontend;

    @RequestMapping("/mail/sendInvoiceNotification")
    public Boolean sendInvoiceNotification(Long idTransaction) {
        Map<String, Object> mapTransaction = transactionService.getServiceById(idTransaction);
        ReqTransactionDto transaction = null;
        if (mapTransaction != null) {
            transaction = (ReqTransactionDto) mapTransaction.get("data");
        }

        EmailDto email = new EmailDto();
        email.setFrom(Constant.DEFAULT_EMAIL);
        email.setTo(transaction.getTravelingEmail());
        email.setSubject("Waiting for Payment " + transaction.getNumberTransaction());
        email.setTemplate(Constant.EMAIL_TEMPLATE.INVOICE);
        email.setLogo(Constant.TRAPEDIA_LOGO.WHITE);

        Map model = new HashMap();
        model.put("name", transaction.getTravelingName());
        model.put("paymentMethode", transaction.getPaymentMethode());
        model.put("paymentCode", transaction.getPaymentCode());
        model.put("paymentStatus", TrapediaHelper.paymentStatus(transaction.getStatusPayment()));
        model.put("paymentAmount", transaction.getPaymentAmount());
        model.put("dueDate", TrapediaHelper.dueDate(new Date()));
        model.put("invoiceId", transaction.getNumberTransaction());
        model.put("date", DateUtils.formatDateToStr(new Date(), "dd-MM-yyyy HH:mm:ss"));
        model.put("services", transaction.getFormDetailCart());
        model.put("total", transaction.getTotal());
        model.put("adminFee", transaction.getFeeAdmin());
        model.put("discount", transaction.getDiscount());
        model.put("uniqueCode", (transaction.getPaymentAmount()-transaction.getTotal()));

        email.setModel(model);

        try {
            emailService.sendEmail(email);
            return true;
        } catch (MessagingException e) {
            logger.error("Error while send invoice mail : " + e.getMessage());
        } catch (IOException e) {
            logger.error("Error while send invoice mail : " + e.getMessage());
        } catch (TemplateException e) {
            logger.error("Error while send invoice mail : " + e.getMessage());
        }

        return false;
    }

    @RequestMapping("/mail/sendReceiptNotification")
    public Boolean sendReceiptNotification(Long idTransaction) {
        Map<String, Object> mapTransaction = transactionService.getServiceById(idTransaction);
        ReqTransactionDto transaction = null;
        if (mapTransaction != null) {
            transaction = (ReqTransactionDto) mapTransaction.get("data");
        }

        EmailDto email = new EmailDto();
        email.setFrom(Constant.DEFAULT_EMAIL);
        email.setTo(transaction.getTravelingEmail());
        email.setSubject("Receipt Transaction " + transaction.getNumberTransaction());
        email.setTemplate(Constant.EMAIL_TEMPLATE.RECEIPT);
        email.setLogo(Constant.TRAPEDIA_LOGO.DEFAULT);

        Map model = new HashMap();
        model.put("invoiceId", transaction.getNumberTransaction());
        model.put("date", DateUtils.formatDateToStr(new Date(), "dd-MM-yyyy HH:mm:ss"));
        model.put("name", transaction.getTravelingName());
        model.put("email", transaction.getTravelingEmail());
        model.put("phone", transaction.getTravelingPhone());
        model.put("paymentMethode", transaction.getPaymentMethode());
        model.put("paymentStatus", TrapediaHelper.paymentStatus(transaction.getStatusPayment()));
        model.put("paymentAmount", transaction.getPaymentAmount());
        model.put("services", transaction.getFormDetailCart());
        model.put("total", transaction.getTotal());
        model.put("adminFee", transaction.getFeeAdmin());
        model.put("discount", transaction.getDiscount());
        model.put("uniqueCode", (transaction.getPaymentAmount()-transaction.getTotal()));

        email.setModel(model);

        try {
            emailService.sendEmail(email);
            return true;
        } catch (MessagingException e) {
            logger.error("Error while send invoice mail : " + e.getMessage());
        } catch (IOException e) {
            logger.error("Error while send invoice mail : " + e.getMessage());
        } catch (TemplateException e) {
            logger.error("Error while send invoice mail : " + e.getMessage());
        }

        return false;
    }

    @RequestMapping("/mail/sendActivationNotification")
    public Boolean sendActivationNotification() {
        EmailDto email = new EmailDto();
        email.setFrom(Constant.DEFAULT_EMAIL);
        email.setTo("paranti.surat@gmail.com");
        email.setSubject("Complete Registration");
        email.setTemplate(Constant.EMAIL_TEMPLATE.USER_ACTIVATION);
        email.setLogo(Constant.TRAPEDIA_LOGO.WHITE);

        Map model = new HashMap();
        model.put("name", "Herdi Agustina");
//        model.put("actionUrl", urlActivation);
        model.put("actionUrl", urlBackend + "/admin/user/activation?token=");

        email.setModel(model);

        try {
            emailService.sendEmail(email);
            return true;
        } catch (MessagingException e) {
            logger.error("Error while send invoice mail : " + e.getMessage());
        } catch (IOException e) {
            logger.error("Error while send invoice mail : " + e.getMessage());
        } catch (TemplateException e) {
            logger.error("Error while send invoice mail : " + e.getMessage());
        }

        return false;
    }
}
