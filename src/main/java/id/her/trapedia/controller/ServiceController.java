package id.her.trapedia.controller;

import id.her.trapedia.dto.PagingDto;
import id.her.trapedia.dto.ReqServiceDto;
import id.her.trapedia.dto.SearchServiceDto;
import id.her.trapedia.dto.ServiceDto;
import id.her.trapedia.service.ServiceService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @RequestMapping(value = "/public/service/list", method = RequestMethod.POST)
    public String list(@RequestBody PagingDto pagingDto) {
        Map<String, Object> response = serviceService.findAllService(pagingDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/service/list-m", method = RequestMethod.POST)
    public String listManual(@RequestBody SearchServiceDto searchServiceDto) {
        Map<String, Object> response = serviceService.findAllServiceManual(searchServiceDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/service/list-popular", method = RequestMethod.POST)
    public String listPopular() {
        Map<String, Object> response = serviceService.findAllPopularService();
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/service/list-new", method = RequestMethod.POST)
    public String listNew() {
        Map<String, Object> response = serviceService.findAllNewService();
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/service/recomendation-category")
    public String getServiceRecomendationCat(@RequestBody ReqServiceDto reqServiceDto) {
        Map<String, Object> response = serviceService.findServiceRecomendation(reqServiceDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/service/list-tranding-week", method = RequestMethod.POST)
    public String listTrandingWeek() {
        Map<String, Object> response = serviceService.findAllTrandingWeekService();
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/service/list-tranding-month", method = RequestMethod.POST)
    public String listTrandingMonth() {
        Map<String, Object> response = serviceService.findAllTrandingMonthService();
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/service/get")
    public String getServiceByIdPub(@RequestBody ReqServiceDto reqServiceDto) {
        Map<String, Object> response = serviceService.getServiceById(reqServiceDto.getId());
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/service/list", method = RequestMethod.POST)
    public String list(HttpServletRequest request) {
        Map<String, Object> response = serviceService.findAllByUser(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/service/get")
    public String getServiceById(@RequestBody ReqServiceDto reqServiceDto, HttpServletRequest request) {
        Map<String, Object> response = serviceService.getServiceByIdUser(reqServiceDto.getId(), request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/service/insert")
    public String insert(HttpServletRequest request, @RequestBody ReqServiceDto reqServiceDto) {
        Map<String, Object> response = serviceService.insert(reqServiceDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/service/insert-image", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public String insertImage(Long id,
                              @RequestPart(value = "images_1", required = false) MultipartFile img1,
                              @RequestPart(value = "images_2", required = false) MultipartFile img2,
                              @RequestPart(value = "images_3", required = false) MultipartFile img3,
                              @RequestPart(value = "images_4", required = false) MultipartFile img4,
                              @RequestPart(value = "images_5", required = false) MultipartFile img5) {
//        System.out.print(id + " " + img1.getOriginalFilename());
        Map<String, Object> response = serviceService.insertImage(id, img1, img2, img3, img4, img5);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/service/delete-image-service", method = RequestMethod.POST)
    public String deleteImageService(HttpServletRequest request, @RequestBody ServiceDto serviceDto) {
        Map<String, Object> response = serviceService.deleteImageService(serviceDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/service/update")
    public String update(@RequestBody ReqServiceDto reqServiceDto) {
        Map<String, Object> response = serviceService.update(reqServiceDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/service/delete")
    public String delete(@RequestBody ReqServiceDto reqServiceDto) {
        Map<String, Object> response = serviceService.delete(reqServiceDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/service/list-by-user", method = RequestMethod.POST)
    public String listByUser(@RequestBody ReqServiceDto reqServiceDto) {
        Map<String, Object> response = serviceService.findAllByUserId(reqServiceDto.getUserId());
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/service/insert-image-editor", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public String insertImageEditor(Long id,
                              @RequestPart(value = "image", required = false) MultipartFile img1) {
//        System.out.print(id + " " + img1.getOriginalFilename());
        Map<String, Object> response = serviceService.insertImageEditor(img1);
        return JSONProcessor.convertToJSON(response).toString();
    }
}
