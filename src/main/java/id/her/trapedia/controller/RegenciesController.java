package id.her.trapedia.controller;

import id.her.trapedia.dto.RegenciesDto;
import id.her.trapedia.service.RegenciesService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class RegenciesController {

    @Autowired
    private RegenciesService regenciesService;

    @RequestMapping(value = "/public/regencies/list")
    public String list() {
        Map<String, Object> response = regenciesService.findAll();
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/regencies/popular")
    public String popular() {
        Map<String, Object> response = regenciesService.findPopular();
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/regencies/get")
    public String get(@RequestBody RegenciesDto regenciesDto) {
        Map<String, Object> response = regenciesService.getRegenciesById(regenciesDto.getId());
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/regencies/get-by-keyword", method = RequestMethod.POST)
    public String listManual(@RequestBody RegenciesDto regenciesDto) {
        Map<String, Object> response = regenciesService.findRegenciesByKeyword(regenciesDto);
        return JSONProcessor.convertToJSON(response).toString();
    }
}
