package id.her.trapedia.controller;

import id.her.trapedia.dto.UserDto;
import id.her.trapedia.model.VerificationTokenModel;
import id.her.trapedia.service.UserService;
import id.her.trapedia.service.VerificationTokenService;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user/register", method = RequestMethod.POST)
    public String register(HttpServletRequest request, @RequestBody UserDto user) {
        Map<String, Object> response = userService.register(user);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/user/change_profile", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public String editProfile(HttpServletRequest request, UserDto user, @RequestPart(value = "file", required = false) MultipartFile file) {
        Map<String, Object> response = userService.editProfile(user, file, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/user/delete-image-profile", method = RequestMethod.POST)
    public String deleteImageProfile(HttpServletRequest request, @RequestBody UserDto user) {
        Map<String, Object> response = userService.deleteImageProfile(user, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/user/change_password", method = RequestMethod.POST)
    public String changePassword(HttpServletRequest request, @RequestBody UserDto user) {
        Map<String, Object> response = userService.changePassword(user, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/user/change_bank", method = RequestMethod.POST)
    public String changeBank(HttpServletRequest request, @RequestBody UserDto user) {
        Map<String, Object> response = userService.changeBank(user, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/user/profile", method = RequestMethod.GET)
    public String getProfile(HttpServletRequest request) {
        Map<String, Object> response = userService.userProfile(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/user/profile", method = RequestMethod.POST)
    public String getProfilePublic(@RequestBody UserDto user) {
        Map<String, Object> response = userService.userProfilePublic(user);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/user/byid", method = RequestMethod.POST)
    public String getProfileByIdPublic(@RequestBody UserDto user) {
        Map<String, Object> response = userService.userProfilePublic(user);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/user/requestResetPassword", method = RequestMethod.POST)
    public String requestResetPassword(@RequestBody UserDto user) {
        Map<String, Object> response = userService.requestResetPassword(user.getUsername());
        return JSONProcessor.convertToJSON(response).toString();
    }

}
