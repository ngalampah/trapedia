package id.her.trapedia.controller;

import id.her.trapedia.dto.CommentServiceDto;
import id.her.trapedia.dto.MessagesDto;
import id.her.trapedia.service.CommentServiceService;
import id.her.trapedia.service.MessagesService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class CommentServiceController {

    @Autowired
    private CommentServiceService commentServiceService;

    @RequestMapping(value = "/comment_service/add", method = RequestMethod.POST)
    public String add(HttpServletRequest request, @RequestBody CommentServiceDto commentServiceDto) {
        Map<String, Object> response = commentServiceService.add(commentServiceDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/comment_service/by-service", method = RequestMethod.POST)
    public String getCommentByService(@RequestBody CommentServiceDto commentServiceDto) {
        Map<String, Object> response = commentServiceService.getListMessageByService(commentServiceDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/comment_service/insert-image", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public String insertImage(Long id,
                              @RequestPart(value = "images_1", required = false) MultipartFile img1,
                              @RequestPart(value = "images_2", required = false) MultipartFile img2,
                              @RequestPart(value = "images_3", required = false) MultipartFile img3,
                              @RequestPart(value = "images_4", required = false) MultipartFile img4,
                              @RequestPart(value = "images_5", required = false) MultipartFile img5) {
//        System.out.print(id + " " + img1.getOriginalFilename());
        Map<String, Object> response = commentServiceService.insertImage(id, img1, img2, img3, img4, img5);
        return JSONProcessor.convertToJSON(response).toString();
    }


    @RequestMapping(value = "/comment_service/add-detail", method = RequestMethod.POST)
    public String addDetail(HttpServletRequest request, @RequestBody CommentServiceDto commentServiceDto) {
        Map<String, Object> response = commentServiceService.addDetail(commentServiceDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/comment_service/by-comment-id", method = RequestMethod.POST)
    public String getCommentByServiceId(@RequestBody CommentServiceDto commentServiceDto) {
        Map<String, Object> response = commentServiceService.getListMessageByCommentId(commentServiceDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

}
