package id.her.trapedia.controller;

import id.her.trapedia.service.RevenuesService;
import id.her.trapedia.service.UserBalanceService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class BalanceController {

    @Autowired
    private UserBalanceService userBalanceService;




    @RequestMapping(value = "/balance/list", method = RequestMethod.POST)
    public String revenuesList(HttpServletRequest request) {
        Map<String, Object> response = userBalanceService.balanceByUser(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

}
