package id.her.trapedia.controller;

import id.her.trapedia.dto.ReqTransactionDto;
import id.her.trapedia.service.TransactionService;
import id.her.trapedia.service.VoucherService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class VoucherController {

    @Autowired
    private VoucherService voucherService;

    @RequestMapping(value = "/voucher/add", method = RequestMethod.POST)
    public String addVoucher(@RequestBody ReqTransactionDto reqTransactionDto) {
        Map<String, Object> response = voucherService.add(reqTransactionDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

}
