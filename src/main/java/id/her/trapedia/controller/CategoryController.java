package id.her.trapedia.controller;

import id.her.trapedia.dao.CategoryDao;
import id.her.trapedia.dto.CategoryDetailDto;
import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.ReqServiceDto;
import id.her.trapedia.model.CategoryModel;
import id.her.trapedia.service.CategoryDetailService;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryDetailService categoryDetailService;

    @RequestMapping(value = "/public/category/list")
    public String list() {
        Map<String, Object> response = categoryService.findAll();
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/category/detail/list")
    public String listDetail() {
        Map<String, Object> response = categoryDetailService.findAll();
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/category/detail/get")
    public String getDetail(@RequestBody CategoryDetailDto categoryDetailDto) {
        Map<String, Object> response = categoryDetailService.getCategoryDetailById(categoryDetailDto.getId());
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/category/get")
    public String get(@RequestBody CategoryDto categoryDto) {
        Map<String, Object> response = categoryService.getCategoryById(categoryDto.getId());
        return JSONProcessor.convertToJSON(response).toString();
    }
}
