package id.her.trapedia.controller;

import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.ReqWishlistDto;
import id.her.trapedia.service.CategoryService;
import id.her.trapedia.service.WishlistService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class WishlistController {

    @Autowired
    private WishlistService wishlistService;

    @RequestMapping(value = "/wishlist/insert")
    public String insert(HttpServletRequest request, @RequestBody ReqWishlistDto reqWishlistDto) {
        Map<String, Object> response = wishlistService.insert(reqWishlistDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/wishlist/list")
    public String list(HttpServletRequest request) {
        Map<String, Object> response = wishlistService.getWishlistByMember(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/wishlist/delete")
    public String delete(HttpServletRequest request, @RequestBody ReqWishlistDto reqWishlistDto) {
        Map<String, Object> response = wishlistService.delete(reqWishlistDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

}
