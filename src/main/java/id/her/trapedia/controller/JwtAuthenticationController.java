package id.her.trapedia.controller;

import id.her.trapedia.dto.JwtRequestDto;
import id.her.trapedia.dto.JwtResponseDto;
import id.her.trapedia.service.JwtUserDetailsService;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ResponseGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    private Logger logger = LogManager.getLogger(JwtAuthenticationController.class);

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public Map<String, Object> createAuthenticationToken(@RequestBody JwtRequestDto jwtRequestDto) {
        Map<String, Object> response;
        try {
            logger.info("create token for username : " + jwtRequestDto.getUsername());
            authenticate(jwtRequestDto.getUsername(), jwtRequestDto.getPassword());

            JwtResponseDto dto = userDetailsService.generateResponse(jwtRequestDto.getUsername());

            logger.info("Token generated : " + dto.getToken());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dto, "");
        } catch (Exception e) {
            logger.error("Error while generated token : " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_401, e.getMessage(), "", "");
        }
        return response;
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("User disabled ", e);
        } catch (BadCredentialsException e) {
            throw new Exception("Invalid Credentials ", e);
        }
    }
}
