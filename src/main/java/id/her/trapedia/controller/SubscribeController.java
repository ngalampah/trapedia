package id.her.trapedia.controller;

import id.her.trapedia.dto.SubscribeDto;
import id.her.trapedia.dto.UserDto;
import id.her.trapedia.service.SubscribeService;
import id.her.trapedia.service.UserService;
import id.her.trapedia.service.VerificationTokenService;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class SubscribeController {

    @Autowired
    private SubscribeService subscribeService;

    @Autowired
    private VerificationTokenService verificationTokenService;

    @RequestMapping(value = "/public/subscribe", method = RequestMethod.POST)
    public String register(HttpServletRequest request, @RequestBody SubscribeDto subscribeDto) {
        Map<String, Object> response = subscribeService.add(subscribeDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

}
