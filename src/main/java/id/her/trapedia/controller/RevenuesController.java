package id.her.trapedia.controller;

import id.her.trapedia.dto.ReqTransactionDto;
import id.her.trapedia.service.RevenuesService;
import id.her.trapedia.service.TransactionService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class RevenuesController {

    @Autowired
    private RevenuesService revenuesService;


    @RequestMapping(value = "/revenues/list", method = RequestMethod.POST)
    public String revenuesList(HttpServletRequest request) {
        Map<String, Object> response = revenuesService.revenueByUser(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/revenues/total", method = RequestMethod.POST)
    public String revenuesTotal(HttpServletRequest request) {
        Map<String, Object> response = revenuesService.revenueByTotal(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }



}
