package id.her.trapedia.controller;

import id.her.trapedia.dto.BlogDto;
import id.her.trapedia.dto.PagesDto;
import id.her.trapedia.dto.PagingDto;
import id.her.trapedia.dto.SearchBlogDto;
import id.her.trapedia.service.BlogService;
import id.her.trapedia.service.PagesService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class BlogController {

    @Autowired
    private BlogService blogService;

    @RequestMapping(value = "/public/blog/list", method = RequestMethod.POST)
    public String getList(@RequestBody SearchBlogDto searchBlogDto) {
        Map<String, Object> response = blogService.getPublicBlogList(searchBlogDto);
        return JSONProcessor.convertToJSON(response).toString();
    }

    @RequestMapping(value = "/public/blog/blog-by-id", method = RequestMethod.POST)
    public String getPageById(@RequestBody BlogDto dto) {
        Map<String, Object> response = blogService.getPublicBlogById(dto.getId());
        return JSONProcessor.convertToJSON(response).toString();
    }
}
