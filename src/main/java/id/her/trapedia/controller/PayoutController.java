package id.her.trapedia.controller;

import id.her.trapedia.dto.PayoutDto;
import id.her.trapedia.service.PayoutService;
import id.her.trapedia.service.RevenuesService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class PayoutController {

    @Autowired
    private PayoutService payoutService;

    @RequestMapping(value = "/payout/request")
    public String insert(HttpServletRequest request, @RequestBody PayoutDto payoutDto) {
        Map<String, Object> response = payoutService.insert(payoutDto, request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }


    @RequestMapping(value = "/payout/list", method = RequestMethod.POST)
    public String payoutList(HttpServletRequest request) {
        Map<String, Object> response = payoutService.payoutByUser(request.getHeader("Authorization"));
        return JSONProcessor.convertToJSON(response).toString();
    }



}
