package id.her.trapedia.controller;

import id.her.trapedia.service.ProvincesService;
import id.her.trapedia.util.JSONProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class ProvincesController {

    @Autowired
    private ProvincesService provincesService;

    @RequestMapping(value = "/public/provinces/list")
    public String list() {
        Map<String, Object> response = provincesService.findAll();
        return JSONProcessor.convertToJSON(response).toString();
    }
}
