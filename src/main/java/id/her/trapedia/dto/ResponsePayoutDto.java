package id.her.trapedia.dto;

import java.util.List;

public class ResponsePayoutDto extends ResponsePagingDataTablesBaseDto {
    private List<PayoutDto> data;

    public List<PayoutDto> getData() {
        return data;
    }

    public void setData(List<PayoutDto> data) {
        this.data = data;
    }
}
