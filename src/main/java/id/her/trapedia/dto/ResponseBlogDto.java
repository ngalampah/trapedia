package id.her.trapedia.dto;

import java.util.List;

public class ResponseBlogDto extends ResponsePagingDataTablesBaseDto {
    private List<BlogDto> data;

    public List<BlogDto> getData() {
        return data;
    }

    public void setData(List<BlogDto> data) {
        this.data = data;
    }
}
