package id.her.trapedia.dto;

import java.util.List;

public class ResponseAdminDto extends ResponsePagingDataTablesBaseDto {
    private List<AdminDto> data;

    public List<AdminDto> getData() {
        return data;
    }

    public void setData(List<AdminDto> data) {
        this.data = data;
    }
}
