package id.her.trapedia.dto;

import javax.validation.constraints.*;

public class CategoryDto {
    private Long id;

    @NotEmpty
    @Size(min = 3, max = 50)
    private String title;

    @NotNull
    @NotEmpty
    private String description;

    private String images;

    @NotEmpty
    @Size(max = 1)
    private String publish;

    private String deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }
}
