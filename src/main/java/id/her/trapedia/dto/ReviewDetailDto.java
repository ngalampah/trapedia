package id.her.trapedia.dto;

public class ReviewDetailDto {

    private Long serviceId;
    private String serviceName;
    private Long serviceDetailId;
    private String seviceDetailName;
    private String image;
    private String imageUser;
    private String nameUser;
    private String date;
    private Long transactionDetailId;
    private Long userId;
    private Long transactionId;
    private Long price;
    private Long qty;
    private Integer ratingReview;
    private String review;
    private String hasReview;

    public String getImageUser() {
        return imageUser;
    }

    public void setImageUser(String imageUser) {
        this.imageUser = imageUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getHasReview() {
        return hasReview;
    }

    public void setHasReview(String hasReview) {
        this.hasReview = hasReview;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Long getServiceDetailId() {
        return serviceDetailId;
    }

    public void setServiceDetailId(Long serviceDetailId) {
        this.serviceDetailId = serviceDetailId;
    }

    public String getSeviceDetailName() {
        return seviceDetailName;
    }

    public void setSeviceDetailName(String seviceDetailName) {
        this.seviceDetailName = seviceDetailName;
    }

    public Long getTransactionDetailId() {
        return transactionDetailId;
    }

    public void setTransactionDetailId(Long transactionDetailId) {
        this.transactionDetailId = transactionDetailId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Integer getRatingReview() {
        return ratingReview;
    }

    public void setRatingReview(Integer ratingReview) {
        this.ratingReview = ratingReview;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
