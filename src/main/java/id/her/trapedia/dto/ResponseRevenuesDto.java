package id.her.trapedia.dto;

import java.util.List;

public class ResponseRevenuesDto extends ResponsePagingDataTablesBaseDto {
    private List<RevenueDto> data;

    public List<RevenueDto> getData() {
        return data;
    }

    public void setData(List<RevenueDto> data) {
        this.data = data;
    }
}
