package id.her.trapedia.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserMoneyDto {
   long revenueTotal;
   long revenueSuccess;
   long revenuePending;
   long revenueFailed;
   long withdraw;
   long balance;
   long balanceDebit;
   long balanceKredit;

    public long getBalanceDebit() {
        return balanceDebit;
    }

    public void setBalanceDebit(long balanceDebit) {
        this.balanceDebit = balanceDebit;
    }

    public long getBalanceKredit() {
        return balanceKredit;
    }

    public void setBalanceKredit(long balanceKredit) {
        this.balanceKredit = balanceKredit;
    }

    public long getRevenueTotal() {
        return revenueTotal;
    }

    public void setRevenueTotal(long revenueTotal) {
        this.revenueTotal = revenueTotal;
    }

    public long getRevenueSuccess() {
        return revenueSuccess;
    }

    public void setRevenueSuccess(long revenueSuccess) {
        this.revenueSuccess = revenueSuccess;
    }

    public long getRevenuePending() {
        return revenuePending;
    }

    public void setRevenuePending(long revenuePending) {
        this.revenuePending = revenuePending;
    }

    public long getRevenueFailed() {
        return revenueFailed;
    }

    public void setRevenueFailed(long revenueFailed) {
        this.revenueFailed = revenueFailed;
    }

    public long getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(long withdraw) {
        this.withdraw = withdraw;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}
