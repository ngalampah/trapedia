package id.her.trapedia.dto;

import java.util.List;

public class ResponseMessagesDto extends ResponsePagingDataTablesBaseDto {
    private List<MessagesDto> data;

    public List<MessagesDto> getData() {
        return data;
    }

    public void setData(List<MessagesDto> data) {
        this.data = data;
    }
}
