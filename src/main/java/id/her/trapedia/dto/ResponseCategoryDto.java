package id.her.trapedia.dto;

import java.util.List;

public class ResponseCategoryDto extends ResponsePagingDataTablesBaseDto {
    private List<CategoryDto> data;

    public List<CategoryDto> getData() {
        return data;
    }

    public void setData(List<CategoryDto> data) {
        this.data = data;
    }
}
