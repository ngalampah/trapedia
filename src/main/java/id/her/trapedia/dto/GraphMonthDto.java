package id.her.trapedia.dto;

public class GraphMonthDto {
    private String month;
    private String totalData;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getTotalData() {
        return totalData;
    }

    public void setTotalData(String totalData) {
        this.totalData = totalData;
    }
}
