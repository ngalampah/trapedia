package id.her.trapedia.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserDto {
    private Long id;

    @NotEmpty
    @Size(max = 100)
    private String name;

//    @NotEmpty
//    @Email
    private String email;

    private String about;

//    @NotEmpty
    @Size(max = 8)
    private String password;

    private String image;
    private String imageDel;

    @Size(max = 16)
    private String phone;

    @Size(max = 10)
    private String gender;

    @Size(max = 255)
    private String address;

    private String dob;

    private String activation;

    @Size(max = 100)
    private String bankName;

    @Size(max = 20)
    private String bankNumber;

    @Size(max = 100)
    private String bankAccountName;

    private String deleted;
    private String isPartnerUser;
    private String isVerifiedUser;

    private String username;

    public String getIsPartnerUser() {
        return isPartnerUser;
    }

    public void setIsPartnerUser(String isPartnerUser) {
        this.isPartnerUser = isPartnerUser;
    }

    public String getIsVerifiedUser() {
        return isVerifiedUser;
    }

    public void setIsVerifiedUser(String isVerifiedUser) {
        this.isVerifiedUser = isVerifiedUser;
    }

    public String getImageDel() {
        return imageDel;
    }

    public void setImageDel(String imageDel) {
        this.imageDel = imageDel;
    }

    public Long getId() {
        return id;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
