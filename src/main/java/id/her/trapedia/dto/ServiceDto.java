package id.her.trapedia.dto;

import id.her.trapedia.model.CategoryModel;
import id.her.trapedia.model.RegenciesModel;

import java.util.List;

public class ServiceDto {

    private Long id;
    private String serviceName;
    private String categoryName;
    private CategoryDto categoryDto;
    private RegenciesDto regenciesDto;
    private String description;
    private String redemptionInstructions;
    private String cancellationPolicy;
    private String termsConditions;
    private String publish;
    private Long countRating;
    private Long countReview;
    private Long price;
    private Long startingPrice;
    private String images1;
    private String images2;
    private String images3;
    private String images4;
    private String images5;
    private String deleted;
    private String approve;
    private String userId;
    private String userName;
    private String date;
    private List<ServiceDetailDto> serviceDetailDtos;
    private UserDto userDto;
    private String imageDel;
    private Long indexImageDel;

    public Long getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(Long startingPrice) {
        this.startingPrice = startingPrice;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public CategoryDto getCategoryDto() {
        return categoryDto;
    }

    public void setCategoryDto(CategoryDto categoryDto) {
        this.categoryDto = categoryDto;
    }

    public RegenciesDto getRegenciesDto() {
        return regenciesDto;
    }

    public void setRegenciesDto(RegenciesDto regenciesDto) {
        this.regenciesDto = regenciesDto;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRedemptionInstructions() {
        return redemptionInstructions;
    }

    public void setRedemptionInstructions(String redemptionInstructions) {
        this.redemptionInstructions = redemptionInstructions;
    }

    public String getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public String getTermsConditions() {
        return termsConditions;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getImages1() {
        return images1;
    }

    public void setImages1(String images1) {
        this.images1 = images1;
    }

    public String getImages2() {
        return images2;
    }

    public void setImages2(String images2) {
        this.images2 = images2;
    }

    public String getImages3() {
        return images3;
    }

    public void setImages3(String images3) {
        this.images3 = images3;
    }

    public String getImages4() {
        return images4;
    }

    public void setImages4(String images4) {
        this.images4 = images4;
    }

    public String getImages5() {
        return images5;
    }

    public void setImages5(String images5) {
        this.images5 = images5;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public Long getCountRating() {
        return countRating;
    }

    public void setCountRating(Long countRating) {
        this.countRating = countRating;
    }

    public Long getCountReview() {
        return countReview;
    }

    public void setCountReview(Long countReview) {
        this.countReview = countReview;
    }

    public List<ServiceDetailDto> getServiceDetailDtos() {
        return serviceDetailDtos;
    }

    public void setServiceDetailDtos(List<ServiceDetailDto> serviceDetailDtos) {
        this.serviceDetailDtos = serviceDetailDtos;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImageDel() {
        return imageDel;
    }

    public void setImageDel(String imageDel) {
        this.imageDel = imageDel;
    }

    public Long getIndexImageDel() {
        return indexImageDel;
    }

    public void setIndexImageDel(Long indexImageDel) {
        this.indexImageDel = indexImageDel;
    }
}
