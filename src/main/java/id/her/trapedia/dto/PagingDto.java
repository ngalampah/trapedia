package id.her.trapedia.dto;

import java.util.List;

public class PagingDto {
    private Integer page;
    private String regencyId;
    private String keyword;
    private String CategoryId;
    private List<Integer> listCategoryId;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getRegencyId() {
        return regencyId;
    }

    public void setRegencyId(String regencyId) {
        this.regencyId = regencyId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public List<Integer> getListCategoryId() {
        return listCategoryId;
    }

    public void setListCategoryId(List<Integer> listCategoryId) {
        this.listCategoryId = listCategoryId;
    }
}
