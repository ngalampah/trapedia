package id.her.trapedia.dto;

import java.util.List;

public class ResponseVoucherDto extends ResponsePagingDataTablesBaseDto {
    private List<VoucherDto> data;

    public List<VoucherDto> getData() {
        return data;
    }

    public void setData(List<VoucherDto> data) {
        this.data = data;
    }
}
