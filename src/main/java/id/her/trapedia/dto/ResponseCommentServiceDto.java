package id.her.trapedia.dto;

import java.util.List;

public class ResponseCommentServiceDto extends ResponsePagingDataTablesBaseDto {
    private List<CommentServiceDto> data;

    public List<CommentServiceDto> getData() {
        return data;
    }

    public void setData(List<CommentServiceDto> data) {
        this.data = data;
    }
}
