package id.her.trapedia.dto;

import java.util.List;

public class ReqTransactionDto {

    private Long id;
    private String voucherCode;
    private String numberTransaction;
    private String travelingName;
    private String travelingEmail;
    private String travelingPhone;
    private Long userId;
    private Long serviceUserId;
    private Long total;
    private Long feeAdmin;
    private Long summaryService;
    private Long discount;
    private String statusPayment;
    private String statusTransaction;
    private String paymentMethode;
    private String paymentChannel;
    private String paymentCode;
    private String date;
    private String notesTransaction;
    private Long paymentAmount;
    private List<ReqTransactionDetailDto> formDetailCart;

    public Long getServiceUserId() {
        return serviceUserId;
    }

    public void setServiceUserId(Long serviceUserId) {
        this.serviceUserId = serviceUserId;
    }

    public String getNotesTransaction() {
        return notesTransaction;
    }

    public void setNotesTransaction(String notesTransaction) {
        this.notesTransaction = notesTransaction;
    }

    public String getStatusTransaction() {
        return statusTransaction;
    }

    public void setStatusTransaction(String statusTransaction) {
        this.statusTransaction = statusTransaction;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public Long getSummaryService() {
        return summaryService;
    }

    public void setSummaryService(Long summaryService) {
        this.summaryService = summaryService;
    }

    public Long getFeeAdmin() {
        return feeAdmin;
    }

    public void setFeeAdmin(Long feeAdmin) {
        this.feeAdmin = feeAdmin;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public String getNumberTransaction() {
        return numberTransaction;
    }

    public void setNumberTransaction(String numberTransaction) {
        this.numberTransaction = numberTransaction;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTravelingName() {
        return travelingName;
    }

    public void setTravelingName(String travelingName) {
        this.travelingName = travelingName;
    }

    public String getTravelingEmail() {
        return travelingEmail;
    }

    public void setTravelingEmail(String travelingEmail) {
        this.travelingEmail = travelingEmail;
    }

    public String getTravelingPhone() {
        return travelingPhone;
    }

    public void setTravelingPhone(String travelingPhone) {
        this.travelingPhone = travelingPhone;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<ReqTransactionDetailDto> getFormDetailCart() {
        return formDetailCart;
    }

    public void setFormDetailCart(List<ReqTransactionDetailDto> formDetailCart) {
        this.formDetailCart = formDetailCart;
    }

    public String getStatusPayment() {
        return statusPayment;
    }

    public void setStatusPayment(String statusPayment) {
        this.statusPayment = statusPayment;
    }

    public String getPaymentMethode() {
        return paymentMethode;
    }

    public void setPaymentMethode(String paymentMethode) {
        this.paymentMethode = paymentMethode;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public Long getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Long paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
