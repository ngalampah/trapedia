package id.her.trapedia.dto;

import java.util.List;

public class ResponseProvincesDto extends ResponsePagingDataTablesBaseDto {
    private List<ProvincesDto> data;

    public List<ProvincesDto> getData() {
        return data;
    }

    public void setData(List<ProvincesDto> data) {
        this.data = data;
    }
}
