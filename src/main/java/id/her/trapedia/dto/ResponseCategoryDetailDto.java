package id.her.trapedia.dto;

import java.util.List;

public class ResponseCategoryDetailDto extends ResponsePagingDataTablesBaseDto {
    private List<CategoryDetailDto> data;

    public List<CategoryDetailDto> getData() {
        return data;
    }

    public void setData(List<CategoryDetailDto> data) {
        this.data = data;
    }
}
