package id.her.trapedia.dto;

import java.util.List;

public class ResponseTransactionDto extends ResponsePagingDataTablesBaseDto {
    private List<TransactionDto> data;

    public List<TransactionDto> getData() {
        return data;
    }

    public void setData(List<TransactionDto> data) {
        this.data = data;
    }
}
