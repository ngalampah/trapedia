package id.her.trapedia.dto;

import java.util.List;

public class ResponseRegenciesDto extends ResponsePagingDataTablesBaseDto {
    private List<RegenciesDto> data;

    public List<RegenciesDto> getData() {
        return data;
    }

    public void setData(List<RegenciesDto> data) {
        this.data = data;
    }
}
