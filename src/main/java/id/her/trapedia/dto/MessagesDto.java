package id.her.trapedia.dto;

public class MessagesDto {
    private Long id;
    private String subject;
    private String message;
    private String date;
    private Long userId;
    private String userName;
    private String isReadUserId;
    private String imageUser;

    private Long toUserId;
    private String toUserName;
    private String isReadToUserId;
    private String imageToUser;
    private String fromUser;
    private String toUser;


    private Long serviceId;
    private Long repplyId;

    private String publish;
    private String deleted;

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageUser() {
        return imageUser;
    }

    public void setImageUser(String imageUser) {
        this.imageUser = imageUser;
    }

    public String getImageToUser() {
        return imageToUser;
    }

    public void setImageToUser(String imageToUser) {
        this.imageToUser = imageToUser;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIsReadUserId() {
        return isReadUserId;
    }

    public void setIsReadUserId(String isReadUserId) {
        this.isReadUserId = isReadUserId;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getIsReadToUserId() {
        return isReadToUserId;
    }

    public void setIsReadToUserId(String isReadToUserId) {
        this.isReadToUserId = isReadToUserId;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public Long getRepplyId() {
        return repplyId;
    }

    public void setRepplyId(Long repplyId) {
        this.repplyId = repplyId;
    }
}
