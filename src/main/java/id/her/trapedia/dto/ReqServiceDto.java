package id.her.trapedia.dto;

import java.util.List;

public class ReqServiceDto {

    private Long id;
    private String serviceName;
    private Long categoryId;
    private Long categoryDetailId;
    private Long regenciesId;
    private String category;
    private String categoryDetail;
    private String regencies;
    private String description;
    private String redemptionInstructions;
    private String cancellationPolicy;
    private String termsConditions;
    private String facilities;
    private String locationIframe;
    private Long countRating;
    private Long countReview;
    private String closeDays;
    private String closeDates;
    private String images1;
    private String images2;
    private String images3;
    private String images4;
    private String images5;
    private String publish;
    private List<ReqServiceDetailDto> formDetailService;
    private Long userId;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryDetail() {
        return categoryDetail;
    }

    public void setCategoryDetail(String categoryDetail) {
        this.categoryDetail = categoryDetail;
    }

    public String getRegencies() {
        return regencies;
    }

    public void setRegencies(String regencies) {
        this.regencies = regencies;
    }

    public String getCloseDays() {
        return closeDays;
    }

    public void setCloseDays(String closeDays) {
        this.closeDays = closeDays;
    }

    public String getCloseDates() {
        return closeDates;
    }

    public void setCloseDates(String closeDates) {
        this.closeDates = closeDates;
    }

    public String getLocationIframe() {
        return locationIframe;
    }

    public void setLocationIframe(String locationIframe) {
        this.locationIframe = locationIframe;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public Long getCategoryDetailId() {
        return categoryDetailId;
    }

    public void setCategoryDetailId(Long categoryDetailId) {
        this.categoryDetailId = categoryDetailId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getRegenciesId() {
        return regenciesId;
    }

    public void setRegenciesId(Long regenciesId) {
        this.regenciesId = regenciesId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRedemptionInstructions() {
        return redemptionInstructions;
    }

    public void setRedemptionInstructions(String redemptionInstructions) {
        this.redemptionInstructions = redemptionInstructions;
    }

    public String getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public String getTermsConditions() {
        return termsConditions;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    public List<ReqServiceDetailDto> getFormDetailService() {
        return formDetailService;
    }

    public void setFormDetailService(List<ReqServiceDetailDto> formDetailService) {
        this.formDetailService = formDetailService;
    }

    public Long getCountRating() {
        return countRating;
    }

    public void setCountRating(Long countRating) {
        this.countRating = countRating;
    }

    public Long getCountReview() {
        return countReview;
    }

    public void setCountReview(Long countReview) {
        this.countReview = countReview;
    }

    public String getImages1() {
        return images1;
    }

    public void setImages1(String images1) {
        this.images1 = images1;
    }

    public String getImages2() {
        return images2;
    }

    public void setImages2(String images2) {
        this.images2 = images2;
    }

    public String getImages3() {
        return images3;
    }

    public void setImages3(String images3) {
        this.images3 = images3;
    }

    public String getImages4() {
        return images4;
    }

    public void setImages4(String images4) {
        this.images4 = images4;
    }

    public String getImages5() {
        return images5;
    }

    public void setImages5(String images5) {
        this.images5 = images5;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
