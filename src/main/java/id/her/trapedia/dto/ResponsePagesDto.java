package id.her.trapedia.dto;

import java.util.List;

public class ResponsePagesDto extends ResponsePagingDataTablesBaseDto {
    private List<PagesDto> data;

    public List<PagesDto> getData() {
        return data;
    }

    public void setData(List<PagesDto> data) {
        this.data = data;
    }
}
