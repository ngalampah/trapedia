package id.her.trapedia.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class TransactionDto {
    private Long id;
    private String invoiceNumber;
    private String travelingName;
    private String travelingEmail;
    private String travelingPhone;
    private Long userId;
    private Long fee;
    private String voucherCode;
    private Long discountVoucherCode;
    private Long summaryService;
    private Long total;
    private Long paymentAmount;
    private String statusPayment;
    private String paymentMethode;
    private String paymentChannel;
    private String paymentCode;
    private String date;
    private String dateFrom;
    private String dateTo;
    private List<ReqTransactionDetailDto> formDetailCart;

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public List<ReqTransactionDetailDto> getFormDetailCart() {
        return formDetailCart;
    }

    public void setFormDetailCart(List<ReqTransactionDetailDto> formDetailCart) {
        this.formDetailCart = formDetailCart;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public Long getDiscountVoucherCode() {
        return discountVoucherCode;
    }

    public void setDiscountVoucherCode(Long discountVoucherCode) {
        this.discountVoucherCode = discountVoucherCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTravelingName() {
        return travelingName;
    }

    public void setTravelingName(String travelingName) {
        this.travelingName = travelingName;
    }

    public String getTravelingEmail() {
        return travelingEmail;
    }

    public void setTravelingEmail(String travelingEmail) {
        this.travelingEmail = travelingEmail;
    }

    public String getTravelingPhone() {
        return travelingPhone;
    }

    public void setTravelingPhone(String travelingPhone) {
        this.travelingPhone = travelingPhone;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFee() {
        return fee;
    }

    public void setFee(Long fee) {
        this.fee = fee;
    }

    public Long getSummaryService() {
        return summaryService;
    }

    public void setSummaryService(Long summaryService) {
        this.summaryService = summaryService;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Long paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getStatusPayment() {
        return statusPayment;
    }

    public void setStatusPayment(String statusPayment) {
        this.statusPayment = statusPayment;
    }

    public String getPaymentMethode() {
        return paymentMethode;
    }

    public void setPaymentMethode(String paymentMethode) {
        this.paymentMethode = paymentMethode;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
