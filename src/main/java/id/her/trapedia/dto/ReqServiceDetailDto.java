package id.her.trapedia.dto;

public class ReqServiceDetailDto {

    private Long id;
    private String name;
    private Long price;
    private String description;
    private String publish;
    private Boolean stockEmpty;

    public Boolean getStockEmpty() {
        return stockEmpty;
    }

    public void setStockEmpty(Boolean stockEmpty) {
        this.stockEmpty = stockEmpty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }
}
