package id.her.trapedia.dto;

import java.util.List;

public class ResponseUserDto extends ResponsePagingDataTablesBaseDto {
    private List<UserDto> data;

    public List<UserDto> getData() {
        return data;
    }

    public void setData(List<UserDto> data) {
        this.data = data;
    }
}
