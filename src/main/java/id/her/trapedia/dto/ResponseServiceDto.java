package id.her.trapedia.dto;

import java.util.List;

public class ResponseServiceDto extends ResponsePagingDataTablesBaseDto {
    private List<ServiceDto> data;

    public List<ServiceDto> getData() {
        return data;
    }

    public void setData(List<ServiceDto> data) {
        this.data = data;
    }
}
