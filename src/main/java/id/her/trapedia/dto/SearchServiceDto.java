package id.her.trapedia.dto;

import java.util.List;

public class SearchServiceDto {
    private Integer page;
    private Integer perPage;
    private String keyword;
    private List<Integer> listCategoryId;
    private List<Integer> listCategoryDetailId;
    private List<Integer> listRegencyId;
    private List<Integer> listRating;

    public List<Integer> getListCategoryDetailId() {
        return listCategoryDetailId;
    }

    public void setListCategoryDetailId(List<Integer> listCategoryDetailId) {
        this.listCategoryDetailId = listCategoryDetailId;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<Integer> getListCategoryId() {
        return listCategoryId;
    }

    public void setListCategoryId(List<Integer> listCategoryId) {
        this.listCategoryId = listCategoryId;
    }

    public List<Integer> getListRegencyId() {
        return listRegencyId;
    }

    public void setListRegencyId(List<Integer> listRegencyId) {
        this.listRegencyId = listRegencyId;
    }

    public List<Integer> getListRating() {
        return listRating;
    }

    public void setListRating(List<Integer> listRating) {
        this.listRating = listRating;
    }
}
