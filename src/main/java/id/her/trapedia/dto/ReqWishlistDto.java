package id.her.trapedia.dto;

import java.util.List;

public class ReqWishlistDto {

    private Long id;
    private Long serviceId;
    private String serviceName;
    private Long serviceDetailId;
    private String serviceDetailName;
    private Long price;
    private String image;

    private String userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getServiceDetailId() {
        return serviceDetailId;
    }

    public void setServiceDetailId(Long serviceDetailId) {
        this.serviceDetailId = serviceDetailId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDetailName() {
        return serviceDetailName;
    }

    public void setServiceDetailName(String serviceDetailName) {
        this.serviceDetailName = serviceDetailName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
