package id.her.trapedia.dto;

import java.util.List;

public class ResponseSubscribeDto extends ResponsePagingDataTablesBaseDto {
    private List<SubscribeDto> data;

    public List<SubscribeDto> getData() {
        return data;
    }

    public void setData(List<SubscribeDto> data) {
        this.data = data;
    }
}
