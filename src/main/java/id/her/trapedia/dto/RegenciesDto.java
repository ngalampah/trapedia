package id.her.trapedia.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegenciesDto {
    private Long id;

    private ProvincesDto provincesDto;

    private String provincesName;

    @NotNull
    private Long provincesId;

    @NotEmpty
    @Size(max = 100)
    private String name;
    private String images;

    @NotEmpty
    @Size(max = 1)
    private String popular;

    @NotEmpty
    @Size(max = 1)
    private String publish;

    private String deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProvincesDto getProvincesDto() {
        return provincesDto;
    }

    public void setProvincesDto(ProvincesDto provincesDto) {
        this.provincesDto = provincesDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getPopular() {
        return popular;
    }

    public void setPopular(String popular) {
        this.popular = popular;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getProvincesName() {
        return provincesName;
    }

    public void setProvincesName(String provincesName) {
        this.provincesName = provincesName;
    }

    public Long getProvincesId() {
        return provincesId;
    }

    public void setProvincesId(Long provincesId) {
        this.provincesId = provincesId;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }
}
