package id.her.trapedia.dto;

import java.util.List;

public class ReqTransactionDetailDto {

    private String invoiceNumber;
    private Long transactionDetailId;
    private String serviceName;
    private String serviceNameDetail;
    private String hasReview;
    private Long price;
    private Long qty;
    private Long total;
    private Long detailServiceId;
    private Long serviceId;
    private Long transactionId;
    private Long id;
    private String date;
    private String dateVisit;
    private String statusPayment;
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getTransactionDetailId() {
        return transactionDetailId;
    }

    public void setTransactionDetailId(Long transactionDetailId) {
        this.transactionDetailId = transactionDetailId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getDateVisit() {
        return dateVisit;
    }

    public void setDateVisit(String dateVisit) {
        this.dateVisit = dateVisit;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getDetailServiceId() {
        return detailServiceId;
    }

    public void setDetailServiceId(Long detailServiceId) {
        this.detailServiceId = detailServiceId;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getServiceNameDetail() {
        return serviceNameDetail;
    }

    public void setServiceNameDetail(String serviceNameDetail) {
        this.serviceNameDetail = serviceNameDetail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHasReview() {
        return hasReview;
    }

    public void setHasReview(String hasReview) {
        this.hasReview = hasReview;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatusPayment() {
        return statusPayment;
    }

    public void setStatusPayment(String statusPayment) {
        this.statusPayment = statusPayment;
    }


}
