package id.her.trapedia.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "trp_blog")
public class BlogModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title", length = 255)
    private String title;

    @Column(name = "content", nullable = false, columnDefinition = "text")
    private String content;

    @Column(name = "images", length = 255)
    private String images;

    @Column(name = "type", length = 50)
    private String type;

    @Column(name = "publish", columnDefinition = "varchar(1) default '0'")
    private String publish;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", length = 0)
    private Date date;

    @Column(name = "deleted", columnDefinition = "varchar(1) default '0'")
    private String deleted;

    @Column(name = "count_view")
    private Long countView;

    public Long getCountView() {
        return countView;
    }

    public void setCountView(Long countView) {
        this.countView = countView;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
