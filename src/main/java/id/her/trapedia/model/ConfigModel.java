package id.her.trapedia.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "trp_config")
public class ConfigModel {
    @Id
    @Column(name = "config_name", length = 50, unique = true)
    private String configName;

    @Column(name = "config_value", length = 255)
    private String configValue;

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }
}
