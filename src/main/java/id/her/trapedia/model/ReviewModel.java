package id.her.trapedia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "trp_review")
public class ReviewModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private UserModel userModel;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "service_detail_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ServiceDetailModel serviceDetailModel;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "transaction_detail_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private TransactionDetailModel transactionDetailModel;

    @Column(name = "rating_review", nullable = false)
    private Integer ratingReview;

    @Column(name = "review")
    private String review;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public ServiceDetailModel getServiceDetailModel() {
        return serviceDetailModel;
    }

    public void setServiceDetailModel(ServiceDetailModel serviceDetailModel) {
        this.serviceDetailModel = serviceDetailModel;
    }

    public TransactionDetailModel getTransactionDetailModel() {
        return transactionDetailModel;
    }

    public void setTransactionDetailModel(TransactionDetailModel transactionDetailModel) {
        this.transactionDetailModel = transactionDetailModel;
    }

    public Integer getRatingReview() {
        return ratingReview;
    }

    public void setRatingReview(Integer ratingReview) {
        this.ratingReview = ratingReview;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
