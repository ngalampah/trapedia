package id.her.trapedia.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "trp_voucher")
public class VoucherModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "voucher_code", length = 50, unique = true, nullable = false)
    private String voucherCode;

    //percentage or fixed
    @Column(name = "amount_type", length = 50, nullable = false)
    private String amountType;

    //percentage 10% or 10.000
    @Column(name = "amount")
    private Long amount;

    @Column(name = "max_amount")
    private Long maxAmount;

    @Column(name = "min_transaction")
    private Long minTransaction;

    @Column(name = "valid_date_from", length = 0)
    private Date validDateFrom;

    @Column(name = "valid_date_to", length = 0)
    private Date validDateTo;

    @Column(name = "deleted", columnDefinition = "varchar(1) default '0'")
    private String deleted;

    @Column(name = "publish", columnDefinition = "varchar(1) default '1'")
    private String publish;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Long maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Long getMinTransaction() {
        return minTransaction;
    }

    public void setMinTransaction(Long minTransaction) {
        this.minTransaction = minTransaction;
    }

    public Date getValidDateFrom() {
        return validDateFrom;
    }

    public void setValidDateFrom(Date validDateFrom) {
        this.validDateFrom = validDateFrom;
    }

    public Date getValidDateTo() {
        return validDateTo;
    }

    public void setValidDateTo(Date validDateTo) {
        this.validDateTo = validDateTo;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }
}
