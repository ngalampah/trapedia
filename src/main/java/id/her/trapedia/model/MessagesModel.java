package id.her.trapedia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "trp_messages")
public class MessagesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "subject", length = 255)
    private String subject;

    @Column(name = "message", nullable = false, columnDefinition = "text")
    private String message;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private UserModel userModel;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "to_user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private UserModel toUserModel;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
//    @JoinColumn(name = "service_id", nullable = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JsonIgnore
//    private ServiceModel serviceModel;

    @Column(name = "is_read_user_id", columnDefinition = "varchar(1) default '0'")
    private String isReadUserId;

    @Column(name = "is_read_to_user_id", columnDefinition = "varchar(1) default '0'")
    private String isReadToUserId;

    @Column(name = "replay_id")
    private Long replayId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", length = 0)
    private Date date;


    @Column(name = "publish", columnDefinition = "varchar(1) default '1'")
    private String publish;

    @Column(name = "deleted", columnDefinition = "varchar(1) default '0'")
    private String deleted;

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public UserModel getToUserModel() {
        return toUserModel;
    }

    public void setToUserModel(UserModel toUserModel) {
        this.toUserModel = toUserModel;
    }

    public Long getReplayId() {
        return replayId;
    }

    public void setReplayId(Long replayId) {
        this.replayId = replayId;
    }

    public String getIsReadUserId() {
        return isReadUserId;
    }

    public void setIsReadUserId(String isReadUserId) {
        this.isReadUserId = isReadUserId;
    }

    public String getIsReadToUserId() {
        return isReadToUserId;
    }

    public void setIsReadToUserId(String isReadToUserId) {
        this.isReadToUserId = isReadToUserId;
    }
}
