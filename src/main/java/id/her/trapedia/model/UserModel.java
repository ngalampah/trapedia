package id.her.trapedia.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "trp_user")
public class UserModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "email", length = 50, unique = true, nullable = false)
    private String email;

    @Column(name = "password", length = 100, nullable = false)
    private String password;

    @Column(name = "image", length = 100)
    private String image;

    @Column(name = "phone", length = 16)
    private String phone;

    @Column(name = "gender", length = 10)
    private String gender;

    @Column(name = "address", length = 255)
    private String address;

    @Column(name = "about", length = 255)
    private String about;

    @Temporal(TemporalType.DATE)
    @Column(name = "dob", length = 0)
    private Date dob;

    @Column(name = "activation", columnDefinition = "varchar(1) default '0'")
    private String activation;

    @Column(name = "is_partner_user", columnDefinition = "varchar(1) default '1'")
    private String isPartnerUser;

    @Column(name = "is_verified_user", columnDefinition = "varchar(1) default '0'")
    private String isVerifiedUser;

    @Column(name = "bank_name", length = 100)
    private String bankName;

    @Column(name = "bank_number", length = 20)
    private String bankNumber;

    @Column(name = "bank_account_name", length = 100)
    private String bankAccountName;

    @Column(name = "deleted", columnDefinition = "varchar(1) default '0'")
    private String deleted;

    public String getIsPartnerUser() {
        return isPartnerUser;
    }

    public void setIsPartnerUser(String isPartnerUser) {
        this.isPartnerUser = isPartnerUser;
    }

    public String getIsVerifiedUser() {
        return isVerifiedUser;
    }

    public void setIsVerifiedUser(String isVerifiedUser) {
        this.isVerifiedUser = isVerifiedUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
