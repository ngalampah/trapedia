package id.her.trapedia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "trp_m_regencies")
public class RegenciesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "province_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ProvincesModel provincesModel;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "images", length = 255)
    private String images;

    @Column(name = "popular", columnDefinition = "varchar(1) default '0'")
    private String popular;

    @Column(name = "publish", columnDefinition = "varchar(1) default '1'")
    private String publish;

    @Column(name = "deleted", columnDefinition = "varchar(1) default '0'")
    private String deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProvincesModel getProvincesModel() {
        return provincesModel;
    }

    public void setProvincesModel(ProvincesModel provincesModel) {
        this.provincesModel = provincesModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getPopular() {
        return popular;
    }

    public void setPopular(String popular) {
        this.popular = popular;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }
}
