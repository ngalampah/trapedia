package id.her.trapedia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "trp_service")
public class ServiceModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "service_name")
    private String serviceName;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "category_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private CategoryModel categoryModel;

    @Column(name = "category_detail_id", nullable = true)
    private Long categoryDetailId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private UserModel userModel;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "regencies_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private RegenciesModel regenciesModel;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "redemption_instructions", columnDefinition = "text")
    private String redemptionInstructions;

    @Column(name = "cancellation_policy", columnDefinition = "text")
    private String cancellationPolicy;

    @Column(name = "terms_conditions", columnDefinition = "text")
    private String termsConditions;

    @Column(name = "facilities", columnDefinition = "text")
    private String facilities;

    @Column(name = "location_iframe", columnDefinition = "text")
    private String locationIframe;

    @Column(name = "count_rating")
    private Long countRating;

    @Column(name = "count_review")
    private Long countReview;

    @Column(name = "count_view")
    private Long countView;

    @Column(name = "close_days", columnDefinition = "text")
    private String closeDays;

    @Column(name = "close_dates", columnDefinition = "text")
    private String closeDates;

    @Column(name = "images_1")
    private String images1;

    @Column(name = "images_2")
    private String images2;

    @Column(name = "images_3")
    private String images3;

    @Column(name = "images_4")
    private String images4;

    @Column(name = "images_5")
    private String images5;

    @Column(name = "approve", columnDefinition = "varchar(1) default '0'")
    private String approve;

    @Column(name = "popular", columnDefinition = "varchar(1) default '0'")
    private String popular;

    @Column(name = "publish", columnDefinition = "varchar(1) default '1'")
    private String publish;

    @Column(name = "deleted", columnDefinition = "varchar(1) default '0'")
    private String deleted;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", length = 0)
    private Date date;

    public String getCloseDays() {
        return closeDays;
    }

    public void setCloseDays(String closeDays) {
        this.closeDays = closeDays;
    }

    public String getCloseDates() {
        return closeDates;
    }

    public void setCloseDates(String closeDates) {
        this.closeDates = closeDates;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public String getLocationIframe() {
        return locationIframe;
    }

    public void setLocationIframe(String locationIframe) {
        this.locationIframe = locationIframe;
    }

    public Long getCountView() {
        return countView;
    }

    public void setCountView(Long countView) {
        this.countView = countView;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public RegenciesModel getRegenciesModel() {
        return regenciesModel;
    }

    public void setRegenciesModel(RegenciesModel regenciesModel) {
        this.regenciesModel = regenciesModel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRedemptionInstructions() {
        return redemptionInstructions;
    }

    public void setRedemptionInstructions(String redemptionInstructions) {
        this.redemptionInstructions = redemptionInstructions;
    }

    public String getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public String getTermsConditions() {
        return termsConditions;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    public Long getCountRating() {
        return countRating;
    }

    public void setCountRating(Long countRating) {
        this.countRating = countRating;
    }

    public Long getCountReview() {
        return countReview;
    }

    public void setCountReview(Long countReview) {
        this.countReview = countReview;
    }

    public String getImages1() {
        return images1;
    }

    public void setImages1(String images1) {
        this.images1 = images1;
    }

    public String getImages2() {
        return images2;
    }

    public void setImages2(String images2) {
        this.images2 = images2;
    }

    public String getImages3() {
        return images3;
    }

    public void setImages3(String images3) {
        this.images3 = images3;
    }

    public String getImages4() {
        return images4;
    }

    public void setImages4(String images4) {
        this.images4 = images4;
    }

    public String getImages5() {
        return images5;
    }

    public void setImages5(String images5) {
        this.images5 = images5;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String getPopular() {
        return popular;
    }

    public void setPopular(String popular) {
        this.popular = popular;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getCategoryDetailId() {
        return categoryDetailId;
    }

    public void setCategoryDetailId(Long categoryDetailId) {
        this.categoryDetailId = categoryDetailId;
    }
}
