package id.her.trapedia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "trp_transaction")
public class TransactionModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private UserModel userModel;

    @Column(name = "traveling_name")
    private String travelingName;

    @Column(name = "traveling_email")
    private String travelingEmail;

    @Column(name = "traveling_phone")
    private String travelingPhone;

    @Column(name = "voucher_code")
    private String voucherCode;

    @Column(name = "discount_voucher_code")
    private Long discount;

    @Column(name = "fee_admin")
    private Long feeAdmin;

    @Column(name = "summary_service")
    private Long summaryService;

    @Column(name = "total")
    private Long total;

    @Column(name = "payment_method", columnDefinition = "varchar(150) default ''")
    private String paymentMethod;

    @Column(name = "payment_channel", columnDefinition = "varchar(50) default ''")
    private String paymentChannel;

    @Column(name = "payment_code", columnDefinition = "varchar(50) default ''")
    private String paymentCode;

    @Column(name = "payment_amount")
    private Long paymentAmount;

    @Column(name = "payment_balance")
    private Long paymentBalance;

    //0 PROSES
    //1 SUCCESS
    //2 CANCEL
    //3 REFUND
    //4 REJECT
    @Column(name = "status_payment", columnDefinition = "varchar(1) default '0'")
    private String statusPayment;

    //0 PROSES
    //1 SUCCESS
    //2 CANCEL
    //3 REFUND
    //4 REJECT
    @Column(name = "status_transaction", columnDefinition = "varchar(1) default '0'")
    private String statusTransaction;

    @Column(name = "log_payment_gw", columnDefinition = "text")
    private String logPaymentGw;

    @Column(name = "deleted", columnDefinition = "varchar(1) default '0'")
    private String deleted;

    @Column(name = "invoice_number", columnDefinition = "varchar(100)")
    private String invoiceNumber;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", length = 0)
    private Date date;

    @Column(name = "notes_transaction", columnDefinition = "varchar(250) default ''")
    private String notesTransaction;

    public String getLogPaymentGw() {
        return logPaymentGw;
    }

    public void setLogPaymentGw(String logPaymentGw) {
        this.logPaymentGw = logPaymentGw;
    }

    public String getStatusTransaction() {
        return statusTransaction;
    }

    public void setStatusTransaction(String statusTransaction) {
        this.statusTransaction = statusTransaction;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public String getTravelingName() {
        return travelingName;
    }

    public void setTravelingName(String travelingName) {
        this.travelingName = travelingName;
    }

    public String getTravelingEmail() {
        return travelingEmail;
    }

    public void setTravelingEmail(String travelingEmail) {
        this.travelingEmail = travelingEmail;
    }

    public String getTravelingPhone() {
        return travelingPhone;
    }

    public void setTravelingPhone(String travelingPhone) {
        this.travelingPhone = travelingPhone;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public Long getFeeAdmin() {
        return feeAdmin;
    }

    public void setFeeAdmin(Long feeAdmin) {
        this.feeAdmin = feeAdmin;
    }

    public Long getSummaryService() {
        return summaryService;
    }

    public void setSummaryService(Long summaryService) {
        this.summaryService = summaryService;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getStatusPayment() {
        return statusPayment;
    }

    public void setStatusPayment(String statusPayment) {
        this.statusPayment = statusPayment;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public Long getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Long paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Long getPaymentBalance() {
        return paymentBalance;
    }

    public void setPaymentBalance(Long paymentBalance) {
        this.paymentBalance = paymentBalance;
    }

    public String getNotesTransaction() {
        return notesTransaction;
    }

    public void setNotesTransaction(String notesTransaction) {
        this.notesTransaction = notesTransaction;
    }
}
