package id.her.trapedia.dao;

import id.her.trapedia.dto.CategoryDetailDto;
import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.CategoryDetailModel;
import id.her.trapedia.model.CategoryModel;

import java.util.List;

public interface CategoryDetailDao extends BaseDao<CategoryDetailModel, Long> {

    List<CategoryDetailModel> findAll();
    List<CategoryDetailDto> getListDT(CategoryDetailDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(CategoryDetailDto dto);

}
