package id.her.trapedia.dao;

import id.her.trapedia.dto.BlogDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.SearchBlogDto;
import id.her.trapedia.dto.SubscribeDto;
import id.her.trapedia.model.BlogModel;
import id.her.trapedia.model.SubscribeModel;

import java.util.List;

public interface SubscribeDao extends BaseDao<SubscribeModel, Long> {

    List<SubscribeDto> getListDT(SubscribeDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(SubscribeDto dto);

}
