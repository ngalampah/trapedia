package id.her.trapedia.dao;

import id.her.trapedia.dto.PagesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.PagesModel;

import java.util.List;

public interface PagesDao extends BaseDao<PagesModel, Long> {

    List<PagesModel> findAll();
    List<PagesDto> getListDT(PagesDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(PagesDto dto);
    List<PagesDto> getPageByType(String type);

}
