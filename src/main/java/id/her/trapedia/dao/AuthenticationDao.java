package id.her.trapedia.dao;

import id.her.trapedia.model.AdminModel;
import id.her.trapedia.model.UserModel;

public interface AuthenticationDao extends BaseDao<AdminModel, Long> {
    AdminModel authenticate(String username);
}
