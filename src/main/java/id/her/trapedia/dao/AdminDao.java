package id.her.trapedia.dao;

import id.her.trapedia.dto.AdminDto;
import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.AdminModel;
import id.her.trapedia.model.CategoryModel;

import java.util.List;

public interface AdminDao extends BaseDao<AdminModel, Long> {

    List<AdminDto> getListDT(AdminDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(AdminDto dto);
    AdminModel authenticate(String username);

}
