package id.her.trapedia.dao;

import id.her.trapedia.dto.ServiceDetailDto;
import id.her.trapedia.model.ServiceDetailModel;

import java.util.List;

public interface ServiceDetailDao extends BaseDao<ServiceDetailModel, Long> {

    List<ServiceDetailModel> findAll();
    List<ServiceDetailModel> findAllByServiceId(Long id);
    List<ServiceDetailDto> findAllByServiceIdManual(Long id);

}
