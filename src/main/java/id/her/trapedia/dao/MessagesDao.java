package id.her.trapedia.dao;

import id.her.trapedia.dto.MessagesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.MessagesModel;

import java.util.List;

public interface MessagesDao extends BaseDao<MessagesModel, Long> {

    List<MessagesModel> getMessageByUser(Long userId);
    List<MessagesModel> getMessageDetailListByUser(Long userId, Long id);
    MessagesModel getMessageDetailByUser(Long userId, Long id);

    List<MessagesDto> getListDT(MessagesDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(MessagesDto dto);
}
