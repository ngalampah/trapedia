package id.her.trapedia.dao;

import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.CategoryModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public interface CategoryDao extends BaseDao<CategoryModel, Long> {

    List<CategoryModel> findAll();
    List<CategoryDto> getListDT(CategoryDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(CategoryDto dto);

}
