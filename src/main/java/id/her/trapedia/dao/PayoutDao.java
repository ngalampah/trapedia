package id.her.trapedia.dao;

import id.her.trapedia.dto.PayoutDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.PayoutModel;
import id.her.trapedia.model.RevenuesModel;

import java.util.List;

public interface PayoutDao extends BaseDao<PayoutModel, Long> {

    List<PayoutModel> findAllByUser(Long userId);

    List<PayoutDto> getListDT(PayoutDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(PayoutDto dto);

}
