package id.her.trapedia.dao;

import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.RevenueDto;
import id.her.trapedia.model.RevenuesModel;
import id.her.trapedia.model.ReviewModel;

import java.util.List;

public interface RevenuesDao extends BaseDao<RevenuesModel, Long> {

    List<RevenuesModel> findAllByUser(Long userId);

    List<RevenueDto> getListDT(RevenueDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(RevenueDto dto);
    String getTotalRevenueByUser(Long userId, String status);
    String getTotalWidrawByUser(Long userId);

}
