package id.her.trapedia.dao;

import id.her.trapedia.dto.CommentServiceDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.CommentServiceModel;
import id.her.trapedia.model.MessagesModel;

import java.util.List;

public interface CommentServiceDao extends BaseDao<CommentServiceModel, Long> {

    List<CommentServiceModel> getCommentByService(Long serviceId);
    List<CommentServiceDto> getCommentByServicePagging(CommentServiceDto commentServiceDto);
    String getCommentByServiceCount(CommentServiceDto commentServiceDto);
    List<CommentServiceDto> getCommentByCommentIdPagging(CommentServiceDto commentServiceDto);
    String getCommentByCommentIdCount(CommentServiceDto commentServiceDto);

    List<CommentServiceDto> getListDT(CommentServiceDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(CommentServiceDto dto);
}
