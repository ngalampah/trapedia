package id.her.trapedia.dao;

/**
 * Created with IntelliJ IDEA.
 * User: herdiagustina73@gmail.com
 * Date: 30/01/2017
 * Time: 9:00
 */
public interface SoftDeleteSupport {
    String getDeleted();

    void setDeleted(String deleted);
}
