package id.her.trapedia.dao;

import id.her.trapedia.model.TransactionDetailModel;

import java.util.List;

public interface TransactionDetailDao extends BaseDao<TransactionDetailModel, Long> {

    List<TransactionDetailModel> findAllByIdTransaction(Long id);
    List<TransactionDetailModel> salesByUser(Long userId);

}
