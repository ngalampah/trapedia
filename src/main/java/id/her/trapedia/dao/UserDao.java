package id.her.trapedia.dao;

import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.UserDto;
import id.her.trapedia.model.UserModel;

import java.util.List;

public interface UserDao extends BaseDao<UserModel, Long> {
    UserModel findUserModelByUsername(String username);
    List<UserDto> listUser(UserDto userDto, Integer limit, Integer offset);

    List<UserDto> getListDT(UserDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(UserDto dto);
}
