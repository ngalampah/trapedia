package id.her.trapedia.dao;

import id.her.trapedia.model.ReviewModel;
import id.her.trapedia.model.WishlistModel;

import java.util.List;

public interface ReviewDao extends BaseDao<ReviewModel, Long> {

    List<ReviewModel> findAllByUser(Long userId);
    List<ReviewModel> findAllByService(Long serviceId);

}
