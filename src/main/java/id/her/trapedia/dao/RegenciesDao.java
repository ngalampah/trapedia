package id.her.trapedia.dao;

import id.her.trapedia.dto.RegenciesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.RegenciesModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RegenciesDao extends BaseDao<RegenciesModel, Long> {

    List<RegenciesModel> findAll();
    List<RegenciesModel> findPopular();

    List<RegenciesDto> getListDT(RegenciesDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(RegenciesDto dto);

    List<RegenciesDto> getRegenciesByKeyword(String name);
}
