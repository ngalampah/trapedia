package id.her.trapedia.dao;

import id.her.trapedia.dto.GraphMonthDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.TransactionDto;
import id.her.trapedia.model.TransactionModel;

import java.util.List;

public interface TransactionDao extends BaseDao<TransactionModel, Long> {
    List<TransactionModel> findAll();
    String getCountAllTransaction();
    List<TransactionModel> findTransactionByUser(Long userId);


    List<TransactionDto> getListDT(TransactionDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(TransactionDto dto);
    String getCountListAll();

    List<GraphMonthDto> getGraphTransaction(String status);
}
