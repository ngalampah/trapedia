package id.her.trapedia.dao;

import id.her.trapedia.model.VerificationTokenModel;

public interface VerificationTokenDao extends BaseDao<VerificationTokenModel, Long> {

    VerificationTokenModel findByToken(String token);

}
