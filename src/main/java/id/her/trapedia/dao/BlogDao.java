package id.her.trapedia.dao;

import id.her.trapedia.dto.BlogDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.SearchBlogDto;
import id.her.trapedia.model.BlogModel;

import java.util.List;

public interface BlogDao extends BaseDao<BlogModel, Long> {

    List<BlogModel> findAll();
    List<BlogDto> getListDT(BlogDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(BlogDto dto);
    List<BlogDto> getBlogListPublic(SearchBlogDto searchBlogDto);
    String getCountListPublic(SearchBlogDto dto);

}
