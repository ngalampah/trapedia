package id.her.trapedia.dao;

import id.her.trapedia.dto.VoucherDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.VoucherModel;

import java.util.List;

public interface VoucherDao extends BaseDao<VoucherModel, Long> {

    List<VoucherModel> findAll();
    List<VoucherDto> getListDT(VoucherDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(VoucherDto dto);
    VoucherModel getVoucherByCode(String voucherCode);

}
