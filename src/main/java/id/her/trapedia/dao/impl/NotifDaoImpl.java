package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.NotifDao;
import id.her.trapedia.dao.ReviewDao;
import id.her.trapedia.dto.NotifDto;
import id.her.trapedia.model.NotifModel;
import id.her.trapedia.model.ReviewModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class NotifDaoImpl extends BaseDaoImpl<NotifModel, Long> implements NotifDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Override
    public List<NotifModel> findAllByUser(Long userId) {
        List<NotifModel> notifModels = new ArrayList<>();
        String sql = "from NotifModel where userModel.id = ?1 order by id desc";
        try {
            Query query = createQuery(sql).setParameter(1, userId);
            notifModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return notifModels;
    }

    @Override
    public List<NotifDto> getNotifByUserPagging(NotifDto notifDto) {
        List<NotifDto> notifDtos = new ArrayList<>();
        String sql ="SELECT " +
                        "s.id as id, " +
                        "s.date as date, " +
                        "s.deleted as deleted, " +
                        "s.is_read as isRead, " +
                        "s.user_id as userId, " +
                        "s.notif as notif, " +
                        "s.publish as publish, " +
                        "s.url as url " +
                    "FROM " +
                        "trp_notif s " +
                    "WHERE s.user_id = "+ notifDto.getUserId() + " "+
                    "ORDER BY s.id desc ";
        if (notifDto.getLength() > 0) {
            sql += "limit 0," + notifDto.getLength();
        }
        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("userId", new LongType()).
                    addScalar("deleted", new StringType()).
                    addScalar("isRead", new StringType()).
                    addScalar("notif", new StringType()).
                    addScalar("publish", new StringType()).
                    addScalar("url", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(NotifDto.class));
            notifDtos = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return notifDtos;
    }

    @Override
    public String getNotifByUserCount(Long userId, String status) {

        String st = "";
        if (status.equals("1")) {
            st += " AND s.is_read = '1'";
        }else if (status.equals("0")) {
            st += " AND s.is_read = '0'";
        }

        String sql ="SELECT " +
                        "count(*) as total_data "+
                    "FROM " +
                        "trp_notif s " +
                    "WHERE s.user_id = "+ userId + " " + st;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    @Override
    public Boolean setIsRead(NotifDto notifDto) {
        try {
            String sql = "update trp_notif set is_read =  '" + notifDto.getIsRead() + "' " +
                    "where user_id= " + notifDto.getUserId();
            Query query = createNativeQuery(sql);
            query.executeUpdate();
            logger.info(sql);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
            return false;
        }
    }
}
