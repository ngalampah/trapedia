package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.ServiceDetailDao;
import id.her.trapedia.dto.ServiceDetailDto;
import id.her.trapedia.model.ServiceDetailModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ServiceDetailDaoImpl extends BaseDaoImpl<ServiceDetailModel, Long> implements ServiceDetailDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Override
    public List<ServiceDetailModel> findAll() {
        List<ServiceDetailModel> serviceDetailModels = new ArrayList<>();
        String sql = "from ServiceDetailModel where deleted = '0' ";
        try {
            Query query = createQuery(sql);
            serviceDetailModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return serviceDetailModels;
    }

    @Override
    public List<ServiceDetailModel> findAllByServiceId(Long id) {
        List<ServiceDetailModel> serviceDetailModels = new ArrayList<>();
        String sql = "from ServiceDetailModel where deleted = '0' AND publish = '1' AND serviceModel.id = ?1 order by price asc";
        try {
            Query query = createQuery(sql)
                    .setParameter(1, id);
            serviceDetailModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return serviceDetailModels;
    }

    @Override
    public List<ServiceDetailDto> findAllByServiceIdManual(Long id) {
        List<ServiceDetailDto> serviceDetailDtos = new ArrayList<>();
        String sql ="SELECT " +
                        "id AS id, " +
                        "name AS name, " +
                        "description AS description, " +
                        "price AS price " +
                    "FROM "+
                        "trp_service_detail "+
                    "WHERE service_id =  " + id + " order by price asc";
        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("name", new StringType()).
                    addScalar("description", new StringType()).
                    addScalar("price", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(ServiceDetailDto.class));
            serviceDetailDtos = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return serviceDetailDtos;
    }
}
