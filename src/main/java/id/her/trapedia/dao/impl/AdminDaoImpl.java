package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.AdminDao;
import id.her.trapedia.dao.RegenciesDao;
import id.her.trapedia.dto.AdminDto;
import id.her.trapedia.dto.RegenciesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.AdminModel;
import id.her.trapedia.model.RegenciesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AdminDaoImpl extends BaseDaoImpl<AdminModel, Long> implements AdminDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public AdminModel authenticate(String username) {
        logger.info("Authenticate for username : " + username);
        AdminModel adminModel = new AdminModel();
        try {
            String sql = "from AdminModel where enabled = '1' and username = ?1";
            logger.info(sql);
            Query query = createQuery(sql)
                    .setParameter(1, username);
            adminModel = (AdminModel) query.uniqueResult();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }
        return adminModel;
    }


    @Override
    public List<AdminDto> getListDT(AdminDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<AdminDto> adminDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "id AS id, " +
                        "username AS username, " +
                        "name AS name, " +
                        "enabled AS enabled " +
                    "FROM " +
                        "trp_admin " +
                    "WHERE 1 = '1' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("username", new StringType()).
                    addScalar("name", new StringType()).
                    addScalar("enabled", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(AdminDto.class));
            adminDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return adminDtoList;
    }

    @Override
    public String getCountListDT(AdminDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_admin "+
                    "WHERE "+
                        "1 = '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(AdminDto dto){
        String param = "";
        if(dto.getUsername() != null && !dto.getUsername().equals("")) {
            param += "AND username like '%"+ dto.getUsername().replace("'","''")+ "%' ";
        }

        if(dto.getName() != null && !dto.getName().equals("")) {
            param += "AND name like '%"+ dto.getUsername().replace("'","''")+ "%' ";
        }

        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }
}
