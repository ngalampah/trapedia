package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.UserDao;
import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.UserDto;
import id.her.trapedia.model.UserModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDaoImpl extends BaseDaoImpl<UserModel, Long> implements UserDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public UserModel findUserModelByUsername(String username) {
        logger.info("find user by username : " + username);
        UserModel userModel = new UserModel();

        try {
            String sql = "from UserModel where deleted = '0' and email = ?1";
            logger.info("sql : " + sql);
            Query query = createQuery(sql).setParameter(1, username);
            userModel = (UserModel) query.uniqueResult();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return userModel;
    }

    @Override
    public List<UserDto> listUser(UserDto userDto, Integer limit, Integer offset) {
        List<UserDto> userDtoList = new ArrayList<>();
        String sql = "from UserModel user where deleted = '0'";
        try {
            Query query = createQuery(sql)
                    .setMaxResults(limit)
                    .setFirstResult(offset);
            userDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return userDtoList;
    }

    @Override
    public List<UserDto> getListDT(UserDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<UserDto> userDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "id AS id, " +
                        "name AS name, " +
                        "email AS email, " +
                        "gender AS gender, " +
                        "phone AS phone, " +
                        "dob AS dob " +
                    "FROM " +
                        "trp_user " +
                    "WHERE deleted != '1' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("name", new StringType()).
                    addScalar("email", new StringType()).
                    addScalar("gender", new StringType()).
                    addScalar("phone", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(UserDto.class));
            userDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return userDtoList;
    }

    @Override
    public String getCountListDT(UserDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_user "+
                    "WHERE "+
                        "deleted != '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(UserDto dto){
        String param = "";
        if(dto.getName() != null && !dto.getName().equals("")) {
            param += "AND name like '%"+ dto.getName().replace("'","''")+ "%' ";
        }
        if(dto.getEmail() != null && !dto.getEmail().equals("")) {
            param += "AND email like '%"+ dto.getName().replace("'","''")+ "%' ";
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }
}
