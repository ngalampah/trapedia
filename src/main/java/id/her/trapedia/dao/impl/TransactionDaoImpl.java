package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.TransactionDao;
import id.her.trapedia.dto.GraphMonthDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.TransactionDto;
import id.her.trapedia.model.TransactionModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TransactionDaoImpl extends BaseDaoImpl<TransactionModel, Long> implements TransactionDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<TransactionModel> findAll() {
        List<TransactionModel> transactionDetailModels = new ArrayList<>();
        String sql = "from TransactionModel";
        try {
            Query query = createQuery(sql);
            transactionDetailModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return transactionDetailModels;
    }

    @Override
    public String getCountAllTransaction() {
        String queryString ="SELECT "+
                                "count(*) as total_data "+
                            "FROM "+
                                "trp_transaction ";
        String result = "0";
        try{
            Query query= createNativeQuery(queryString).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    @Override
    public List<TransactionModel> findTransactionByUser(Long userId) {
        List<TransactionModel> transactionDetailModels = new ArrayList<>();
        String sql = "from TransactionModel where userModel.id  = ?1 order by date desc";
        try {
            Query query = createQuery(sql).setParameter(1, userId);
            transactionDetailModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return transactionDetailModels;
    }

    @Override
    public List<TransactionDto> getListDT(TransactionDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<TransactionDto> transactionDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "t.id AS id, " +
                        "t.discount_voucher_code AS discountVoucherCode, " +
                        "t.fee_admin AS fee, " +
                        "t.summary_service AS summaryService, " +
                        "t.total AS total, " +
                        "t.date AS date, " +
                        "t.status_payment AS statusPayment, " +
                        "t.payment_amount AS paymentAmount, " +
                        "t.traveling_email AS travelingEmail, " +
                        "t.traveling_name AS travelingName, " +
                        "t.traveling_phone AS travelingPhone " +
                "FROM " +
                "trp_transaction t " +
                "WHERE 1 = 1  " + param + " order by t.date desc " + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("discountVoucherCode", new LongType()).
                    addScalar("fee", new LongType()).
                    addScalar("summaryService", new LongType()).
                    addScalar("total", new LongType()).
                    addScalar("paymentAmount", new LongType()).
                    addScalar("date", new StringType()).
                    addScalar("statusPayment", new StringType()).
                    addScalar("travelingEmail", new StringType()).
                    addScalar("travelingName", new StringType()).
                    addScalar("travelingPhone", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(TransactionDto.class));
            transactionDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return transactionDtoList;
    }

    @Override
    public String getCountListDT(TransactionDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                "count(*) as total_data "+
                "FROM "+
                "trp_transaction t "+
                "WHERE "+
                "1=1 "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    @Override
    public List<GraphMonthDto> getGraphTransaction(String status) {
        List<GraphMonthDto> graphMonthDtos = new ArrayList<>();

        String sql ="select DATE_FORMAT(date, '%b') as month, count(*) as totalData from trp_transaction " +
                "where YEAR(date) = YEAR(NOW()) AND status_payment = '" + status+ "' "+
                "group by DATE_FORMAT(date, '%b') " +
                "order by month asc";

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("month", new StringType()).
                    addScalar("totalData", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(GraphMonthDto.class));
            graphMonthDtos = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return graphMonthDtos;
    }

    private String getSearchDT(TransactionDto dto){
        String param = "";
        if(dto.getDateFrom() != null && !dto.getDateFrom().equals("") &&
           dto.getDateTo() != null && !dto.getDateTo().equals("") ) {
            param += "AND DATE(t.date) BETWEEN '" + dto.getDateFrom()  + "' AND '" + dto.getDateTo() + "' " ;
        }else if(dto.getDateFrom() != null && !dto.getDateFrom().equals("")) {
            param += "AND DATE(t.date) = '" + dto.getDateFrom()  + "' " ;
        }

        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }

    @Override
    public String getCountListAll() {
        String sql = "SELECT "+
                "count(*) as total_data "+
                "FROM "+
                "trp_transaction t ";
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }
}
