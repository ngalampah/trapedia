package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.VerificationTokenDao;
import id.her.trapedia.model.VerificationTokenModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class VerificationTokenDaoImpl extends BaseDaoImpl<VerificationTokenModel, Long> implements VerificationTokenDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Override
    public VerificationTokenModel findByToken(String token) {
        VerificationTokenModel verificationTokenModel = new VerificationTokenModel();
        String sql = "from VerificationTokenModel where token = ?1";
        try {
            Query query = createQuery(sql)
                    .setParameter(1, token);
            verificationTokenModel = (VerificationTokenModel) query.uniqueResult();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return verificationTokenModel;
    }
}
