package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.ServiceDao;
import id.her.trapedia.dto.ReqServiceDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ServiceDto;
import id.her.trapedia.model.ServiceModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ServiceDaoImpl extends BaseDaoImpl<ServiceModel, Long> implements ServiceDao {
    private Logger logger = LogManager.getLogger(ServiceDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<ServiceModel> findAll() {
        List<ServiceModel> serviceModels = new ArrayList<>();
        String sql = "from ServiceModel where deleted = '0' ";
        try {
            Query query = createQuery(sql);
            serviceModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return serviceModels;
    }

    @Override
    public List<ServiceModel> findAll(String parameter, int limit, int offset) {
        List<ServiceModel> serviceModels = new ArrayList<>();
        String sql = "from ServiceModel where deleted = '0' " + parameter;
        try {
            Query query = createQuery(sql)
                    .setMaxResults(limit)
                    .setFirstResult(offset);
            serviceModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return serviceModels;
    }

    @Override
    public List<ServiceDto> listPopular() {
        List<ServiceDto> va = new ArrayList<>();
        String sql ="SELECT " +
                "ts.id AS id, " +
                "ts.service_name AS serviceName, " +
                "ts.count_rating AS countRating, " +
                "ts.count_review AS countReview, " +
                "tsd.price AS price, " +
                "min(tsd.price) as startingPrice, " +
                "ts.images_1 AS images1, " +
                "ts.images_2 AS images2, " +
                "ts.images_3 AS images3, " +
                "ts.images_4 AS images4, " +
                "ts.images_5 AS images5 " +
                "FROM " +
                "trp_service ts " +
                "join trp_service_detail tsd on tsd.service_id = ts.id "+
                "join trp_user tu on tu.id = ts.user_id "+
                "WHERE tu.deleted != '1' AND tsd.publish = '1' and ts.popular = '1' and ts.approve = '1'  and ts.deleted != '1' and ts.publish = '1' group by ts.id order by ts.id desc, tsd.price asc  limit 4";
        logger.info(sql);
        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceName", new StringType()).
                    addScalar("countRating", new LongType()).
                    addScalar("countReview", new LongType()).
                    addScalar("price", new LongType()).
                    addScalar("startingPrice", new LongType()).
                    addScalar("images1", new StringType()).
                    addScalar("images2", new StringType()).
                    addScalar("images3", new StringType()).
                    addScalar("images4", new StringType()).
                    addScalar("images5", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(ServiceDto.class));
            va = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return va;
    }

    @Override
    public List<ServiceDto> listNew() {
        List<ServiceDto> va = new ArrayList<>();
        String sql ="SELECT " +
                "ts.id AS id, " +
                "ts.service_name AS serviceName, " +
                "ts.count_rating AS countRating, " +
                "ts.count_review AS countReview, " +
                "tsd.price AS price, " +
                "min(tsd.price) as startingPrice, " +
                "ts.images_1 AS images1, " +
                "ts.images_2 AS images2, " +
                "ts.images_3 AS images3, " +
                "ts.images_4 AS images4, " +
                "ts.images_5 AS images5 " +
                "FROM " +
                "trp_service ts " +
                "join trp_service_detail tsd on tsd.service_id = ts.id "+
                "join trp_user tu on tu.id = ts.user_id "+
                "WHERE tu.deleted != '1' AND tsd.publish = '1' and ts.approve = '1'  and ts.deleted != '1' and ts.publish = '1' group by ts.id order by ts.id desc limit 4";
        logger.info(sql);
        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceName", new StringType()).
                    addScalar("countRating", new LongType()).
                    addScalar("countReview", new LongType()).
                    addScalar("price", new LongType()).
                    addScalar("startingPrice", new LongType()).
                    addScalar("images1", new StringType()).
                    addScalar("images2", new StringType()).
                    addScalar("images3", new StringType()).
                    addScalar("images4", new StringType()).
                    addScalar("images5", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(ServiceDto.class));
            va = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return va;
    }

    @Override
    public List<ServiceDto> findAllManual(String parameter) {
        List<ServiceDto> va = new ArrayList<>();
        String sql ="SELECT " +
                "ts.id AS id, " +
                "ts.service_name AS serviceName, " +
                "ts.count_rating AS countRating, " +
                "ts.count_review AS countReview, " +
                "tsd.price AS price, " +
                "min(tsd.price) as startingPrice, " +
                "ts.images_1 AS images1, " +
                "ts.images_2 AS images2, " +
                "ts.images_3 AS images3, " +
                "ts.images_4 AS images4, " +
                "ts.images_5 AS images5 " +
                "FROM " +
                "trp_service ts " +
                "join trp_service_detail tsd on tsd.service_id = ts.id "+
                "join trp_user tu on tu.id = ts.user_id "+
                "WHERE tu.deleted != '1' AND tsd.publish = '1' and ts.approve = '1'  and ts.deleted != '1' and ts.publish = '1' " + parameter;
        logger.info(sql);

        logger.info("Query : " + sql);
        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceName", new StringType()).
                    addScalar("countRating", new LongType()).
                    addScalar("countReview", new LongType()).
                    addScalar("startingPrice", new LongType()).
                    addScalar("images1", new StringType()).
                    addScalar("images2", new StringType()).
                    addScalar("images3", new StringType()).
                    addScalar("images4", new StringType()).
                    addScalar("images5", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(ServiceDto.class));
            va = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return va;
    }

    @Override
    public String getCountAllManual(String parameter) {
        String sql ="SELECT "+
                                "count(*) as total_data "+
                            "FROM "+
                                "trp_service ts " +
                            "join trp_service_detail tsd on tsd.service_id = ts.id "+
                            "WHERE tsd.publish = '1' and ts.approve = '1'  and ts.deleted != '1' and ts.publish = '1' " + parameter;
        String result = "0";
        logger.info(sql);
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }


    @Override
    public List<ServiceDto> getListDT(ServiceDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<ServiceDto> categoryDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "s.id as id, " +
                        "s.service_name as serviceName, " +
                        "s.approve as approve, " +
                        "s.date as date, " +
                        "u.name as userName, " +
                        "c.title as categoryName " +
                    "FROM " +
                        "trp_service s " +
                    "JOIN "+
                        "trp_user u on u.id = s.user_id "+
                    "JOIN "+
                        "trp_m_category c on c.id = s.category_id "+
                    "WHERE s.deleted != '1' " + param +
                    " ORDER BY s.id desc " + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceName", new StringType()).
                    addScalar("date", new StringType()).
                    addScalar("approve", new StringType()).
                    addScalar("userName", new StringType()).
                    addScalar("categoryName", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(ServiceDto.class));
            categoryDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return categoryDtoList;
    }

    @Override
    public String getCountListDT(ServiceDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_service s "+
                    "WHERE "+
                        "s.deleted != '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(ServiceDto dto){
        String param = "";
        if(dto.getServiceName() != null && !dto.getServiceName().equals("")) {
            param += "AND s.service_name like '%"+ dto.getServiceName().replace("'","''")+ "%' ";
        }
        if(dto.getApprove() != null && !dto.getApprove().equals("")) {
            param += "AND s.approve ='"+ dto.getApprove()+ "' ";
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }

    @Override
    public List<ServiceDto> findAllByUser(Long userId) {
        List<ServiceDto> va = new ArrayList<>();
        String sql ="SELECT " +
                "ts.id AS id, " +
                "ts.service_name AS serviceName, " +
                "ts.count_rating AS countRating, " +
                "ts.count_review AS countReview, " +
                "tsd.price AS price, " +
                "min(tsd.price) as startingPrice, " +
                "ts.images_1 AS images1, " +
                "ts.images_2 AS images2, " +
                "ts.images_3 AS images3, " +
                "ts.images_4 AS images4, " +
                "ts.images_5 AS images5, " +
                "ts.approve AS approve, " +
                "ts.date AS date " +
                "FROM " +
                "trp_service ts " +
                "join trp_service_detail tsd on tsd.service_id = ts.id "+
                "WHERE " +
                "ts.user_id = " + userId + " AND "+
                "ts.deleted != '1'  group by ts.id order by ts.id desc ";
        logger.info(sql);
        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceName", new StringType()).
                    addScalar("countRating", new LongType()).
                    addScalar("countReview", new LongType()).
                    addScalar("price", new LongType()).
                    addScalar("startingPrice", new LongType()).
                    addScalar("images1", new StringType()).
                    addScalar("images2", new StringType()).
                    addScalar("images3", new StringType()).
                    addScalar("images4", new StringType()).
                    addScalar("images5", new StringType()).
                    addScalar("approve", new StringType()).
                    addScalar("date", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(ServiceDto.class));
            va = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return va;
    }


    @Override
    public List<ServiceDto> serviceTrandingWeekTransaction() {
        List<ServiceDto> va = new ArrayList<>();
        String sql ="SELECT " +
                "count(ts.id) as count_service, "+
                "ts.id AS id, " +
                "ts.service_name AS serviceName, " +
                "ts.count_rating AS countRating, " +
                "ts.count_review AS countReview, " +
                "min(tsd.price) as startingPrice, " +
                "ts.images_1 AS images1, " +
                "ts.images_2 AS images2, " +
                "ts.images_3 AS images3, " +
                "ts.images_4 AS images4, " +
                "ts.images_5 AS images5 " +
                "FROM " +
                "trp_transaction tp " +
                "join trp_transaction_detail td on td.transaction_id = tp.id " +
                "join trp_service ts on ts.id = td.service_id " +
                "join trp_service_detail tsd ON tsd.service_id = ts.id " +
                "join trp_user tu on tu.id = ts.user_id "+
                "WHERE " +
                "tu.deleted != '1' AND "+
                "DATE(tp.date) >= DATE(DATE_SUB(NOW(), INTERVAL 7 DAY)) AND " +
                "ts.approve = '1' and ts.publish = '1' and ts.deleted != '1' " +
                "AND tsd.publish = '1' AND tsd.deleted != '1' "+
                "group by ts.id " +
                "order by count_service desc " +
                "limit 6";
        logger.info("Query : " + sql);
        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceName", new StringType()).
                    addScalar("countRating", new LongType()).
                    addScalar("countReview", new LongType()).
                    addScalar("startingPrice", new LongType()).
                    addScalar("images1", new StringType()).
                    addScalar("images2", new StringType()).
                    addScalar("images3", new StringType()).
                    addScalar("images4", new StringType()).
                    addScalar("images5", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(ServiceDto.class));
            va = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return va;
    }


    @Override
    public List<ServiceDto> serviceTrandingMonthTransaction() {
        List<ServiceDto> va = new ArrayList<>();
        String sql ="SELECT " +
                "count(ts.id) as count_service, "+
                "ts.id AS id, " +
                "ts.service_name AS serviceName, " +
                "ts.count_rating AS countRating, " +
                "ts.count_review AS countReview, " +
                "min(tsd.price) as startingPrice, " +
                "ts.images_1 AS images1, " +
                "ts.images_2 AS images2, " +
                "ts.images_3 AS images3, " +
                "ts.images_4 AS images4, " +
                "ts.images_5 AS images5 " +
                "FROM " +
                "trp_transaction tp " +
                "join trp_transaction_detail td on td.transaction_id = tp.id " +
                "join trp_service ts on ts.id = td.service_id " +
                "join trp_service_detail tsd ON tsd.service_id = ts.id " +
                "join trp_user tu on tu.id = ts.user_id "+
                "WHERE " +
                "tu.deleted != '1' AND "+
                "DATE(tp.date) >= DATE(DATE_SUB(NOW(), INTERVAL 30 DAY)) AND " +
                "ts.approve = '1' and ts.publish = '1' and ts.deleted != '1' " +
                "AND tsd.publish = '1' AND tsd.deleted != '1' "+
                "group by ts.id " +
                "order by count_service desc " +
                "limit 6";
        logger.info("Query : " + sql);
        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceName", new StringType()).
                    addScalar("countRating", new LongType()).
                    addScalar("countReview", new LongType()).
                    addScalar("startingPrice", new LongType()).
                    addScalar("images1", new StringType()).
                    addScalar("images2", new StringType()).
                    addScalar("images3", new StringType()).
                    addScalar("images4", new StringType()).
                    addScalar("images5", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(ServiceDto.class));
            va = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return va;
    }

    @Override
    public List<ServiceDto> listRecomendation(ReqServiceDto reqServiceDto) {
        List<ServiceDto> va = new ArrayList<>();

        String w = "";
        if(reqServiceDto.getRegenciesId() != null && !reqServiceDto.getRegenciesId().equals("")){
            w += " AND ts.regencies_id = "+ reqServiceDto.getRegenciesId() +"  ";
        }
        if(reqServiceDto.getCategoryId() != null && !reqServiceDto.getCategoryId().equals("")){
            w += " AND ts.category_id = "+ reqServiceDto.getCategoryId() +"  ";
        }
        if(reqServiceDto.getId() != null && !reqServiceDto.getId().equals("")){
            w += " AND ts.id != "+ reqServiceDto.getId() +"  ";
        }

        String sql ="SELECT " +
                "ts.id AS id, " +
                "ts.service_name AS serviceName, " +
                "ts.count_rating AS countRating, " +
                "ts.count_review AS countReview, " +
                "tsd.price AS price, " +
                "min(tsd.price) as startingPrice, " +
                "ts.images_1 AS images1, " +
                "ts.images_2 AS images2, " +
                "ts.images_3 AS images3, " +
                "ts.images_4 AS images4, " +
                "ts.images_5 AS images5 " +
                "FROM " +
                "trp_service ts " +
                "join trp_service_detail tsd on tsd.service_id = ts.id "+
                "join trp_user tu on tu.id = ts.user_id "+
                "WHERE  tu.deleted != '1' AND tsd.publish = '1' and ts.approve = '1'  and ts.deleted != '1' and ts.publish = '1' "+ w +" group by ts.id order by ts.id desc limit 10";
        logger.info(sql);
        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceName", new StringType()).
                    addScalar("countRating", new LongType()).
                    addScalar("countReview", new LongType()).
                    addScalar("price", new LongType()).
                    addScalar("startingPrice", new LongType()).
                    addScalar("images1", new StringType()).
                    addScalar("images2", new StringType()).
                    addScalar("images3", new StringType()).
                    addScalar("images4", new StringType()).
                    addScalar("images5", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(ServiceDto.class));
            va = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return va;
    }


}
