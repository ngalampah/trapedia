package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.PayoutDao;
import id.her.trapedia.dao.RevenuesDao;
import id.her.trapedia.dto.PayoutDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ServiceDto;
import id.her.trapedia.model.PayoutModel;
import id.her.trapedia.model.RevenuesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PayoutDaoImpl extends BaseDaoImpl<PayoutModel, Long> implements PayoutDao {

    private Logger logger = LogManager.getLogger(PayoutDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<PayoutModel> findAllByUser(Long userId) {
        List<PayoutModel> PayoutModel = new ArrayList<>();
        String sql = "from PayoutModel where userModel.id = ?1 order by id desc";
        try {
            Query query = createQuery(sql)
                    .setParameter(1, userId);
            PayoutModel = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return PayoutModel;
    }

    @Override
    public List<PayoutDto> getListDT(PayoutDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<PayoutDto> payoutDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "p.id AS id, " +
                        "p.amount AS amount, " +
                        "p.bank_name AS bankName, " +
                        "p.bank_number AS bankNumber, " +
                        "p.bank_account_name AS bankAccountName, " +
                        "p.status AS status, " +
                        "p.date AS date, " +
                        "u.name AS userName " +
                    "FROM " +
                        "trp_payout p " +
                    "JOIN "+
                        "trp_user u on u.id = p.user_id "+
                    "WHERE 1 = 1 " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("amount", new LongType()).
                    addScalar("bankName", new StringType()).
                    addScalar("bankNumber", new StringType()).
                    addScalar("bankAccountName", new StringType()).
                    addScalar("status", new StringType()).
                    addScalar("date", new StringType()).
                    addScalar("userName", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(PayoutDto.class));
            payoutDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return payoutDtoList;
    }

    @Override
    public String getCountListDT(PayoutDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_payout p "+
                    "WHERE "+
                        "1=1 "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(PayoutDto dto){
        String param = "";
        if(dto.getStatus() != null && !dto.getStatus().equals("")) {
            param += "AND p.status ='"+ dto.getStatus()+ "' ";
        }
        if(dto.getDateFrom() != null && !dto.getDateFrom().equals("") &&
                dto.getDateTo() != null && !dto.getDateTo().equals("") ) {
            param += "AND DATE(p.date) BETWEEN '" + dto.getDateFrom()  + "' AND '" + dto.getDateTo() + "' " ;
        }else if(dto.getDateFrom() != null && !dto.getDateFrom().equals("")) {
            param += "AND DATE(p.date) = '" + dto.getDateFrom()  + "' " ;
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }
}
