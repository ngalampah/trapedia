package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.ReviewDao;
import id.her.trapedia.dao.WishlistDao;
import id.her.trapedia.model.ReviewModel;
import id.her.trapedia.model.WishlistModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ReviewDaoImpl extends BaseDaoImpl<ReviewModel, Long> implements ReviewDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Override
    public List<ReviewModel> findAllByUser(Long userId) {
        List<ReviewModel> reviewModels = new ArrayList<>();
        String sql = "from ReviewModel where userModel.id = ?1 order by id desc";
        try {
            Query query = createQuery(sql).setParameter(1, userId);
            reviewModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return reviewModels;
    }

    @Override
    public List<ReviewModel> findAllByService(Long serviceId) {
        List<ReviewModel> reviewModels = new ArrayList<>();
        String sql = "from ReviewModel where serviceDetailModel.serviceModel.id = ?1 order by id desc";
        try {
            Query query = createQuery(sql).setParameter(1, serviceId);
            reviewModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return reviewModels;
    }
}
