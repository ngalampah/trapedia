package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.PagesDao;
import id.her.trapedia.dto.PagesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.PagesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PagesDaoImpl extends BaseDaoImpl<PagesModel, Long> implements PagesDao {

    private Logger logger = LogManager.getLogger(PagesDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<PagesModel> findAll() {
        List<PagesModel> pagesModels = new ArrayList<>();
        String sql = "from PagesModel where deleted = '0' ";
        try {
            Query query = createQuery(sql);
            pagesModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return pagesModels;
    }

    @Override
    public List<PagesDto> getListDT(PagesDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<PagesDto> pagesDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                "id AS id, " +
                "content AS content, " +
                "deleted AS deleted, " +
                "images AS images, " +
                "publish AS publish, " +
                "title AS title, " +
                "type AS type " +
                "FROM " +
                "trp_pages " +
                "WHERE deleted = '0' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("content", new StringType()).
                    addScalar("deleted", new StringType()).
                    addScalar("images", new StringType()).
                    addScalar("publish", new StringType()).
                    addScalar("title", new StringType()).
                    addScalar("type", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(PagesDto.class));
            pagesDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return pagesDtoList;
    }

    @Override
    public String getCountListDT(PagesDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                "count(*) as total_data "+
                "FROM "+
                "trp_pages "+
                "WHERE "+
                "1 = '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(PagesDto dto){
        String param = "";
        if(dto.getTitle() != null && !dto.getTitle().equals("")) {
            param += "AND title like '%"+ dto.getTitle().replace("'","''")+ "%' ";
        }

        if(dto.getPublish() != null && !dto.getPublish().equals("")) {
            param += "AND publish = '"+ dto.getPublish()+"'";
        }

        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }


    @Override
    public List<PagesDto> getPageByType(String type) {
        List<PagesDto> pagesDtoList = new ArrayList<>();

        String sql ="SELECT " +
                        "id AS id, " +
                        "content AS content, " +
                        "deleted AS deleted, " +
                        "images AS images, " +
                        "publish AS publish, " +
                        "title AS title, " +
                        "type AS type " +
                    "FROM " +
                        "trp_pages " +
                    "WHERE deleted = '0' && type = '" + type + "'";

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("content", new StringType()).
                    addScalar("deleted", new StringType()).
                    addScalar("images", new StringType()).
                    addScalar("publish", new StringType()).
                    addScalar("title", new StringType()).
                    addScalar("type", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(PagesDto.class));
            pagesDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return pagesDtoList;
    }
}
