package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.CategoryDao;
import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.CategoryModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryDaoImpl extends BaseDaoImpl<CategoryModel, Long> implements CategoryDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<CategoryModel> findAll() {
        List<CategoryModel> categoryModels = new ArrayList<>();
        String sql = "from CategoryModel where deleted != '1' and publish = '1' order by title asc ";
        try {
            Query query = createQuery(sql);
            categoryModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return categoryModels;
    }

    @Override
    public List<CategoryDto> getListDT(CategoryDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<CategoryDto> categoryDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "id AS id, " +
                        "title AS title, " +
                        "images AS images, " +
                        "publish AS publish, " +
                        "description AS description " +
                    "FROM " +
                        "trp_m_category " +
                        "WHERE deleted != '1' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("title", new StringType()).
                    addScalar("publish", new StringType()).
                    addScalar("images", new StringType()).
                    addScalar("description", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(CategoryDto.class));
            categoryDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return categoryDtoList;
    }

    @Override
    public String getCountListDT(CategoryDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                "count(*) as total_data "+
                "FROM "+
                "trp_m_category "+
                "WHERE "+
                "deleted != '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(CategoryDto dto){
        String param = "";
        if(dto.getTitle() != null && !dto.getTitle().equals("")) {
            param += "AND title like '%"+ dto.getTitle().replace("'","''")+ "%' ";
        }
        if(dto.getPublish() != null && !dto.getPublish().equals("")) {
            param += "AND publish ='"+ dto.getPublish()+ "' ";
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }
}
