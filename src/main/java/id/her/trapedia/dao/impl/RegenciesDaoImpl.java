package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.RegenciesDao;
import id.her.trapedia.dto.RegenciesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.RegenciesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RegenciesDaoImpl extends BaseDaoImpl<RegenciesModel, Long> implements RegenciesDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<RegenciesModel> findAll() {
        List<RegenciesModel> regenciesModels = new ArrayList<>();
        String sql = "from RegenciesModel where deleted = '0' and publish = '1' order by name asc";
        try {
            Query query = createQuery(sql);
            regenciesModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return regenciesModels;
    }

    @Override
    public List<RegenciesModel> findPopular() {
        List<RegenciesModel> regenciesModels = new ArrayList<>();
        String sql = "from RegenciesModel where deleted = '0' and publish = '1' and popular = '1' order by name asc";
        try {
            Query query = createQuery(sql);
            regenciesModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return regenciesModels;
    }

    @Override
    public List<RegenciesDto> getListDT(RegenciesDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<RegenciesDto> regenciesDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "r.id AS id, " +
                        "r.name AS name, " +
                        "r.popular AS popular, " +
                        "r.images AS images, " +
                        "r.publish AS publish, " +
                        "r.province_id AS provincesId, " +
                        "p.name AS provincesName " +
                    "FROM " +
                        "trp_m_regencies r " +
                    "JOIN " +
                        "trp_m_provinces p on p.id = r.province_id " +
                    "WHERE r.deleted != '1' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("name", new StringType()).
                    addScalar("popular", new StringType()).
                    addScalar("images", new StringType()).
                    addScalar("publish", new StringType()).
                    addScalar("provincesId", new LongType()).
                    addScalar("provincesName", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(RegenciesDto.class));
            regenciesDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return regenciesDtoList;
    }

    @Override
    public String getCountListDT(RegenciesDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_m_regencies r "+
                    "WHERE "+
                        "deleted != '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(RegenciesDto dto){
        String param = "";
        if(dto.getName() != null && !dto.getName().equals("")) {
            param += "AND r.name like '%"+ dto.getName().replace("'","''")+ "%' ";
        }

        if(dto.getPublish() != null && !dto.getPublish().equals("")) {
            param += "AND r.publish ='"+ dto.getPublish()+ "' ";
        }

        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }

    @Override
    public List<RegenciesDto> getRegenciesByKeyword(String name) {
        List<RegenciesDto> regenciesDtoList = new ArrayList<>();
        String sql ="SELECT " +
                "id as id, " +
                "name as name, " +
                "province_id as provincesId " +
                "FROM " +
                "trp_m_regencies " +
                "WHERE name like '%"+name+"%' and publish='1' and deleted='0' order by name asc limit 10" ;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("name", new StringType()).
                    addScalar("provincesId", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(RegenciesDto.class));
            regenciesDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return regenciesDtoList;
    }
}
