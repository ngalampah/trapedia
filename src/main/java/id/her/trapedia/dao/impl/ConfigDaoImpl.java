package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.ConfigDao;
import id.her.trapedia.dao.UserDao;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.UserDto;
import id.her.trapedia.model.ConfigModel;
import id.her.trapedia.model.UserModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ConfigDaoImpl extends BaseDaoImpl<ConfigModel, Long> implements ConfigDao {

    private Logger logger = LogManager.getLogger(ConfigDaoImpl.class);

    @Override
    public ConfigModel getConfigById(String configName) {
        ConfigModel configModel = new ConfigModel();
        try {
            String sql = "from ConfigModel where configName = ?1";
            logger.info("param : " + configName);
            logger.info("sql : " + sql);
            Query query = createQuery(sql)
                    .setParameter(1, configName);
            configModel = (ConfigModel) query.uniqueResult();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }
        return configModel;
    }

    @Override
    public List<ConfigModel> getAllConfig() {
        List<ConfigModel> configModels = new ArrayList<>();
        try {
            String sql = "from ConfigModel";
            Query query = createQuery(sql);
            configModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }
        return configModels;
    }
}
