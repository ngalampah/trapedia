package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.RevenuesDao;
import id.her.trapedia.dao.UserBalanceDao;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.RevenueDto;
import id.her.trapedia.model.RevenuesModel;
import id.her.trapedia.model.UserBalanceModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserBalanceDaoImpl extends BaseDaoImpl<UserBalanceModel, Long> implements UserBalanceDao {

    private Logger logger = LogManager.getLogger(UserBalanceDaoImpl.class);
    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public String getBalanceByUser(Long userId, String status) {
        String sql = "SELECT "+
                    "sum(balance) as total_data "+
                    "FROM "+
                    "trp_user_balance "+
                    "WHERE "+
                    "user_id =  " + userId;

        if(!status.equals("")){
            sql += " AND balance_type = '"+status+"'";
        }
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    @Override
    public List<UserBalanceModel> findAllByUser(Long userId) {
        List<UserBalanceModel> userBalanceModelList = new ArrayList<>();
        String sql = "from UserBalanceModel where userModel.id = ?1 order by id desc";
        try {
            Query query = createQuery(sql)
                    .setParameter(1, userId);
            userBalanceModelList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return userBalanceModelList;
    }
}
