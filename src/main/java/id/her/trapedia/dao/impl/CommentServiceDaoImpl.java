package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.CommentServiceDao;
import id.her.trapedia.dao.MessagesDao;
import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.CommentServiceDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.CommentServiceModel;
import id.her.trapedia.model.MessagesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CommentServiceDaoImpl extends BaseDaoImpl<CommentServiceModel, Long> implements CommentServiceDao {

    private Logger logger = LogManager.getLogger(CommentServiceDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;


    @Override
    public List<CommentServiceModel> getCommentByService(Long serviceId) {
        List<CommentServiceModel> commentServiceModels = new ArrayList<>();
        String sql = "from CommentServiceModel where serviceModel.id = ?1  order by id desc";
        try {
            Query query = createQuery(sql).setParameter(1, serviceId);;
            commentServiceModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return commentServiceModels;
    }

    @Override
    public List<CommentServiceDto> getCommentByServicePagging(CommentServiceDto commentServiceDto) {
        List<CommentServiceDto> commentServiceDtos = new ArrayList<>();
        String sql ="SELECT " +
                    "s.id as id, " +
                    "s.comment as comment, " +
                    "s.date as date, " +
                    "s.service_id as serviceId, " +
                    "c.image as imageUser, " +
                    "s.images_1 as image1, " +
                    "s.images_2 as image2, " +
                    "s.images_3 as image3, " +
                    "s.images_4 as image4, " +
                    "s.images_5 as image5, " +
                    "(select count(id) from trp_comment_service where parent_id = s.id) as parentCount, " +
                    "c.name as userName " +
                    "FROM " +
                    "trp_comment_service s " +
                    "JOIN "+
                    "trp_user c ON c.id = s.user_id "+
                    "WHERE s.deleted != '1' AND s.parent_id = '0' AND s.service_id = "+ commentServiceDto.getServiceId() + " "+
                    "ORDER BY s.id desc ";
        if (commentServiceDto.getLength() > 0) {
            sql += "limit 0,"+commentServiceDto.getLength();
        }
        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceId", new LongType()).
                    addScalar("parentCount", new LongType()).
                    addScalar("comment", new StringType()).
                    addScalar("date", new StringType()).
                    addScalar("userName", new StringType()).
                    addScalar("imageUser", new StringType()).
                    addScalar("image1", new StringType()).
                    addScalar("image2", new StringType()).
                    addScalar("image3", new StringType()).
                    addScalar("image4", new StringType()).
                    addScalar("image5", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(CommentServiceDto.class));
            commentServiceDtos = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return commentServiceDtos;
    }

    @Override
    public String getCommentByServiceCount(CommentServiceDto commentServiceDto) {
        String sql = "SELECT "+
                "count(*) as total_data "+
                "FROM " +
                "trp_comment_service s " +
                "JOIN "+
                "trp_user c ON c.id = s.user_id "+
                "WHERE s.deleted != '1' AND s.parent_id = '0' AND s.service_id = "+ commentServiceDto.getServiceId() + " "+
                "ORDER BY s.id desc ";
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }


    @Override
    public List<CommentServiceDto> getCommentByCommentIdPagging(CommentServiceDto commentServiceDto) {
        List<CommentServiceDto> commentServiceDtos = new ArrayList<>();
        String sql ="SELECT " +
                "s.id as id, " +
                "s.comment as comment, " +
                "s.date as date, " +
                "s.service_id as serviceId, " +
                "c.image as imageUser, " +
                "s.images_1 as image1, " +
                "s.images_2 as image2, " +
                "s.images_3 as image3, " +
                "s.images_4 as image4, " +
                "s.images_5 as image5, " +
                "c.name as userName " +
                "FROM " +
                "trp_comment_service s " +
                "JOIN "+
                "trp_user c ON c.id = s.user_id "+
                "WHERE s.deleted != '1' AND s.parent_id = "+ commentServiceDto.getId() + " "+
                "ORDER BY s.id asc ";
        if (commentServiceDto.getLength() > 0) {
            sql += "limit 0,"+commentServiceDto.getLength();
        }
        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("serviceId", new LongType()).
                    addScalar("comment", new StringType()).
                    addScalar("date", new StringType()).
                    addScalar("userName", new StringType()).
                    addScalar("imageUser", new StringType()).
                    addScalar("image1", new StringType()).
                    addScalar("image2", new StringType()).
                    addScalar("image3", new StringType()).
                    addScalar("image4", new StringType()).
                    addScalar("image5", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(CommentServiceDto.class));
            commentServiceDtos = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return commentServiceDtos;
    }


    @Override
    public String getCommentByCommentIdCount(CommentServiceDto commentServiceDto) {
        String sql = "SELECT "+
                "count(*) as total_data "+
                "FROM " +
                "trp_comment_service s " +
                "JOIN "+
                "trp_user c ON c.id = s.user_id "+
                "WHERE s.deleted != '1' AND s.parent_id = "+ commentServiceDto.getId() + " "+
                "ORDER BY s.id desc ";
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }


    @Override
    public List<CommentServiceDto> getListDT(CommentServiceDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<CommentServiceDto> commentServiceDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "tcs.id AS id, " +
                        "tcs.comment AS comment, " +
                        "tcs.date AS date, " +
                        "tcs.publish AS publish, " +
                        "ts.service_name AS serviceName, " +
                        "tu.name AS userName " +
                "FROM " +
                    "trp_comment_service tcs " +
                "JOIN " +
                    "trp_service ts on ts.id = tcs.service_id " +
                "JOIN " +
                    "trp_user tu on tu.id = tcs.user_id " +
                "WHERE tcs.deleted != '1' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("comment", new StringType()).
                    addScalar("serviceName", new StringType()).
                    addScalar("userName", new StringType()).
                    addScalar("date", new StringType()).
                    addScalar("publish", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(CommentServiceDto.class));
            commentServiceDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return commentServiceDtoList;
    }

    @Override
    public String getCountListDT(CommentServiceDto dto) {
        String param = getSearchDT(dto);
        String sql ="SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_comment_service tcs "+
                    "WHERE "+
                "tcs.deleted != '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(CommentServiceDto dto){
        String param = "";
        if(dto.getComment() != null && !dto.getComment().equals("")) {
            param += "AND tcs.comment like '%"+ dto.getComment().replace("'","''")+ "%' ";
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }


}
