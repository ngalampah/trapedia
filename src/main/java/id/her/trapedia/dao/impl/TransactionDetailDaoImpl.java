package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.TransactionDetailDao;
import id.her.trapedia.model.TransactionDetailModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TransactionDetailDaoImpl extends BaseDaoImpl<TransactionDetailModel, Long> implements TransactionDetailDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Override
    public List<TransactionDetailModel> findAllByIdTransaction(Long id) {
        List<TransactionDetailModel> transactionDetailModels = new ArrayList<>();
        String sql = "from TransactionDetailModel where transactionModel.id = ?1 ";
        try {
            Query query = createQuery(sql)
                    .setParameter(1, id);
            transactionDetailModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return transactionDetailModels;
    }

    @Override
    public List<TransactionDetailModel> salesByUser(Long userId) {
        List<TransactionDetailModel> transactionDetailModels = new ArrayList<>();
        String sql = "from TransactionDetailModel where serviceModel.userModel.id = ?1 order by id desc";
        try {
            Query query = createQuery(sql)
                    .setParameter(1, userId);
            transactionDetailModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return transactionDetailModels;
    }
}
