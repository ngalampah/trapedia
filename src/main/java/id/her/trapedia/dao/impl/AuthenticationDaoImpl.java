package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.AuthenticationDao;
import id.her.trapedia.model.AdminModel;
import id.her.trapedia.model.UserModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class AuthenticationDaoImpl extends BaseDaoImpl<AdminModel, Long> implements AuthenticationDao {

    private Logger logger = LogManager.getLogger(AuthenticationDaoImpl.class);

    @Override
    public AdminModel authenticate(String username) {
        logger.info("Authenticate for username : " + username);
        AdminModel adminModel = new AdminModel();
        try {
            String sql = "from AdminModel where enabled = '1' and username = ?1";
            logger.info(sql);
            Query query = createQuery(sql)
                    .setParameter(1, username);
            adminModel = (AdminModel) query.uniqueResult();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }
        return adminModel;
    }
}
