package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.WishlistDao;
import id.her.trapedia.model.WishlistModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class WishlistDaoImpl extends BaseDaoImpl<WishlistModel, Long> implements WishlistDao {
    private Logger logger = LogManager.getLogger(WishlistDaoImpl.class);

    @Override
    public WishlistModel cekWishlistByUser(Long serviceDetailId, Long userId) {
        WishlistModel wishlistModel = new WishlistModel();
        try {
            String sql = "from WishlistModel where serviceDetailModel.id = ?1 and userModel.id = ?2";
            Query query = createQuery(sql)
                    .setParameter(1, serviceDetailId)
                    .setParameter(2, userId);
            wishlistModel = (WishlistModel) query.uniqueResult();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }
        return wishlistModel;
    }

    @Override
    public List<WishlistModel> findAllByUser(Long userId) {
        List<WishlistModel> wishlistModelList = new ArrayList<>();
        String sql = "from WishlistModel where userModel.id = ?1";
        try {
            Query query = createQuery(sql)
                    .setParameter(1, userId);
            wishlistModelList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }
        return wishlistModelList;
    }
}
