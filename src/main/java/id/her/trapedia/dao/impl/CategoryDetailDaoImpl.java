package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.CategoryDao;
import id.her.trapedia.dao.CategoryDetailDao;
import id.her.trapedia.dto.CategoryDetailDto;
import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.CategoryDetailModel;
import id.her.trapedia.model.CategoryModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryDetailDaoImpl extends BaseDaoImpl<CategoryDetailModel, Long> implements CategoryDetailDao {

    private Logger logger = LogManager.getLogger(CategoryDetailDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<CategoryDetailModel> findAll() {
        List<CategoryDetailModel> categoryDetailModels = new ArrayList<>();
        String sql = "from CategoryDetailModel where deleted != '1' and publish = '1' order by title";
        try {
            Query query = createQuery(sql);
            categoryDetailModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return categoryDetailModels;
    }

    @Override
    public List<CategoryDetailDto> getListDT(CategoryDetailDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<CategoryDetailDto> categoryDetailDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "cd.id AS id, " +
                        "cd.title AS title, " +
                        "cd.publish AS publish, " +
                        "cc.id AS categoryId, " +
                        "cc.title AS categoryName, " +
                        "cd.description AS description " +
                    "FROM " +
                        "trp_m_category_detail cd " +
                    "JOIN " +
                        "trp_m_category cc on cc.id = cd.category_id " +
                        "WHERE cd.deleted != '1' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("title", new StringType()).
                    addScalar("publish", new StringType()).
                    addScalar("categoryId", new LongType()).
                    addScalar("categoryName", new StringType()).
                    addScalar("description", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(CategoryDetailDto.class));
            categoryDetailDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return categoryDetailDtoList;
    }

    @Override
    public String getCountListDT(CategoryDetailDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                        "count(*) as total_data "+
                    "FROM " +
                        "trp_m_category_detail cd " +
                    "JOIN " +
                        "trp_m_category cc on cc.id = cd.category_id " +
                    "WHERE "+
                        "cd.deleted != '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(CategoryDetailDto dto){
        String param = "";
        if(dto.getTitle() != null && !dto.getTitle().equals("")) {
            param += "AND cd.title like '%"+ dto.getTitle().replace("'","''")+ "%' ";
        }
        if(dto.getPublish() != null && !dto.getPublish().equals("")) {
            param += "AND cd.publish ='"+ dto.getPublish()+ "' ";
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }
}
