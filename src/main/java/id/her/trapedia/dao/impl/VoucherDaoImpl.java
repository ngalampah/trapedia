package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.VoucherDao;
import id.her.trapedia.dto.VoucherDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.VoucherModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class VoucherDaoImpl extends BaseDaoImpl<VoucherModel, Long> implements VoucherDao {

    private Logger logger = LogManager.getLogger(VoucherDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<VoucherModel> findAll() {
        List<VoucherModel> voucherModels = new ArrayList<>();
        String sql = "from VoucherModel where deleted = '0' ";
        try {
            Query query = createQuery(sql);
            voucherModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return voucherModels;
    }

    @Override
    public List<VoucherDto> getListDT(VoucherDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<VoucherDto> voucherDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                "id AS id, " +
                "name AS name, " +
                "voucher_code AS voucherCode, " +
                "amount_type AS amountType, " +
                "amount AS amount, " +
                "max_amount AS maxAmount, " +
                "min_transaction AS minTransaction, " +
                "DATE(valid_date_from) AS validDateFrom, " +
                "DATE(valid_date_to) AS validDateTo, " +
                "publish AS publish, " +
                "deleted AS deleted " +
                "FROM " +
                "trp_voucher " +
                "WHERE deleted = '0' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("name", new StringType()).
                    addScalar("voucherCode", new StringType()).
                    addScalar("amountType", new StringType()).
                    addScalar("amount", new LongType()).
                    addScalar("maxAmount", new LongType()).
                    addScalar("minTransaction", new LongType()).
                    addScalar("validDateFrom", new StringType()).
                    addScalar("validDateTo", new StringType()).
                    addScalar("publish", new StringType()).
                    addScalar("deleted", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(VoucherDto.class));
            voucherDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return voucherDtoList;
    }

    @Override
    public String getCountListDT(VoucherDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                "count(*) as total_data "+
                "FROM "+
                "trp_voucher "+
                "WHERE "+
                "1 = '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(VoucherDto dto){
        String param = "";
        if(dto.getName() != null && !dto.getName().equals("")) {
            param += "AND title like '%"+ dto.getName().replace("'","''")+ "%' ";
        }

        if(dto.getPublish() != null && !dto.getPublish().equals("")) {
            param += "AND publish = '"+ dto.getPublish()+"'";
        }

        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }

    @Override
    public VoucherModel getVoucherByCode(String voucherCode) {
        VoucherModel voucherModel = new VoucherModel();
        try {
            String sql = "from VoucherModel where voucherCode = ?1 and (now() BETWEEN validDateFrom AND validDateTo)";
            logger.info(sql);
            Query query = createQuery(sql)
                    .setParameter(1, voucherCode);
            voucherModel = (VoucherModel) query.uniqueResult();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }
        return voucherModel;
    }
}
