package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.RevenuesDao;
import id.her.trapedia.dao.ReviewDao;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.RevenueDto;
import id.her.trapedia.model.RevenuesModel;
import id.her.trapedia.model.ReviewModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RevenuesDaoImpl extends BaseDaoImpl<RevenuesModel, Long> implements RevenuesDao {

    private Logger logger = LogManager.getLogger(RevenuesDaoImpl.class);
    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<RevenuesModel> findAllByUser(Long userId) {
        List<RevenuesModel> revenuesModels = new ArrayList<>();
        String sql = "from RevenuesModel where userModel.id = ?1 order by id desc";
        try {
            Query query = createQuery(sql)
                    .setParameter(1, userId);
            revenuesModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return revenuesModels;
    }

    @Override
    public List<RevenueDto> getListDT(RevenueDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<RevenueDto> revenueDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "p.id AS id, " +
                        "p.date AS date, " +
                        "p.sale AS sale, " +
                        "p.charge AS charge, " +
                        "p.revenue AS revenue, " +
                        "u.name AS userName " +
                    "FROM " +
                        "trp_revenues p " +
                    "JOIN "+
                        "trp_user u on u.id = p.user_id "+
                    "WHERE 1 = 1 " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("date", new StringType()).
                    addScalar("sale", new LongType()).
                    addScalar("charge", new LongType()).
                    addScalar("revenue", new LongType()).
                    addScalar("userName", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(RevenueDto.class));
            revenueDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return revenueDtoList;
    }

    @Override
    public String getCountListDT(RevenueDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_revenues p "+
                    "WHERE "+
                        "1=1 "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    @Override
    public String getTotalRevenueByUser(Long userId, String status) {
        String sql = "SELECT "+
                        "sum(revenue) as total_data "+
                        "FROM "+
                        "trp_revenues "+
                        "WHERE "+
                        "user_id =  " + userId;

        if(!status.equals("")){
            sql += " AND status = '"+status+"'";
        }
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    @Override
    public String getTotalWidrawByUser(Long userId) {
        String sql = "SELECT "+
                        "sum(amount) as total_data "+
                        "FROM "+
                        "trp_payout "+
                        "WHERE "+
                        "status = 1 AND user_id =  " + userId;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(RevenueDto dto){
        String param = "";
        if(dto.getUserName() != null && !dto.getUserName().equals("")) {
            param += "AND u.name like '%"+ dto.getUserName()+ "'% ";
        }
        if(dto.getDateFrom() != null && !dto.getDateFrom().equals("") &&
                dto.getDateTo() != null && !dto.getDateTo().equals("") ) {
            param += "AND DATE(p.date) BETWEEN '" + dto.getDateFrom()  + "' AND '" + dto.getDateTo() + "' " ;
        }else if(dto.getDateFrom() != null && !dto.getDateFrom().equals("")) {
            param += "AND DATE(p.date) = '" + dto.getDateFrom()  + "' " ;
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }
}
