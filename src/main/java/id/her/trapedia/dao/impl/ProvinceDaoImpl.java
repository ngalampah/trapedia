package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.ProvincesDao;
import id.her.trapedia.dto.ProvincesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.ProvincesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProvinceDaoImpl extends BaseDaoImpl<ProvincesModel, Long> implements ProvincesDao {

    private Logger logger = LogManager.getLogger(CategoryDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<ProvincesModel> findAll() {
        List<ProvincesModel> provincesModels = new ArrayList<>();
        String sql = "from ProvincesModel where deleted = '0' ";
        try {
            Query query = createQuery(sql);
            provincesModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return provincesModels;
    }

    @Override
    public List<ProvincesDto> getListDT(ProvincesDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<ProvincesDto> provincesDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "id AS id, " +
                        "publish AS publish, " +
                        "name AS name " +
                    "FROM " +
                    "trp_m_provinces " +
                "WHERE deleted != '1' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("name", new StringType()).
                    addScalar("publish", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(ProvincesDto.class));
            provincesDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return provincesDtoList;
    }

    @Override
    public String getCountListDT(ProvincesDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_m_provinces "+
                    "WHERE "+
                        "deleted != '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(ProvincesDto dto){
        String param = "";
        if(dto.getName() != null && !dto.getName().equals("")) {
            param += "AND name like '%"+ dto.getName().replace("'","''")+ "%' ";
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }
}
