package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.MessagesDao;
import id.her.trapedia.dao.SubscribeDao;
import id.her.trapedia.dto.MessagesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.MessagesModel;
import id.her.trapedia.model.SubscribeModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MessagesDaoImpl extends BaseDaoImpl<MessagesModel, Long> implements MessagesDao {

    private Logger logger = LogManager.getLogger(MessagesDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;


    @Override
    public List<MessagesModel> getMessageByUser(Long userId) {
        List<MessagesModel> messagesModels = new ArrayList<>();
        String sql = "from MessagesModel where (userModel.id = ?1 OR toUserModel.id = ?1) AND replayId = 0  order by date desc";
        try {
            Query query = createQuery(sql).setParameter(1, userId);;
            messagesModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return messagesModels;
    }

    @Override
    public MessagesModel getMessageDetailByUser(Long userId, Long id) {
        MessagesModel messagesModel = new MessagesModel();

        try {
            String sql = "from MessagesModel where (userModel.id = ?1 OR toUserModel.id = ?1) AND id = ?2 AND replayId = 0";
            logger.info("sql : " + sql);
            Query query = createQuery(sql)
                    .setParameter(1, userId)
                    .setParameter(2, id);
            messagesModel = (MessagesModel) query.uniqueResult();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return messagesModel;
    }

    @Override
    public List<MessagesModel> getMessageDetailListByUser(Long userId, Long id) {
        List<MessagesModel> messagesModels = new ArrayList<>();
//        String sql = "from MessagesModel where (userModel.id = ?1 OR toUserModel.id = ?1) AND replayId = ?2  order by date asc";
        String sql = "from MessagesModel where  replayId = ?1  order by date asc";
        try {
            Query query = createQuery(sql)
//                    .setParameter(1, userId)
                    .setParameter(1, id);
            messagesModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return messagesModels;
    }


    @Override
    public List<MessagesDto> getListDT(MessagesDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<MessagesDto> messagesDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "tm.id AS id, " +
                        "tm.message AS message, " +
                        "tm.date AS date, " +
                        "tm.publish AS publish, " +
                        "tu.name AS fromUser, " +
                        "tu2.name as toUser " +
                    "FROM " +
                        "trp_messages tm " +
                    "JOIN " +
                        "trp_user tu on tu.id = tm.user_id " +
                    "JOIN " +
                        "trp_user tu2 on tu2.id = tm.to_user_id " +
                    "WHERE tm.deleted != '1' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("message", new StringType()).
                    addScalar("date", new StringType()).
                    addScalar("fromUser", new StringType()).
                    addScalar("toUser", new StringType()).
                    addScalar("publish", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(MessagesDto.class));
            messagesDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return messagesDtoList;
    }

    @Override
    public String getCountListDT(MessagesDto dto) {
        String param = getSearchDT(dto);
        String sql ="SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_messages tm "+
                    "WHERE "+
                        "tm.deleted != '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(MessagesDto dto){
        String param = "";
        if(dto.getMessage() != null && !dto.getMessage().equals("")) {
            param += "AND tm.messages like '%"+ dto.getMessage().replace("'","''")+ "%' ";
        }

        if(dto.getPublish() != null && !dto.getPublish().equals("")) {
            param += "AND tm.publish ='"+ dto.getPublish()+ "' ";
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }
}
