package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.AdminDao;
import id.her.trapedia.dao.SubscribeDao;
import id.her.trapedia.dto.AdminDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.SubscribeDto;
import id.her.trapedia.model.AdminModel;
import id.her.trapedia.model.SubscribeModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SubscribeDaoImpl extends BaseDaoImpl<SubscribeModel, Long> implements SubscribeDao {

    private Logger logger = LogManager.getLogger(SubscribeDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<SubscribeDto> getListDT(SubscribeDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<SubscribeDto> subscribeDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                        "email AS email, " +
                        "date AS date " +
                    "FROM " +
                        "trp_subscribe "  +
                    "WHERE 1 = 1 " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("email", new StringType()).
                    addScalar("date", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(SubscribeDto.class));
            subscribeDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return subscribeDtoList;
    }

    @Override
    public String getCountListDT(SubscribeDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                        "count(*) as total_data "+
                    "FROM "+
                        "trp_subscribe "+
                    "WHERE 1 = 1 "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(SubscribeDto dto){
        String param = "";
        if(dto.getEmail() != null && !dto.getEmail().equals("")) {
            param += "AND email like '%"+ dto.getEmail().replace("'","''")+ "%' ";
        }
        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }

}
