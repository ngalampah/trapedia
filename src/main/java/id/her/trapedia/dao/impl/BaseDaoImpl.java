package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.BaseDao;
import id.her.trapedia.dao.SoftDeleteSupport;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

@Repository
public abstract class BaseDaoImpl<ET, ID extends Serializable> implements BaseDao<ET, ID>  {

    private Class<ET> entityClass;

    @Autowired
    private SessionFactory sessionFactory;

    public BaseDaoImpl() {
        Type[] actualTypeArguments = ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments();
        entityClass = (Class<ET>) actualTypeArguments[0];
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    protected Query createQuery(String query) {
        return getSession().createQuery(query);
    }

    protected SQLQuery createNativeQuery(String query) {
        return getSession().createSQLQuery(query);
    }

    public ET get(ID id) {
        return (ET) getSession().get(entityClass, id);
    }

    public ET delete(ID id) {
        try {
            ET entity = (ET) getSession().load(entityClass, id);
            if (SoftDeleteSupport.class.isAssignableFrom(entity.getClass())) {
                ((SoftDeleteSupport) entity)
                        .setDeleted("Y");
            } else {
                getSession().delete(entity);
            }
            return entity;
        } catch (HibernateException e) {
            throw new HibernateException(e);
        }
    }

    public ET save(ET entity) {
        try {
            getSession().saveOrUpdate(entity);
            return entity;
        } catch (HibernateException e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public boolean isExists(ID id) {
        return get(id) != null;
    }

    public void setEntityClass(Class<ET> entityClass) {
        this.entityClass = entityClass;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
