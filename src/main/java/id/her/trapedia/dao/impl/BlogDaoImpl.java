package id.her.trapedia.dao.impl;

import id.her.trapedia.dao.BlogDao;
import id.her.trapedia.dto.BlogDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.SearchBlogDto;
import id.her.trapedia.model.BlogModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BlogDaoImpl extends BaseDaoImpl<BlogModel, Long> implements BlogDao {

    private Logger logger = LogManager.getLogger(BlogDaoImpl.class);

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    @Override
    public List<BlogModel> findAll() {
        List<BlogModel> blogModels = new ArrayList<>();
        String sql = "from BlogModel where deleted = '0' ";
        try {
            Query query = createQuery(sql);
            blogModels = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return blogModels;
    }

    @Override
    public List<BlogDto> getListDT(BlogDto dto, RequestPagingDataTablesBaseDto rdt) {
        List<BlogDto> blogDtoList = new ArrayList<>();
        String param = getSearchDT(dto);
        String limit = getPagging(rdt.getStart(), rdt.getLength());
        String sql ="SELECT " +
                "id AS id, " +
                "content AS content, " +
                "deleted AS deleted, " +
                "images AS images, " +
                "publish AS publish, " +
                "title AS title, " +
                "type AS type " +
                "FROM " +
                "trp_blog " +
                "WHERE deleted = '0' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("content", new StringType()).
                    addScalar("deleted", new StringType()).
                    addScalar("images", new StringType()).
                    addScalar("publish", new StringType()).
                    addScalar("title", new StringType()).
                    addScalar("type", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(BlogDto.class));
            blogDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return blogDtoList;
    }

    @Override
    public String getCountListDT(BlogDto dto) {
        String param = getSearchDT(dto);
        String sql = "SELECT "+
                "count(*) as total_data "+
                "FROM "+
                "trp_blog "+
                "WHERE "+
                "1 = '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchDT(BlogDto dto){
        String param = "";
        if(dto.getTitle() != null && !dto.getTitle().equals("")) {
            param += "AND title like '%"+ dto.getTitle().replace("'","''")+ "%' ";
        }

        if(dto.getPublish() != null && !dto.getPublish().equals("")) {
            param += "AND publish = '"+ dto.getPublish()+"'";
        }

        return param;
    }

    private String getPagging(int page, int length){
        Integer offset = (page);
        return " LIMIT " + offset + ", "+ limit;
    }

    @Override
    public List<BlogDto> getBlogListPublic(SearchBlogDto searchBlogDto) {
        List<BlogDto> blogDtoList = new ArrayList<>();
        String param = getSearchPublic(searchBlogDto);
        String limit = getPaggingPublic(searchBlogDto.getPage(), searchBlogDto.getPerPage());
        String sql ="SELECT " +
                    "id AS id, " +
                    "content AS content, " +
                    "deleted AS deleted, " +
                    "images AS images, " +
                    "publish AS publish, " +
                    "title AS title, " +
                    "date AS date, " +
                    "type AS type " +
                    "FROM " +
                    "trp_blog " +
                "WHERE deleted = '0' " + param + limit;

        logger.info(sql);

        try {
            Query query = createNativeQuery(sql).
                    addScalar("id", new LongType()).
                    addScalar("content", new StringType()).
                    addScalar("date", new StringType()).
                    addScalar("deleted", new StringType()).
                    addScalar("images", new StringType()).
                    addScalar("publish", new StringType()).
                    addScalar("title", new StringType()).
                    addScalar("type", new StringType()).
                    setResultTransformer(Transformers.aliasToBean(BlogDto.class));
            blogDtoList = query.list();
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return blogDtoList;
    }

    @Override
    public String getCountListPublic(SearchBlogDto dto) {
        String param = getSearchPublic(dto);
        String sql = "SELECT "+
                "count(*) as total_data "+
                "FROM "+
                "trp_blog "+
                "WHERE "+
                "1 = '1' "+ param;
        logger.info(sql);
        String result = "0";
        try{
            Query query= createNativeQuery(sql).
                    addScalar("total_data");
            result = query.uniqueResult().toString();
        }   catch (Exception e){
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info("error Exception : " + e);
        }
        return result;
    }

    private String getSearchPublic(SearchBlogDto dto){
        String param = "";
        if(dto.getKeyword() != null && !dto.getKeyword().equals("")) {
            param += "AND title like '%"+ dto.getKeyword().replace("'","''")+ "%' ";
        }

        return param;
    }

    private String getPaggingPublic(int page, int length){
        Integer offset = (page-1) * length;
        return " LIMIT " + offset + ", "+ length;
    }
}
