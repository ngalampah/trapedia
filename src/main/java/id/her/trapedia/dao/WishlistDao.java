package id.her.trapedia.dao;

import id.her.trapedia.model.WishlistModel;

import java.util.List;

public interface WishlistDao extends BaseDao<WishlistModel, Long> {

    WishlistModel cekWishlistByUser(Long serviceDetailId, Long userId);
    List<WishlistModel> findAllByUser(Long userId);

}
