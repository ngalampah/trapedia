package id.her.trapedia.dao;

import org.hibernate.SessionFactory;

import java.io.Serializable;

public interface BaseDao<ET, ID extends Serializable> {

    ET get(ID id);

    ET save(ET entity);

    ET delete(ID id);

    boolean isExists(ID id);

    SessionFactory getSessionFactory();
}