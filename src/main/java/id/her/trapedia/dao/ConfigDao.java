package id.her.trapedia.dao;

import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.UserDto;
import id.her.trapedia.model.ConfigModel;
import id.her.trapedia.model.UserModel;

import java.util.List;

public interface ConfigDao extends BaseDao<ConfigModel, Long> {
    ConfigModel getConfigById(String configName);
    List<ConfigModel> getAllConfig();
}
