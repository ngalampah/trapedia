package id.her.trapedia.dao;

import id.her.trapedia.dto.ReqServiceDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ServiceDto;
import id.her.trapedia.model.ServiceModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.ws.ServiceMode;
import java.util.List;

public interface ServiceDao extends BaseDao<ServiceModel, Long> {

    List<ServiceModel> findAll();
    List<ServiceDto> findAllByUser(Long userId);
    List<ServiceModel> findAll(String parameter, int limit, int offset);
    List<ServiceDto> listPopular();
    List<ServiceDto> listNew();
    List<ServiceDto> listRecomendation(ReqServiceDto reqServiceDto);
    List<ServiceDto> findAllManual(String parameter);
    String getCountAllManual(String parameter);

    List<ServiceDto> getListDT(ServiceDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(ServiceDto dto);

    List<ServiceDto> serviceTrandingMonthTransaction();
    List<ServiceDto> serviceTrandingWeekTransaction();



}
