package id.her.trapedia.dao;

import id.her.trapedia.dto.ProvincesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.model.ProvincesModel;

import java.util.List;

public interface ProvincesDao extends BaseDao<ProvincesModel, Long> {

    List<ProvincesModel> findAll();
    List<ProvincesDto> getListDT(ProvincesDto dto, RequestPagingDataTablesBaseDto rdt);
    String getCountListDT(ProvincesDto dto);

}
