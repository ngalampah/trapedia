package id.her.trapedia.dao;

import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.RevenueDto;
import id.her.trapedia.model.RevenuesModel;
import id.her.trapedia.model.UserBalanceModel;

import java.util.List;

public interface UserBalanceDao extends BaseDao<UserBalanceModel, Long> {
    String getBalanceByUser(Long userId, String status);
    List<UserBalanceModel> findAllByUser(Long userId);
}
