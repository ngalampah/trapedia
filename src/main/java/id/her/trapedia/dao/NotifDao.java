package id.her.trapedia.dao;

import id.her.trapedia.dto.NotifDto;
import id.her.trapedia.model.NotifModel;
import id.her.trapedia.model.ReviewModel;

import java.util.List;

public interface NotifDao extends BaseDao<NotifModel, Long> {

    List<NotifModel> findAllByUser(Long userId);
    List<NotifDto> getNotifByUserPagging(NotifDto notifDto);
    String getNotifByUserCount(Long userId, String status);
    Boolean setIsRead(NotifDto notifDto);


}
