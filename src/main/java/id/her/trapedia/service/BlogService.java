package id.her.trapedia.service;

import id.her.trapedia.dao.BlogDao;
import id.her.trapedia.dto.BlogDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseBlogDto;
import id.her.trapedia.dto.SearchBlogDto;
import id.her.trapedia.model.BlogModel;
import id.her.trapedia.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class BlogService {

    private Logger logger = LogManager.getLogger(BlogService.class);

    @Autowired
    private BlogDao blogDao;

    @Autowired
    private ImageUtils imageUtils;

    public Map<String, Object> findAll() {
        Map<String, Object> response;

        try {
            List<BlogDto> blogDtoList = new ArrayList<>();
            List<BlogModel> blogModelList = blogDao.findAll();
            for (BlogModel categoryModel : blogModelList) {
                BlogDto dto = new BlogDto();
                dto.setId(categoryModel.getId());
                dto.setTitle(categoryModel.getTitle());
                dto.setContent(categoryModel.getContent());
                dto.setDeleted(categoryModel.getDeleted());
                blogDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, "", blogDtoList, blogDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public BlogDto getBlogById(Long id) {
        BlogDto dto = new BlogDto();
        try {
            BlogModel blogModel = blogDao.get(id);
            dto.setId(blogModel.getId());
            dto.setTitle(blogModel.getTitle());
            dto.setContent(blogModel.getContent());
            dto.setType(blogModel.getType());
            dto.setImages(blogModel.getImages());
            dto.setPublish(blogModel.getPublish());
            dto.setDeleted(blogModel.getDeleted());
        } catch (Exception e) {
            return new BlogDto();
        }
        return dto;
    }

    public Map<String, Object> insert(BlogDto dto) {
        Map<String, Object> response;

        try {
            BlogModel model = new BlogModel();
            model.setTitle(dto.getTitle());
            model.setContent(dto.getContent());
            model.setDeleted(Constant.BooleanStatus.FALSE);

            model = blogDao.save(model);

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, model, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> update(BlogDto dto) {
        Map<String, Object> response;

        try {
            BlogModel model = blogDao.get(dto.getId());
            if (model == null) {
                model = new BlogModel();
                model.setTitle(dto.getTitle());
                model.setContent(dto.getContent());
                model.setDeleted(Constant.BooleanStatus.FALSE);
                blogDao.save(model);
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", "");
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.PAGES_DATA_NOT_FOUND + " for id " + dto.getId(), "", "");
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    /*** Function for Web MVC ***/

    public List<BlogDto> list() {
        List<BlogDto> blogDtoList = new ArrayList<>();
        try {
            List<BlogModel> blogModelList = blogDao.findAll();
            for (BlogModel categoryModel : blogModelList) {
                BlogDto dto = new BlogDto();
                dto.setId(categoryModel.getId());
                dto.setTitle(categoryModel.getTitle());
                dto.setContent(categoryModel.getContent());
                dto.setDeleted(categoryModel.getDeleted());
                blogDtoList.add(dto);
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return blogDtoList;
    }

    public Boolean saveDataForm(BlogDto dto) {
        BlogModel blogModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            blogModel = blogDao.get(dto.getId());
        }else{
            blogModel = new BlogModel();
            blogModel.setDate(new Date());
            blogModel.setDeleted(Constant.BooleanStatus.FALSE);
        }

        if (dto.getFile() != null) {
            if (dto.getFile().getOriginalFilename().trim().length() > 0) {
                blogModel.setImages(imageUtils.uploadImage(dto.getFile()));
            }
        }

        blogModel.setTitle(dto.getTitle());
        blogModel.setType(dto.getType());
        blogModel.setContent(dto.getContent());
        blogModel.setPublish(dto.getPublish());
        try {
            blogDao.save(blogModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }

    public BlogDto delete(BlogDto dto) {
        try {
            BlogModel model = blogDao.get(dto.getId());
            model.setDeleted(Constant.BooleanStatus.TRUE);
            blogDao.save(model);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return dto;
    }

    public ResponseBlogDto getListDT(BlogDto adminDto, RequestPagingDataTablesBaseDto rpdt){
        ResponseBlogDto responseBlogDto = new ResponseBlogDto();
        List<BlogDto> listResult = new ArrayList<BlogDto>();
        String sum = "0";
        listResult = blogDao.getListDT(adminDto, rpdt);
        sum = blogDao.getCountListDT(adminDto);
        responseBlogDto.setRecordsTotal(sum);
        responseBlogDto.setData(listResult);
        return responseBlogDto;
    }

    public Map<String, Object> getPublicBlogById(Long id) {
        Map<String, Object> response;
        try {
            BlogModel blogModel = blogDao.get(id);
            BlogDto blogDto = new BlogDto();

            if(blogModel != null){
                blogDto.setId(blogModel.getId());
                blogDto.setContent(blogModel.getContent());
                blogDto.setTitle(blogModel.getTitle());
                blogDto.setImages(blogModel.getImages());
                blogDto.setDate(DateUtils.formatDateToStr(blogModel.getDate(), "yyyy-MM-dd"));
            }
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, blogDto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> getPublicBlogList(SearchBlogDto searchBlogDto){
        Map<String, Object> response;

        try {
            List<BlogDto> blogListPublic = blogDao.getBlogListPublic(searchBlogDto);
            String sum = blogDao.getCountListPublic(searchBlogDto);

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, blogListPublic, Integer.valueOf(sum));
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }
}
