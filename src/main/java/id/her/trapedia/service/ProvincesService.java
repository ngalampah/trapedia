package id.her.trapedia.service;

import id.her.trapedia.dao.ProvincesDao;
import id.her.trapedia.dto.ProvincesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseProvincesDto;
import id.her.trapedia.model.ProvincesModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ResponseGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ProvincesService {
    private Logger logger = LogManager.getLogger(ProvincesService.class);

    @Autowired
    private ProvincesDao provincesDao;

    public Map<String, Object> findAll() {
        Map<String, Object> response = new HashMap<>();

        try {
            List<ProvincesDto> provincesDtoList = new ArrayList<>();
            List<ProvincesModel> provincesModelList = provincesDao.findAll();
            for (ProvincesModel provincesModel : provincesModelList) {
                ProvincesDto dto = new ProvincesDto();
                dto.setId(provincesModel.getId());
                dto.setName(provincesModel.getName());
                provincesDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, provincesDtoList, provincesDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public List<ProvincesDto> getAllProvinces() {
        List<ProvincesDto> provincesDtoList = new ArrayList<>();

        try {
            List<ProvincesModel> provincesModelList = provincesDao.findAll();
            for (ProvincesModel provincesModel : provincesModelList) {
                ProvincesDto dto = new ProvincesDto();
                dto.setId(provincesModel.getId());
                dto.setName(provincesModel.getName());
                provincesDtoList.add(dto);
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }
        return provincesDtoList;
    }

    public ResponseProvincesDto getListDT(ProvincesDto provincesDto, RequestPagingDataTablesBaseDto rpdt){
        ResponseProvincesDto responseProvincesDto = new ResponseProvincesDto();
        List<ProvincesDto> listResult = new ArrayList<ProvincesDto>();
        String sum = "0";
        listResult = provincesDao.getListDT(provincesDto, rpdt);
        sum = provincesDao.getCountListDT(provincesDto);
        responseProvincesDto.setRecordsTotal(sum);
        responseProvincesDto.setData(listResult);
        return responseProvincesDto;
    }

    public ProvincesDto getProvincesByIdAdmin(Long id) {
        ProvincesDto provincesDto = new ProvincesDto();
        try {
            ProvincesModel regenciesModel = provincesDao.get(id);
            provincesDto.setId(regenciesModel.getId());
            provincesDto.setName(regenciesModel.getName());
            provincesDto.setPublish(regenciesModel.getPublish());
            provincesDto.setDeleted(regenciesModel.getDeleted());
        } catch (Exception e) {
            return new ProvincesDto();
        }

        return provincesDto;
    }

    public Boolean saveDataForm(ProvincesDto dto) {
        ProvincesModel provincesModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            provincesModel = provincesDao.get(dto.getId());
        }else{
            provincesModel = new ProvincesModel();
            provincesModel.setDeleted("0");
        }
        provincesModel.setName(dto.getName());
        provincesModel.setPublish(dto.getPublish());
        try {
            provincesDao.save(provincesModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            return false;
        }
    }
}
