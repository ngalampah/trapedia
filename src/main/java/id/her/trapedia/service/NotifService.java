package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.CommentServiceDao;
import id.her.trapedia.dao.NotifDao;
import id.her.trapedia.dao.ServiceDao;
import id.her.trapedia.dao.UserDao;
import id.her.trapedia.dto.CommentServiceDto;
import id.her.trapedia.dto.NotifDto;
import id.her.trapedia.model.CommentServiceModel;
import id.her.trapedia.model.NotifModel;
import id.her.trapedia.model.ServiceModel;
import id.her.trapedia.model.UserModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ImageUtils;
import id.her.trapedia.util.ResponseGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class NotifService {

    private Logger logger = LogManager.getLogger(NotifService.class);

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private NotifDao notifDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    @Autowired
    private ImageUtils imageUtils;

    @Value("${trapedia.url.backend}")
    private String urlBackend;

    @Value("${trapedia.url.frontend}")
    private String urlFrontend;



    public Map<String, Object> getListNotifByUser(NotifDto notifDto, String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            notifDto.setUserId(userModel.getId());
            List<NotifDto> dto = notifDao.getNotifByUserPagging(notifDto);
            String count = notifDao.getNotifByUserCount(userModel.getId(), "");
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dto, Integer.valueOf(count));
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> getCountStatus(NotifDto notifDto, String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            String count = notifDao.getNotifByUserCount(userModel.getId(), notifDto.getIsRead());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, count, Integer.valueOf(count));
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> getDetail(NotifDto notifDto, String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            NotifModel notifModel = notifDao.get(notifDto.getId());
            notifModel.setIsRead("1");
            notifDao.save(notifModel);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> setStatus(NotifDto notifDto, String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            Boolean st = notifDao.setIsRead(notifDto);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, st, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }




    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }

}
