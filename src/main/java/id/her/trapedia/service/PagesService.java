package id.her.trapedia.service;

import id.her.trapedia.dao.PagesDao;
import id.her.trapedia.dto.AdminDto;
import id.her.trapedia.dto.PagesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponsePagesDto;
import id.her.trapedia.model.PagesModel;
import id.her.trapedia.model.PagesModel;
import id.her.trapedia.model.PagesModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ImageUtils;
import id.her.trapedia.util.ResponseGenerator;
import id.her.trapedia.util.TrapediaHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

@Service
@Transactional
public class PagesService {

    private Logger logger = LogManager.getLogger(PagesService.class);

    @Autowired
    private PagesDao pagesDao;

    @Autowired
    private ImageUtils imageUtils;

    public Map<String, Object> findAll() {
        Map<String, Object> response;

        try {
            List<PagesDto> pagesDtoList = new ArrayList<>();
            List<PagesModel> pagesModelList = pagesDao.findAll();
            for (PagesModel categoryModel : pagesModelList) {
                PagesDto dto = new PagesDto();
                dto.setId(categoryModel.getId());
                dto.setTitle(categoryModel.getTitle());
                dto.setContent(categoryModel.getContent());
                dto.setDeleted(categoryModel.getDeleted());
                pagesDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, "", pagesDtoList, pagesDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public PagesDto getPagesById(Long id) {
        PagesDto dto = new PagesDto();
        try {
            PagesModel pagesModel = pagesDao.get(id);
            dto.setId(pagesModel.getId());
            dto.setTitle(pagesModel.getTitle());
            dto.setContent(pagesModel.getContent());
            dto.setType(pagesModel.getType());
            dto.setImages(pagesModel.getImages());
            dto.setPublish(pagesModel.getPublish());
            dto.setDeleted(pagesModel.getDeleted());
        } catch (Exception e) {
            return new PagesDto();
        }
        return dto;
    }

    public Map<String, Object> insert(PagesDto dto) {
        Map<String, Object> response;

        try {
            PagesModel model = new PagesModel();
            model.setTitle(dto.getTitle());
            model.setContent(dto.getContent());
            model.setDeleted(Constant.BooleanStatus.FALSE);

            model = pagesDao.save(model);

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, model, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> update(PagesDto dto) {
        Map<String, Object> response;

        try {
            PagesModel model = pagesDao.get(dto.getId());
            if (model == null) {
                model = new PagesModel();
                model.setTitle(dto.getTitle());
                model.setContent(dto.getContent());
                model.setDeleted(Constant.BooleanStatus.FALSE);
                pagesDao.save(model);
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", "");
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.PAGES_DATA_NOT_FOUND + " for id " + dto.getId(), "", "");
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    /*** Function for Web MVC ***/

    public List<PagesDto> list() {
        List<PagesDto> pagesDtoList = new ArrayList<>();
        try {
            List<PagesModel> pagesModelList = pagesDao.findAll();
            for (PagesModel categoryModel : pagesModelList) {
                PagesDto dto = new PagesDto();
                dto.setId(categoryModel.getId());
                dto.setTitle(categoryModel.getTitle());
                dto.setContent(categoryModel.getContent());
                dto.setDeleted(categoryModel.getDeleted());
                pagesDtoList.add(dto);
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return pagesDtoList;
    }

    public Boolean saveDataForm(PagesDto dto) {
        PagesModel pagesModel;

        if (dto.getId() != null && !dto.getId().equals("")) {
            pagesModel = pagesDao.get(dto.getId());
        } else {
            pagesModel = new PagesModel();
            pagesModel.setDate(new Date());
            pagesModel.setDeleted(Constant.BooleanStatus.FALSE);
        }

        if (dto.getFile() != null) {
            if (dto.getFile().getOriginalFilename().trim().length() > 0) {
                pagesModel.setImages(imageUtils.uploadImage(dto.getFile()));
            }
        }

        pagesModel.setTitle(dto.getTitle());
        pagesModel.setType(dto.getType());
        pagesModel.setContent(dto.getContent());
        pagesModel.setPublish(dto.getPublish());

        try {
            pagesDao.save(pagesModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }

    public PagesDto delete(PagesDto dto) {
        try {
            PagesModel model = pagesDao.get(dto.getId());
            model.setDeleted(Constant.BooleanStatus.TRUE);
            pagesDao.save(model);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return dto;
    }

    public ResponsePagesDto getListDT(PagesDto adminDto, RequestPagingDataTablesBaseDto rpdt) {
        ResponsePagesDto responsePagesDto = new ResponsePagesDto();
        List<PagesDto> listResult = new ArrayList<PagesDto>();
        String sum = "0";
        listResult = pagesDao.getListDT(adminDto, rpdt);
        sum = pagesDao.getCountListDT(adminDto);
        responsePagesDto.setRecordsTotal(sum);
        responsePagesDto.setData(listResult);
        return responsePagesDto;
    }

    public Map<String, Object> getServiceByType(String type) {
        Map<String, Object> response;
        try {
            List<PagesDto> pagesDtoList = pagesDao.getPageByType(type);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, pagesDtoList, pagesDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> getServiceById(Long id) {
        Map<String, Object> response;
        try {
            PagesModel pagesModel = pagesDao.get(id);
            PagesDto pagesDto = new PagesDto();

            if(pagesModel != null){
                pagesDto.setId(pagesModel.getId());
                pagesDto.setContent(pagesModel.getContent());
                pagesDto.setTitle(pagesModel.getTitle());
                pagesDto.setPublish(pagesModel.getPublish());
                pagesDto.setImages(pagesModel.getImages());
                pagesDto.setType(pagesModel.getType());
            }
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, pagesDto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }
}
