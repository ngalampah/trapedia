package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.*;
import id.her.trapedia.dto.PayoutDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponsePayoutDto;
import id.her.trapedia.dto.RevenueDto;
import id.her.trapedia.model.PayoutModel;
import id.her.trapedia.model.RevenuesModel;
import id.her.trapedia.model.UserBalanceModel;
import id.her.trapedia.model.UserModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ResponseGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class PayoutService {

    @Autowired
    private TransactionDao transactionDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserBalanceDao userBalanceDao;

    @Autowired
    private PayoutDao payoutDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    @Value("${trapedia.fee.admin}")
    private Long feeAdmin;

    private Logger logger = LogManager.getLogger(PayoutService.class);

    public Map<String, Object> insert(PayoutDto payoutDto, String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            PayoutModel payoutModel = new PayoutModel();
            payoutModel.setDate(new Date());
            payoutModel.setAmount(payoutDto.getAmount());
            payoutModel.setStatus("0");
            payoutModel.setBankName(userModel.getBankName());
            payoutModel.setBankNumber(userModel.getBankNumber());
            payoutModel.setBankAccountName(userModel.getBankAccountName());
            payoutModel.setUserModel(userModel);
            payoutDao.save(payoutModel);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }


    public Map<String, Object> payoutByUser(String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            List<PayoutModel> payoutModels = payoutDao.findAllByUser(userModel.getId());
            List<PayoutDto> payoutDtos = new ArrayList<>();
            for (PayoutModel model : payoutModels) {
                PayoutDto dto = new PayoutDto();
                dto.setId(model.getId());
                dto.setDate(DateUtils.formatDateToStr(model.getDate(), "yyyy-MM-dd"));
                dto.setAmount(model.getAmount());
                dto.setBankAccountName(model.getBankAccountName());
                dto.setBankName(model.getBankName());
                dto.setBankNumber(model.getBankNumber());
                dto.setStatus(model.getStatus());
                payoutDtos.add(dto);
            }
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, payoutDtos, payoutDtos.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }

    public ResponsePayoutDto getListDT(PayoutDto payoutDto, RequestPagingDataTablesBaseDto rpdt){
        ResponsePayoutDto responseServiceDto = new ResponsePayoutDto();
        List<PayoutDto> listResult = new ArrayList<PayoutDto>();
        String sum = "0";
        listResult = payoutDao.getListDT(payoutDto, rpdt);
        sum = payoutDao.getCountListDT(payoutDto);
        responseServiceDto.setRecordsTotal(sum);
        responseServiceDto.setData(listResult);
        return responseServiceDto;
    }

    public PayoutDto getPayoutByIdAdmin(Long id) {
        PayoutDto dto = new PayoutDto();
        try {
            PayoutModel payoutModel = payoutDao.get(id);
            dto.setId(payoutModel.getId());
            dto.setBankAccountName(payoutModel.getBankAccountName());
            dto.setBankName(payoutModel.getBankName());
            dto.setBankNumber(payoutModel.getBankNumber());
            dto.setAmount(payoutModel.getAmount());
            dto.setUserName(payoutModel.getUserModel().getName());
            dto.setDate(DateUtils.formatDateToStr(payoutModel.getDate(), "yyyy-MM-dd"));
        } catch (Exception e) {
            return new PayoutDto();
        }

        return dto;
    }

    public Boolean setApproveService(PayoutDto dto) {
        PayoutModel payoutModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            payoutModel = payoutDao.get(dto.getId());
        }else{
            return false;
        }
        try {
            if(payoutModel.getStatus().equals("0")){
                payoutModel.setStatus(dto.getStatus());
                payoutModel = payoutDao.save(payoutModel);
                UserBalanceModel userBalanceModel = new UserBalanceModel();
                UserModel userModel = userDao.get(payoutModel.getUserModel().getId());
                userBalanceModel.setBalance(payoutModel.getAmount());
                userBalanceModel.setUserModel(userModel);
                userBalanceModel.setBalanceType("K");
                userBalanceModel.setDate(new Date());
                userBalanceModel.setNotes("Request payout");
                userBalanceDao.save(userBalanceModel);
            }

            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }

}
