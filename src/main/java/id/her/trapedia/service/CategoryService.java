package id.her.trapedia.service;

import id.her.trapedia.dao.CategoryDao;
import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.ProvincesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseCategoryDto;
import id.her.trapedia.model.CategoryModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ResponseGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CategoryService {
    @Autowired
    CategoryDao categoryDao;

    private Logger logger = LogManager.getLogger(CategoryService.class);

    public List<CategoryDto> getAllCategory() {
        List<CategoryDto> categoryDtoList = new ArrayList<>();

        try {
            List<CategoryModel> categoryModelList = categoryDao.findAll();
            for (CategoryModel categoryModel : categoryModelList) {
                CategoryDto dto = new CategoryDto();
                dto.setId(categoryModel.getId());
                dto.setTitle(categoryModel.getTitle());
                dto.setImages(categoryModel.getImages());
                dto.setDescription(categoryModel.getDescription());
                dto.setDeleted(categoryModel.getDeleted());
                categoryDtoList.add(dto);
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }
        return categoryDtoList;
    }

    public Map<String, Object> findAll() {
        Map<String, Object> response = new HashMap<>();

        try {
            List<CategoryDto> categoryDtoList = new ArrayList<>();
            List<CategoryModel> categoryModelList = categoryDao.findAll();
            for (CategoryModel categoryModel : categoryModelList) {
                CategoryDto dto = new CategoryDto();
                dto.setId(categoryModel.getId());
                dto.setTitle(categoryModel.getTitle());
                dto.setImages(categoryModel.getImages());
                dto.setDescription(categoryModel.getDescription());
                dto.setDeleted(categoryModel.getDeleted());
                categoryDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, "", categoryDtoList, categoryDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> getCategoryById(Long id) {
        Map<String, Object> response = new HashMap<>();

        try {
            CategoryModel categoryModel = categoryDao.get(id);
            CategoryDto dto = new CategoryDto();
            dto.setId(categoryModel.getId());
            dto.setTitle(categoryModel.getTitle());
            dto.setImages(categoryModel.getImages());
            dto.setDescription(categoryModel.getDescription());
            dto.setDeleted(categoryModel.getDeleted());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, "", dto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public ResponseCategoryDto getListDT(CategoryDto categoryDto,
                                           RequestPagingDataTablesBaseDto requestPagingDataTablesBaseDto){
        ResponseCategoryDto responseUserDto = new ResponseCategoryDto();
        List<CategoryDto> listResult = new ArrayList<CategoryDto>();
        String sum = "0";
        listResult = categoryDao.getListDT(categoryDto, requestPagingDataTablesBaseDto);
        sum = categoryDao.getCountListDT(categoryDto);
        responseUserDto.setRecordsTotal(sum);
        responseUserDto.setData(listResult);
        return responseUserDto;
    }

    public CategoryDto getCategoriByIdAdmin(Long id) {
        Map<String, Object> response = new HashMap<>();
        CategoryDto dto = new CategoryDto();
        try {
            CategoryModel categoryModel = categoryDao.get(id);
            dto.setId(categoryModel.getId());
            dto.setTitle(categoryModel.getTitle());
            dto.setImages(categoryModel.getImages());
            dto.setDescription(categoryModel.getDescription());
            dto.setDeleted(categoryModel.getDeleted());
            dto.setPublish(categoryModel.getPublish());
        } catch (Exception e) {
            return new CategoryDto();
        }

        return dto;
    }

    public Boolean saveDataForm(CategoryDto dto) {
        CategoryModel categoryModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            categoryModel = categoryDao.get(dto.getId());
        }else{
            categoryModel = new CategoryModel();
            categoryModel.setDeleted("0");
        }
        categoryModel.setTitle(dto.getTitle());
        categoryModel.setDescription(dto.getDescription());
        categoryModel.setImages(dto.getImages());
        categoryModel.setPublish(dto.getPublish());
        try {
            categoryDao.save(categoryModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }


}
