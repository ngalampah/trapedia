package id.her.trapedia.service;

import freemarker.template.TemplateException;
import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.AuthenticationDao;
import id.her.trapedia.dao.UserDao;
import id.her.trapedia.dao.VerificationTokenDao;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.AdminModel;
import id.her.trapedia.model.UserModel;
import id.her.trapedia.model.VerificationTokenModel;
import id.her.trapedia.util.*;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;

@Service
@Transactional
public class UserService {

    private Logger logger = LogManager.getLogger(UserService.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private AuthenticationDao authenticationDao;

    @Autowired
    private VerificationTokenDao verificationTokenDao;

    @Autowired
    private SendingEmailService emailService;

    @Autowired
    public PasswordEncoder bcryptEncoder;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    @Autowired
    private ImageUtils imageUtils;

    @Value("${trapedia.url.backend}")
    private String urlBackend;

    @Value("${trapedia.url.frontend}")
    private String urlFrontend;

    public Map<String, Object> register(UserDto user) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(user.getEmail());

            if (userModel != null) {
                return ResponseGenerator.generate(Constant.HTTPCode.HTTP_409, Constant.STATUS.USER_ALREADY_EXIST + " for email " + user.getEmail(), "", "");
            }

            userModel = new UserModel();
            userModel.setName(user.getName());
            userModel.setEmail(user.getEmail());
            userModel.setPhone(user.getPhone());
            userModel.setPassword(bcryptEncoder.encode(user.getPassword()));
            userModel.setActivation(Constant.BooleanStatus.FALSE);
            userModel.setDeleted(Constant.BooleanStatus.FALSE);
            userModel = userDao.save(userModel);

            VerificationTokenModel verificationTokenModel = new VerificationTokenModel(userModel);
            verificationTokenDao.save(verificationTokenModel);

            EmailDto email = new EmailDto();
            email.setFrom(Constant.DEFAULT_EMAIL);
            email.setTo(userModel.getEmail());
            email.setSubject("Complete Registration");
            email.setTemplate(Constant.EMAIL_TEMPLATE.USER_ACTIVATION);
            email.setLogo(Constant.TRAPEDIA_LOGO.WHITE);

            Map model = new HashMap();
            model.put("name", userModel.getName());
            model.put("actionUrl", urlBackend+ "/admin/user/activation?token=" + verificationTokenModel.getToken());

            email.setModel(model);

            try {
                emailService.sendEmail(email);
            } catch (MessagingException e) {
                logger.error("Error while send invoice mail : " + e.getMessage());
            } catch (IOException e) {
                logger.error("Error while send invoice mail : " + e.getMessage());
            } catch (TemplateException e) {
                logger.error("Error while send invoice mail : " + e.getMessage());
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, userModel, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> editProfile(UserDto user, MultipartFile file, String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            if (userModel != null) {
                userModel.setName(user.getName());
                userModel.setPhone(user.getPhone());
                userModel.setGender(user.getGender());
                userModel.setAddress(user.getAddress());
                userModel.setDob(DateUtils.convertStringToDate(user.getDob(), "yyyy-mm-dd"));

                if (file != null && !file.isEmpty()) {
                    userModel.setImage(imageUtils.uploadImage(file));
                }

                userModel = userDao.save(userModel);

                user = setUserDto(userModel);

                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, user, 1);
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, Constant.STATUS.USER_NOT_FOUND, user, 1);
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> deleteImageProfile(UserDto user, String authorization) {
        Map<String, Object> response;
        Boolean status = imageUtils.deleteImage(user.getImageDel());
        if (status) {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            userModel.setImage("");
            userDao.save(userModel);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);
        } else
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, Constant.STATUS.ERROR, "", "");
        return response;
    }

    public Map<String, Object> changePassword(UserDto user, String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            if (userModel != null) {
                userModel.setPassword(bcryptEncoder.encode(user.getPassword()));
                userModel = userDao.save(userModel);
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, userModel, 1);
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, Constant.STATUS.USER_NOT_FOUND, user, 1);
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> changeBank(UserDto user, String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            if (userModel != null) {
                userModel.setBankName(user.getBankName());
                userModel.setBankNumber(user.getBankNumber());
                userModel.setBankAccountName(user.getBankAccountName());
                userModel = userDao.save(userModel);
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, userModel, 1);
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, Constant.STATUS.USER_NOT_FOUND, user, 1);
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }

    public Map<String, Object> userProfile(String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            UserDto userDto = setUserDto(userModel);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, userDto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public AdminDto authenticate(String username, String password) {
        AdminDto adminDto = null;
        try {
            AdminModel adminModel = authenticationDao.authenticate(username);
            if (adminModel != null) {
                if (bcryptEncoder.matches(password, adminModel.getPassword())) {
                    logger.info("Username and password is authenticated");
                    adminDto = new AdminDto();
                    adminDto.setUsername(adminModel.getUsername());
                    adminDto.setPassword(adminModel.getPassword());
                    adminDto.setEnabled(adminModel.getEnabled());
                }
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return adminDto;
    }

    public ResponseUserDto listUser(UserDto userDto, RequestPagingDataTablesBaseDto dataTablesBaseDto) {
        ResponseUserDto responseUserDto = new ResponseUserDto();
        List<UserDto> userDtoList = new ArrayList<UserDto>();
        Integer limit = dataTablesBaseDto.getLength();
        Integer offset = 0;
        if (dataTablesBaseDto.getStart() > 0) {
            offset = (dataTablesBaseDto.getStart() - 1) * limit;
        }
        userDtoList = userDao.listUser(userDto, limit, offset);
        responseUserDto.setRecordsTotal(String.valueOf(userDtoList.size()));
        responseUserDto.setData(userDtoList);

        return responseUserDto;
    }

    private UserDto setUserDto(UserModel model) {
        UserDto user = new UserDto();
        user.setId(model.getId());
        user.setName(model.getName());
        user.setEmail(model.getEmail());
        user.setAbout(model.getAbout());
//        user.setPassword(model.getPassword());
        user.setImage(model.getImage());
        user.setPhone(model.getPhone());
        user.setGender(model.getGender());
        user.setAddress(model.getAddress());
        user.setDob(DateUtils.formatDateToStr(model.getDob(), "yyyy-MM-dd"));
        user.setActivation(model.getActivation());
        user.setBankName(model.getBankName());
        user.setBankNumber(model.getBankNumber());
        user.setBankAccountName(model.getBankAccountName());
        user.setDeleted(model.getDeleted());
        user.setIsPartnerUser(model.getIsPartnerUser());
        user.setIsVerifiedUser(model.getIsVerifiedUser());
        return user;
    }

    public ResponseUserDto getListDT(UserDto userDto, RequestPagingDataTablesBaseDto rpdt) {
        ResponseUserDto responseUserDto = new ResponseUserDto();
        List<UserDto> listResult = new ArrayList<UserDto>();
        String sum = "0";
        listResult = userDao.getListDT(userDto, rpdt);
        sum = userDao.getCountListDT(userDto);
        responseUserDto.setRecordsTotal(sum);
        responseUserDto.setData(listResult);
        return responseUserDto;
    }

    public UserDto getUserByIdAdmin(Long id) {
        Map<String, Object> response = new HashMap<>();
        UserDto dto = new UserDto();
        try {
            UserModel userModel = userDao.get(id);
            dto = setUserDto(userModel);
        } catch (Exception e) {
            return new UserDto();
        }

        return dto;
    }

    public Boolean saveDataProfileForm(UserDto dto) {
        UserModel userModel;

        if (dto.getId() != null && !dto.getId().equals("")) {
            userModel = userDao.get(dto.getId());
        } else {
            userModel = new UserModel();
            userModel.setDeleted("0");
        }
        userModel.setName(dto.getName());
        userModel.setAddress(dto.getAddress());
        userModel.setGender(dto.getGender());
        userModel.setPhone(dto.getPhone());
        userModel.setIsVerifiedUser(dto.getIsVerifiedUser());
        userModel.setIsPartnerUser(dto.getIsPartnerUser());
        try {
            userDao.save(userModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }

    public Map<String, Object> userProfilePublic(UserDto userDto) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.get(userDto.getId());
            UserDto dd = new UserDto();
            dd.setId(userModel.getId());
            dd.setAbout(userModel.getAbout());
            dd.setName(userModel.getName());
            dd.setAbout(userModel.getAbout());
            dd.setImage(userModel.getImage());
            dd.setIsVerifiedUser(userModel.getIsVerifiedUser());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dd, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> requestResetPassword (String username) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(username);
            if (userModel != null) {
                EmailDto email = new EmailDto();
                email.setFrom(Constant.DEFAULT_EMAIL);
                email.setTo(userModel.getEmail());
                email.setSubject("Request for Reset Password");
                email.setTemplate(Constant.EMAIL_TEMPLATE.REQ_RESET_PASSWORD);
                email.setLogo(Constant.TRAPEDIA_LOGO.WHITE);

                Map model = new HashMap();
                model.put("name", userModel.getName());
                model.put("actionUrl", urlBackend + "/admin/user/resetPassword?username=" + userModel.getEmail());

                email.setModel(model);

                try {
                    emailService.sendEmail(email);
                } catch (MessagingException e) {
                    logger.error("Error while send invoice mail : " + e.getMessage());
                } catch (IOException e) {
                    logger.error("Error while send invoice mail : " + e.getMessage());
                } catch (TemplateException e) {
                    logger.error("Error while send invoice mail : " + e.getMessage());
                }

                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, userModel.getEmail(), 1);
            } else {
                logger.info("Username " + username + " does not exist!");
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, "Username " + username + " does not exist!", "", "");
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public String resetPassword(String username) {
        String response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(username);
            String newPassword = TrapediaHelper.generateRandomPassword(6);
            if (userModel != null) {
                userModel.setPassword(bcryptEncoder.encode(newPassword));
                userModel = userDao.save(userModel);

                EmailDto email = new EmailDto();
                email.setFrom(Constant.DEFAULT_EMAIL);
                email.setTo(userModel.getEmail());
                email.setSubject("Reset Password Successful");
                email.setTemplate(Constant.EMAIL_TEMPLATE.RESET_PASSWORD);
                email.setLogo(Constant.TRAPEDIA_LOGO.WHITE);

                Map model = new HashMap();
                model.put("name", userModel.getName());
                model.put("newPassword", newPassword);

                email.setModel(model);

                try {
                    emailService.sendEmail(email);
                } catch (MessagingException e) {
                    logger.error("Error while send invoice mail : " + e.getMessage());
                } catch (IOException e) {
                    logger.error("Error while send invoice mail : " + e.getMessage());
                } catch (TemplateException e) {
                    logger.error("Error while send invoice mail : " + e.getMessage());
                }

                response = Constant.STATUS.SUCCESS;
            } else {
                logger.info("Username " + username + " does not exist!");
                response = Constant.STATUS.ERROR;
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = Constant.STATUS.ERROR;
        }
        return response;
    }
}
