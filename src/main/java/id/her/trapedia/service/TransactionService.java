package id.her.trapedia.service;

import freemarker.template.TemplateException;
import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.*;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.*;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ResponseGenerator;
import id.her.trapedia.util.TrapediaHelper;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;

@Service
@Transactional
public class TransactionService {

    @Autowired
    private RevenuesDao revenuesDao;

    @Autowired
    private UserBalanceDao userBalanceDao;

    @Autowired
    private TransactionDao transactionDao;

    @Autowired
    private TransactionDetailDao transactionDetailDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private ServiceDetailDao serviceDetailDao;

    @Autowired
    private VoucherDao voucherDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    @Autowired
    private NotifDao notifDao;

    @Value("${trapedia.fee.admin}")
    private Long feeAdmin;

    @Value("${trapedia.charge}")
    private Long charge;

    @Value("${trapedia.url.backend}")
    private String urlBackend;

    @Value("${trapedia.url.frontend}")
    private String urlFrontend;

    @Autowired
    private SendingEmailService emailService;

    private Logger logger = LogManager.getLogger(TransactionService.class);

    public Map<String, Object> cancel(ReqTransactionDto reqTransactionDto) {
        Map<String, Object> response;

        try {
            TransactionModel transactionModel = transactionDao.get(reqTransactionDto.getId());

            if(transactionModel.getStatusTransaction().equals("0")){
                transactionModel.setStatusPayment("2");
                transactionModel.setStatusTransaction("2");
                transactionModel.setNotesTransaction(reqTransactionDto.getNotesTransaction());
                transactionModel = transactionDao.save(transactionModel);

                UserModel userModel = userDao.get(transactionModel.getUserModel().getId());

                UserBalanceModel userBalanceModel = new UserBalanceModel();
                userBalanceModel.setBalance(transactionModel.getTotal());
                userBalanceModel.setUserModel(userModel);
                userBalanceModel.setBalanceType("D");
                userBalanceModel.setDate(new Date());
                userBalanceModel.setNotes("Cancel transaction #" + transactionModel.getInvoiceNumber());
                userBalanceDao.save(userBalanceModel);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "OK", 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> insert(ReqTransactionDto reqTransactionDto) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.get(reqTransactionDto.getUserId());
//            List<TransactionModel> transactionModels = transactionDao.findAll();
            String sum = transactionDao.getCountListAll();

            TransactionModel transactionModel = new TransactionModel();
            Long subTotal = 0L;
            Long total = 0L;
            long discount = 0;

            for (ReqTransactionDetailDto dto : reqTransactionDto.getFormDetailCart()) {
                subTotal += dto.getPrice() * dto.getQty();
            }
            total = subTotal + feeAdmin;

            //check discount
            if(!reqTransactionDto.getVoucherCode().equals("")){
                VoucherModel voucherModel = voucherDao.getVoucherByCode(reqTransactionDto.getVoucherCode());

                if(voucherModel != null){

                    if(voucherModel.getMinTransaction() <= total){
                        if(voucherModel.getAmountType().equals("FIXED")){
                            if(voucherModel.getMaxAmount() <= total){
                                discount = voucherModel.getMaxAmount();
                            }else{
                                if(voucherModel.getAmount() >= total){
                                    discount = total;
                                }else{
                                    discount = voucherModel.getAmount();
                                }
                            }
                        }else{
                            long dis =  total - (voucherModel.getAmount() / 100 * total);
                            if(voucherModel.getMaxAmount() <= dis){
                                discount = voucherModel.getMaxAmount();
                            }else{
                                if(dis >= total){
                                    discount = total;
                                }else{
                                    discount = dis;
                                }
                            }
                        }
                    }
                }

                if(discount > 0){
                    transactionModel.setVoucherCode(reqTransactionDto.getVoucherCode());
                }
            }


            Long uniquePayment = TrapediaHelper.uniqueCodeTrx(Long.valueOf(sum) + 1L);
            System.out.println("================================");
            System.out.println(Long.valueOf(sum));
            System.out.println(uniquePayment);
            String numberInvoice = TrapediaHelper.setNumberInvoice(Long.valueOf(sum) + 1L);

            total = total - discount;

            if(total == 0){
                transactionModel.setStatusPayment("1");
            }else{
                transactionModel.setStatusPayment("0");
            }

            transactionModel.setDiscount(discount);
            transactionModel.setInvoiceNumber(numberInvoice);
            transactionModel.setTravelingName(reqTransactionDto.getTravelingName());
            transactionModel.setTravelingEmail(reqTransactionDto.getTravelingEmail());
            transactionModel.setTravelingPhone(reqTransactionDto.getTravelingPhone());
            transactionModel.setFeeAdmin(feeAdmin);
            transactionModel.setSummaryService(subTotal);
            transactionModel.setTotal(total);
            transactionModel.setPaymentCode(reqTransactionDto.getPaymentCode());
            transactionModel.setPaymentChannel(reqTransactionDto.getPaymentChannel());
            transactionModel.setPaymentMethod(reqTransactionDto.getPaymentMethode());
            transactionModel.setPaymentAmount(total + uniquePayment);
            transactionModel.setUserModel(userModel);
            transactionModel.setDate(new Date());
            transactionModel.setStatusTransaction("0");
            transactionModel = transactionDao.save(transactionModel);


            UserModel userModelService = null;

            for (ReqTransactionDetailDto dto : reqTransactionDto.getFormDetailCart()) {
                ServiceModel serviceModel = serviceDao.get(dto.getServiceId());
                ServiceDetailModel serviceDetailModel = serviceDetailDao.get(dto.getDetailServiceId());

                TransactionDetailModel transactionDetailModel = new TransactionDetailModel();
                transactionDetailModel.setServiceModel(serviceModel);
                transactionDetailModel.setServiceDetailModel(serviceDetailModel);
                transactionDetailModel.setTransactionModel(transactionModel);

                transactionDetailModel.setServiceName(dto.getServiceName());
                transactionDetailModel.setServiceDetailName(dto.getServiceNameDetail());
                transactionDetailModel.setNotes(dto.getNotes());
                transactionDetailModel.setPrice(dto.getPrice());
                transactionDetailModel.setQty(dto.getQty());
                transactionDetailModel.setDateVisit(DateUtils.convertStringToDate(dto.getDate(), "yyyy-MM-dd"));
                transactionDetailModel.setTotal(dto.getQty() * dto.getPrice());

                transactionDetailModel = transactionDetailDao.save(transactionDetailModel);

                userModelService = userDao.get(serviceModel.getUserModel().getId());

                // Set total
                dto.setTotal(dto.getQty() * dto.getPrice());
            }
            reqTransactionDto.setId(transactionModel.getId());

            if(total == 0){
                double revenue = subTotal - (charge/100.0f * (double) subTotal);
                RevenuesModel revenuesModel = new RevenuesModel();
                revenuesModel.setSale(subTotal);
                revenuesModel.setCharge(charge);
                revenuesModel.setRevenue((long) Math.ceil(revenue));
                revenuesModel.setDate(new Date());
                revenuesModel.setUserModel(transactionModel.getUserModel());
                revenuesModel.setTransactionModel(transactionModel);
                revenuesDao.save(revenuesModel);
            }

            //SEND NOTIFIKASI TRANSACTION
            NotifModel notifModel = new NotifModel();
            notifModel.setDate(new Date());
            notifModel.setDeleted("0");
            notifModel.setIsRead("0");
            notifModel.setPublish("1");
            notifModel.setUrl(urlFrontend + "/member/booking/payment/" + transactionModel.getId());
            notifModel.setNotif(userModel.getName() + " booking your service");
            notifModel.setUserModel(userModelService);
            notifDao.save(notifModel);


            //send email
            EmailDto email = new EmailDto();
            email.setFrom(Constant.DEFAULT_EMAIL);
            email.setTo(transactionModel.getTravelingEmail());
            email.setSubject("Waiting for Payment " + transactionModel.getInvoiceNumber());
            email.setTemplate(Constant.EMAIL_TEMPLATE.INVOICE);
            email.setLogo(Constant.TRAPEDIA_LOGO.WHITE);
            email.setBankCode(transactionModel.getPaymentChannel());

            Map model = new HashMap();
            model.put("name", transactionModel.getTravelingName());
            model.put("paymentMethode", transactionModel.getPaymentMethod());
            model.put("paymentCode", transactionModel.getPaymentCode());
            model.put("paymentStatus", TrapediaHelper.paymentStatus(transactionModel.getStatusPayment()));
            model.put("paymentAmount", transactionModel.getPaymentAmount());
            model.put("dueDate", TrapediaHelper.dueDate(transactionModel.getDate()));
            model.put("paymentReference", transactionModel.getInvoiceNumber());
            model.put("date", DateUtils.formatDateToStr(transactionModel.getDate(), "dd MMM yyyy HH:mm"));
            model.put("services", reqTransactionDto.getFormDetailCart());
            model.put("total", transactionModel.getTotal());
            model.put("adminFee", transactionModel.getFeeAdmin());
            model.put("discount", transactionModel.getDiscount());
            model.put("uniqueCode", (transactionModel.getPaymentAmount()-transactionModel.getTotal()));

            email.setModel(model);

            try {
                emailService.sendEmail(email);
            } catch (MessagingException e) {
                logger.error("Error while send invoice mail : " + e.getMessage());
            } catch (IOException e) {
                logger.error("Error while send invoice mail : " + e.getMessage());
            } catch (TemplateException e) {
                logger.error("Error while send invoice mail : " + e.getMessage());
            }


            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, reqTransactionDto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> getTranById(Long id, String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            TransactionModel transactionModel = transactionDao.get(id);
            List<TransactionDetailModel> transactionDetailModels = transactionDetailDao.findAllByIdTransaction(transactionModel.getId());


            if (transactionModel != null &&
                    (transactionModel.getUserModel().getId().equals(userModel.getId())) ||
                    transactionDetailModels.get(0).getServiceModel().getUserModel().getId().equals(userModel.getId()) ){
                ReqTransactionDto reqTransactionDto = new ReqTransactionDto();
                reqTransactionDto.setId(transactionModel.getId());
                reqTransactionDto.setTravelingEmail(transactionModel.getTravelingEmail());
                reqTransactionDto.setTravelingName(transactionModel.getTravelingName());
                reqTransactionDto.setTravelingPhone(transactionModel.getTravelingPhone());
                reqTransactionDto.setUserId(transactionModel.getUserModel().getId());
                reqTransactionDto.setPaymentMethode(transactionModel.getPaymentMethod());
                reqTransactionDto.setPaymentChannel(transactionModel.getPaymentChannel());
                reqTransactionDto.setPaymentCode(transactionModel.getPaymentCode());
                reqTransactionDto.setPaymentAmount(transactionModel.getPaymentAmount());
                reqTransactionDto.setStatusPayment(transactionModel.getStatusPayment());
                reqTransactionDto.setFeeAdmin(transactionModel.getFeeAdmin());
                reqTransactionDto.setDiscount(transactionModel.getDiscount());
                reqTransactionDto.setSummaryService(transactionModel.getSummaryService());
                reqTransactionDto.setTotal(transactionModel.getTotal());
                reqTransactionDto.setNumberTransaction(transactionModel.getInvoiceNumber());
                reqTransactionDto.setDate(DateUtils.formatDateToStr(transactionModel.getDate(),"yyyy-MM-dd"));
                reqTransactionDto.setStatusTransaction(transactionModel.getStatusTransaction());

                List<ReqTransactionDetailDto> dtos = new ArrayList<>();
                for (TransactionDetailModel model : transactionDetailModels) {
                    ReqTransactionDetailDto reqServiceDetailDto = new ReqTransactionDetailDto();
                    reqServiceDetailDto.setId(model.getId());
                    reqServiceDetailDto.setServiceId(model.getServiceModel().getId());
                    reqServiceDetailDto.setDetailServiceId(model.getServiceDetailModel().getId());
                    reqServiceDetailDto.setDateVisit(DateUtils.formatDateToStr(model.getDateVisit(),"yyyy-MM-dd"));
                    reqServiceDetailDto.setServiceName(model.getServiceName());
                    reqServiceDetailDto.setServiceNameDetail(model.getServiceDetailName());
                    reqServiceDetailDto.setPrice(model.getPrice());
                    reqServiceDetailDto.setQty(model.getQty());
                    reqServiceDetailDto.setTotal(model.getTotal());
                    reqServiceDetailDto.setHasReview(model.getHasReview());
                    reqServiceDetailDto.setNotes(model.getNotes());
                    dtos.add(reqServiceDetailDto);
                }
                reqTransactionDto.setFormDetailCart(dtos);

                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, reqTransactionDto, 1);
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.SERVICE_NOT_FOUND + " for id " + id, "", "");
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> getServiceById(Long id) {
        Map<String, Object> response;
        try {
            TransactionModel transactionModel = transactionDao.get(id);
            if (transactionModel != null){
                ReqTransactionDto reqTransactionDto = new ReqTransactionDto();
                reqTransactionDto.setId(transactionModel.getId());
                reqTransactionDto.setTravelingEmail(transactionModel.getTravelingEmail());
                reqTransactionDto.setTravelingName(transactionModel.getTravelingName());
                reqTransactionDto.setTravelingPhone(transactionModel.getTravelingPhone());
                reqTransactionDto.setUserId(transactionModel.getUserModel().getId());
                reqTransactionDto.setPaymentMethode(transactionModel.getPaymentMethod());
                reqTransactionDto.setPaymentChannel(transactionModel.getPaymentChannel());
                reqTransactionDto.setPaymentCode(transactionModel.getPaymentCode());
                reqTransactionDto.setPaymentAmount(transactionModel.getPaymentAmount());
                reqTransactionDto.setStatusPayment(transactionModel.getStatusPayment());
                reqTransactionDto.setFeeAdmin(transactionModel.getFeeAdmin());
                reqTransactionDto.setDiscount(transactionModel.getDiscount());
                reqTransactionDto.setSummaryService(transactionModel.getSummaryService());
                reqTransactionDto.setTotal(transactionModel.getTotal());
                reqTransactionDto.setNumberTransaction(transactionModel.getInvoiceNumber());
                reqTransactionDto.setDate(DateUtils.formatDateToStr(transactionModel.getDate(),"yyyy-MM-dd"));

                List<ReqTransactionDetailDto> dtos = new ArrayList<>();
                List<TransactionDetailModel> transactionDetailModels = transactionDetailDao.findAllByIdTransaction(transactionModel.getId());
                for (TransactionDetailModel model : transactionDetailModels) {
                    ReqTransactionDetailDto reqServiceDetailDto = new ReqTransactionDetailDto();
                    reqServiceDetailDto.setId(model.getId());
                    reqServiceDetailDto.setServiceId(model.getServiceModel().getId());
                    reqServiceDetailDto.setDetailServiceId(model.getServiceDetailModel().getId());
                    reqServiceDetailDto.setDateVisit(DateUtils.formatDateToStr(model.getDateVisit(),"yyyy-MM-dd"));
                    reqServiceDetailDto.setServiceName(model.getServiceName());
                    reqServiceDetailDto.setServiceNameDetail(model.getServiceDetailName());
                    reqServiceDetailDto.setPrice(model.getPrice());
                    reqServiceDetailDto.setQty(model.getQty());
                    reqServiceDetailDto.setTotal(model.getTotal());
                    reqServiceDetailDto.setHasReview(model.getHasReview());
                    reqServiceDetailDto.setNotes(model.getNotes());
                    dtos.add(reqServiceDetailDto);
                }
                reqTransactionDto.setFormDetailCart(dtos);

                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, reqTransactionDto, 1);
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.SERVICE_NOT_FOUND + " for id " + id, "", "");
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> findByUser(String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));

            List<ReqTransactionDto> transactionDtos = new ArrayList<>();
//            List<TransactionModel> transactionModels = transactionDao.findAll();
            List<TransactionModel> transactionModels = transactionDao.findTransactionByUser(userModel.getId());
            for (TransactionModel transactionModel : transactionModels) {
                ReqTransactionDto dto = new ReqTransactionDto();
                dto.setId(transactionModel.getId());
                dto.setTravelingName(transactionModel.getTravelingName());
                dto.setTravelingEmail(transactionModel.getTravelingEmail());
                dto.setTravelingPhone(transactionModel.getTravelingPhone());
                dto.setTotal(transactionModel.getTotal());
                dto.setPaymentAmount(transactionModel.getPaymentAmount());
                dto.setStatusPayment(transactionModel.getStatusPayment());
                dto.setNumberTransaction(transactionModel.getInvoiceNumber());
                dto.setDate(DateUtils.formatDateToStr(transactionModel.getDate(),"yyyy-MM-dd"));

                List<ReqTransactionDetailDto> dtos = new ArrayList<>();
                List<TransactionDetailModel> transactionDetailModels = transactionDetailDao.findAllByIdTransaction(transactionModel.getId());
                for (TransactionDetailModel model : transactionDetailModels) {
//                    ServiceModel serviceModel = serviceDao.getOne(model.getServiceModel().getId());

                    ReqTransactionDetailDto reqServiceDetailDto = new ReqTransactionDetailDto();
                    reqServiceDetailDto.setId(model.getId());
                    reqServiceDetailDto.setServiceId(model.getId());
                    reqServiceDetailDto.setServiceName(model.getServiceName());
                    reqServiceDetailDto.setServiceNameDetail(model.getServiceDetailName());
                    reqServiceDetailDto.setPrice(model.getPrice());
                    reqServiceDetailDto.setQty(model.getQty());
                    reqServiceDetailDto.setTotal(model.getTotal());
                    reqServiceDetailDto.setHasReview(model.getHasReview());
                    dtos.add(reqServiceDetailDto);
                }
                dto.setFormDetailCart(dtos);

                transactionDtos.add(dto);
            }
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, transactionDtos, transactionDtos.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> salesByUser(String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            List<TransactionDetailModel> transactionDetailModels = transactionDetailDao.salesByUser(userModel.getId());
            List<ReqTransactionDetailDto> reqTransactionDetailDtos = new ArrayList<>();
            for (TransactionDetailModel model : transactionDetailModels) {
                ReqTransactionDetailDto dto = new ReqTransactionDetailDto();
                dto.setTransactionId(model.getTransactionModel().getId());
                dto.setTransactionDetailId(model.getId());
                dto.setInvoiceNumber(model.getTransactionModel().getInvoiceNumber());
                dto.setServiceName(model.getServiceName());
                dto.setServiceNameDetail(model.getServiceDetailName());
                dto.setTotal(model.getTotal());
                dto.setQty(model.getQty());
                dto.setPrice(model.getPrice());
                dto.setDate(DateUtils.formatDateToStr(model.getTransactionModel().getDate(),"yyyy-MM-dd"));
                dto.setStatusPayment(model.getTransactionModel().getStatusPayment());
                dto.setTransactionId(model.getTransactionModel().getId());
                dto.setDateVisit(DateUtils.formatDateToStr(model.getDateVisit(),"yyyy-MM-dd"));
                reqTransactionDetailDtos.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, reqTransactionDetailDtos, reqTransactionDetailDtos.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }

    public ResponseTransactionDto getListDT(TransactionDto transactionDto, RequestPagingDataTablesBaseDto rpdt){
        ResponseTransactionDto responseTransactionDto = new ResponseTransactionDto();
        List<TransactionDto> listResult = new ArrayList<TransactionDto>();
        String sum = "0";
        listResult = transactionDao.getListDT(transactionDto, rpdt);
        sum = transactionDao.getCountListDT(transactionDto);
        responseTransactionDto.setRecordsTotal(sum);
        responseTransactionDto.setData(listResult);
        return responseTransactionDto;
    }

    public TransactionDto getTransactionByIdAdmin(Long id) {
        TransactionDto dto = new TransactionDto();
        try {
            TransactionModel transactionModel = transactionDao.get(id);
            dto.setId(transactionModel.getId());
            dto.setFee(transactionModel.getFeeAdmin());
            dto.setSummaryService(transactionModel.getSummaryService());
            dto.setDiscountVoucherCode(transactionModel.getDiscount());
            dto.setVoucherCode(transactionModel.getVoucherCode());
            dto.setTotal(transactionModel.getTotal());
            dto.setInvoiceNumber(transactionModel.getInvoiceNumber());
            dto.setPaymentCode(transactionModel.getPaymentCode());
            dto.setPaymentMethode(transactionModel.getPaymentMethod());
            dto.setPaymentAmount(transactionModel.getPaymentAmount());
            dto.setStatusPayment(transactionModel.getStatusPayment());
            dto.setUserId(transactionModel.getUserModel().getId());
            dto.setTravelingName(transactionModel.getTravelingName());
            dto.setTravelingEmail(transactionModel.getTravelingEmail());
            dto.setTravelingPhone(transactionModel.getTravelingPhone());
            dto.setDate(DateUtils.formatDateToStr(transactionModel.getDate(), "yyyy-MM-dd"));

            List<ReqTransactionDetailDto> dtos = new ArrayList<>();
            List<TransactionDetailModel> transactionDetailModels = transactionDetailDao.findAllByIdTransaction(transactionModel.getId());
            for (TransactionDetailModel model : transactionDetailModels) {
                ReqTransactionDetailDto reqServiceDetailDto = new ReqTransactionDetailDto();
                reqServiceDetailDto.setServiceId(model.getId());
                reqServiceDetailDto.setServiceName(model.getServiceName());
                reqServiceDetailDto.setServiceNameDetail(model.getServiceDetailName());
                reqServiceDetailDto.setDateVisit(DateUtils.formatDateToStr(model.getDateVisit(), "yyyy-MM-dd"));
                reqServiceDetailDto.setPrice(model.getPrice());
                reqServiceDetailDto.setQty(model.getQty());
                reqServiceDetailDto.setTotal(model.getTotal());
                reqServiceDetailDto.setHasReview(model.getHasReview());
                dtos.add(reqServiceDetailDto);
            }
            dto.setFormDetailCart(dtos);

        } catch (Exception e) {
            return new TransactionDto();
        }

        return dto;
    }

    public Boolean setApproveTransaction(TransactionDto dto) {
        TransactionModel transactionModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            transactionModel = transactionDao.get(dto.getId());
        }else{
            return false;
        }

        try {  
            //CHANGE STATUS PAYMENT TRANSACTION
            transactionModel.setStatusPayment(dto.getStatusPayment());
            transactionDao.save(transactionModel);

            //IF PAYMENT SUCCESS
            if(dto.getStatusPayment().equals("1")){
                List<TransactionDetailModel> transactionDetailModels = transactionDetailDao.findAllByIdTransaction(transactionModel.getId());

                double revenue = transactionModel.getSummaryService() - (charge/100.0f * (double) transactionModel.getSummaryService());
                logger.info("-----------------------------");
                logger.info(Math.ceil(revenue));
                RevenuesModel revenuesModel = new RevenuesModel();
                revenuesModel.setSale(transactionModel.getSummaryService());
                revenuesModel.setCharge(charge);
                revenuesModel.setRevenue((long) Math.ceil(revenue));
                revenuesModel.setDate(new Date());
                revenuesModel.setUserModel(transactionDetailModels.get(0).getServiceModel().getUserModel());
                revenuesModel.setTransactionModel(transactionModel);
                revenuesModel.setStatus("0");
                revenuesDao.save(revenuesModel);


                //SEND NOTIFIKASI TRANSACTION HAVE SERVICE USER
                NotifModel notifModel = new NotifModel();
                notifModel.setDate(new Date());
                notifModel.setDeleted("0");
                notifModel.setIsRead("0");
                notifModel.setPublish("1");
                notifModel.setUrl(urlFrontend + "/member/booking/payment/" + transactionModel.getId());
                notifModel.setNotif("invoice Number " +transactionModel.getInvoiceNumber() + " payment is success");
                notifModel.setUserModel(transactionDetailModels.get(0).getServiceModel().getUserModel());
                notifDao.save(notifModel);

                NotifModel notifModel2 = new NotifModel();
                notifModel2.setDate(new Date());
                notifModel2.setDeleted("0");
                notifModel2.setIsRead("0");
                notifModel2.setPublish("1");
                notifModel2.setUrl(urlFrontend + "/member/booking/payment/" + transactionModel.getId());
                notifModel2.setNotif("invoice Number " +transactionModel.getInvoiceNumber() + " payment is success");
                notifModel2.setUserModel(transactionModel.getUserModel());
                notifDao.save(notifModel2);
            }
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }

}
