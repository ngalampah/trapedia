package id.her.trapedia.service;

import id.her.trapedia.dao.UserDao;
import id.her.trapedia.dao.VerificationTokenDao;
import id.her.trapedia.dto.UserDto;
import id.her.trapedia.model.UserModel;
import id.her.trapedia.model.VerificationTokenModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ResponseGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

@Service
@Transactional
public class VerificationTokenService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private VerificationTokenDao verificationTokenDao;

    public String verificationToken(String token) {
        String response;

        VerificationTokenModel verificationTokenModel = verificationTokenDao.findByToken(token);

        if(verificationTokenModel != null) {
            if (verificationTokenModel.getExpiryDate().compareTo(new Date()) > 0) {
                UserModel userModel = userDao.findUserModelByUsername(verificationTokenModel.getUserModel().getEmail());
                userModel.setActivation(Constant.BooleanStatus.TRUE);
                userModel = userDao.save(userModel);
                response = Constant.STATUS.SUCCESS;
            }
            else {
                response = Constant.STATUS.TOKEN_EXPIRED;
            }
        }
        else {
            response = Constant.STATUS.TOKEN_DATA_NOT_FOUND;
        }

        return response;
    }
}
