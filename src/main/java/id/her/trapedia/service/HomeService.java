package id.her.trapedia.service;

import id.her.trapedia.dao.CategoryDao;
import id.her.trapedia.dao.TransactionDao;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.CategoryModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ResponseGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class HomeService {
    @Autowired
    TransactionDao transactionDao;

    private Logger logger = LogManager.getLogger(HomeService.class);

    public Map<String, Object> graphMonthYear() {
        Map<String, Object> response = new HashMap<>();

        try {
            List<GraphDto> listData = new ArrayList<GraphDto>();
            String[] monthsArray = {"JAN","FEB","MAR","APR","MAY", "JUN","JUL", "AUG","SEP", "OCT","NOV","DEC"};

            //waiting
            GraphDto valWaiting = new GraphDto();
            List<GraphMonthDto> dtMonthWaiting = transactionDao.getGraphTransaction("0");
            int tmp = 0;
            int[] dataWaiting = new int[12];
            for(int index = 0; index < monthsArray.length; index++) {
                tmp = 0;
                for(int index2 = 0; index2 < dtMonthWaiting.size(); index2++) {
                    if(dtMonthWaiting.get(index2).getMonth().equalsIgnoreCase(monthsArray[index])){
                        tmp = Integer.valueOf(dtMonthWaiting.get(index2).getTotalData());
                    }
                }
                dataWaiting[index] = tmp;
            }
            valWaiting.setName("Payment Waiting");
            valWaiting.setData(dataWaiting);


            GraphDto valSuccess = new GraphDto();
            List<GraphMonthDto> dtMonthSuccess = transactionDao.getGraphTransaction("1");
            int[] dataSuccess = new int[12];
            for(int index = 0; index < monthsArray.length; index++) {
                tmp = 0;
                for(int index2 = 0; index2 < dtMonthSuccess.size(); index2++) {
                    if(dtMonthSuccess.get(index2).getMonth().equalsIgnoreCase(monthsArray[index])){
                        tmp = Integer.valueOf(dtMonthSuccess.get(index2).getTotalData());
                    }
                }
                dataSuccess[index] = tmp;
            }
            valSuccess.setName("Payment Success");
            valSuccess.setData(dataSuccess);


            listData.add(0, valWaiting);
            listData.add(1, valSuccess);


            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, "", listData, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

}
