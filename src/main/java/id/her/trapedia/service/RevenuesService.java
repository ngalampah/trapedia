package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.*;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.*;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ResponseGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RevenuesService {

    @Autowired
    private TransactionDao transactionDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RevenuesDao revenuesDao;

    @Autowired
    private UserBalanceDao userBalanceDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    @Value("${trapedia.fee.admin}")
    private Long feeAdmin;

    private Logger logger = LogManager.getLogger(RevenuesService.class);



    public Map<String, Object> revenueByUser(String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            List<RevenuesModel> revenuesModels = revenuesDao.findAllByUser(userModel.getId());
            List<RevenueDto> revenueDtos = new ArrayList<>();
            for (RevenuesModel model : revenuesModels) {
                RevenueDto dto = new RevenueDto();
                dto.setTransactionId(model.getTransactionModel().getId());
                dto.setInvoiceNumber(model.getTransactionModel().getInvoiceNumber());
                dto.setDate(DateUtils.formatDateToStr(model.getDate(), "yyyy-MM-dd"));
                dto.setSale(model.getSale());
                dto.setCharge(model.getCharge());
                dto.setRevenue(model.getRevenue());
                dto.setStatus(model.getStatus());
                revenueDtos.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, revenueDtos, revenueDtos.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> revenueByTotal(String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            String revenueSuccess = revenuesDao.getTotalRevenueByUser(userModel.getId(), "1");
            String revenuePending = revenuesDao.getTotalRevenueByUser(userModel.getId(), "0");
            String revenueFailed = revenuesDao.getTotalRevenueByUser(userModel.getId(), "-1");

            String totalWithdraw = revenuesDao.getTotalWidrawByUser(userModel.getId());

            String balanceDebit = userBalanceDao.getBalanceByUser(userModel.getId(), "D");
            String balanceKredit = userBalanceDao.getBalanceByUser(userModel.getId(), "K");

            UserMoneyDto userMoneyDto = new UserMoneyDto();
            userMoneyDto.setRevenueTotal(Long.parseLong(revenueSuccess) + Long.parseLong(revenuePending));
            userMoneyDto.setRevenueSuccess(Long.parseLong(revenueSuccess));
            userMoneyDto.setRevenuePending(Long.parseLong(revenuePending));
            userMoneyDto.setRevenueFailed(Long.parseLong(revenueFailed));

            userMoneyDto.setWithdraw(Long.parseLong(totalWithdraw));
            userMoneyDto.setBalance(Long.parseLong(balanceDebit) - Long.parseLong(balanceKredit));




            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, userMoneyDto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }


    public ResponseRevenuesDto getListDT(RevenueDto revenueDto, RequestPagingDataTablesBaseDto rpdt){
        ResponseRevenuesDto responseRevenuesDto = new ResponseRevenuesDto();
        List<RevenueDto> listResult = new ArrayList<RevenueDto>();
        String sum = "0";
        listResult = revenuesDao.getListDT(revenueDto, rpdt);
        sum = revenuesDao.getCountListDT(revenueDto);
        responseRevenuesDto.setRecordsTotal(sum);
        responseRevenuesDto.setData(listResult);
        return responseRevenuesDto;
    }

    public RevenueDto getRevenuesByIdAdmin(Long id) {
        RevenueDto dto = new RevenueDto();
        try {
            RevenuesModel revenuesModel = revenuesDao.get(id);
            dto.setId(revenuesModel.getId());
            dto.setDate(DateUtils.formatDateToStr(revenuesModel.getDate(), "yyyy-MM-dd"));
            dto.setSale(revenuesModel.getSale());
            dto.setCharge(revenuesModel.getCharge());
            dto.setRevenue(revenuesModel.getRevenue());
            dto.setUserName(revenuesModel.getUserModel().getName());
        } catch (Exception e) {
            return new RevenueDto();
        }

        return dto;
    }

}
