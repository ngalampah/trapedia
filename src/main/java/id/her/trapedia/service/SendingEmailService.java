package id.her.trapedia.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import id.her.trapedia.dto.EmailDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class SendingEmailService {
    private Logger logger = LogManager.getLogger(SendingEmailService.class);

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    @Qualifier("emailConfigBean")
    private Configuration emailConfig;

    public void sendEmail(EmailDto mail) throws MessagingException, IOException, TemplateException {
        logger.info("Sending email to : " + mail.getTo());

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);

            // Load mail template
            Template template = emailConfig.getTemplate(mail.getTemplate());
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, mail.getModel());

            messageHelper.setTo(mail.getTo());
            messageHelper.setFrom(mail.getFrom());
            messageHelper.setText(html, true);
            messageHelper.setSubject(mail.getSubject());

            messageHelper.addInline("logo", new ClassPathResource("static/img/"+mail.getLogo()));
            if (mail.getBankCode() != null) {
                ClassPathResource resource = new ClassPathResource("static/img/bank/"+mail.getBankCode()+".png");
                if (resource.exists()) {
                    messageHelper.addInline("bank", resource);
                }
            }
            messageHelper.addInline("instagram", new ClassPathResource("static/img/icon/instagram.png"));
            messageHelper.addInline("linkedin", new ClassPathResource("static/img/icon/linkedin.png"));
            messageHelper.addInline("facebook", new ClassPathResource("static/img/icon/facebook.png"));

            mailSender.send(message);
        } catch (MessagingException e) {
            logger.error("Error while send mail : " + e.getMessage());
        } catch (IOException e) {
            logger.error("Error while send mail : " + e.getMessage());
        } catch (TemplateException e) {
            logger.error("Error while send mail : " + e.getMessage());
        } catch (MailException e) {
            logger.error("Error while send mail : " + e.getMessage());
        }
    }
}
