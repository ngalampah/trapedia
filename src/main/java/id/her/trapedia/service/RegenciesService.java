package id.her.trapedia.service;

import id.her.trapedia.dao.ProvincesDao;
import id.her.trapedia.dao.RegenciesDao;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.CategoryModel;
import id.her.trapedia.model.ProvincesModel;
import id.her.trapedia.model.RegenciesModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ResponseGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RegenciesService {

    private Logger logger = LogManager.getLogger(RegenciesService.class);

    @Autowired
    private RegenciesDao regenciesDao;

    @Autowired
    private ProvincesDao provincesDao;

    public Map<String, Object> findAll() {
        Map<String, Object> response = new HashMap<>();

        try {
            List<RegenciesDto> regenciesDtoList = new ArrayList<>();
            List<RegenciesModel> regenciesModelList = regenciesDao.findAll();
            for (RegenciesModel regenciesModel : regenciesModelList) {
                RegenciesDto dto = new RegenciesDto();
                dto.setId(regenciesModel.getId());

                ProvincesDto provincesDto = new ProvincesDto();
                provincesDto.setId(regenciesModel.getProvincesModel().getId());
                provincesDto.setName(regenciesModel.getProvincesModel().getName());

                dto.setProvincesDto(provincesDto);
                dto.setName(regenciesModel.getName());
                dto.setImages(regenciesModel.getImages());
                dto.setPopular(regenciesModel.getPopular());
                regenciesDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, regenciesDtoList, regenciesDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> findPopular() {
        Map<String, Object> response = new HashMap<>();

        try {
            List<RegenciesDto> regenciesDtoList = new ArrayList<>();
            List<RegenciesModel> regenciesModelList = regenciesDao.findPopular();
            for (RegenciesModel regenciesModel : regenciesModelList) {
                RegenciesDto dto = new RegenciesDto();
                dto.setId(regenciesModel.getId());

                ProvincesDto provincesDto = new ProvincesDto();
                provincesDto.setId(regenciesModel.getProvincesModel().getId());
                provincesDto.setName(regenciesModel.getProvincesModel().getName());

                dto.setProvincesDto(provincesDto);
                dto.setName(regenciesModel.getName());
                dto.setImages(regenciesModel.getImages());
                dto.setPopular(regenciesModel.getPopular());
                regenciesDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, regenciesDtoList, regenciesDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> findRegenciesByKeyword(RegenciesDto regenciesDto){
        Map<String, Object> response = new HashMap<>();
        try {
            List<RegenciesDto> regenciesDtoList = regenciesDao.getRegenciesByKeyword(regenciesDto.getName());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, regenciesDtoList, regenciesDtoList.size());
        }catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> getRegenciesById(Long id) {
        Map<String, Object> response;

        try {
            RegenciesModel regenciesModel = regenciesDao.get(id);
            if (regenciesModel != null){
                RegenciesDto dto = new RegenciesDto();
                dto.setId(regenciesModel.getId());
                dto.setName(regenciesModel.getName());
                dto.setImages(regenciesModel.getImages());
                dto.setPopular(regenciesModel.getPopular());
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dto, 1);
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.SERVICE_NOT_FOUND + " for id " + id, "", "");
            }

        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public ResponseRegenciesDto getListDT(RegenciesDto regenciesDto, RequestPagingDataTablesBaseDto rpdt){
        ResponseRegenciesDto responseRegenciesDto = new ResponseRegenciesDto();
        List<RegenciesDto> listResult = new ArrayList<RegenciesDto>();
        String sum = "0";
        listResult = regenciesDao.getListDT(regenciesDto, rpdt);
        sum = regenciesDao.getCountListDT(regenciesDto);
        responseRegenciesDto.setRecordsTotal(sum);
        responseRegenciesDto.setData(listResult);
        return responseRegenciesDto;
    }

    public RegenciesDto getRegenciesByIdAdmin(Long id) {
        Map<String, Object> response = new HashMap<>();
        RegenciesDto dto = new RegenciesDto();
        try {
            RegenciesModel regenciesModel = regenciesDao.get(id);
            dto.setId(regenciesModel.getId());
            dto.setName(regenciesModel.getName());
            dto.setPopular(regenciesModel.getPopular());
            dto.setImages(regenciesModel.getImages());
            dto.setPublish(regenciesModel.getPublish());
            dto.setProvincesId(regenciesModel.getProvincesModel().getId());
            dto.setProvincesName(regenciesModel.getProvincesModel().getName());
        } catch (Exception e) {
            return new RegenciesDto();
        }

        return dto;
    }

    public Boolean saveDataForm(RegenciesDto dto) {
        RegenciesModel regenciesModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            regenciesModel = regenciesDao.get(dto.getId());
        }else{
            regenciesModel = new RegenciesModel();
            regenciesModel.setDeleted("0");
        }

        ProvincesModel provincesModel = provincesDao.get(dto.getProvincesId());
        regenciesModel.setName(dto.getName());
        regenciesModel.setImages(dto.getImages());
        regenciesModel.setPopular(dto.getPopular());
        regenciesModel.setPublish(dto.getPublish());
        regenciesModel.setProvincesModel(provincesModel);
        try {
            regenciesDao.save(regenciesModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }
}
