package id.her.trapedia.service;

import id.her.trapedia.dao.CategoryDao;
import id.her.trapedia.dao.CategoryDetailDao;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.CategoryDetailModel;
import id.her.trapedia.model.CategoryModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ResponseGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CategoryDetailService {
    @Autowired
    CategoryDetailDao categoryDetailDao;

    @Autowired
    CategoryDao categoryDao;

    private Logger logger = LogManager.getLogger(CategoryDetailService.class);

    public Map<String, Object> findAll() {
        Map<String, Object> response = new HashMap<>();

        try {
            List<CategoryDetailDto> categoryDetailDtoList = new ArrayList<>();
            List<CategoryDetailModel> categoryModelList = categoryDetailDao.findAll();
            for (CategoryDetailModel categoryDetailModel : categoryModelList) {
                CategoryDetailDto dto = new CategoryDetailDto();
                dto.setId(categoryDetailModel.getId());
                dto.setTitle(categoryDetailModel.getTitle());
                dto.setDescription(categoryDetailModel.getDescription());
                dto.setPublish(categoryDetailModel.getPublish());
                dto.setCategoryId(categoryDetailModel.getCategory().getId());
                dto.setCategoryName(categoryDetailModel.getCategory().getTitle());
                categoryDetailDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, "", categoryDetailDtoList, categoryDetailDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> getCategoryDetailById(Long id) {
        Map<String, Object> response = new HashMap<>();

        try {
            CategoryDetailModel categoryDetailModel = categoryDetailDao.get(id);
            CategoryDetailDto dto = new CategoryDetailDto();
            dto.setId(categoryDetailModel.getId());
            dto.setTitle(categoryDetailModel.getTitle());
            dto.setDescription(categoryDetailModel.getDescription());
            dto.setPublish(categoryDetailModel.getPublish());
            dto.setDeleted(categoryDetailModel.getDeleted());
            dto.setCategoryName(categoryDetailModel.getCategory().getTitle());
            dto.setCategoryId(categoryDetailModel.getCategory().getId());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, "", dto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public ResponseCategoryDetailDto getListDT(CategoryDetailDto categoryDetailDto,
                                               RequestPagingDataTablesBaseDto requestPagingDataTablesBaseDto){
        ResponseCategoryDetailDto responseCategoryDetailDto = new ResponseCategoryDetailDto();
        List<CategoryDetailDto> listResult = new ArrayList<CategoryDetailDto>();
        String sum = "0";
        listResult = categoryDetailDao.getListDT(categoryDetailDto, requestPagingDataTablesBaseDto);
        sum = categoryDetailDao.getCountListDT(categoryDetailDto);
        responseCategoryDetailDto.setRecordsTotal(sum);
        responseCategoryDetailDto.setData(listResult);
        return responseCategoryDetailDto;
    }

    public CategoryDetailDto getCategoriDetailByIdAdmin(Long id) {
        Map<String, Object> response = new HashMap<>();
        CategoryDetailDto dto = new CategoryDetailDto();
        try {
            CategoryDetailModel categoryDetailModel = categoryDetailDao.get(id);
            CategoryModel categoryModel = categoryDao.get(categoryDetailModel.getCategory().getId());
            dto.setId(categoryDetailModel.getId());
            dto.setTitle(categoryDetailModel.getTitle());
            dto.setCategoryId(categoryModel.getId());
            dto.setDescription(categoryDetailModel.getDescription());
            dto.setDeleted(categoryDetailModel.getDeleted());
            dto.setPublish(categoryDetailModel.getPublish());
        } catch (Exception e) {
            return new CategoryDetailDto();
        }

        return dto;
    }

    public Boolean saveDataForm(CategoryDetailDto dto) {
        CategoryDetailModel categoryDetailModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            categoryDetailModel = categoryDetailDao.get(dto.getId());
        }else{
            categoryDetailModel = new CategoryDetailModel();
            categoryDetailModel.setDeleted("0");
        }
        CategoryModel categoryModel = categoryDao.get(dto.getCategoryId());
        categoryDetailModel.setTitle(dto.getTitle());
        categoryDetailModel.setDescription(dto.getDescription());
        categoryDetailModel.setCategory(categoryModel);
        categoryDetailModel.setPublish(dto.getPublish());
        try {
            categoryDetailDao.save(categoryDetailModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }


}
