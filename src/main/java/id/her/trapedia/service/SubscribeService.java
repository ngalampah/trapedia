package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.AuthenticationDao;
import id.her.trapedia.dao.SubscribeDao;
import id.her.trapedia.dao.UserDao;
import id.her.trapedia.dao.VerificationTokenDao;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.AdminModel;
import id.her.trapedia.model.SubscribeModel;
import id.her.trapedia.model.UserModel;
import id.her.trapedia.model.VerificationTokenModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ResponseGenerator;
import id.her.trapedia.util.TrapediaHelper;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@Service
@Transactional
public class SubscribeService {

    private Logger logger = LogManager.getLogger(SubscribeService.class);

    @Autowired
    private SubscribeDao subscribeDao;


    public Map<String, Object> add(SubscribeDto subscribeDto) {
        Map<String, Object> response;
        try {
            SubscribeModel subscribeModel = new SubscribeModel();
            subscribeModel.setEmail(subscribeDto.getEmail());
            subscribeModel.setDate(new Date());
            subscribeModel = subscribeDao.save(subscribeModel);

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
//            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, "email already exist", "", "");
        }
        return response;
    }

    public ResponseSubscribeDto getListDT(SubscribeDto subscribeDto,
                                         RequestPagingDataTablesBaseDto requestPagingDataTablesBaseDto){
        ResponseSubscribeDto responseSubscribeDto = new ResponseSubscribeDto();
        List<SubscribeDto> listResult = new ArrayList<SubscribeDto>();
        String sum = "0";
        listResult = subscribeDao.getListDT(subscribeDto, requestPagingDataTablesBaseDto);
        sum = subscribeDao.getCountListDT(subscribeDto);
        responseSubscribeDto.setRecordsTotal(sum);
        responseSubscribeDto.setData(listResult);
        return responseSubscribeDto;
    }

}
