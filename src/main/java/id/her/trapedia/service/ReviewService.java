package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.*;
import id.her.trapedia.dto.ReqReviewDto;
import id.her.trapedia.dto.ReqWishlistDto;
import id.her.trapedia.dto.ReviewDetailDto;
import id.her.trapedia.model.*;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ResponseGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ReviewService {
    private Logger logger = LogManager.getLogger(ReviewService.class);

    @Autowired
    ReviewDao reviewDao;

    @Autowired
    UserDao userDao;

    @Autowired
    TransactionDetailDao transactionDetailDao;

    @Autowired
    ServiceDetailDao serviceDetailDao;

    @Autowired
    ServiceDao serviceDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    public Map<String, Object> insert(ReqReviewDto reqReviewDto, String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            TransactionDetailModel transactionDetailModel = transactionDetailDao.get(reqReviewDto.getTransactionDetailId());
            ServiceDetailModel serviceDetailModel = serviceDetailDao.get(transactionDetailModel.getServiceDetailModel().getId());

            ReviewModel reviewModel = new ReviewModel();
            reviewModel.setReview(reqReviewDto.getReview());
            reviewModel.setRatingReview(reqReviewDto.getRatingReview());
            reviewModel.setUserModel(userModel);
            reviewModel.setTransactionDetailModel(transactionDetailModel);
            reviewModel.setServiceDetailModel(serviceDetailModel);

            transactionDetailModel.setHasReview("1");
            transactionDetailDao.save(transactionDetailModel);

            reviewModel = reviewDao.save(reviewModel);

            //calculation rating
            long cr = 0L;
            if(serviceDetailModel.getServiceModel().getCountRating() > 0){
                cr = (serviceDetailModel.getServiceModel().getCountRating() + reqReviewDto.getRatingReview()) / 2L;
            }else{
                cr = reqReviewDto.getRatingReview();
            }
            ServiceModel serviceModel = serviceDao.get(serviceDetailModel.getServiceModel().getId());
            serviceModel.setCountRating(cr);
            serviceModel.setCountReview(serviceDetailModel.getServiceModel().getCountReview() + 1L);
            serviceDao.save(serviceModel);

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> findAllByUser(String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            List<ReviewModel> reviewModels = reviewDao.findAllByUser(userModel.getId());
            List<ReviewDetailDto> list = new ArrayList<>();
            for (ReviewModel ReviewModel : reviewModels) {
                ReviewDetailDto reviewDetailDto = new ReviewDetailDto();
                reviewDetailDto.setServiceId(ReviewModel.getTransactionDetailModel().getServiceModel().getId());
                reviewDetailDto.setServiceName(ReviewModel.getTransactionDetailModel().getServiceModel().getServiceName());
                reviewDetailDto.setServiceDetailId(ReviewModel.getTransactionDetailModel().getServiceDetailModel().getId());
                reviewDetailDto.setSeviceDetailName(ReviewModel.getTransactionDetailModel().getServiceDetailModel().getName());
                reviewDetailDto.setQty(ReviewModel.getTransactionDetailModel().getQty());
                reviewDetailDto.setPrice(ReviewModel.getTransactionDetailModel().getPrice());
                reviewDetailDto.setRatingReview(ReviewModel.getRatingReview());
                reviewDetailDto.setReview(ReviewModel.getReview());
                reviewDetailDto.setDate(DateUtils.formatDateToStr(ReviewModel.getTransactionDetailModel().getDateVisit(), "yyyy-MM-dd"));
                reviewDetailDto.setImage(ReviewModel.getServiceDetailModel().getServiceModel().getImages1());
                list.add(reviewDetailDto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, list, list.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> findAllByService(Long serviceId) {
        Map<String, Object> response;

        try {
            List<ReviewModel> reviewModels = reviewDao.findAllByService(serviceId);
            List<ReviewDetailDto> list = new ArrayList<>();
            for (ReviewModel ReviewModel : reviewModels) {
                ReviewDetailDto reviewDetailDto = new ReviewDetailDto();
                reviewDetailDto.setServiceId(ReviewModel.getTransactionDetailModel().getServiceModel().getId());
                reviewDetailDto.setServiceName(ReviewModel.getTransactionDetailModel().getServiceModel().getServiceName());
                reviewDetailDto.setServiceDetailId(ReviewModel.getTransactionDetailModel().getServiceDetailModel().getId());
                reviewDetailDto.setSeviceDetailName(ReviewModel.getTransactionDetailModel().getServiceDetailModel().getName());
                reviewDetailDto.setQty(ReviewModel.getTransactionDetailModel().getQty());
                reviewDetailDto.setPrice(ReviewModel.getTransactionDetailModel().getPrice());
                reviewDetailDto.setRatingReview(ReviewModel.getRatingReview());
                reviewDetailDto.setReview(ReviewModel.getReview());
                reviewDetailDto.setImageUser(ReviewModel.getUserModel().getImage());
                reviewDetailDto.setNameUser(ReviewModel.getUserModel().getName());
                reviewDetailDto.setDate(DateUtils.formatDateToStr(ReviewModel.getTransactionDetailModel().getDateVisit(), "yyyy-MM-dd"));
                reviewDetailDto.setImage(ReviewModel.getServiceDetailModel().getServiceModel().getImages1());
                list.add(reviewDetailDto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, list, list.size());

        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> detailTransactionService(ReqReviewDto reqReviewDto, String authorization) {
        Map<String, Object> response;
        UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
        try {
            TransactionDetailModel transactionDetailModel = transactionDetailDao.get(reqReviewDto.getTransactionDetailId());
            ServiceDetailModel serviceDetailModel = serviceDetailDao.get(transactionDetailModel.getServiceDetailModel().getId());
            ServiceModel serviceModel = serviceDao.get(serviceDetailModel.getServiceModel().getId());
            ReviewDetailDto reviewDetailDto = new ReviewDetailDto();
            reviewDetailDto.setServiceId(serviceDetailModel.getServiceModel().getId());
            reviewDetailDto.setServiceName(serviceDetailModel.getServiceModel().getServiceName());
            reviewDetailDto.setServiceDetailId(serviceDetailModel.getId());
            reviewDetailDto.setSeviceDetailName(serviceDetailModel.getName());
            reviewDetailDto.setTransactionDetailId(transactionDetailModel.getId());
            reviewDetailDto.setTransactionId(transactionDetailModel.getTransactionModel().getId());
            reviewDetailDto.setPrice(transactionDetailModel.getPrice());
            reviewDetailDto.setQty(transactionDetailModel.getQty());
            reviewDetailDto.setDate(DateUtils.formatDateToStr(transactionDetailModel.getDateVisit(), "yyyy-MM-dd"));
            reviewDetailDto.setImage(transactionDetailModel.getServiceModel().getImages1());
            reviewDetailDto.setHasReview(transactionDetailModel.getHasReview());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, reviewDetailDto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }


    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }



}
