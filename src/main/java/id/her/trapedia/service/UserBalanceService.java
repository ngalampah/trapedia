package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.RevenuesDao;
import id.her.trapedia.dao.TransactionDao;
import id.her.trapedia.dao.UserBalanceDao;
import id.her.trapedia.dao.UserDao;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.RevenuesModel;
import id.her.trapedia.model.UserBalanceModel;
import id.her.trapedia.model.UserModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ResponseGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class UserBalanceService {

    @Autowired
    private TransactionDao transactionDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RevenuesDao revenuesDao;

    @Autowired
    private UserBalanceDao userBalanceDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    @Value("${trapedia.fee.admin}")
    private Long feeAdmin;

    private Logger logger = LogManager.getLogger(UserBalanceService.class);



    public Map<String, Object> balanceByUser(String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            List<UserBalanceModel> userBalanceModelList = userBalanceDao.findAllByUser(userModel.getId());
            List<UserBalanceDto> userBalanceDtos = new ArrayList<>();
            for (UserBalanceModel model : userBalanceModelList) {
                UserBalanceDto dto = new UserBalanceDto();
                dto.setDate(DateUtils.formatDateToStr(model.getDate(), "yyyy-MM-dd"));
                dto.setId(model.getId());
                dto.setBalance(model.getBalance());
                dto.setBalanceType(model.getBalanceType());
                dto.setNotes(model.getNotes());
                userBalanceDtos.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, userBalanceDtos, userBalanceDtos.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }






}
