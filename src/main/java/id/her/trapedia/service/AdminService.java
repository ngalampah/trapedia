package id.her.trapedia.service;

import id.her.trapedia.dao.AdminDao;
import id.her.trapedia.dao.CategoryDao;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.AdminModel;
import id.her.trapedia.model.CategoryModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ResponseGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class AdminService {
    @Autowired
    AdminDao adminDao;

    @Autowired
    public PasswordEncoder bcryptEncoder;

    private Logger logger = LogManager.getLogger(AdminService.class);

    public AdminDto authenticate(String username, String password) {
        AdminDto adminDto = null;
        try {
            AdminModel adminModel = adminDao.authenticate(username);
            if (adminModel != null) {
                if (bcryptEncoder.matches(password, adminModel.getPassword())) {
                    logger.info("Username and password is authenticated");
                    adminDto = new AdminDto();
                    adminDto.setUsername(adminModel.getUsername());
                    adminDto.setPassword(adminModel.getPassword());
                    adminDto.setName(adminModel.getName());
                    adminDto.setEnabled(adminModel.getEnabled());
                }
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return adminDto;
    }

    public ResponseAdminDto getListDT(AdminDto adminDto, RequestPagingDataTablesBaseDto rpdt){
        ResponseAdminDto responseAdminDto = new ResponseAdminDto();
        List<AdminDto> listResult = new ArrayList<AdminDto>();
        String sum = "0";
        listResult = adminDao.getListDT(adminDto, rpdt);
        sum = adminDao.getCountListDT(adminDto);
        responseAdminDto.setRecordsTotal(sum);
        responseAdminDto.setData(listResult);
        return responseAdminDto;
    }

    public AdminDto getAdminByIdAdmin(Long id) {
        Map<String, Object> response = new HashMap<>();
        AdminDto dto = new AdminDto();
        try {
            AdminModel adminModel = adminDao.get(id);
            dto.setId(adminModel.getId());
            dto.setName(adminModel.getName());
            dto.setUsername(adminModel.getUsername());
            dto.setPassword(adminModel.getPassword());
            dto.setEnabled(adminModel.getEnabled());
        } catch (Exception e) {
            return new AdminDto();
        }
        return dto;
    }

    public Boolean saveDataForm(AdminDto dto) {
        AdminModel adminModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            adminModel = adminDao.get(dto.getId());
        }else{
            adminModel = new AdminModel();
            adminModel.setUsername(dto.getUsername());
            adminModel.setPassword(bcryptEncoder.encode(dto.getPassword()));
        }
        adminModel.setName(dto.getName());
        adminModel.setEnabled(dto.getEnabled());
        try {
            adminDao.save(adminModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }


}
