package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.UserDao;
import id.her.trapedia.dto.JwtResponseDto;
import id.her.trapedia.model.UserModel;
import id.her.trapedia.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
@Transactional
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel userModel = userDao.findUserModelByUsername(username);
        if (userModel != null) {
            if (userModel.getActivation().equalsIgnoreCase(Constant.BooleanStatus.TRUE)) {
                return new User(userModel.getEmail(), userModel.getPassword(), new ArrayList<>());
            } else {
                throw new DisabledException("User has not activated with username : " + username);
            }
        } else {
            throw new UsernameNotFoundException("User not found with username : " + username);
        }
    }

    public JwtResponseDto generateResponse(String username) {
        UserModel userModel = userDao.findUserModelByUsername(username);
        if (userModel != null) {
            if (userModel.getActivation().equalsIgnoreCase(Constant.BooleanStatus.TRUE)) {
                UserDetails userDetails = new User(userModel.getEmail(), userModel.getPassword(), new ArrayList<>());
                String token = jwtTokenUtil.generateToken(userDetails);
                JwtResponseDto responseDto = new JwtResponseDto();
                responseDto.setId(userModel.getId());
                responseDto.setName(userModel.getName());
                responseDto.setEmail(userModel.getEmail());
                responseDto.setPassword(userModel.getPassword());
                responseDto.setPhone(userModel.getPhone());
                responseDto.setGender(userModel.getGender());
                responseDto.setAddress(userModel.getAddress());
                responseDto.setDob(userModel.getDob());
                responseDto.setActivation(userModel.getActivation());
                responseDto.setBankName(userModel.getBankName());
                responseDto.setBankNumber(userModel.getBankNumber());
                responseDto.setDeleted(userModel.getDeleted());
                responseDto.setToken(token);
                return responseDto;
            } else {
                throw new DisabledException("User has not activated with username : " + username);
            }
        } else {
            throw new UsernameNotFoundException("User not found with username : " + username);
        }
    }
}
