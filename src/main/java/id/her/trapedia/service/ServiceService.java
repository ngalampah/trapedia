package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.*;
import id.her.trapedia.dto.*;
import id.her.trapedia.model.*;
import id.her.trapedia.util.*;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Multipart;
import java.util.*;

@Service
@Transactional
public class ServiceService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private ServiceDetailDao serviceDetailDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private CategoryDetailDao categoryDetailDao;

    @Autowired
    private ProvincesDao provincesDao;

    @Autowired
    private RegenciesDao regenciesDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    @Autowired
    private NotifDao notifDao;

    @Value("${trapedia.url.backend}")
    private String urlBackend;

    @Value("${trapedia.url.frontend}")
    private String urlFrontend;

    @Autowired
    private ImageUtils imageUtils;

    @Value("${trapedia.item.per.page}")
    private Integer limit;

    private Logger logger = LogManager.getLogger(ServiceService.class);

    public Map<String, Object> findAllService(PagingDto pagingDto) {
        Map<String, Object> response;

        try {
            List<ServiceDto> serviceDtoList = new ArrayList<>();
            Integer offset = (pagingDto.getPage()-1) * limit;
//            String regenciesId = "null is null";
//            if(pagingDto.getRegencyId() != null)
//                regenciesId = "regencies_id in ("+pagingDto.getRegencyId()+")";
//            List<ServiceModel> serviceModelList = serviceDao.findAll(limit, offset, regenciesId, pagingDto.getCategoryId(), pagingDto.getKeyword());
            List<ServiceModel> serviceModelList = serviceDao.findAll(generateParam(pagingDto), limit, offset);
            for (ServiceModel serviceModel : serviceModelList) {
                ServiceDto dto = new ServiceDto();
                dto.setId(serviceModel.getId());
                dto.setServiceName(serviceModel.getServiceName());
                dto.setCountRating(serviceModel.getCountRating());
                dto.setCountReview(serviceModel.getCountReview());

                CategoryDto categoryDto = new CategoryDto();
                categoryDto.setId(serviceModel.getCategoryModel().getId());
                categoryDto.setTitle(serviceModel.getCategoryModel().getTitle());
                categoryDto.setDescription(serviceModel.getCategoryModel().getDescription());
                categoryDto.setDeleted(serviceModel.getCategoryModel().getDeleted());

                dto.setCategoryDto(categoryDto);

                RegenciesDto regenciesDto = new RegenciesDto();
                regenciesDto.setId(serviceModel.getRegenciesModel().getId());
                regenciesDto.setName(serviceModel.getRegenciesModel().getName());

                ProvincesDto provincesDto = new ProvincesDto();
                provincesDto.setId(serviceModel.getRegenciesModel().getProvincesModel().getId());
                provincesDto.setName(serviceModel.getRegenciesModel().getProvincesModel().getName());

                regenciesDto.setProvincesDto(provincesDto);

                dto.setRegenciesDto(regenciesDto);
                dto.setDescription(serviceModel.getDescription());
                dto.setRedemptionInstructions(serviceModel.getRedemptionInstructions());
                dto.setCancellationPolicy(serviceModel.getCancellationPolicy());
                dto.setTermsConditions(serviceModel.getTermsConditions());
                dto.setPublish(serviceModel.getPublish());
                dto.setDeleted(serviceModel.getDeleted());

                List<ServiceDetailDto>  listServiceDetailDtos = new ArrayList<>();
                List<ServiceDetailModel> serviceDetailModels = serviceDetailDao.findAllByServiceId(serviceModel.getId());
                for (ServiceDetailModel model : serviceDetailModels) {
                    ServiceDetailDto d = new ServiceDetailDto();
                    d.setId(model.getId());
                    d.setName(model.getName());
                    d.setPrice(model.getPrice());
                    listServiceDetailDtos.add(d);
                }
                dto.setServiceDetailDtos(listServiceDetailDtos);

                serviceDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, serviceDtoList, serviceDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> findAllByUser(String authorization) {
        Map<String, Object> response;
        UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
        try {
            List<ServiceDto> serviceDtoList = serviceDao.findAllByUser(userModel.getId());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, serviceDtoList, serviceDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> findAllPopularService() {
        Map<String, Object> response;
        try {
            List<ServiceDto> serviceDtoList = serviceDao.listPopular();
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, serviceDtoList, serviceDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> findServiceRecomendation(ReqServiceDto reqServiceDto) {
        Map<String, Object> response;
        try {
            List<ServiceDto> serviceDtoList = serviceDao.listRecomendation(reqServiceDto);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, serviceDtoList, serviceDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> findAllNewService() {
        Map<String, Object> response;

        try {
            List<ServiceDto> serviceDtoList = serviceDao.listNew();
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, serviceDtoList, serviceDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }


    public Map<String, Object> findAllTrandingWeekService() {
        Map<String, Object> response;
        try {
            List<ServiceDto> serviceDtoList = serviceDao.serviceTrandingWeekTransaction();
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, serviceDtoList, serviceDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> findAllTrandingMonthService() {
        Map<String, Object> response;
        try {
            List<ServiceDto> serviceDtoList = serviceDao.serviceTrandingMonthTransaction();
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, serviceDtoList, serviceDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }



    public Map<String, Object> getServiceByIdUser(Long id, String authorization) {
        Map<String, Object> response;

        UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));

        try {
            ServiceModel serviceModel = serviceDao.get(id);

            if (serviceModel != null && userModel.getId() == serviceModel.getUserModel().getId()){

                //add counting view

                serviceModel.setCountView(serviceModel.getCountView() + 1L);
                serviceModel = serviceDao.save(serviceModel);

                ReqServiceDto dto = new ReqServiceDto();
                dto.setId(serviceModel.getId());
                dto.setServiceName(serviceModel.getServiceName());
                dto.setCategoryId(serviceModel.getCategoryModel().getId());
                dto.setCategoryDetailId(serviceModel.getCategoryDetailId());
                dto.setRegenciesId(serviceModel.getRegenciesModel().getId());
                dto.setDescription(serviceModel.getDescription());
                dto.setRedemptionInstructions(serviceModel.getRedemptionInstructions());
                dto.setCancellationPolicy(serviceModel.getCancellationPolicy());
                dto.setTermsConditions(serviceModel.getTermsConditions());
                dto.setFacilities(serviceModel.getFacilities());
                dto.setLocationIframe(serviceModel.getLocationIframe());
                dto.setCountRating(serviceModel.getCountRating());
                dto.setCountReview(serviceModel.getCountReview());
                dto.setUserId(serviceModel.getUserModel().getId());
                dto.setCloseDays(serviceModel.getCloseDays());
                dto.setCloseDates(serviceModel.getCloseDates());
                dto.setPublish(serviceModel.getPublish());
                dto.setImages1(serviceModel.getImages1());
                dto.setImages2(serviceModel.getImages2());
                dto.setImages3(serviceModel.getImages3());
                dto.setImages4(serviceModel.getImages4());
                dto.setImages5(serviceModel.getImages5());
                List<ReqServiceDetailDto> dtos = new ArrayList<>();
                List<ServiceDetailModel> detailModel = serviceDetailDao.findAllByServiceId(serviceModel.getId());
                for (ServiceDetailModel model : detailModel) {
                    ReqServiceDetailDto detailDto = new ReqServiceDetailDto();
                    detailDto.setId(model.getId());
                    detailDto.setName(model.getName());
                    detailDto.setDescription(model.getDescription());
                    detailDto.setPrice(model.getPrice());
                    detailDto.setPublish(model.getPublish());
                    if(model.getStockEmpty().equals("1")){
                        detailDto.setStockEmpty(true);
                    }else{
                        detailDto.setStockEmpty(false);
                    }
                    dtos.add(detailDto);
                }
                dto.setFormDetailService(dtos);
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dto, 1);
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.SERVICE_NOT_FOUND + " for id " + id, "", "");
            }

        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> getServiceById(Long id) {
        Map<String, Object> response;


        try {
            ServiceModel serviceModel = serviceDao.get(id);

            if (serviceModel != null){

                //add counting view

                serviceModel.setCountView(serviceModel.getCountView() + 1L);
                serviceModel = serviceDao.save(serviceModel);

                ReqServiceDto dto = new ReqServiceDto();
                dto.setId(serviceModel.getId());
                dto.setServiceName(serviceModel.getServiceName());
                dto.setCategoryId(serviceModel.getCategoryModel().getId());
                dto.setCategoryDetailId(serviceModel.getCategoryDetailId());
                dto.setRegenciesId(serviceModel.getRegenciesModel().getId());
                dto.setDescription(serviceModel.getDescription());
                dto.setRedemptionInstructions(serviceModel.getRedemptionInstructions());
                dto.setCancellationPolicy(serviceModel.getCancellationPolicy());
                dto.setTermsConditions(serviceModel.getTermsConditions());
                dto.setFacilities(serviceModel.getFacilities());
                dto.setLocationIframe(serviceModel.getLocationIframe());
                dto.setCountRating(serviceModel.getCountRating());
                dto.setCountReview(serviceModel.getCountReview());
                dto.setUserId(serviceModel.getUserModel().getId());
                dto.setPublish(serviceModel.getPublish());
                dto.setCloseDates(serviceModel.getCloseDates());
                dto.setCloseDays(serviceModel.getCloseDays());
                dto.setImages1(serviceModel.getImages1());
                dto.setImages2(serviceModel.getImages2());
                dto.setImages3(serviceModel.getImages3());
                dto.setImages4(serviceModel.getImages4());
                dto.setImages5(serviceModel.getImages5());
                dto.setRegencies(serviceModel.getRegenciesModel().getName());
                dto.setCategory(serviceModel.getCategoryModel().getTitle());
                List<ReqServiceDetailDto> dtos = new ArrayList<>();
                List<ServiceDetailModel> detailModel = serviceDetailDao.findAllByServiceId(serviceModel.getId());
                for (ServiceDetailModel model : detailModel) {
                    ReqServiceDetailDto detailDto = new ReqServiceDetailDto();
                    detailDto.setId(model.getId());
                    detailDto.setName(model.getName());
                    detailDto.setDescription(model.getDescription());
                    detailDto.setPrice(model.getPrice());
                    detailDto.setPublish(model.getPublish());
                    if(model.getStockEmpty().equals("1")){
                        detailDto.setStockEmpty(true);
                    }else{
                        detailDto.setStockEmpty(false);
                    }
                    dtos.add(detailDto);
                }
                dto.setFormDetailService(dtos);
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dto, 1);
            } else {
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.SERVICE_NOT_FOUND + " for id " + id, "", "");
            }

        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> findAllServiceDetail() {
        Map<String, Object> response;

        try {
            List<ServiceDetailDto> serviceDetailDtoList = new ArrayList<>();
            List<ServiceDetailModel> serviceDetailModelList = serviceDetailDao.findAll();
            for (ServiceDetailModel serviceDetailModel : serviceDetailModelList) {
                ServiceDetailDto dto = new ServiceDetailDto();
                dto.setId(serviceDetailModel.getId());
                dto.setName(serviceDetailModel.getName());
                dto.setPrice(serviceDetailModel.getPrice());
                dto.setDescription(serviceDetailModel.getDescription());

                ServiceDto serviceDto = new ServiceDto();
                serviceDto.setId(serviceDetailModel.getServiceModel().getId());
                serviceDto.setServiceName(serviceDetailModel.getServiceModel().getServiceName());

                CategoryDto categoryDto = new CategoryDto();
                categoryDto.setId(serviceDetailModel.getServiceModel().getCategoryModel().getId());
                categoryDto.setTitle(serviceDetailModel.getServiceModel().getCategoryModel().getTitle());
                categoryDto.setDescription(serviceDetailModel.getServiceModel().getCategoryModel().getDescription());
                categoryDto.setDeleted(serviceDetailModel.getServiceModel().getCategoryModel().getDeleted());

                serviceDto.setCategoryDto(categoryDto);

                RegenciesDto regenciesDto = new RegenciesDto();
                regenciesDto.setId(serviceDetailModel.getServiceModel().getRegenciesModel().getId());
                regenciesDto.setName(serviceDetailModel.getServiceModel().getRegenciesModel().getName());

                ProvincesDto provincesDto = new ProvincesDto();
                provincesDto.setId(serviceDetailModel.getServiceModel().getRegenciesModel().getProvincesModel().getId());
                provincesDto.setName(serviceDetailModel.getServiceModel().getRegenciesModel().getProvincesModel().getName());

                regenciesDto.setProvincesDto(provincesDto);

                serviceDto.setRegenciesDto(regenciesDto);
                serviceDto.setDescription(serviceDetailModel.getServiceModel().getDescription());
                serviceDto.setRedemptionInstructions(serviceDetailModel.getServiceModel().getRedemptionInstructions());
                serviceDto.setCancellationPolicy(serviceDetailModel.getServiceModel().getCancellationPolicy());
                serviceDto.setTermsConditions(serviceDetailModel.getServiceModel().getTermsConditions());
                serviceDto.setPublish(serviceDetailModel.getServiceModel().getPublish());
                serviceDto.setDeleted(serviceDetailModel.getServiceModel().getDeleted());

                dto.setServiceDto(serviceDto);
                dto.setPublish(serviceDetailModel.getPublish());
                dto.setDeleted(serviceDetailModel.getDeleted());
                serviceDetailDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, "", serviceDetailDtoList, serviceDetailDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> insert(ReqServiceDto reqServiceDto, String authorization) {
        Map<String, Object> response;

        try {
            ServiceModel model = new ServiceModel();
            model.setServiceName(reqServiceDto.getServiceName());

            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            model.setUserModel(userModel);

            CategoryModel categoryModel = categoryDao.get(reqServiceDto.getCategoryId());
            model.setCategoryModel(categoryModel);

            if(reqServiceDto.getCategoryDetailId() != null && !reqServiceDto.getCategoryDetailId().equals("")){
//                CategoryDetailModel categoryDetailModel = categoryDetailDao.get(reqServiceDto.getCategoryDetailId());
                model.setCategoryDetailId(reqServiceDto.getCategoryDetailId());
            }

            RegenciesModel regenciesModel = regenciesDao.get(reqServiceDto.getRegenciesId());
            model.setRegenciesModel(regenciesModel);

            model.setDescription(reqServiceDto.getDescription());
            model.setFacilities(reqServiceDto.getFacilities());
            model.setLocationIframe(reqServiceDto.getLocationIframe());
//            model.setRedemptionInstructions(reqServiceDto.getRedemptionInstructions());
//            model.setCancellationPolicy(reqServiceDto.getCancellationPolicy());
            model.setTermsConditions(reqServiceDto.getTermsConditions());
            model.setPublish(reqServiceDto.getPublish());
            model.setDeleted(Constant.BooleanStatus.FALSE);
            model.setApprove(Constant.BooleanStatus.FALSE);
            model.setDate(new Date());
            model.setCountView(0L);
            model.setCountRating(0L);
            model.setCountReview(0L);
            model.setCloseDays(reqServiceDto.getCloseDays());
            model.setCloseDates(reqServiceDto.getCloseDates());

            model = serviceDao.save(model);

            for (ReqServiceDetailDto dto : reqServiceDto.getFormDetailService()) {
                ServiceDetailModel serviceDetailModel = new ServiceDetailModel();
                serviceDetailModel.setName(dto.getName());
                serviceDetailModel.setDescription(dto.getDescription());
                serviceDetailModel.setPrice(dto.getPrice());
                serviceDetailModel.setServiceModel(model);
                serviceDetailModel.setPublish(dto.getPublish());
                serviceDetailModel.setDeleted(Constant.BooleanStatus.FALSE);
                if(dto.getStockEmpty()){
                    serviceDetailModel.setStockEmpty(Constant.BooleanStatus.TRUE);
                }else{
                    serviceDetailModel.setStockEmpty(Constant.BooleanStatus.FALSE);
                }
                serviceDetailDao.save(serviceDetailModel);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, model.getId(), 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> insertImage(Long id, MultipartFile img1,
                                           MultipartFile img2, MultipartFile img3,
                                           MultipartFile img4, MultipartFile img5) {
        Map<String, Object> response;
        ServiceModel model = serviceDao.get(id);
        if (model != null) {
            if (img1 != null) {
                model.setImages1(imageUtils.uploadImage(img1));
            }
            if (img2 != null) {
                model.setImages2(imageUtils.uploadImage(img2));
            }
            if (img3 != null) {
                model.setImages3(imageUtils.uploadImage(img3));
            }
            if (img4 != null) {
                model.setImages4(imageUtils.uploadImage(img4));
            }
            if (img5 != null) {
                model.setImages5(imageUtils.uploadImage(img5));
            }
            model = serviceDao.save(model);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, model.getId(), 1);
        } else {
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, "Service not found", "", "");
        }
        return response;
    }

    public Map<String, Object> deleteImageService(ServiceDto serviceDto, String authorization){
        Map<String, Object> response;
        Boolean status = imageUtils.deleteImage(serviceDto.getImageDel());
        if(status){
            ServiceModel serviceModel = serviceDao.get(serviceDto.getId());
            if(serviceDto.getIndexImageDel() == 1){
                serviceModel.setImages1("");
            }else if(serviceDto.getIndexImageDel() == 2){
                serviceModel.setImages2("");
            }else if(serviceDto.getIndexImageDel() == 3){
                serviceModel.setImages3("");
            }else if(serviceDto.getIndexImageDel() == 4){
                serviceModel.setImages4("");
            }else if(serviceDto.getIndexImageDel() == 5){
                serviceModel.setImages5("");
            }

            serviceDao.save(serviceModel);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);
        }
        else
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, Constant.STATUS.ERROR, "", "");
        return response;
    }

    public Map<String, Object> update(ReqServiceDto reqServiceDto) {
        Map<String, Object> response;

        try {
            ServiceModel model = serviceDao.get(reqServiceDto.getId());

            //cek this guys
            List<ServiceDetailModel> detailModelList = serviceDetailDao.findAllByServiceId(reqServiceDto.getId());
            for (ServiceDetailModel d : detailModelList) {
                boolean cek = true;
                for (ReqServiceDetailDto dto : reqServiceDto.getFormDetailService()) {
                    if(dto.getId() == d.getId()){
                        cek = false;
                    }
                }
                if(cek){
                    ServiceDetailModel serviceDetailModel = serviceDetailDao.get(d.getId());
                    serviceDetailModel.setDeleted(Constant.BooleanStatus.TRUE);
                    serviceDetailDao.save(serviceDetailModel);
                }
            }

            if (model != null) {
                model.setServiceName(reqServiceDto.getServiceName());

                CategoryModel categoryModel = categoryDao.get(reqServiceDto.getCategoryId());
                if (categoryModel == null) {
                    return ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.CATEGORY_NOT_FOUND + " for id " + reqServiceDto.getCategoryId(), "", "");
                }
                model.setCategoryModel(categoryModel);

//                if(reqServiceDto.getCategoryDetailId() != null &&  reqServiceDto.getCategoryDetailId() != 0 && !reqServiceDto.getCategoryDetailId().equals("")){
//                    CategoryDetailModel categoryDetailModel = categoryDetailDao.get(reqServiceDto.getCategoryDetailId());
//                    if (categoryDetailModel == null) {
//                        return ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.CATEGORY_NOT_FOUND + " for id " + reqServiceDto.getCategoryId(), "", "");
//                    }
//                    model.setCategoryDetailId(reqServiceDto.getCategoryDetailId());
//                }


                RegenciesModel regenciesModel = regenciesDao.get(reqServiceDto.getRegenciesId());
                if (regenciesModel == null) {
                    return ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.REGENCIES_NOT_FOUND + " for id " + reqServiceDto.getRegenciesId(), "", "");
                }
                model.setRegenciesModel(regenciesModel);
                model.setCategoryDetailId(reqServiceDto.getCategoryDetailId());
                model.setDescription(reqServiceDto.getDescription());
                model.setFacilities(reqServiceDto.getFacilities());
                model.setLocationIframe(reqServiceDto.getLocationIframe());
//                model.setRedemptionInstructions(reqServiceDto.getRedemptionInstructions());
//                model.setCancellationPolicy(reqServiceDto.getCancellationPolicy());
                model.setTermsConditions(reqServiceDto.getTermsConditions());
                model.setPublish(reqServiceDto.getPublish());
                model.setApprove(Constant.BooleanStatus.FALSE);
                model.setDeleted(Constant.BooleanStatus.FALSE);
                model.setCloseDays(reqServiceDto.getCloseDays());
                model.setCloseDates(reqServiceDto.getCloseDates());
                model = serviceDao.save(model);

                for (ReqServiceDetailDto dto : reqServiceDto.getFormDetailService()) {
                    ServiceDetailModel serviceDetailModel;
                    if(dto.getId() != null && !dto.getId().equals("")) {
                        serviceDetailModel = serviceDetailDao.get(dto.getId());
                        if (serviceDetailModel == null) {
                            serviceDetailModel = new ServiceDetailModel();
                        }
                    }else{
                        serviceDetailModel = new ServiceDetailModel();
                    }
                    serviceDetailModel.setName(dto.getName());
                    serviceDetailModel.setDescription(dto.getDescription());
                    serviceDetailModel.setPrice(dto.getPrice());
                    serviceDetailModel.setServiceModel(model);
                    serviceDetailModel.setPublish(dto.getPublish());
                    serviceDetailModel.setDeleted(Constant.BooleanStatus.FALSE);
                    if(dto.getStockEmpty()){
                        serviceDetailModel.setStockEmpty(Constant.BooleanStatus.TRUE);
                    }else{
                        serviceDetailModel.setStockEmpty(Constant.BooleanStatus.FALSE);
                    }

                    serviceDetailDao.save(serviceDetailModel);

                }



                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", "");
            } else {
                return ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.SERVICE_NOT_FOUND + " for id " + reqServiceDto.getId(), "", "");
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> delete(ReqServiceDto reqServiceDto) {
        Map<String, Object> response;

        try {
            ServiceModel model = serviceDao.get(reqServiceDto.getId());
            if (model == null) {
                return ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, Constant.STATUS.SERVICE_NOT_FOUND + " for id " + reqServiceDto.getId(), "", "");
            }
            model.setDeleted(Constant.BooleanStatus.TRUE);
            serviceDao.save(model);

            List<ServiceDetailModel> serviceDetailModelList = serviceDetailDao.findAllByServiceId(reqServiceDto.getId());
            if (serviceDetailModelList != null) {
                for (ServiceDetailModel serviceDetailModel : serviceDetailModelList) {
                    serviceDetailModel.setDeleted(Constant.BooleanStatus.TRUE);
                    serviceDetailDao.save(serviceDetailModel);
                }
            }
            return ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", "");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    private String generateParam(PagingDto pagingDto) {
        String param = "";

        if (pagingDto.getRegencyId() != null && !pagingDto.getRegencyId().isEmpty()) {
            param = param + " and regenciesModel.id in (" + pagingDto.getRegencyId() + ") ";
        }

        if (pagingDto.getKeyword() != null && !pagingDto.getKeyword().isEmpty()) {
            param = param + " and categoryModel.id = " + pagingDto.getKeyword();
        }

        logger.info("Parameter pagging : " + param);
        return param;
    }



    public Map<String, Object> findAllServiceManual(SearchServiceDto searchServiceDto) {
        Map<String, Object> response;

        try {
            List<ServiceDto> serviceDtoList = serviceDao.findAllManual(generateParamService(searchServiceDto, true));
            List<ServiceDto> serviceDtoList2 = serviceDao.findAllManual(generateParamService(searchServiceDto, false));
//            String t = serviceDao.getCountAllManual(generateParamService(searchServiceDto, false));
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, serviceDtoList, serviceDtoList2.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    private String generateParamService(SearchServiceDto searchServiceDto, Boolean usePagging) {
        String param = "";

        if (searchServiceDto.getListCategoryId() != null && searchServiceDto.getListCategoryId().size() >= 1) {
            String  t = "";
            for (int i = 0; i < searchServiceDto.getListCategoryId().size(); i++) {
                t += "ts.category_id = " + searchServiceDto.getListCategoryId().get(i);
                if(i + 1 < searchServiceDto.getListCategoryId().size()) t+= " OR ";
            }
            param += " and (" + t +") ";
        }

        if (searchServiceDto.getListRegencyId() != null && searchServiceDto.getListRegencyId().size() >= 1) {
            String  t = "";
            for (int i = 0; i < searchServiceDto.getListRegencyId().size(); i++) {
                t += "ts.regencies_id = " + searchServiceDto.getListRegencyId().get(i);
                if(i + 1 < searchServiceDto.getListRegencyId().size()) t+= " OR ";
            }
            param += " and (" + t +") ";
        }

        if (searchServiceDto.getListRating() != null && searchServiceDto.getListRating().size() >= 1) {
            String  t = "";
            for (int i = 0; i < searchServiceDto.getListRating().size(); i++) {
                t += "ts.count_rating = " + searchServiceDto.getListRating().get(i);
                if(i + 1 < searchServiceDto.getListRating().size()) t+= " OR ";
            }
            param += " and (" + t +") ";
        }

        if (searchServiceDto.getListCategoryDetailId() != null && searchServiceDto.getListCategoryDetailId().size() >= 1) {
            String  t = "";
            for (int i = 0; i < searchServiceDto.getListCategoryDetailId().size(); i++) {
                t += "ts.category_detail_id = " + searchServiceDto.getListCategoryDetailId().get(i);
                if(i + 1 < searchServiceDto.getListCategoryDetailId().size()) t+= " OR ";
            }
            param += " and (" + t +") ";
        }

        if (searchServiceDto.getKeyword() != null && !searchServiceDto.getKeyword().isEmpty()) {
            param +=  " and ts.service_name like '%" + searchServiceDto.getKeyword()+ "%'";
        }

        param += "  group by ts.id order by ts.id desc ";

        if(usePagging){
            if(searchServiceDto.getPage() != null){
                param += setPagging(searchServiceDto.getPage(), searchServiceDto.getPerPage());
            }else{
                param += setPagging(1, searchServiceDto.getPerPage());
            }
        }


        logger.info("Parameter pagging : " + param);
        return param;
    }

    private String setPagging(int page, int perPage){
        Integer serviceLimit = perPage;
        Integer offset = (page-1) * serviceLimit;
        return " LIMIT " + offset + ", "+ serviceLimit;
    }

    public ResponseServiceDto getListDT(ServiceDto serviceDto,  RequestPagingDataTablesBaseDto rpdt){
        ResponseServiceDto responseServiceDto = new ResponseServiceDto();
        List<ServiceDto> listResult = new ArrayList<ServiceDto>();
        String sum = "0";
        listResult = serviceDao.getListDT(serviceDto, rpdt);
        sum = serviceDao.getCountListDT(serviceDto);
        responseServiceDto.setRecordsTotal(sum);
        responseServiceDto.setData(listResult);
        return responseServiceDto;
    }

    public ServiceDto getServiceByIdAdmin(Long id) {
        Map<String, Object> response = new HashMap<>();
        ServiceDto dto = new ServiceDto();
        try {
            ServiceModel serviceModel = serviceDao.get(id);
            dto.setId(serviceModel.getId());
            dto.setServiceName(serviceModel.getServiceName());
            dto.setCategoryName(serviceModel.getCategoryModel().getTitle());
            dto.setUserName(serviceModel.getUserModel().getName());
            dto.setDate(DateUtils.formatDateToStr(serviceModel.getDate(), "yyyy-mm-dd"));
        } catch (Exception e) {
            return new ServiceDto();
        }

        return dto;
    }

    public List<ServiceDetailDto> getServiceDetailByIdAdmin(Long id) {
        Map<String, Object> response = new HashMap<>();
        List<ServiceDetailDto> serviceDetailDtos;
        try {
            serviceDetailDtos = serviceDetailDao.findAllByServiceIdManual(id);
        } catch (Exception e) {
            return new ArrayList<ServiceDetailDto>();
        }

        return serviceDetailDtos;
    }

    public Boolean saveDataForm(CategoryDto dto) {
        CategoryModel categoryModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            categoryModel = categoryDao.get(dto.getId());
        }else{
            categoryModel = new CategoryModel();
            categoryModel.setDeleted("0");
        }
        categoryModel.setTitle(dto.getTitle());
        categoryModel.setDescription(dto.getDescription());
        categoryModel.setImages(dto.getImages());
        categoryModel.setPublish(dto.getPublish());
        try {
            categoryDao.save(categoryModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }


    public Boolean setApproveService(ServiceDto dto) {
        ServiceModel serviceModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            serviceModel = serviceDao.get(dto.getId());
        }else{
            return false;
        }
        serviceModel.setApprove(dto.getApprove());
        try {
            serviceDao.save(serviceModel);

            UserModel userModelNOtif = userDao.get(serviceModel.getUserModel().getId());
            NotifModel notifModel = new NotifModel();
            notifModel.setDate(new Date());
            notifModel.setDeleted("0");
            notifModel.setIsRead("0");
            notifModel.setPublish("1");
            notifModel.setUserModel(serviceModel.getUserModel());


            if(dto.getApprove().equals("1")){
                notifModel.setUrl(urlFrontend + "/info/"+serviceModel.getId()+"-"+serviceModel.getServiceName().replace(" ", "-"));
                notifModel.setNotif("Your service is approval");
            }else{
                notifModel.setUrl(urlFrontend + "/member/service");
                notifModel.setNotif("Your service is reject");
            }
            notifDao.save(notifModel);

            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }

    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }

    public Map<String, Object> findAllByUserId(Long userId) {
        Map<String, Object> response;
        UserModel userModel = userDao.get(userId);
        try {
            List<ServiceDto> serviceDtoList = serviceDao.findAllByUser(userModel.getId());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, serviceDtoList, serviceDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }


    public Map<String, Object> insertImageEditor(MultipartFile img1) {
        Map<String, Object> response;
        String pathName = imageUtils.uploadImage(img1);
        response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, pathName, 1);

        return response;
    }
}
