package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.*;
import id.her.trapedia.dto.CategoryDto;
import id.her.trapedia.dto.ReqWishlistDto;
import id.her.trapedia.model.*;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.ResponseGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class WishlistService {
    private Logger logger = LogManager.getLogger(WishlistService.class);

    @Autowired
    WishlistDao wishlistDao;

    @Autowired
    ServiceDetailDao serviceDetailDao;

    @Autowired
    ServiceDao serviceDao;

    @Autowired
    UserDao userDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    public Map<String, Object> insert(ReqWishlistDto reqWishlistDto, String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            WishlistModel t = wishlistDao.cekWishlistByUser(reqWishlistDto.getServiceDetailId(), userModel.getId());

            if(t == null){
                WishlistModel wishlistModel = new WishlistModel();
                ServiceDetailModel serviceDetailModel = serviceDetailDao.get(reqWishlistDto.getServiceDetailId());
                wishlistModel.setServiceDetailModel(serviceDetailModel);
                wishlistModel.setUserModel(userModel);
                wishlistModel = wishlistDao.save(wishlistModel);
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);
            }else{
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, Constant.STATUS.ERROR, "", "");
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public Map<String, Object> getWishlistByMember(String authorization) {
        Map<String, Object> response;

        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            List<ReqWishlistDto> wishlistDto = new ArrayList<>();
            List<WishlistModel> wm = wishlistDao.findAllByUser(userModel.getId());
            for (WishlistModel wishlistModel : wm) {
                ReqWishlistDto dto = new ReqWishlistDto();
                ServiceDetailModel serviceDetailModel = serviceDetailDao.get(wishlistModel.getServiceDetailModel().getId());
                ServiceModel serviceModel = serviceDao.get(serviceDetailModel.getServiceModel().getId());

                dto.setId(wishlistModel.getId());
                dto.setServiceId(serviceModel.getId());
                dto.setServiceName(serviceModel.getServiceName());
                dto.setImage(serviceModel.getImages1());
                dto.setPrice(serviceDetailModel.getPrice());
                dto.setServiceDetailId(serviceDetailModel.getId());
                dto.setServiceDetailName(serviceDetailModel.getName());
                wishlistDto.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, wishlistDto, wishlistDto.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }

    public Map<String, Object> delete(ReqWishlistDto reqWishlistDto, String authorization) {
        Map<String, Object> response;

        try {
            WishlistModel wishlistModel = wishlistDao.get(reqWishlistDto.getId());
            wishlistDao.delete(wishlistModel.getId());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);

        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

}
