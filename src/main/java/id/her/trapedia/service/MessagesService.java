package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.*;
import id.her.trapedia.dto.MessagesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseMessagesDto;
import id.her.trapedia.dto.SubscribeDto;
import id.her.trapedia.model.*;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ResponseGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class MessagesService {

    private Logger logger = LogManager.getLogger(MessagesService.class);

    @Autowired
    private MessagesDao messagesDao;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private NotifDao notifDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    @Value("${trapedia.url.backend}")
    private String urlBackend;

    @Value("${trapedia.url.frontend}")
    private String urlFrontend;



    public Map<String, Object> add(MessagesDto messagesDto, String authorization) {
        Map<String, Object> response;
        try {
            MessagesModel messagesModel = new MessagesModel();
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            ServiceModel serviceModel = serviceDao.get(messagesDto.getServiceId());

            messagesModel.setReplayId(0L);
            messagesModel.setSubject("Message from : " + serviceModel.getServiceName());
            messagesModel.setMessage(messagesDto.getMessage());
            messagesModel.setToUserModel(serviceModel.getUserModel());
            messagesModel.setUserModel(userModel);
            messagesModel.setIsReadToUserId("0");
            messagesModel.setIsReadUserId("1");
            messagesModel.setDate(new Date());
            messagesModel = messagesDao.save(messagesModel);

            //SEND NOTIFIKASI MESSAGES PROFILE
            UserModel userModelNOtif = userDao.get(serviceModel.getUserModel().getId());
            NotifModel notifModel = new NotifModel();
            notifModel.setDate(new Date());
            notifModel.setDeleted("0");
            notifModel.setIsRead("0");
            notifModel.setPublish("1");
            notifModel.setUrl(urlFrontend + "/member/messages/detail/" + messagesModel.getId());
            notifModel.setNotif("you have message from " + userModel.getName());
            notifModel.setUserModel(userModelNOtif);
            notifDao.save(notifModel);

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> getListMessageByUser(String authorization) {
        Map<String, Object> response;
        try {
            List<MessagesDto> dto = new ArrayList<>();
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            List<MessagesModel> messagesDtoList = messagesDao.getMessageByUser(userModel.getId());
            for (MessagesModel messagesModel : messagesDtoList) {
                MessagesDto m = new MessagesDto();
                m.setId(messagesModel.getId());
                m.setMessage(messagesModel.getMessage());
                m.setSubject(messagesModel.getSubject());
                m.setDate(DateUtils.formatDateToStr(messagesModel.getDate(), "yyyy-MM-dd"));

                m.setToUserId(messagesModel.getToUserModel().getId());
                m.setToUserName(messagesModel.getToUserModel().getName());
                m.setIsReadToUserId(messagesModel.getIsReadToUserId());
                m.setImageToUser(messagesModel.getToUserModel().getImage());

                m.setUserId(messagesModel.getUserModel().getId());
                m.setUserName(messagesModel.getUserModel().getName());
                m.setIsReadUserId(messagesModel.getIsReadUserId());
                m.setImageUser(messagesModel.getUserModel().getImage());

                dto.add(m);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dto, dto.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> getMessageDetailByUser(String authorization, Long id) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            MessagesModel messagesModel = messagesDao.getMessageDetailByUser(userModel.getId(), id);

            MessagesDto m = new MessagesDto();
            m.setId(messagesModel.getId());
            m.setMessage(messagesModel.getMessage());
            m.setSubject(messagesModel.getSubject());
            m.setDate(DateUtils.formatDateToStr(messagesModel.getDate(), "yyyy-MM-dd"));

            m.setToUserId(messagesModel.getToUserModel().getId());
            m.setToUserName(messagesModel.getToUserModel().getName());
            m.setIsReadToUserId(messagesModel.getIsReadToUserId());
            m.setImageToUser(messagesModel.getToUserModel().getImage());

            m.setUserId(messagesModel.getUserModel().getId());
            m.setUserName(messagesModel.getUserModel().getName());
            m.setIsReadUserId(messagesModel.getIsReadUserId());
            m.setImageUser(messagesModel.getUserModel().getImage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, m, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> addDetail(MessagesDto messagesDto, String authorization) {
        Map<String, Object> response;
        try {
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            MessagesModel messagesModel = messagesDao.getMessageDetailByUser(userModel.getId(), messagesDto.getId());

            MessagesModel add = new MessagesModel();
            add.setReplayId(messagesModel.getId());
            add.setSubject("");
            add.setMessage(messagesDto.getMessage());
            add.setToUserModel(messagesModel.getUserModel());
            add.setUserModel(userModel);

            add.setIsReadToUserId("0");
            add.setIsReadUserId("0");
            add.setDate(new Date());
            add = messagesDao.save(add);

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, "", 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }


    public Map<String, Object> getMessageDetailListByUser(String authorization, Long id) {
        Map<String, Object> response;
        try {
            List<MessagesDto> dto = new ArrayList<>();
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            List<MessagesModel> messagesDtoList = messagesDao.getMessageDetailListByUser(userModel.getId(), id);
            for (MessagesModel messagesModel : messagesDtoList) {
                MessagesDto m = new MessagesDto();
                m.setId(messagesModel.getId());
                m.setMessage(messagesModel.getMessage());
                m.setSubject(messagesModel.getSubject());
                m.setDate(DateUtils.formatDateToStr(messagesModel.getDate(), "yyyy-MM-dd"));

                m.setToUserId(messagesModel.getToUserModel().getId());
                m.setToUserName(messagesModel.getToUserModel().getName());
                m.setIsReadToUserId(messagesModel.getIsReadToUserId());
                m.setImageToUser(messagesModel.getToUserModel().getImage());

                m.setUserId(messagesModel.getUserModel().getId());
                m.setUserName(messagesModel.getUserModel().getName());
                m.setIsReadUserId(messagesModel.getIsReadUserId());
                m.setImageUser(messagesModel.getUserModel().getImage());

                dto.add(m);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dto, dto.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }

    public ResponseMessagesDto getListDT(MessagesDto messagesDto,
                                         RequestPagingDataTablesBaseDto requestPagingDataTablesBaseDto){
        ResponseMessagesDto responseMessagesDto = new ResponseMessagesDto();
        List<MessagesDto> listResult = new ArrayList<MessagesDto>();
        String sum = "0";
        listResult = messagesDao.getListDT(messagesDto, requestPagingDataTablesBaseDto);
        sum = messagesDao.getCountListDT(messagesDto);
        responseMessagesDto.setRecordsTotal(sum);
        responseMessagesDto.setData(listResult);
        return responseMessagesDto;
    }

    public MessagesDto getMessagesByIdAdmin(Long id) {
        Map<String, Object> response = new HashMap<>();
        MessagesDto dto = new MessagesDto();
        try {
            MessagesModel messagesModel = messagesDao.get(id);
            dto.setId(messagesModel.getId());
            dto.setMessage(messagesModel.getMessage());
            dto.setDeleted(messagesModel.getDeleted());
            dto.setPublish(messagesModel.getPublish());
        } catch (Exception e) {
            return new MessagesDto();
        }

        return dto;
    }

    public Boolean saveDataForm(MessagesDto dto) {
        MessagesModel messagesModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            messagesModel = messagesDao.get(dto.getId());
        }else{
            messagesModel = new MessagesModel();
            messagesModel.setDeleted("0");
        }
        messagesModel.setMessage(dto.getMessage());
        messagesModel.setPublish(dto.getPublish());
        try {
            messagesDao.save(messagesModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }

}
