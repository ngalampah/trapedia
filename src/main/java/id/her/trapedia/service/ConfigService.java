package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.AuthenticationDao;
import id.her.trapedia.dao.ConfigDao;
import id.her.trapedia.dao.UserDao;
import id.her.trapedia.dao.VerificationTokenDao;
import id.her.trapedia.dto.AdminDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseUserDto;
import id.her.trapedia.dto.UserDto;
import id.her.trapedia.model.AdminModel;
import id.her.trapedia.model.ConfigModel;
import id.her.trapedia.model.UserModel;
import id.her.trapedia.model.VerificationTokenModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ResponseGenerator;
import id.her.trapedia.util.TrapediaHelper;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ConfigService {

    private Logger logger = LogManager.getLogger(ConfigService.class);

    @Autowired
    private ConfigDao configDao;


    public Map<String, Object> getConfigById(String configName) {
        Map<String, Object> response;
        try {
            ConfigModel configModel = configDao.getConfigById(configName);

            if (configModel == null) {
                return ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, "Not found", "", "");
            }
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, configModel, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> getConfigAll() {
        Map<String, Object> response;
        try {
            List<ConfigModel> configModel = configDao.getAllConfig();
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, configModel, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

}
