package id.her.trapedia.service;

import id.her.trapedia.config.JwtTokenUtil;
import id.her.trapedia.dao.*;
import id.her.trapedia.dto.CommentServiceDto;
import id.her.trapedia.dto.MessagesDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseCommentServiceDto;
import id.her.trapedia.model.*;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ImageUtils;
import id.her.trapedia.util.ResponseGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

@Service
@Transactional
public class CommentServiceService {

    private Logger logger = LogManager.getLogger(CommentServiceService.class);

    @Autowired
    private CommentServiceDao commentServiceDao;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private NotifDao notifDao;

    @Autowired
    public JwtTokenUtil jwtTokenUtil;

    @Autowired
    private ImageUtils imageUtils;

    @Value("${trapedia.url.backend}")
    private String urlBackend;

    @Value("${trapedia.url.frontend}")
    private String urlFrontend;



    public Map<String, Object> add(CommentServiceDto commentServiceDto, String authorization) {
        Map<String, Object> response;
        try {
            CommentServiceModel commentServiceModel = new CommentServiceModel();
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            ServiceModel serviceModel = serviceDao.get(commentServiceDto.getServiceId());

            commentServiceModel.setParentId(0L);
            commentServiceModel.setComment(commentServiceDto.getComment());
            commentServiceModel.setDate(new Date());
            commentServiceModel.setUserModel(userModel);
            commentServiceModel.setServiceModel(serviceModel);
            commentServiceModel.setPublish("1");
            commentServiceModel.setDeleted("0");

            commentServiceModel = commentServiceDao.save(commentServiceModel);

            commentServiceDto.setId(commentServiceModel.getId());

            //SEND NOTIFIKASI COMMENT SERVICE
            UserModel userModelNOtif = userDao.get(serviceModel.getUserModel().getId());
            NotifModel notifModel = new NotifModel();
            notifModel.setDate(new Date());
            notifModel.setDeleted("0");
            notifModel.setIsRead("0");
            notifModel.setPublish("1");
            notifModel.setUrl(urlFrontend + "/info/"+serviceModel.getId()+"-"+serviceModel.getServiceName().replace(" ", "-"));
            notifModel.setNotif(userModel.getName() + " comment your service");
            notifModel.setUserModel(userModelNOtif);
            notifDao.save(notifModel);


            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, commentServiceDto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> addDetail(CommentServiceDto commentServiceDto, String authorization) {
        Map<String, Object> response;
        try {
            CommentServiceModel commentServiceModel = new CommentServiceModel();
            UserModel userModel = userDao.findUserModelByUsername(getUsernameFromToken(authorization));
            ServiceModel serviceModel = serviceDao.get(commentServiceDto.getServiceId());

            commentServiceModel.setParentId(commentServiceDto.getId());
            commentServiceModel.setComment(commentServiceDto.getComment());
            commentServiceModel.setDate(new Date());
            commentServiceModel.setUserModel(userModel);
            commentServiceModel.setServiceModel(serviceModel);
            commentServiceModel.setPublish("1");
            commentServiceModel.setDeleted("0");

            commentServiceModel = commentServiceDao.save(commentServiceModel);

            commentServiceDto.setId(commentServiceModel.getId());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, commentServiceDto, 1);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> getListMessageByService(CommentServiceDto commentServiceDto) {
        Map<String, Object> response;
        try {
            List<CommentServiceDto> dto = commentServiceDao.getCommentByServicePagging(commentServiceDto);
            String count = commentServiceDao.getCommentByServiceCount(commentServiceDto);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dto, Integer.valueOf(count));
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> getListMessageByCommentId(CommentServiceDto commentServiceDto) {
        Map<String, Object> response;
        try {
            List<CommentServiceDto> dto = commentServiceDao.getCommentByCommentIdPagging(commentServiceDto);
            String count = commentServiceDao.getCommentByCommentIdCount(commentServiceDto);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, dto, Integer.valueOf(count));
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }

    public Map<String, Object> insertImage(Long id, MultipartFile img1,
                                           MultipartFile img2, MultipartFile img3,
                                           MultipartFile img4, MultipartFile img5) {
        Map<String, Object> response;
        CommentServiceModel model = commentServiceDao.get(id);
        if (model != null) {
            if (img1 != null) {
                model.setImages1(imageUtils.uploadImage(img1));
            }
            if (img2 != null) {
                model.setImages2(imageUtils.uploadImage(img2));
            }
            if (img3 != null) {
                model.setImages3(imageUtils.uploadImage(img3));
            }
            if (img4 != null) {
                model.setImages4(imageUtils.uploadImage(img4));
            }
            if (img5 != null) {
                model.setImages5(imageUtils.uploadImage(img5));
            }
            model = commentServiceDao.save(model);
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, model.getId(), 1);
        } else {
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_404, "Service not found", "", "");
        }
        return response;
    }


    public String getUsernameFromToken(String requestTokenHeader) {
        String username = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            System.out.println("JWT Token does not begin with Bearer String");
        }
        return username;
    }

    public ResponseCommentServiceDto getListDT(CommentServiceDto commentServiceDto,
                                               RequestPagingDataTablesBaseDto requestPagingDataTablesBaseDto){
        ResponseCommentServiceDto responseCommentServiceDto = new ResponseCommentServiceDto();
        List<CommentServiceDto> listResult = new ArrayList<CommentServiceDto>();
        String sum = "0";
        listResult = commentServiceDao.getListDT(commentServiceDto, requestPagingDataTablesBaseDto);
        sum = commentServiceDao.getCountListDT(commentServiceDto);
        responseCommentServiceDto.setRecordsTotal(sum);
        responseCommentServiceDto.setData(listResult);
        return responseCommentServiceDto;
    }

    public CommentServiceDto getCommentByIdAdmin(Long id) {
        Map<String, Object> response = new HashMap<>();
        CommentServiceDto dto = new CommentServiceDto();
        try {
            CommentServiceModel commentServiceModel = commentServiceDao.get(id);
            dto.setId(commentServiceModel.getId());
            dto.setComment(commentServiceModel.getComment());
            dto.setDeleted(commentServiceModel.getDeleted());
            dto.setPublish(commentServiceModel.getPublish());
        } catch (Exception e) {
            return new CommentServiceDto();
        }

        return dto;
    }

    public Boolean saveDataForm(CommentServiceDto dto) {
        CommentServiceModel commentServiceModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            commentServiceModel = commentServiceDao.get(dto.getId());
        }else{
            commentServiceModel = new CommentServiceModel();
            commentServiceModel.setDeleted("0");
        }
        commentServiceModel.setComment(dto.getComment());
        commentServiceModel.setPublish(dto.getPublish());
        try {
            commentServiceDao.save(commentServiceModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }

}
