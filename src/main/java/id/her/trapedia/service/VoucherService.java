package id.her.trapedia.service;

import id.her.trapedia.dao.VoucherDao;
import id.her.trapedia.dto.ReqTransactionDto;
import id.her.trapedia.dto.VoucherDto;
import id.her.trapedia.dto.RequestPagingDataTablesBaseDto;
import id.her.trapedia.dto.ResponseVoucherDto;
import id.her.trapedia.model.VoucherModel;
import id.her.trapedia.util.Constant;
import id.her.trapedia.util.DateUtils;
import id.her.trapedia.util.ResponseGenerator;
import id.her.trapedia.util.TrapediaHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class VoucherService {

    private Logger logger = LogManager.getLogger(VoucherService.class);

    @Autowired
    private VoucherDao voucherDao;

    public Map<String, Object> findAll() {
        Map<String, Object> response;

        try {
            List<VoucherDto> voucherDtoList = new ArrayList<>();
            List<VoucherModel> voucherModelList = voucherDao.findAll();
            for (VoucherModel voucherModel : voucherModelList) {
                VoucherDto dto = new VoucherDto();
                dto.setId(voucherModel.getId());
                dto.setAmount(voucherModel.getAmount());
                dto.setAmountType(voucherModel.getAmountType());
                dto.setDeleted(voucherModel.getDeleted());
                dto.setMaxAmount(voucherModel.getMaxAmount());
                dto.setMinTransaction(voucherModel.getMinTransaction());
                dto.setName(voucherModel.getName());
                dto.setPublish(voucherModel.getPublish());
                dto.setValidDateFrom(DateUtils.formatDateToStr(voucherModel.getValidDateFrom(), "yyyy-mm-dd"));
                dto.setValidDateTo(DateUtils.formatDateToStr(voucherModel.getValidDateTo(), "yyyy-mm-dd"));
                dto.setVoucherCode(voucherModel.getVoucherCode());
                voucherDtoList.add(dto);
            }

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, "", voucherDtoList, voucherDtoList.size());
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());

            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }

        return response;
    }

    public VoucherDto getVoucherById(Long id) {
        VoucherDto dto = new VoucherDto();
        try {
            VoucherModel voucherModel = voucherDao.get(id);
            dto.setId(voucherModel.getId());
            dto.setAmount(voucherModel.getAmount());
            dto.setAmountType(voucherModel.getAmountType());
            dto.setDeleted(voucherModel.getDeleted());
            dto.setMaxAmount(voucherModel.getMaxAmount());
            dto.setMinTransaction(voucherModel.getMinTransaction());
            dto.setName(voucherModel.getName());
            dto.setPublish(voucherModel.getPublish());
            dto.setValidDateFrom(DateUtils.formatDateToStr(voucherModel.getValidDateFrom(), "yyyy-mm-dd"));
            dto.setValidDateTo(DateUtils.formatDateToStr(voucherModel.getValidDateTo(), "yyyy-mm-dd"));
            dto.setVoucherCode(voucherModel.getVoucherCode());
        } catch (Exception e) {
            return new VoucherDto();
        }
        return dto;
    }

    /*** Function for Web MVC ***/

    public List<VoucherDto> list() {
        List<VoucherDto> voucherDtoList = new ArrayList<>();
        try {
            List<VoucherModel> voucherModelList = voucherDao.findAll();
            for (VoucherModel voucherModel : voucherModelList) {
                VoucherDto dto = new VoucherDto();
                dto.setAmount(voucherModel.getAmount());
                dto.setAmountType(voucherModel.getAmountType());
                dto.setDeleted(voucherModel.getDeleted());
                dto.setMaxAmount(voucherModel.getMaxAmount());
                dto.setMinTransaction(voucherModel.getMinTransaction());
                dto.setName(voucherModel.getName());
                dto.setPublish(voucherModel.getPublish());
                dto.setValidDateFrom(DateUtils.formatDateToStr(voucherModel.getValidDateFrom(), "yyyy-mm-dd"));
                dto.setValidDateTo(DateUtils.formatDateToStr(voucherModel.getValidDateTo(), "yyyy-mm-dd"));
                dto.setVoucherCode(voucherModel.getVoucherCode());
                voucherDtoList.add(dto);
            }
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return voucherDtoList;
    }

    public Boolean saveDataForm(VoucherDto dto) {
        VoucherModel voucherModel;

        if(dto.getId() != null && !dto.getId().equals("")){
            voucherModel = voucherDao.get(dto.getId());
        }else{
            voucherModel = new VoucherModel();
            voucherModel.setDeleted(Constant.BooleanStatus.FALSE);
        }
        voucherModel.setAmount(dto.getAmount());
        voucherModel.setAmountType(dto.getAmountType());
        voucherModel.setMaxAmount(dto.getMaxAmount());
        voucherModel.setMinTransaction(dto.getMinTransaction());
        voucherModel.setName(dto.getName());
        voucherModel.setPublish(dto.getPublish());
        voucherModel.setValidDateFrom(DateUtils.convertStringToDate(dto.getValidDateFrom(), "yyyy-mm-dd"));
        voucherModel.setValidDateTo(DateUtils.convertStringToDate(dto.getValidDateTo(), "yyyy-mm-dd"));
        voucherModel.setVoucherCode(dto.getVoucherCode());
        try {
            voucherDao.save(voucherModel);
            return true;
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            logger.info(e);
            return false;
        }
    }

    public VoucherDto delete(VoucherDto dto) {
        try {
            VoucherModel model = voucherDao.get(dto.getId());
            model.setDeleted(Constant.BooleanStatus.TRUE);
            voucherDao.save(model);
        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
        }

        return dto;
    }

    public ResponseVoucherDto getListDT(VoucherDto adminDto, RequestPagingDataTablesBaseDto rpdt){
        ResponseVoucherDto responseVoucherDto = new ResponseVoucherDto();
        List<VoucherDto> listResult = new ArrayList<VoucherDto>();
        String sum = "0";
        listResult = voucherDao.getListDT(adminDto, rpdt);
        sum = voucherDao.getCountListDT(adminDto);
        responseVoucherDto.setRecordsTotal(sum);
        responseVoucherDto.setData(listResult);
        return responseVoucherDto;
    }

    public Map<String, Object> add(ReqTransactionDto reqTransactionDto) {
        Map<String, Object> response;
        try {
            VoucherModel voucherModel = voucherDao.getVoucherByCode(reqTransactionDto.getVoucherCode());
            long discount = 0;

            if(voucherModel != null){
                if(voucherModel.getMinTransaction() <= reqTransactionDto.getTotal()){
                    if(voucherModel.getAmountType().equals("FIXED")){
                        if(voucherModel.getMaxAmount() <= reqTransactionDto.getTotal()){
                            discount = voucherModel.getMaxAmount();
                        }else{
                            if(voucherModel.getAmount() >= reqTransactionDto.getTotal()){
                                discount = reqTransactionDto.getTotal();
                            }else{
                                discount = voucherModel.getAmount();
                            }
                        }
                        response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, discount, 1);
                    }else{
                        long dis =  reqTransactionDto.getTotal() - (voucherModel.getAmount() / 100 * reqTransactionDto.getTotal());
                        if(voucherModel.getMaxAmount() <= dis){
                            discount = voucherModel.getMaxAmount();
                        }else{
                            if(dis >= reqTransactionDto.getTotal()){
                                discount = reqTransactionDto.getTotal();
                            }else{
                                discount = dis;
                            }
                        }
                        response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_200, Constant.STATUS.SUCCESS, discount, 1);
                    }
                }else{
                    response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_409, Constant.STATUS.ERROR, "Minimal transaction Rp. "+ TrapediaHelper.formatCurrency(voucherModel.getMinTransaction()), 1);
                }
            }else{
                response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_409, Constant.STATUS.ERROR, "Invalid voucher code", 1);
            }

        } catch (Exception e) {
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            response = ResponseGenerator.generate(Constant.HTTPCode.HTTP_500, e.getMessage(), "", "");
        }
        return response;
    }


}
