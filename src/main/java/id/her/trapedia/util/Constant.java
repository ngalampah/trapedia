package id.her.trapedia.util;

import java.util.Arrays;
import java.util.List;

public class Constant {
    public static final String HIBERNATE_XML_PATH = "hibernate.cfg.xml";
    public static final String PROPERTY_MYSQL_DRIVERCLASS = "hibernate.connection.driver_class";
    public static final String DRIVERCLASS_MYSQL = "com.mysql.jdbc.Driver";

    public static final String PROPERTY_NAME_DATABASE_DRIVER = "db.driver";
    public static final String PROPERTY_NAME_DATABASE_PASSWORD = "db.password";
    public static final String PROPERTY_NAME_DATABASE_URL = "db.url";
    public static final String PROPERTY_NAME_DATABASE_USERNAME = "db.username";
    public static final String PROPERTY_NAME_HIBERNATE_DEFAULT_SCHEMA = "hibernate.default.schema";
    public static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";
    public static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    public static final String PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
    public static final String PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";

    public static final String PROPERTY_NAME_HIBERNATE_C3P0_MIN_SIZE = "hibernate.c3p0.min_size";
    public static final String PROPERTY_NAME_HIBERNATE_C3P0_MAX_SIZE = "hibernate.c3p0.max_size";
    public static final String PROPERTY_NAME_HIBERNATE_C3P0_TIMEOUT = "hibernate.c3p0.timeout";
    public static final String PROPERTY_NAME_HIBERNATE_C3P0_MAX_STATEMENTS = "hibernate.c3p0.max_statements";
    public static final String PROPERTY_NAME_HIBERNATE_C3P0_IDLE_TEST_PERIOD = "hibernate.c3p0.idle_test_period";
    public static final String PROPERTY_NAME_HIBERNATE_C3P0_ACQUIRE_INCREMENT = "hibernate.c3p0.acquire_increment";
    public static final String PROPERTY_NAME_HIBERNATE_C3P0_MAX_IDLE_TIME = "hibernate.c3p0.maxIdleTime";
    public static final String PROPERTY_NAME_HIBERNATE_C3P0_MAX_IDLE_TIME_EXCESS_CONNECTION = "hibernate.c3p0.maxIdleTimeExcessConnections";

    public static final List<String> IMG_MIME_TYPE = Arrays.asList(new String[] {"image/png", "image/jpg", "image/jpeg"});
    // set max upload size per file is 5 Mb
    public static final long MAX_UPLOAD_SIZE = 15*1024*1024;

    public static final String SQL_DATE_FORMAT = "yyyy-MM-dd";
    public static final String SQL_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String DEFAULT_EMAIL = "noreply@trapedia.com";

    public static class EMAIL_TEMPLATE {
        public static final String INVOICE = "invoice.ftl";
        public static final String RECEIPT = "receipt.ftl";
        public static final String USER_ACTIVATION = "user-activation.ftl";
        public static final String REQ_RESET_PASSWORD = "request-reset-password.ftl";
        public static final String RESET_PASSWORD = "reset-password.ftl";
    }

    public static class TRAPEDIA_LOGO {
        public static final String DEFAULT = "trapedia-default.png";
        public static final String WHITE = "trapedia-logo.png";
    }

    public static class APIResponseCode {
        public static final String STATUS = "status";
        public static final String MESSAGE = "message";
        public static final String DATA = "data";
        public static final String LENGTH = "length";
    }

    public static class HTTPCode {
        public static final String HTTP_200 = "200";
        public static final String HTTP_401 = "401";
        public static final String HTTP_404 = "404";
        public static final String HTTP_409 = "409";
        public static final String HTTP_500 = "500";
    }

    public static class BooleanStatus {
        public static final String TRUE = "1";
        public static final String FALSE = "0";
    }

    public static class STATUS {
        public static final String SUCCESS = "Success";
        public static final String ERROR = "Error";
        public static final String USER_NOT_FOUND = "User data not found";
        public static final String USER_ALREADY_EXIST = "User already exist";
        public static final String SERVICE_NOT_FOUND = "Service data not found";
        public static final String SERVICE_DETAIL_NOT_FOUND = "Service data not found";
        public static final String CATEGORY_NOT_FOUND = "Category data not found";
        public static final String PROVINCE_NOT_FOUND = "Province data not found";
        public static final String REGENCIES_NOT_FOUND = "Regencies data not found";
        public static final String PAGES_DATA_NOT_FOUND = "Pages data not found";
        public static final String TOKEN_EXPIRED = "Token has expired";
        public static final String TOKEN_DATA_NOT_FOUND = "Token data not found";
    }
}
