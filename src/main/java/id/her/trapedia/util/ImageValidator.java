package id.her.trapedia.util;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ImageValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return MultipartFile.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        MultipartFile file = (MultipartFile) o;
        if (file.isEmpty()) {
            errors.rejectValue("file", "upload.file.required");
        } else if (!Constant.IMG_MIME_TYPE.contains(file.getContentType().toLowerCase())) {
            errors.rejectValue("file", "upload.invalid.file.type");
        } else if (file.getSize() > Constant.MAX_UPLOAD_SIZE) {
            errors.rejectValue("file", "upload.exceeded.file.size");
        }
    }
}
