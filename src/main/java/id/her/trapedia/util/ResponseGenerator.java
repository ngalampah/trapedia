package id.her.trapedia.util;

import java.util.HashMap;
import java.util.Map;

public class ResponseGenerator {
    public static Map<String, Object> generate(Object status, Object message, Object data, Object length) {
        Map<String, Object> response = new HashMap<>();
        response.put(Constant.APIResponseCode.STATUS, status);
        response.put(Constant.APIResponseCode.MESSAGE, message);
        response.put(Constant.APIResponseCode.DATA, data);
        response.put(Constant.APIResponseCode.LENGTH, length);
        return response;
    }
}
