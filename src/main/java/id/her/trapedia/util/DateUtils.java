package id.her.trapedia.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class DateUtils {

    private static final Log log = LogFactory.getLog(DateUtils.class);

    public static final String FORMAT_DATE = "dd/MM/yyyy";
    public static final String FORMAT_DATE_COMMON= "yyyy-MM-dd";
    public static final String FORMAT_MONTH = "MMMM/yyyy";
    public static final String FORMAT_TIME = "HH:mm";

    public static final String FORMAT_DEFAULT_DB = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String FORMAT_DEFAULT = "yyyy-MM-dd HH:mm:ss";

    public static Date now() {
        return Calendar.getInstance().getTime();
    }

    public static int month(Date date) {
        return toCalendar(date).get(Calendar.MONTH) + 1;
    }

    public static int year(Date date) {
        return toCalendar(date).get(Calendar.YEAR);
    }

    public static int date(Date d) {
        return toCalendar(d).get(Calendar.DATE);
    }

    public static Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static String formatDateToStr(Date date, String dateFormat) {
        String formattedDate = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(
                    dateFormat != null ? dateFormat : FORMAT_DATE);
            formattedDate = formatter.format(date);
        } catch (Exception e) {
            log.error("Fail to format date.", e);
        }
        return formattedDate;
    }

    public static String formatDateToStr(Date date, String dateFormat, String timeZOne) {
        String formattedDate = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(
                    dateFormat != null ? dateFormat : FORMAT_DATE);
            formatter.setTimeZone(TimeZone.getTimeZone(timeZOne));
            formattedDate = formatter.format(date);
        } catch (Exception e) {
            log.error("Fail to format date.", e);
        }
        return formattedDate;
    }

    public static Date convertStringToDate(String dtStr) {
        if(StringUtils.isNotEmpty(dtStr)){
            return null;
        }

        Date date = null;
        SimpleDateFormat df = new SimpleDateFormat(FORMAT_DATE);
        log.debug("Converting '" + dtStr + "' to date with format '" + FORMAT_DATE + "'");
        try {
            date = df.parse(dtStr);
        } catch (ParseException pe) {
            log.error("Unable to conver a string to date" , pe);
        }
        return date;
    }

    public static Date convertStringToDate(String dtStr, String dateFormat) {
        if(StringUtils.isEmpty(dtStr)){
            log.info("masuk sini karena kosong");
            return null;
        }

        Date date = null;
        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        log.info("Converting '" + dtStr + "' to date with format '" + dateFormat + "'");

        try {
            date = df.parse(dtStr);
        } catch (ParseException pe) {
            log.info("Unable to convert a string to date", pe);
        }
        log.info(" return value" +date.toString());
        return date;
    }

    private static HashMap<String, String> getMonths() {
        HashMap<String, String> monthHashMap = new HashMap<String, String>();
        monthHashMap.put("01", "Januari");
        monthHashMap.put("02", "Februari");
        monthHashMap.put("03", "Maret");
        monthHashMap.put("04", "April");
        monthHashMap.put("05", "Mei");
        monthHashMap.put("06", "Juni");
        monthHashMap.put("07", "July");
        monthHashMap.put("08", "Agustus");
        monthHashMap.put("09", "September");
        monthHashMap.put("10", "October");
        monthHashMap.put("11", "November");
        monthHashMap.put("12", "Desember");
        return monthHashMap;
    }

    public static Date toDate() {
        DateTime dt = new DateTime();
        return dt.toDate();
    }

    public static String toDate(String format) {
        DateTime dt = new DateTime();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt.toDate());
    }

    public static String toDate(DateTime dt, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt.toDate());
    }

    public static String StringDateToDateTimeFormat(String strDate, String format1, String format2) {
        String dateString = "";
        try {
            Date date = null;
            SimpleDateFormat sdf1 = new SimpleDateFormat(format1);
            date = sdf1.parse(strDate);
            SimpleDateFormat sdf2 = new SimpleDateFormat(format2);
            dateString = sdf2.format(date);
        } catch (ParseException e) {
            // To change body of catch statement use File | Settings | File Templates.
            log.error("[ERROR DETAILS]  " + e.getMessage());
            return null;
        }
        return dateString;
    }

    public static int daysCounter (Date startDate, Date endDate) {
        DateTime dt1 = new DateTime(startDate);
        DateTime dt2 = new DateTime(endDate);
        int days = Days.daysBetween(dt1, dt2).getDays();
        return days;
    }
}
