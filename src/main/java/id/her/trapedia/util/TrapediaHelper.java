package id.her.trapedia.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

public class TrapediaHelper {
    private static final Logger logger = LogManager.getLogger(TrapediaHelper.class);

    public static Long uniqueCodeTrx(Long total) {
        Long uniqueCode = total;
        if (total > 999) {
            uniqueCode = total % 999;
        }
        return uniqueCode;
    }

    public static String setNumberInvoice(Long n) {
        String number = "";
        if (n < 10) {
            number = "000" + String.valueOf(n);
        } else if (n < 100) {
            number = "00" + String.valueOf(n);
        } else if (n < 1000) {
            number = "0" + String.valueOf(n);
        } else {
            number = String.valueOf(n);
        }
        String date = DateUtils.formatDateToStr(new Date(), "yyyyMMdd");
        String inv = "TRA/" + date + "/" + number;
        return inv;
    }

    public static String formatCurrency(Long currency) {
        return new java.text.DecimalFormat("#,##0").format(Double.valueOf(currency));
    }

    public static String paymentStatus(String code) {
        String status = "";
        if (code.equalsIgnoreCase("0")) {
            status = "Waiting";
        } else if (code.equalsIgnoreCase("1")) {
            status = "Success";
        } else {
            status = "Failed";
        }
        return status;
    }

    public static String dueDate(Date date) {
        Date dt = date;
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        dt = c.getTime();
        return DateUtils.formatDateToStr(dt, "dd MMM yyyy HH:mm:ss");
    }

    /*** GENERATE RANDOM PASSWORD ***/
    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";
    private static final String OTHER_CHAR = "!@#$%&*()_+-=[]?";

    private static final String PASSWORD_ALLOW_BASE = CHAR_LOWER + CHAR_UPPER + NUMBER + OTHER_CHAR;
    // optional, make it more random
    private static final String PASSWORD_ALLOW_BASE_SHUFFLE = shuffleString(PASSWORD_ALLOW_BASE);
    private static final String PASSWORD_ALLOW = PASSWORD_ALLOW_BASE_SHUFFLE;

    private static SecureRandom random = new SecureRandom();

    public static String generateRandomPassword(int length) {
        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {

            int rndCharAt = random.nextInt(PASSWORD_ALLOW.length());
            char rndChar = PASSWORD_ALLOW.charAt(rndCharAt);

            sb.append(rndChar);
        }

        return sb.toString();
    }

    // shuffle
    public static String shuffleString(String string) {
        List<String> letters = Arrays.asList(string.split(""));
        Collections.shuffle(letters);
        return letters.stream().collect(Collectors.joining());
    }

}
