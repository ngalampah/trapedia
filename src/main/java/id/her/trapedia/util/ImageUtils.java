package id.her.trapedia.util;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;

@Service
public class ImageUtils {
    Logger logger = LogManager.getLogger(ImageUtils.class);

    @Value("${trapedia.path.upload}")
    private String path;

    public String uploadImage(MultipartFile multipartFile) {
        try {
            String filename = FilenameUtils.getBaseName(multipartFile.getOriginalFilename()).replace(" ", "_") +
                    DateUtils.formatDateToStr(new Date(), "yyyyMMddHHmmss.")
                    + FilenameUtils.getExtension(multipartFile.getOriginalFilename());
            File file = new File(path + filename);
            multipartFile.transferTo(file);
            logger.info("File " + filename + " was uploaded to " + file.getAbsolutePath());
            return filename;
        } catch (IOException e) {
            logger.error("Error while uploading file!");
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            return null;
        }
    }

    public Boolean deleteImage(String filename) {
        try {
            File file = new File(path + filename);
            return file.delete();
        } catch (Exception e) {
            logger.error("Error while deleting file!");
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            return false;
        }
    }
}
