/**
 *
 */
package id.her.trapedia.util;

import net.sf.json.*;
import net.sf.json.processors.JsonValueProcessor;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;

import java.util.*;

public class JSONProcessor {

    private JSONProcessor() {
        super();
    }

    /*
     * This function will get JSON of the Object that pass in parameter
     */
    public static JSON convertToJSON(Object object) {
        JsonConfig jsonConfig = getJsonConfig();
        JSON json = JSONSerializer.toJSON(object, jsonConfig);
        return json;
    }

    public static JSONArray convertToJSONArray(Object object) {
        JsonConfig jsonConfig = getJsonConfig();
        JSONArray json = (JSONArray) JSONSerializer.toJSON(object, jsonConfig);
        return json;
    }

    public static <T> List<T> getObjectFromJSONArray(JSONArray json, Class<T> clazz) {

        Date defaultDate = null;

        JsonConfig jsonConfig = getJsonConfig();
        jsonConfig.setRootClass(clazz);
        Map<String, Class<?>> map = getMapJsonConfig();

        DateConverter converter = new DateConverter(defaultDate);
        ConvertUtils.register(converter, Date.class);


        List<T> list = new ArrayList<T>();
        for (Object o : json) {
            JSONObject jsonObject = JSONObject.fromObject(o, jsonConfig);
            list.add((T) JSONObject.toBean(jsonObject, clazz, map));
        }

        return (List<T>) list;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getObjectFromJSON(JSON json, Class<T> clazz) {
        Date defaultDate = null;

        JsonConfig jsonConfig = getJsonConfig();
        jsonConfig.setRootClass(clazz);
        Map<String, Class<?>> map = getMapJsonConfig();

        DateConverter converter = new DateConverter(defaultDate);
        ConvertUtils.register(converter, Date.class);

        JSONObject jsonObject = JSONObject.fromObject(json, jsonConfig);

        return (T) JSONObject.toBean(jsonObject, clazz, map);
    }

    private static JsonConfig getJsonConfig() {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class,
                jsonDateValueProcessor);
        return jsonConfig;
    }

    private static Map<String, Class<?>> getMapJsonConfig() {
        Map<String, Class<?>> map = new HashMap<String, Class<?>>();
        return map;
    }

    public static Map<String, Object> jsonToMap(String t) throws JSONException {
        JSONObject jsonObject = JSONObject.fromObject(t);
        Map<String, Object> map = jsonObject;
        return map;
    }

    private static JsonValueProcessor jsonDateValueProcessor = new JsonValueProcessor() {

        public Object processArrayValue(Object value, JsonConfig jsonConfig) {
            return process(value, jsonConfig);
        }

        public Object processObjectValue(String key, Object value,
                                         JsonConfig jsonConfig) {
            return process(value, jsonConfig);
        }

        private Object process(Object bean, JsonConfig jsonConfig) {
            JSONObject jsonObject = null;
            if (bean != null) {
                if (bean instanceof java.sql.Date) {
                    bean = new Date(((java.sql.Date) bean).getTime());
                }
                if (bean instanceof Date) {
                    Calendar c = Calendar.getInstance();
                    c.setTime((Date) bean);
                    jsonObject = JSONObject.fromObject(bean);
                } else {
                    jsonObject = new JSONObject(true);
                }
                return jsonObject;
            }
            return JSONObject.fromObject(bean, jsonConfig);
        }
    };
}
