package id.her.trapedia.util;

import id.her.trapedia.dto.ReqTransactionDto;
import id.her.trapedia.service.TransactionService;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Service
public class InvoiceGenerator {
    Logger logger = LogManager.getLogger(InvoiceGenerator.class);

    @Autowired
    TransactionService transactionService;

    @Value("${trapedia.fee.admin}")
    private Long adminFee;

    private static final String logo = "/static/img/trapedia-logo.png";
    private final String invoice_template_path = "/jasper/invoice.jrxml";

    public String generateInvoiceFor(Long idTrx) throws IOException {
        try {
            Map<String, Object> mapTransaction = transactionService.getServiceById(idTrx);
            ReqTransactionDto transaction = null;
            if (mapTransaction != null) {
                transaction = (ReqTransactionDto) mapTransaction.get("data");
            }

            // Compile the Jasper report from .jrxml to .japser
            final InputStream reportInputStream = getClass().getResourceAsStream(invoice_template_path);
            final JasperDesign jasperDesign = JRXmlLoader.load(reportInputStream);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

            // Get your data source
            JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(transaction.getFormDetailCart());

            // Add parameters
            Map<String, Object> parameters = new HashMap<>();

            // Title
            parameters.put("numberTransaction", transaction.getNumberTransaction());
            parameters.put("dateTransaction", transaction.getDate());

            // Header
            parameters.put("travelingName", transaction.getTravelingName());
            parameters.put("travelingEmail", transaction.getTravelingEmail());
            parameters.put("travelingPhone", transaction.getTravelingPhone());
            parameters.put("paymentMethode", transaction.getPaymentMethode());
            parameters.put("paymentAmount", transaction.getPaymentAmount());
            parameters.put("paymentStatus", paymentStatus(transaction.getStatusPayment()));

            // Summarize
            parameters.put("total", transaction.getSummaryService());
            parameters.put("adminFee", adminFee);
            parameters.put("discount", transaction.getDiscount());
            parameters.put("uniqueCode", transaction.getPaymentAmount() - transaction.getTotal());

            // Logo
            parameters.put("logo", getClass().getResourceAsStream(logo));

            // Fill the report
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
                    jrBeanCollectionDataSource);

            File temp = File.createTempFile(transaction.getNumberTransaction().replace("/", "-"), ".pdf");

            // Export the report to a PDF file
            JasperExportManager.exportReportToPdfFile(jasperPrint, temp.getAbsolutePath());

            logger.info("Invoice report " + temp.getAbsolutePath() + " is generated");

            return temp.getAbsolutePath();

        } catch (Exception e) {
            logger.error("Error while generating report!");
            logger.error("[ERROR DETAILS]  " + e.getMessage());
            return e.getMessage();
        }
    }

    private String paymentStatus(String code) {
        String status = "";
        if (code.equalsIgnoreCase("0")) {
            status = "Waiting";
        } else if (code.equalsIgnoreCase("1")) {
            status = "Success";
        } else {
            status = "Failed";
        }
        return status;
    }
}
