package id.her.trapedia.config;

import id.her.trapedia.util.Constant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "id.her.trapedia")
public class HibernateConfig {

    @Resource
    private Environment env;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(env.getRequiredProperty(Constant.PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));
        Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.default.schema", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_DEFAULT_SCHEMA));
        hibernateProperties.put("hibernate.dialect", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_DIALECT));
        hibernateProperties.put("hibernate.show_sql", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_SHOW_SQL));
        hibernateProperties.put("hibernate.hbm2ddl.auto", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO));
        hibernateProperties.put("hibernate.connection.release_mode", "after_transaction");

        hibernateProperties.put("hibernate.c3p0.min_size", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_C3P0_MIN_SIZE));
        hibernateProperties.put("hibernate.c3p0.max_size", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_C3P0_MAX_SIZE));
        hibernateProperties.put("hibernate.c3p0.timeout", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_C3P0_TIMEOUT));
        hibernateProperties.put("hibernate.c3p0.max_statements", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_C3P0_MAX_STATEMENTS));
        hibernateProperties.put("hibernate.c3p0.idle_test_period", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_C3P0_IDLE_TEST_PERIOD));
        hibernateProperties.put("hibernate.c3p0.acquire_increment", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_C3P0_ACQUIRE_INCREMENT));
        hibernateProperties.put("hibernate.c3p0.maxIdleTime", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_C3P0_MAX_IDLE_TIME));
        hibernateProperties.put("hibernate.c3p0.maxIdleTimeExcessConnections", env.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_C3P0_MAX_IDLE_TIME_EXCESS_CONNECTION));

        sessionFactory.setHibernateProperties(hibernateProperties);
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getRequiredProperty(Constant.PROPERTY_NAME_DATABASE_DRIVER));
        dataSource.setUrl(env.getRequiredProperty(Constant.PROPERTY_NAME_DATABASE_URL));
        dataSource.setUsername(env.getRequiredProperty(Constant.PROPERTY_NAME_DATABASE_USERNAME));
        dataSource.setPassword(env.getRequiredProperty(Constant.PROPERTY_NAME_DATABASE_PASSWORD));
        return dataSource;
    }

    @Bean
    public HibernateTransactionManager transactionManager() {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory().getObject());
        return txManager;
    }

}
