package id.her.trapedia;

import id.her.trapedia.service.JwtUserDetailsService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class UtilTest {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    public void EncryptPassword() {
        System.out.println("=====> " + passwordEncoder.encode("password"));
    }
}
