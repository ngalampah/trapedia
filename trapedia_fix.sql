-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping structure for table trapedia_fix.hibernate_sequence
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.hibernate_sequence: ~0 rows (approximately)
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` (`next_val`) VALUES
	(2);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_admin
CREATE TABLE IF NOT EXISTS `trp_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enabled` varchar(1) DEFAULT '1',
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `publish` varchar(1) DEFAULT '1',
  `username` varchar(50) NOT NULL,
  `deleted` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qrt73sgdn637ehxcnl3wx7b0e` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_admin: ~4 rows (approximately)
/*!40000 ALTER TABLE `trp_admin` DISABLE KEYS */;
INSERT INTO `trp_admin` (`id`, `enabled`, `name`, `password`, `publish`, `username`, `deleted`) VALUES
	(1, '1', 'Nuryanto', '$2a$10$16yMtsQHaWgJRI2wRpiQmewdJ7ggidO8KLTtyLqwIDi/0AHhCO6Ji', '1', 'admin', '0'),
	(2, '1', 'Nuryanto', '$2a$10$16yMtsQHaWgJRI2wRpiQmewdJ7ggidO8KLTtyLqwIDi/0AHhCO6Ji', '1', 'alex', '0'),
	(3, '1', 'Nuryanto', '$2a$10$16yMtsQHaWgJRI2wRpiQmewdJ7ggidO8KLTtyLqwIDi/0AHhCO6Ji', '1', 'nuryanto.aza@gmail.com', '0'),
	(4, '1', 'trapedia', '$2a$10$1CnngOepeztzm1xGVlnK/OG88dPtInVVjDt/qnxqK5uypQAQrT7Fu', '1', 'trapedia', '0');
/*!40000 ALTER TABLE `trp_admin` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_admin_roles
CREATE TABLE IF NOT EXISTS `trp_admin_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  `admin_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrmkgb2pbtlxs8ajk8gi9u94xl` (`admin_id`),
  CONSTRAINT `FKrmkgb2pbtlxs8ajk8gi9u94xl` FOREIGN KEY (`admin_id`) REFERENCES `trp_admin` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_admin_roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `trp_admin_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `trp_admin_roles` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_blog
CREATE TABLE IF NOT EXISTS `trp_blog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `date` datetime DEFAULT NULL,
  `deleted` varchar(1) DEFAULT '0',
  `images` varchar(255) DEFAULT NULL,
  `publish` varchar(1) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `count_view` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_blog: ~17 rows (approximately)
/*!40000 ALTER TABLE `trp_blog` DISABLE KEYS */;
INSERT INTO `trp_blog` (`id`, `content`, `date`, `deleted`, `images`, `publish`, `title`, `type`, `count_view`) VALUES
	(3, 'Lorem Ipsum', '2019-10-13 12:59:03', '0', '429940547_2121420191009031014.jpg', '1', 'Jasa Pembuatan Web Wordpress', NULL, 0),
	(4, 'Test #1', '2019-10-13 06:39:10', '0', '429940547_2121420191009031014.jpg', '1', 'xcc', NULL, 0),
	(5, 'Test #2', '2019-10-13 13:44:30', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(6, 'Test #3', '2019-10-13 13:44:35', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(7, 'Test #4', '2019-10-13 13:49:54', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(8, 'Test #5', '2019-10-13 13:49:58', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(9, 'Test #6', '2019-10-13 13:50:06', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(10, 'Test #7', '2019-10-13 13:50:08', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(11, 'Test #8', '2019-10-13 13:50:09', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(12, 'Test #9', '2019-10-13 13:50:10', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(13, 'Test #10', '2019-10-13 13:50:10', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(14, 'Test #11', '2019-10-13 13:50:12', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(15, 'Test #12', '2019-10-13 13:50:13', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(16, 'Test #13', '2019-10-13 13:50:13', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(17, 'Test #14', '2019-10-13 13:50:14', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(18, 'Test #15', '2019-10-13 13:50:14', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0),
	(19, 'Test #16', '2019-10-13 13:50:16', '0', '429940547_2121420191009031014.jpg', '0', 'xcc', NULL, 0);
/*!40000 ALTER TABLE `trp_blog` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_config
CREATE TABLE IF NOT EXISTS `trp_config` (
  `config_name` varchar(50) NOT NULL,
  `config_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`config_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_config: ~7 rows (approximately)
/*!40000 ALTER TABLE `trp_config` DISABLE KEYS */;
INSERT INTO `trp_config` (`config_name`, `config_value`) VALUES
	('company_address', 'Komp. Manglayangsari Blok A9 No 14 Bandung 40615 - Indonesia'),
	('company_email', 'indimartcom@gmail.com'),
	('company_name', 'PT. Galaksi Trapedia Muda'),
	('company_phone', '022-87827074'),
	('facebook', 'https://www.facebook.com/trapedia/'),
	('instagram', 'https://www.instagram.com/trapedia/'),
	('twitter', 'https://twitter.com/trapediaID'),
	('youtube', '#');
/*!40000 ALTER TABLE `trp_config` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_messages
CREATE TABLE IF NOT EXISTS `trp_messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `message` text NOT NULL,
  `replay_id` bigint(20) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `to_user_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `is_read_to_user_id` varchar(1) DEFAULT '0',
  `is_read_user_id` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK88l58p1loyjaewbu4x2y5skeu` (`to_user_id`),
  KEY `FKfks5jl55sac1hrttb9e478957` (`user_id`),
  CONSTRAINT `FK88l58p1loyjaewbu4x2y5skeu` FOREIGN KEY (`to_user_id`) REFERENCES `trp_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKfks5jl55sac1hrttb9e478957` FOREIGN KEY (`user_id`) REFERENCES `trp_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_messages: ~8 rows (approximately)
/*!40000 ALTER TABLE `trp_messages` DISABLE KEYS */;
INSERT INTO `trp_messages` (`id`, `date`, `message`, `replay_id`, `subject`, `to_user_id`, `user_id`, `is_read_to_user_id`, `is_read_user_id`) VALUES
	(6, '2019-10-14 18:57:26', 'Hello WOrld', 0, 'Message from : Waterbom Bali Tickets', 5, 8, '0', '0'),
	(7, '2019-10-14 19:17:38', 'Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search fo', 0, 'Message from : Trip to Nusa Penida at Bali 1 Day Tour', 5, 8, '0', '0'),
	(8, '2019-10-14 19:27:37', 'Ini saya sendiri lgo yang comment', 0, 'Message from : Waterbom Bali Tickets', 8, 5, '0', '0'),
	(9, '2019-10-14 20:34:59', 'xxxx', 8, '', 5, 8, '0', '0'),
	(10, '2019-10-14 20:35:07', 'asasas', 8, '', 5, 8, '0', '0'),
	(11, '2019-10-14 20:54:38', 'wkkwkwkwkkwkw', 8, '', 5, 5, '0', '0'),
	(12, '2019-10-14 20:55:54', '111111', 8, '', 5, 8, '0', '0'),
	(13, '2019-10-14 20:56:27', 'aaaaaaaaaaaaaaaaaaa', 8, '', 5, 5, '0', '0'),
	(14, '2019-10-14 20:59:30', 'xxxxxx', 8, '', 5, 5, '0', '0');
/*!40000 ALTER TABLE `trp_messages` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_m_category
CREATE TABLE IF NOT EXISTS `trp_m_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` varchar(1) DEFAULT '0',
  `description` varchar(255) NOT NULL,
  `images` varchar(255) DEFAULT NULL,
  `publish` varchar(1) DEFAULT '1',
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8f53s0jj7gxpthotl2bibjxto` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_m_category: ~11 rows (approximately)
/*!40000 ALTER TABLE `trp_m_category` DISABLE KEYS */;
INSERT INTO `trp_m_category` (`id`, `deleted`, `description`, `images`, `publish`, `title`) VALUES
	(1, '0', 'Menyediakan berbagai hotel', '/img/icon/03_hotel_64.png', '1', 'Hotels'),
	(2, '0', 'Penginapan Vila', '/img/icon/06_shop_64.png', '1', 'Vila'),
	(3, '0', 'Penginapan Apartement', '/img/icon/06_shop_64.png', '1', 'Apartement'),
	(4, '0', '', '/img/icon/28_calendar_64.png', '1', 'Event'),
	(5, '0', '', '/img/icon/19_suitcase_64.png', '1', 'Paket Wisata'),
	(6, '0', '', '/img/icon/05_sight_64.png', '1', 'Homestay'),
	(7, '0', '', '/img/icon/35_coconut_64.png', '1', 'Tempat Kuliner'),
	(8, '0', '', '/img/icon/39_fruit_64.png', '1', 'Oleh-oleh'),
	(9, '0', '', '/img/icon/31_map_64.png', '1', 'Wisata'),
	(10, '0', '', '/img/icon/41_airplane_64.png', '1', 'Travel'),
	(14, '0', '...', '....', '1', 'Test 23');
/*!40000 ALTER TABLE `trp_m_category` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_m_provinces
CREATE TABLE IF NOT EXISTS `trp_m_provinces` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` varchar(1) DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `publish` varchar(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_m_provinces: ~34 rows (approximately)
/*!40000 ALTER TABLE `trp_m_provinces` DISABLE KEYS */;
INSERT INTO `trp_m_provinces` (`id`, `deleted`, `name`, `publish`) VALUES
	(11, '0', 'ACEH', '0'),
	(12, '0', 'SUMATERA UTARA', '1'),
	(13, '0', 'SUMATERA BARAT', '1'),
	(14, '0', 'RIAU', '1'),
	(15, '0', 'JAMBI', '1'),
	(16, '0', 'SUMATERA SELATAN', '1'),
	(17, '0', 'BENGKULU', '1'),
	(18, '0', 'LAMPUNG', '1'),
	(19, '0', 'KEPULAUAN BANGKA BELITUNG', '1'),
	(21, '0', 'KEPULAUAN RIAU', '1'),
	(31, '0', 'DKI JAKARTA', '1'),
	(32, '0', 'JAWA BARAT', '1'),
	(33, '0', 'JAWA TENGAH', '1'),
	(34, '0', 'DI YOGYAKARTA', '1'),
	(35, '0', 'JAWA TIMUR', '1'),
	(36, '0', 'BANTEN', '1'),
	(51, '0', 'BALI', '1'),
	(52, '0', 'NUSA TENGGARA BARAT', '1'),
	(53, '0', 'NUSA TENGGARA TIMUR', '1'),
	(61, '0', 'KALIMANTAN BARAT', '1'),
	(62, '0', 'KALIMANTAN TENGAH', '1'),
	(63, '0', 'KALIMANTAN SELATAN', '1'),
	(64, '0', 'KALIMANTAN TIMUR', '1'),
	(65, '0', 'KALIMANTAN UTARA', '1'),
	(71, '0', 'SULAWESI UTARA', '1'),
	(72, '0', 'SULAWESI TENGAH', '1'),
	(73, '0', 'SULAWESI SELATAN', '1'),
	(74, '0', 'SULAWESI TENGGARA', '1'),
	(75, '0', 'GORONTALO', '1'),
	(76, '0', 'SULAWESI BARAT', '1'),
	(81, '0', 'MALUKU', '1'),
	(82, '0', 'MALUKU UTARA', '1'),
	(91, '0', 'PAPUA BARAT', '1'),
	(94, '0', 'PAPUA', '1');
/*!40000 ALTER TABLE `trp_m_provinces` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_m_regencies
CREATE TABLE IF NOT EXISTS `trp_m_regencies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` varchar(1) DEFAULT '0',
  `images` varchar(255) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `popular` varchar(1) DEFAULT '0',
  `publish` varchar(1) DEFAULT '1',
  `province_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkkxrcjjgivirhf6es6d7abwjk` (`province_id`),
  CONSTRAINT `FKkkxrcjjgivirhf6es6d7abwjk` FOREIGN KEY (`province_id`) REFERENCES `trp_m_provinces` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9472 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_m_regencies: ~97 rows (approximately)
/*!40000 ALTER TABLE `trp_m_regencies` DISABLE KEYS */;
INSERT INTO `trp_m_regencies` (`id`, `deleted`, `images`, `name`, `popular`, `publish`, `province_id`) VALUES
	(1171, '0', '/img/sea_tour_img_3.jpg', 'BANDA ACEH', '1', '1', 11),
	(1172, '0', NULL, 'SABANG', NULL, '1', 11),
	(1173, '0', NULL, 'LANGSA', NULL, '1', 11),
	(1174, '0', NULL, 'LHOKSEUMAWE', NULL, '1', 11),
	(1175, '0', NULL, 'SUBULUSSALAM', NULL, '1', 11),
	(1271, '0', NULL, 'SIBOLGA', NULL, '1', 12),
	(1272, '0', NULL, 'TANJUNGBALAI', NULL, '1', 12),
	(1273, '0', NULL, 'PEMATANGSIANTAR', NULL, '1', 12),
	(1274, '0', NULL, 'TEBINGTINGGI', NULL, '1', 12),
	(1275, '0', NULL, 'MEDAN', NULL, '1', 12),
	(1276, '0', NULL, 'BINJAI', NULL, '1', 12),
	(1277, '0', NULL, 'PADANGSIDIMPUAN', NULL, '1', 12),
	(1278, '0', NULL, 'GUNUNGSITOLI', NULL, '1', 12),
	(1371, '0', NULL, 'PADANG', NULL, '1', 13),
	(1372, '0', NULL, 'SOLOK', NULL, '1', 13),
	(1373, '0', NULL, 'SAWAHLUNTO', NULL, '1', 13),
	(1374, '0', NULL, 'PADANGPANJANG', NULL, '1', 13),
	(1375, '0', NULL, 'BUKITTINGGI', NULL, '1', 13),
	(1376, '0', NULL, 'PAYAKUMBUH', NULL, '1', 13),
	(1377, '0', NULL, 'PARIAMAN', NULL, '1', 13),
	(1471, '0', NULL, 'PEKANBARU', NULL, '1', 14),
	(1473, '0', NULL, 'DUMAI', NULL, '1', 14),
	(1571, '0', NULL, 'JAMBI', NULL, '1', 15),
	(1572, '0', NULL, 'SUNGAIPENUH', NULL, '1', 15),
	(1671, '0', NULL, 'PALEMBANG', NULL, '1', 16),
	(1672, '0', NULL, 'PRABUMULIH', NULL, '1', 16),
	(1673, '0', NULL, 'PAGARALAM', NULL, '1', 16),
	(1674, '0', NULL, 'LUBUKLINGGAU', NULL, '1', 16),
	(1771, '0', NULL, 'BENGKULU', NULL, '1', 17),
	(1871, '0', NULL, 'BANDARLAMPUNG', NULL, '1', 18),
	(1872, '0', NULL, 'METRO', NULL, '1', 18),
	(1971, '0', NULL, 'PANGKALPINANG', NULL, '1', 19),
	(2171, '0', '/img/sea_tour_img_5.jpg', 'BATAM', '1', '1', 21),
	(2172, '0', NULL, 'TANJUNGPINANG', NULL, '1', 21),
	(3171, '0', NULL, 'JAKARTA SELATAN', NULL, '1', 31),
	(3172, '0', NULL, 'JAKARTA TIMUR', NULL, '1', 31),
	(3173, '0', NULL, 'JAKARTA PUSAT', NULL, '1', 31),
	(3174, '0', NULL, 'JAKARTA BARAT', NULL, '1', 31),
	(3175, '0', NULL, 'JAKARTA UTARA', NULL, '1', 31),
	(3271, '0', NULL, 'BOGOR', NULL, '1', 32),
	(3273, '0', '/img/sea_tour_img_1.jpg', 'BANDUNG', '1', '1', 32),
	(3274, '0', NULL, 'CIREBON', NULL, '1', 32),
	(3275, '0', '/img/sea_tour_img_1.jpg', 'BEKASI', '1', '1', 32),
	(3276, '0', NULL, 'DEPOK', NULL, '1', 32),
	(3277, '0', '/img/sea_tour_img_2.jpg', 'CIMAHI', '1', '1', 32),
	(3278, '0', NULL, 'TASIKMALAYA', NULL, '1', 32),
	(3279, '0', NULL, 'BANJAR', NULL, '1', 32),
	(3371, '0', NULL, 'MAGELANG', NULL, '1', 33),
	(3372, '0', NULL, 'SURAKARTA', NULL, '1', 33),
	(3373, '0', NULL, 'SALATIGA', NULL, '1', 33),
	(3374, '0', NULL, 'SEMARANG', NULL, '1', 33),
	(3375, '0', NULL, 'PEKALONGAN', NULL, '1', 33),
	(3376, '0', NULL, 'TEGAL', NULL, '1', 33),
	(3471, '0', '/img/sea_tour_img_4.jpg', 'YOGYAKARTA', '1', '1', 34),
	(3571, '0', NULL, 'KEDIRI', NULL, '1', 35),
	(3572, '0', NULL, 'BLITAR', NULL, '1', 35),
	(3573, '0', '/img/sea_tour_img_4.jpg', 'MALANG', '1', '1', 35),
	(3574, '0', NULL, 'PROBOLINGGO', NULL, '1', 35),
	(3575, '0', NULL, 'PASURUAN', NULL, '1', 35),
	(3576, '0', NULL, 'MOJOKERTO', NULL, '1', 35),
	(3577, '0', NULL, 'MADIUN', NULL, '1', 35),
	(3578, '0', NULL, 'SURABAYA', NULL, '1', 35),
	(3579, '0', NULL, 'BATU', NULL, '1', 35),
	(3671, '0', NULL, 'TANGERANG', NULL, '1', 36),
	(3672, '0', NULL, 'CILEGON', NULL, '1', 36),
	(3673, '0', NULL, 'SERANG', NULL, '1', 36),
	(3674, '0', NULL, 'TANGERANGSELATAN', NULL, '1', 36),
	(5171, '0', NULL, 'DENPASAR', NULL, '1', 51),
	(5271, '0', NULL, 'MATARAM', NULL, '1', 52),
	(5272, '0', NULL, 'BIMA', NULL, '1', 52),
	(5371, '0', NULL, 'KUPANG', NULL, '1', 53),
	(6171, '0', 'img/sea_tour_img_6.jpg', 'PONTIANAK', '1', '1', 61),
	(6172, '0', NULL, 'SINGKAWANG', NULL, '1', 61),
	(6271, '0', NULL, 'PALANGKARAYA', NULL, '1', 62),
	(6371, '0', NULL, 'BANJARMASIN', NULL, '1', 63),
	(6372, '0', NULL, 'BANJARBARU', NULL, '1', 63),
	(6471, '0', NULL, 'BALIKPAPAN', NULL, '1', 64),
	(6472, '0', NULL, 'SAMARINDA', NULL, '1', 64),
	(6474, '0', NULL, 'BONTANG', NULL, '1', 64),
	(6571, '0', NULL, 'TARAKAN', NULL, '1', 65),
	(7171, '0', NULL, 'MANADO', NULL, '1', 71),
	(7172, '0', NULL, 'BITUNG', NULL, '1', 71),
	(7173, '0', NULL, 'TOMOHON', NULL, '1', 71),
	(7174, '0', NULL, 'MOBAGU', NULL, '1', 71),
	(7271, '0', NULL, 'PALU', NULL, '1', 72),
	(7371, '0', NULL, 'MAKASSAR', NULL, '1', 73),
	(7372, '0', NULL, 'PAREPARE', NULL, '1', 73),
	(7373, '0', NULL, 'PALOPO', NULL, '1', 73),
	(7471, '0', NULL, 'KENDARI', NULL, '1', 74),
	(7472, '0', NULL, 'BAUBAU', NULL, '1', 74),
	(7571, '0', NULL, 'GORONTALO', NULL, '1', 75),
	(8171, '0', NULL, 'AMBON', NULL, '1', 81),
	(8172, '0', NULL, 'TUAL', NULL, '1', 81),
	(8271, '0', NULL, 'TERNATE', NULL, '1', 82),
	(8272, '0', NULL, 'TIDOREKEPULAUAN', NULL, '1', 82),
	(9171, '0', NULL, 'SORONG', NULL, '1', 91),
	(9471, '0', NULL, 'JAYAPURA', NULL, '1', 94);
/*!40000 ALTER TABLE `trp_m_regencies` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_pages
CREATE TABLE IF NOT EXISTS `trp_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `date` datetime DEFAULT NULL,
  `deleted` varchar(1) DEFAULT '0',
  `images` varchar(255) DEFAULT NULL,
  `publish` varchar(1) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_e4d24rt5h5wmd1smuepq9owrr` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_pages: ~5 rows (approximately)
/*!40000 ALTER TABLE `trp_pages` DISABLE KEYS */;
INSERT INTO `trp_pages` (`id`, `content`, `date`, `deleted`, `images`, `publish`, `title`, `type`) VALUES
	(4, 'Pellentesque ac turpis egestas, varius justo et, condimentum augue. Praesent aliquam, nisl feugiat vehicula condimentum, justo tellus scelerisque metus. Pellentesque ac turpis egestas, varius justo et, condimentum augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br><br>Pellentesque ac turpis egestas, varius justo et, condimentum augue. Praesent aliquam, nisl feugiat vehicula condimentum, justo tellus scelerisque metus. Pellentesque ac turpis egestas, varius justo et, condimentum augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.?<br>', '2019-10-13 12:55:32', '0', NULL, '1', 'Test #1', 'INFORMATION_LINK'),
	(5, 'qwerty', '2019-10-13 12:55:32', '0', NULL, '1', 'Link Sample #2', 'EDU_LINK'),
	(6, '..', '2019-10-13 12:55:32', '0', NULL, '1', 'Link Sample #3', 'EDU_LINK'),
	(7, '123', '2019-10-13 06:24:02', '1', NULL, '1', 'ho', 'STATIC_PAGES'),
	(8, 'x', '2019-10-13 06:30:26', '1', NULL, '1', 'x', 'STATIC_PAGES');
/*!40000 ALTER TABLE `trp_pages` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_payout
CREATE TABLE IF NOT EXISTS `trp_payout` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` bigint(20) NOT NULL,
  `bank_account_name` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_number` varchar(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `status` varchar(1) DEFAULT '0',
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgic3rqah81t271x8oek3sqxtg` (`user_id`),
  CONSTRAINT `FKgic3rqah81t271x8oek3sqxtg` FOREIGN KEY (`user_id`) REFERENCES `trp_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_payout: ~7 rows (approximately)
/*!40000 ALTER TABLE `trp_payout` DISABLE KEYS */;
INSERT INTO `trp_payout` (`id`, `amount`, `bank_account_name`, `bank_name`, `bank_number`, `date`, `status`, `user_id`) VALUES
	(1, 10000, 'Muhamad Nuryanto', 'Bank BRI', '111111', '2019-10-19 18:11:58', '0', 5),
	(2, 12000, 'Muhamad Nuryanto', 'Bank BRI', '111111', '2019-10-19 18:12:16', '1', 5),
	(3, 0, 'Muhamad Nuryanto', 'Bank BRI', '111111', '2019-10-21 12:45:43', '0', 5),
	(4, 0, 'Muhamad Nuryanto', 'Bank BRI', '111111', '2019-10-21 13:18:16', '0', 5),
	(5, 500000, 'Muhamad Nuryanto', 'Bank BRI', '111111', '2019-10-21 13:28:45', '0', 5),
	(6, 500000, 'Muhamad Nuryanto', 'Bank BRI', '111111', '2019-10-21 13:29:43', '0', 5),
	(7, 300000, 'Muhamad Nuryanto', 'Bank BRI', '111111', '2019-10-21 13:30:16', '0', 5);
/*!40000 ALTER TABLE `trp_payout` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_revenues
CREATE TABLE IF NOT EXISTS `trp_revenues` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `charge` bigint(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `revenue` bigint(20) DEFAULT NULL,
  `sale` bigint(20) DEFAULT NULL,
  `transaction_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkm0t4uol3bl6y3qfl4b02hno6` (`transaction_id`),
  KEY `FKrrotcp1sp30g03hfvarbmk9xg` (`user_id`),
  CONSTRAINT `FKkm0t4uol3bl6y3qfl4b02hno6` FOREIGN KEY (`transaction_id`) REFERENCES `trp_transaction` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKrrotcp1sp30g03hfvarbmk9xg` FOREIGN KEY (`user_id`) REFERENCES `trp_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_revenues: ~2 rows (approximately)
/*!40000 ALTER TABLE `trp_revenues` DISABLE KEYS */;
INSERT INTO `trp_revenues` (`id`, `charge`, `date`, `revenue`, `sale`, `transaction_id`, `user_id`) VALUES
	(1, 10, '2019-10-15 17:46:34', 222300, 247000, 1, 5),
	(2, 10, '2019-10-15 18:32:57', 114300, 127000, 3, 5),
	(3, 10, '2019-10-20 20:26:02', 89100, 99000, 28, 5);
/*!40000 ALTER TABLE `trp_revenues` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_review
CREATE TABLE IF NOT EXISTS `trp_review` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rating_review` int(11) NOT NULL,
  `review` varchar(255) DEFAULT NULL,
  `service_detail_id` bigint(20) NOT NULL,
  `transaction_detail_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlgpai5vtulxf0od2usum2goax` (`service_detail_id`),
  KEY `FKq2bxisu6bo67qh5tvqhtp5198` (`transaction_detail_id`),
  KEY `FKemeh26ppr6f9iej1x3vja3yfd` (`user_id`),
  CONSTRAINT `FKemeh26ppr6f9iej1x3vja3yfd` FOREIGN KEY (`user_id`) REFERENCES `trp_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKlgpai5vtulxf0od2usum2goax` FOREIGN KEY (`service_detail_id`) REFERENCES `trp_service_detail` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKq2bxisu6bo67qh5tvqhtp5198` FOREIGN KEY (`transaction_detail_id`) REFERENCES `trp_transaction_detail` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_review: ~6 rows (approximately)
/*!40000 ALTER TABLE `trp_review` DISABLE KEYS */;
INSERT INTO `trp_review` (`id`, `rating_review`, `review`, `service_detail_id`, `transaction_detail_id`, `user_id`) VALUES
	(1, 3, 'Lumayan juga untuk hiburan akhir minggu', 63, 1, 5),
	(2, 3, 'Lumayan ramah supirnya dan baik', 61, 28, 5),
	(3, 4, 'OKE MANTUL', 61, 27, 5),
	(4, 1, 'qwqwqw', 61, 27, 5),
	(5, 1, 'qqqqqqqqqqq', 61, 27, 5),
	(6, 3, 'Hello world', 65, 3, 5);
/*!40000 ALTER TABLE `trp_review` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_service
CREATE TABLE IF NOT EXISTS `trp_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `approve` varchar(1) DEFAULT '0',
  `cancellation_policy` text,
  `count_rating` bigint(20) DEFAULT NULL,
  `count_review` bigint(20) DEFAULT NULL,
  `count_view` bigint(20) DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `deleted` varchar(1) DEFAULT '0',
  `description` text,
  `images_1` varchar(255) DEFAULT NULL,
  `images_2` varchar(255) DEFAULT NULL,
  `images_3` varchar(255) DEFAULT NULL,
  `images_4` varchar(255) DEFAULT NULL,
  `images_5` varchar(255) DEFAULT NULL,
  `popular` varchar(1) DEFAULT '0',
  `publish` varchar(1) DEFAULT '1',
  `redemption_instructions` text,
  `service_name` varchar(255) DEFAULT NULL,
  `terms_conditions` text,
  `category_id` bigint(20) NOT NULL,
  `regencies_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKnqt2lcfd6wnw7vuv95dtt2msx` (`category_id`),
  KEY `FK65ku0jb54mlh5xaii4weikndd` (`regencies_id`),
  KEY `FKoymyjb5onx35841pe7t6dn5x1` (`user_id`),
  CONSTRAINT `FK65ku0jb54mlh5xaii4weikndd` FOREIGN KEY (`regencies_id`) REFERENCES `trp_m_regencies` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKnqt2lcfd6wnw7vuv95dtt2msx` FOREIGN KEY (`category_id`) REFERENCES `trp_m_category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKoymyjb5onx35841pe7t6dn5x1` FOREIGN KEY (`user_id`) REFERENCES `trp_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_service: ~13 rows (approximately)
/*!40000 ALTER TABLE `trp_service` DISABLE KEYS */;
INSERT INTO `trp_service` (`id`, `approve`, `cancellation_policy`, `count_rating`, `count_review`, `count_view`, `date`, `deleted`, `description`, `images_1`, `images_2`, `images_3`, `images_4`, `images_5`, `popular`, `publish`, `redemption_instructions`, `service_name`, `terms_conditions`, `category_id`, `regencies_id`, `user_id`) VALUES
	(51, '1', '<p><strong style="color: rgb(0, 0, 0);">Non-refundable</strong></p><ul><li><span style="color: rgb(3, 18, 26);">This booking is non-refundable. This policy applies once your booking is confirmed.</span></li><li><span style="color: rgb(3, 18, 26);">Rescheduling is not available for this booking.</span></li></ul>', 3, 2, 5, '2019-10-11 17:05:30', '0', '<p><span style="color: rgb(3, 18, 26);">Are you going to Bali for your next vacation? Don\'t waste your time waiting for a taxi to pick you up from the airport. Make your trip in Bali easier with a private transfer service that will take you from or to Ngurah Rai Airport. No need to worry about getting cramped with other passangers since you will be riding in a spacious car just for you and your group. Relax and sit back while the the driver take you to your hotel or the airport. Whether you travel alone or with your loved ones, this private transfer service is the right choice to travel efficiently.</span></p>', 'trav320191012012009.jpg', 'trav220191012012052.jpg', 'aa20191012012255.jpg', 'bb20191012012255.jpg', NULL, NULL, '1', '<p><strong style="color: rgb(0, 0, 0);">How to Redeem</strong></p><ul><li><span style="color: rgb(3, 18, 26);">Only your Traveloka voucher is valid. Your payment receipt or proof of payment may not be used to enter.</span></li><li><span style="color: rgb(3, 18, 26);"><span class="ql-cursor">????????</span>Enter directly by showing or scanning the Traveloka voucher on your phone to the staff. Please adjust the brightness of your mobile phone screen before scanning the QR code on your voucher.</span></li></ul>', 'Rental Mobil Pribadi Yogyakarta', '<p><strong>General Information</strong></p><ul><li><span style="color: rgb(3, 18, 26);">Indonesian visitors need to show valid ID KTP/KITAS upon participating the trip.</span></li><li><span style="color: rgb(3, 18, 26);">International tourists need to show valid passport upon participating the trip.</span></li><li><span style="color: rgb(3, 18, 26);">Great Day Holiday reserves the right to deny entry to visitors who are unable to present a valid ID.</span></li><li><span style="color: rgb(3, 18, 26);">This ticket is valid for single trip.</span></li><li><span style="color: rgb(3, 18, 26);">For arrival airport pickup service, our representative will hold a "Traveloka Xperience" logo sign and wait at the airport arrivals hall.</span></li><li><span style="color: rgb(3, 18, 26);">For arrival airport pickup service, driver will wait up to 2 hours after your flight landing. Please inform us if you will be late.</span></li><li><span style="color: rgb(3, 18, 26);">If you will be picked up at your hotel, please input your hotel name along with other important information to the “Special Request” field before payment.</span></li><li><span style="color: rgb(3, 18, 26);">You must be ready at the hotel lobby for pickup at least 10 minutes in advance of your scheduled pickup time.</span></li></ul><p><br></p>', 10, 3273, 5),
	(52, '1', '<p><strong style="color: rgb(0, 0, 0);">Non-refundable</strong></p><ul><li><span style="color: rgb(3, 18, 26);">This booking is non-refundable. This policy applies once your booking is confirmed.</span></li><li><span style="color: rgb(3, 18, 26);">Rescheduling is not available for this booking.</span></li></ul><p><br></p>', 2, 0, 12, '2019-10-11 17:12:16', '0', '<p><span style="color: rgb(3, 18, 26);">Are you going to Bali for your next vacation? Don\'t waste your time waiting for a taxi to pick you up from the airport. Make your trip in Bali easier with a private transfer service that will take you from or to Ngurah Rai Airport. No need to worry about getting cramped with other passangers since you will be riding in a spacious car just for you and your group. Relax and sit back while the the driver take you to your hotel or the airport. Whether you travel alone or with your loved ones, this private transfer service is the right choice to travel efficiently.</span></p>', 'c120191012001216.jpg', 'c220191012001216.jpg', 'c420191012001216.jpg', 'c420191012001216.jpg', NULL, NULL, '1', '<p><strong style="color: rgb(0, 0, 0);">How to Redeem</strong></p><ul><li><span style="color: rgb(3, 18, 26);">Only your Traveloka voucher is valid. Your payment receipt or proof of payment may not be used to enter.</span></li><li><span style="color: rgb(3, 18, 26);">??Enter directly by showing or scanning the Traveloka voucher on your phone to the staff. Please adjust the brightness of your mobile phone screen before scanning the QR code on your voucher.</span></li></ul><p><br></p>', 'Waterbom Bali Tickets', '<p><strong>General Information</strong></p><ul><li><span style="color: rgb(3, 18, 26);">Indonesian visitors need to show valid ID KTP/KITAS upon participating the trip.</span></li><li><span style="color: rgb(3, 18, 26);">International tourists need to show valid passport upon participating the trip.</span></li><li><span style="color: rgb(3, 18, 26);">Great Day Holiday reserves the right to deny entry to visitors who are unable to present a valid ID.</span></li><li><span style="color: rgb(3, 18, 26);">This ticket is valid for single trip.</span></li><li><span style="color: rgb(3, 18, 26);">For arrival airport pickup service, our representative will hold a "Traveloka Xperience" logo sign and wait at the airport arrivals hall.</span></li><li><span style="color: rgb(3, 18, 26);">For arrival airport pickup service, driver will wait up to 2 hours after your flight landing. Please inform us if you will be late.</span></li><li><span style="color: rgb(3, 18, 26);">If you will be picked up at your hotel, please input your hotel name along with other important information to the “Special Request” field before payment.</span></li><li><span style="color: rgb(3, 18, 26);">You must be ready at the hotel lobby for pickup at least 10 minutes in advance of your scheduled pickup time.</span></li></ul><p><br></p>', 3, 3273, 5),
	(53, '1', '<p>...</p>', 3, 0, 2, '2019-10-11 17:15:44', '0', '<p>...</p>', 'trav220191012012052.jpg', 'bb20191012001545.jpg', NULL, NULL, NULL, NULL, '1', '<p>...</p>', 'Trip to Nusa Penida at Bali 1 Day Tour', '<p>...</p>', 9, 3273, 5),
	(54, '1', '<p>q</p>', 0, 0, 0, '2019-10-13 08:28:41', '0', '<p>q</p>', 'trav220191013152842.jpg', NULL, NULL, NULL, NULL, '0', '1', '<p>q</p>', 'Wisata Gilis Tenggara dan Pantai Pink', '<p>q</p>', 4, 1172, 5),
	(55, '1', '<p>q</p>', 0, 0, 1, '2019-10-13 08:29:15', '0', '<p>q</p>', 'trav320191013152915.jpg', NULL, NULL, NULL, NULL, '0', '1', '<p>q</p>', 'Wisata Sehari Pribadi Pulau Gili Lombok', '<p>q</p>', 2, 1375, 5),
	(56, '1', '<p>q</p>', 0, 0, 1, '2019-10-13 08:29:43', '0', '<p>q</p>', 'trav20191013152944.jpg', NULL, NULL, NULL, NULL, '1', '1', '<p>q</p>', 'Wisata Kuliner Cikini', '<p>q</p>', 3, 1374, 5),
	(57, '1', '<p>q</p>', 0, 0, 0, '2019-10-13 08:30:13', '0', '<p>q</p>', 'c320191013153013.jpg', NULL, NULL, NULL, NULL, '1', '1', '<p>q</p>', 'Chinatown Walking Day Tour di Jakarta', '<p>q</p>', 2, 1172, 5),
	(58, '1', '<p>1</p>', 0, 0, 0, '2019-10-13 08:30:58', '0', '<p>1</p>', 'aa20191013153058.jpg', NULL, NULL, NULL, NULL, NULL, '1', '<p>1</p>', 'Jakarta Landmarks Tour di Indonesia', '<p>1</p>', 3, 1372, 5),
	(59, '1', '<p>q</p>', 0, 0, 0, '2019-10-13 08:31:25', '0', '<p>q</p>', 'bb20191013153126.jpg', NULL, NULL, NULL, NULL, NULL, '1', '<p>q</p>', 'Jakarta Old Batavia Walking Tour', '<p>q</p>', 3, 1175, 5),
	(60, '1', '<p>qwer</p>', 3, 0, 0, '2019-10-13 08:31:53', '0', '<p>Qwer</p>', 'trav320191013153154.jpg', NULL, NULL, NULL, NULL, '0', '1', '<p>qwer</p>', 'Waterbom Bali', '<p>1</p>', 2, 1572, 5),
	(61, '1', '<p>q</p>', 0, 0, 0, '2019-10-13 08:32:23', '0', '<p>q</p>', 'c120191013153223.jpg', NULL, NULL, NULL, NULL, '0', '1', '<p>q</p>', 'Nusa Penida Full Day Trip dari Bali', '<p>q</p>', 6, 1372, 5),
	(62, '1', '<p>q</p>', 0, 0, 0, '2019-10-13 08:55:46', '0', '<p>q</p>', 'aa20191013155546.jpg', NULL, NULL, NULL, NULL, '0', '1', '<p>q</p>', 'Ayung White Water Rafting dengan Red Paddles', '<p>q</p>', 4, 1372, 5),
	(63, '1', '<p>a</p>', 0, 0, 0, '2019-10-13 08:59:37', '0', '<p>a</p>', 'c320191013155938.jpg', NULL, NULL, NULL, NULL, '0', '1', '<p>a</p>', 'Bali Safari and Marine Park', '<p>a</p>', 1, 1374, 5);
/*!40000 ALTER TABLE `trp_service` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_service_detail
CREATE TABLE IF NOT EXISTS `trp_service_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` varchar(1) DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `price` bigint(20) DEFAULT NULL,
  `publish` varchar(1) DEFAULT '1',
  `service_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8hdynh6ipmy5vjt32k2vsvfjw` (`service_id`),
  CONSTRAINT `FK8hdynh6ipmy5vjt32k2vsvfjw` FOREIGN KEY (`service_id`) REFERENCES `trp_service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_service_detail: ~16 rows (approximately)
/*!40000 ALTER TABLE `trp_service_detail` DISABLE KEYS */;
INSERT INTO `trp_service_detail` (`id`, `deleted`, `description`, `name`, `price`, `publish`, `service_id`) VALUES
	(61, '0', 'Valid from Monday to Sunday', 'Private Transfer - From/To Jimbaran', 99000, '1', 51),
	(62, '0', 'Valid from Monday to Sunday', 'Private Transfer - From/To Sanur/Denpasar', 125000, '1', 51),
	(63, '0', '', 'Single Day Pass (Indonesian Residents)', 240000, '1', 52),
	(64, '0', '', 'Single Day Pass (International Tourists)', 500000, '1', 52),
	(65, '0', '', 'Local Tourists', 120000, '1', 53),
	(66, '0', '', 'Internation Tourist', 150000, '1', 53),
	(67, '0', '', 'Paket #1', 12000, '1', 54),
	(68, '0', '', 'Tiket Administrasi #2', 12000, '1', 55),
	(69, '0', '', 'Paket #2', 1222, '1', 56),
	(70, '0', '', 'Tiket Administrasi #1', 12000, '1', 57),
	(71, '0', '', 'aaa', 12000, '1', 58),
	(72, '0', '', 'Herdi xxx', 12000, '1', 59),
	(73, '0', '', 'aaaa', 18000, '1', 60),
	(74, '0', '', 'Herdi xxx', 0, '1', 61),
	(75, '0', '', 'Tiket Administrasi #2', 0, '1', 62),
	(76, '0', '', 'Tiket Administrasi #4444', 1200, '1', 63);
/*!40000 ALTER TABLE `trp_service_detail` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_subscribe
CREATE TABLE IF NOT EXISTS `trp_subscribe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_tpmr0athmujyris8cqs335rbi` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_subscribe: ~0 rows (approximately)
/*!40000 ALTER TABLE `trp_subscribe` DISABLE KEYS */;
/*!40000 ALTER TABLE `trp_subscribe` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_transaction
CREATE TABLE IF NOT EXISTS `trp_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `deleted` varchar(1) DEFAULT '0',
  `discount_voucher_code` bigint(20) DEFAULT NULL,
  `fee_admin` bigint(20) DEFAULT NULL,
  `invoice_number` varchar(100) DEFAULT NULL,
  `payment_amount` bigint(20) DEFAULT NULL,
  `payment_channel` varchar(50) DEFAULT '0',
  `payment_code` varchar(50) DEFAULT '0',
  `payment_method` varchar(150) DEFAULT '0',
  `status_payment` varchar(1) DEFAULT '0',
  `summary_service` bigint(20) unsigned DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `traveling_email` varchar(255) DEFAULT NULL,
  `traveling_name` varchar(255) DEFAULT NULL,
  `traveling_phone` varchar(255) DEFAULT NULL,
  `voucher_code` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK172wodslrddnopgcdewx5ujb8` (`user_id`),
  CONSTRAINT `FK172wodslrddnopgcdewx5ujb8` FOREIGN KEY (`user_id`) REFERENCES `trp_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_transaction: ~30 rows (approximately)
/*!40000 ALTER TABLE `trp_transaction` DISABLE KEYS */;
INSERT INTO `trp_transaction` (`id`, `date`, `deleted`, `discount_voucher_code`, `fee_admin`, `invoice_number`, `payment_amount`, `payment_channel`, `payment_code`, `payment_method`, `status_payment`, `summary_service`, `total`, `traveling_email`, `traveling_name`, `traveling_phone`, `voucher_code`, `user_id`) VALUES
	(1, '2019-10-15 14:49:55', NULL, 0, 7000, 'TRA/20191015/0001', 247001, '001', '1212121212', 'Bank Mandiri', '1', 240000, 247000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(2, '2019-10-15 14:50:28', NULL, 0, 7000, 'TRA/20191015/0002', 106002, '001', '1212121212', 'Bank Mandiri', '0', 99000, 106000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(3, '2019-10-15 14:51:11', NULL, 0, 7000, 'TRA/20191015/0003', 127003, '001', '1212121212', 'Bank Mandiri', '1', 120000, 127000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(4, '2019-10-15 14:55:30', NULL, 0, 7000, 'TRA/20191015/0004', 106004, '001', '1212121212', 'Bank Mandiri', '0', 99000, 106000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(5, '2019-10-17 19:09:34', NULL, 0, 7000, 'TRA/20191018/0005', 106005, '001', '1212121212', 'Bank Mandiri', '0', 99000, 106000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(6, '2019-10-17 19:09:50', NULL, 0, 7000, 'TRA/20191018/0006', 19006, '001', '1212121212', 'Bank Mandiri', '0', 12000, 19000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(7, '2019-10-17 19:10:10', NULL, 0, 7000, 'TRA/20191018/0007', 132007, '001', '1212121212', 'Bank Mandiri', '0', 125000, 132000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(8, '2019-10-17 19:11:00', NULL, 0, 7000, 'TRA/20191018/0008', 19008, '001', '1212121212', 'Bank Mandiri', '0', 12000, 19000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(9, '2019-10-17 19:11:56', NULL, 0, 7000, 'TRA/20191018/0009', 19009, '001', '1212121212', 'Bank Mandiri', '0', 12000, 19000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(10, '2019-10-18 16:22:49', NULL, 0, 7000, 'TRA/20191018/0010', 127010, '001', '1212121212', 'Bank Mandiri', '0', 120000, 127000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(11, '2019-10-20 06:33:10', NULL, 0, 7000, 'TRA/20191020/0011', 127011, '001', '1212121212', 'Bank Mandiri', '0', 120000, 127000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(12, '2019-10-20 14:36:12', NULL, 0, 7000, 'TRA/20191020/0012', 8234, '001', '1212121212', 'Bank Mandiri', '0', 1222, 8222, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(13, '2019-10-20 14:40:28', NULL, 0, 7000, 'TRA/20191020/0013', 247013, '001', '1212121212', 'Bank Mandiri', '0', 240000, 247000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(14, '2019-10-20 14:53:23', NULL, 0, 7000, 'TRA/20191020/0014', 247014, '001', '1212121212', 'Bank Mandiri', '0', 240000, 247000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(15, '2019-10-20 14:58:23', NULL, 0, 7000, 'TRA/20191020/0015', 127015, '001', '1212121212', 'Bank Mandiri', '0', 120000, 127000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(16, '2019-10-20 15:30:07', NULL, 0, 7000, 'TRA/20191020/0016', 127016, '001', '1212121212', 'Bank Mandiri', '0', 120000, 127000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(17, '2019-10-20 15:31:51', NULL, 0, 7000, 'TRA/20191020/0017', 127017, '001', '1212121212', 'Bank Mandiri', '0', 120000, 127000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(18, '2019-10-20 15:48:16', NULL, 0, 7000, 'TRA/20191020/0018', 8218, '001', '1212121212', 'Bank Mandiri', '0', 1200, 8200, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(19, '2019-10-20 18:09:49', NULL, 0, 7000, 'TRA/20191021/0019', 247019, '001', '1212121212', 'Bank Mandiri', '0', 240000, 247000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(20, '2019-10-20 18:11:26', NULL, 0, 7000, 'TRA/20191021/0020', 247020, '001', '1212121212', 'Bank Mandiri', '0', 240000, 247000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(21, '2019-10-20 18:22:13', NULL, 0, 7000, 'TRA/20191021/0021', 127021, '001', '1212121212', 'Bank Mandiri', '0', 120000, 127000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(22, '2019-10-20 18:30:30', NULL, 0, 7000, 'TRA/20191021/0022', 247022, '001', '1212121212', 'Bank Mandiri', '0', 240000, 247000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(23, '2019-10-20 18:45:52', NULL, 0, 7000, 'TRA/20191021/0023', 127023, '001', '1212121212', 'Bank Mandiri', '0', 120000, 127000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(24, '2019-10-20 19:13:57', NULL, 0, 7000, 'TRA/20191021/0024', 127024, '001', '1212121212', 'Bank Mandiri', '0', 120000, 127000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(25, '2019-10-20 19:42:12', NULL, 0, 7000, 'TRA/20191021/0025', 106025, '001', '1212121212', 'Bank Mandiri', '0', 99000, 106000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(26, '2019-10-20 20:00:12', NULL, 106000, 7000, 'TRA/20191021/0026', 106026, '001', '1212121212', 'Bank Mandiri', '0', 99000, 106000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', 'PRMLEB123', 5),
	(27, '2019-10-20 20:14:05', NULL, 106000, 7000, 'TRA/20191021/0027', 27, '001', '1212121212', 'Bank Mandiri', '1', 99000, 0, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', 'PRMLEB123', 5),
	(28, '2019-10-20 20:26:02', NULL, 106000, 7000, 'TRA/20191021/0028', 28, '001', '1212121212', 'Bank Mandiri', '1', 99000, 0, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', 'PRMLEB123', 5),
	(29, '2019-10-20 20:26:57', NULL, 0, 7000, 'TRA/20191021/0029', 106029, '001', '1212121212', 'Bank Mandiri', '0', 99000, 106000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5),
	(30, '2019-10-21 17:07:46', NULL, 0, 7000, 'TRA/20191022/0030', 19030, '001', '1212121212', 'Bank Mandiri', '0', 12000, 19000, 'nuryanto.aza@gmail.com', 'Herdi Ret', '123', NULL, 5);
/*!40000 ALTER TABLE `trp_transaction` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_transaction_detail
CREATE TABLE IF NOT EXISTS `trp_transaction_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_visit` date DEFAULT NULL,
  `deleted` varchar(1) DEFAULT '0',
  `has_review` varchar(1) DEFAULT '0',
  `price` bigint(20) DEFAULT NULL,
  `qty` bigint(20) DEFAULT NULL,
  `service_detail_name` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `service_detail_id` bigint(20) NOT NULL,
  `service_id` bigint(20) NOT NULL,
  `transaction_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4w6odo7wq3lsn35mx5fbodp1p` (`service_detail_id`),
  KEY `FKjtptwnehxypjb1a4ps2s14a6q` (`service_id`),
  KEY `FK7c1q69vdo158jwsr4kbonll0w` (`transaction_id`),
  CONSTRAINT `FK4w6odo7wq3lsn35mx5fbodp1p` FOREIGN KEY (`service_detail_id`) REFERENCES `trp_service_detail` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK7c1q69vdo158jwsr4kbonll0w` FOREIGN KEY (`transaction_id`) REFERENCES `trp_transaction` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKjtptwnehxypjb1a4ps2s14a6q` FOREIGN KEY (`service_id`) REFERENCES `trp_service` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_transaction_detail: ~30 rows (approximately)
/*!40000 ALTER TABLE `trp_transaction_detail` DISABLE KEYS */;
INSERT INTO `trp_transaction_detail` (`id`, `date_visit`, `deleted`, `has_review`, `price`, `qty`, `service_detail_name`, `service_name`, `total`, `service_detail_id`, `service_id`, `transaction_id`) VALUES
	(1, '2019-10-16', NULL, '1', 240000, 1, 'Single Day Pass (Indonesian Residents)', 'Waterbom Bali Tickets', 240000, 63, 52, 1),
	(2, '2019-10-17', NULL, NULL, 99000, 1, 'Private Transfer - From/To Jimbaran', 'Rental Mobil Pribadi Yogyakarta', 99000, 61, 51, 2),
	(3, '2019-10-23', NULL, '1', 120000, 1, 'Local Tourists', 'Trip to Nusa Penida at Bali 1 Day Tour', 120000, 65, 53, 3),
	(4, '2019-10-15', NULL, NULL, 99000, 1, 'Private Transfer - From/To Jimbaran', 'Rental Mobil Pribadi Yogyakarta', 99000, 61, 51, 4),
	(5, '2019-10-16', NULL, NULL, 99000, 1, 'Private Transfer - From/To Jimbaran', 'Rental Mobil Pribadi Yogyakarta', 99000, 61, 51, 5),
	(6, '2019-10-23', NULL, NULL, 12000, 1, 'Tiket Administrasi #2', 'Wisata Sehari Pribadi Pulau Gili Lombok', 12000, 68, 55, 6),
	(7, '2019-10-22', NULL, NULL, 125000, 1, 'Private Transfer - From/To Sanur/Denpasar', 'Rental Mobil Pribadi Yogyakarta', 125000, 62, 51, 7),
	(8, '2019-10-23', NULL, NULL, 12000, 1, 'Herdi xxx', 'Jakarta Old Batavia Walking Tour', 12000, 72, 59, 8),
	(9, '2019-10-23', NULL, NULL, 12000, 1, 'Tiket Administrasi #1', 'Chinatown Walking Day Tour di Jakarta', 12000, 70, 57, 9),
	(10, '2019-10-22', NULL, NULL, 120000, 1, 'Local Tourists', 'Trip to Nusa Penida at Bali 1 Day Tour', 120000, 65, 53, 10),
	(11, '2019-10-23', NULL, NULL, 120000, 1, 'Local Tourists', 'Trip to Nusa Penida at Bali 1 Day Tour', 120000, 65, 53, 11),
	(12, '2019-10-21', NULL, NULL, 1222, 1, 'Paket #2', 'Wisata Kuliner Cikini', 1222, 69, 56, 12),
	(13, '2019-10-22', NULL, NULL, 240000, 1, 'Single Day Pass (Indonesian Residents)', 'Waterbom Bali Tickets', 240000, 63, 52, 13),
	(14, '2019-10-22', NULL, NULL, 240000, 1, 'Single Day Pass (Indonesian Residents)', 'Waterbom Bali Tickets', 240000, 63, 52, 14),
	(15, '2019-10-22', NULL, NULL, 120000, 1, 'Local Tourists', 'Trip to Nusa Penida at Bali 1 Day Tour', 120000, 65, 53, 15),
	(16, '2019-10-23', NULL, NULL, 120000, 1, 'Local Tourists', 'Trip to Nusa Penida at Bali 1 Day Tour', 120000, 65, 53, 16),
	(17, '2019-10-28', NULL, NULL, 120000, 1, 'Local Tourists', 'Trip to Nusa Penida at Bali 1 Day Tour', 120000, 65, 53, 17),
	(18, '2019-10-23', NULL, NULL, 1200, 1, 'Tiket Administrasi #4444', 'Bali Safari and Marine Park', 1200, 76, 63, 18),
	(19, '2019-10-21', NULL, NULL, 240000, 1, 'Single Day Pass (Indonesian Residents)', 'Waterbom Bali Tickets', 240000, 63, 52, 19),
	(20, '2019-10-28', NULL, NULL, 240000, 1, 'Single Day Pass (Indonesian Residents)', 'Waterbom Bali Tickets', 240000, 63, 52, 20),
	(21, '2019-10-22', NULL, NULL, 120000, 1, 'Local Tourists', 'Trip to Nusa Penida at Bali 1 Day Tour', 120000, 65, 53, 21),
	(22, '2019-10-23', NULL, NULL, 240000, 1, 'Single Day Pass (Indonesian Residents)', 'Waterbom Bali Tickets', 240000, 63, 52, 22),
	(23, '2019-10-23', NULL, NULL, 120000, 1, 'Local Tourists', 'Trip to Nusa Penida at Bali 1 Day Tour', 120000, 65, 53, 23),
	(24, '2019-10-21', NULL, NULL, 120000, 1, 'Local Tourists', 'Trip to Nusa Penida at Bali 1 Day Tour', 120000, 65, 53, 24),
	(25, '2019-10-22', NULL, NULL, 99000, 1, 'Private Transfer - From/To Jimbaran', 'Rental Mobil Pribadi Yogyakarta', 99000, 61, 51, 25),
	(26, '2019-10-21', NULL, NULL, 99000, 1, 'Private Transfer - From/To Jimbaran', 'Rental Mobil Pribadi Yogyakarta', 99000, 61, 51, 26),
	(27, '2019-10-22', NULL, '1', 99000, 1, 'Private Transfer - From/To Jimbaran', 'Rental Mobil Pribadi Yogyakarta', 99000, 61, 51, 27),
	(28, '2019-10-21', NULL, '1', 99000, 1, 'Private Transfer - From/To Jimbaran', 'Rental Mobil Pribadi Yogyakarta', 99000, 61, 51, 28),
	(29, '2019-10-29', NULL, NULL, 99000, 1, 'Private Transfer - From/To Jimbaran', 'Rental Mobil Pribadi Yogyakarta', 99000, 61, 51, 29),
	(30, '2019-10-22', NULL, NULL, 12000, 1, 'Tiket Administrasi #2', 'Wisata Sehari Pribadi Pulau Gili Lombok', 12000, 68, 55, 30);
/*!40000 ALTER TABLE `trp_transaction_detail` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_user
CREATE TABLE IF NOT EXISTS `trp_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `about` varchar(255) DEFAULT NULL,
  `activation` varchar(1) DEFAULT '0',
  `address` varchar(255) DEFAULT NULL,
  `bank_account_name` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_number` varchar(20) DEFAULT NULL,
  `deleted` varchar(1) DEFAULT '0',
  `dob` date DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2bu374unf71oiq3gy7u700usj` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_user: ~3 rows (approximately)
/*!40000 ALTER TABLE `trp_user` DISABLE KEYS */;
INSERT INTO `trp_user` (`id`, `about`, `activation`, `address`, `bank_account_name`, `bank_name`, `bank_number`, `deleted`, `dob`, `email`, `gender`, `image`, `name`, `password`, `phone`) VALUES
	(5, 'Lorem ipsum dolar sit amet', '1', 'Jl. Petojo Enclek III No. 11, Gambir, Jakarta Pusat', 'Muhamad Nuryanto', 'Bank BRI', '111111', '0', '1989-01-08', 'nuryanto.aza@gmail.com', 'L', 'IMG_094420191013232232.JPG', 'Herdi Ret', '$2a$10$88lxUOcbljzsXAdq9nb5A.uHBaYc/mTGRLSTTsVSTVZgi5e6ILrBy', '123'),
	(8, 'xxxxx', '1', 'Jl. Petojo Enclek III No. 11, Gambir, Jakarta Pusat', 'Muhamad Nuryanto', 'Bank BRI', '111111', '0', '1989-01-08', 'mnuryanto4@gmail.com', 'L', '', 'nur', '$2a$10$IdAnnMU4N7O65fJOhFPQX.8rTwEnr.sOTuQ75W6v3M.RQeE9HifC6', '123'),
	(9, '1', '1', NULL, NULL, NULL, NULL, '0', NULL, 'nur@gmail.com', NULL, NULL, 'Nur', '$2a$10$ZSStHImLmMie3eNA3e/KduxC/K5mdf4u/fMKXImJxS3n1xVQ185F.', NULL);
/*!40000 ALTER TABLE `trp_user` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_verification_token
CREATE TABLE IF NOT EXISTS `trp_verification_token` (
  `id` bigint(20) NOT NULL,
  `expire_date` datetime DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qxiu2rpl9okl2r51s8vsnl8cl` (`user_id`),
  CONSTRAINT `FKgmlnmty4cdrpbjx8opu518kg2` FOREIGN KEY (`user_id`) REFERENCES `trp_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_verification_token: ~0 rows (approximately)
/*!40000 ALTER TABLE `trp_verification_token` DISABLE KEYS */;
INSERT INTO `trp_verification_token` (`id`, `expire_date`, `token`, `user_id`) VALUES
	(1, '2019-10-15 16:57:07', '71a6d12f-a57c-4ed3-9916-b2ba8d6f1489', 9);
/*!40000 ALTER TABLE `trp_verification_token` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_voucher
CREATE TABLE IF NOT EXISTS `trp_voucher` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` bigint(20) DEFAULT NULL,
  `amount_type` varchar(50) NOT NULL,
  `deleted` varchar(1) DEFAULT '0',
  `max_amount` bigint(20) DEFAULT NULL,
  `min_transaction` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `publish` varchar(1) DEFAULT '1',
  `valid_date_from` datetime DEFAULT NULL,
  `valid_date_to` datetime DEFAULT NULL,
  `voucher_code` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_1mpm5unfb7oms9krxwescbjm4` (`voucher_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_voucher: ~0 rows (approximately)
/*!40000 ALTER TABLE `trp_voucher` DISABLE KEYS */;
INSERT INTO `trp_voucher` (`id`, `amount`, `amount_type`, `deleted`, `max_amount`, `min_transaction`, `name`, `publish`, `valid_date_from`, `valid_date_to`, `voucher_code`) VALUES
	(1, 200000, 'FIXED', '0', 200000, 100000, 'Promo #1', '1', '2019-01-12 17:10:00', '2019-12-30 00:00:00', 'PRMLEB123');
/*!40000 ALTER TABLE `trp_voucher` ENABLE KEYS */;

-- Dumping structure for table trapedia_fix.trp_wishlist
CREATE TABLE IF NOT EXISTS `trp_wishlist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_detail_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpsa789k8utfva21hc5n1mkk1e` (`service_detail_id`),
  KEY `FKjfm9sbkffeojes40et3jdaaja` (`user_id`),
  CONSTRAINT `FKjfm9sbkffeojes40et3jdaaja` FOREIGN KEY (`user_id`) REFERENCES `trp_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKpsa789k8utfva21hc5n1mkk1e` FOREIGN KEY (`service_detail_id`) REFERENCES `trp_service_detail` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table trapedia_fix.trp_wishlist: ~5 rows (approximately)
/*!40000 ALTER TABLE `trp_wishlist` DISABLE KEYS */;
INSERT INTO `trp_wishlist` (`id`, `service_detail_id`, `user_id`) VALUES
	(1, 69, 5),
	(2, 65, 5),
	(3, 63, 5),
	(4, 64, 5),
	(5, 61, 5);
/*!40000 ALTER TABLE `trp_wishlist` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
